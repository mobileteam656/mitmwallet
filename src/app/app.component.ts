import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ToastController, LoadingController, App, ViewController, AlertController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { File } from '@ionic-native/file';
import { Login } from '../pages/login/login';
import { Changefont } from '../pages/changefont/changeFont';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { GlobalProvider } from '../providers/global/global';
import { Profile } from '../pages/profile/profile'; 
import { TabsPage } from '../pages/tabs/tabs';
import { Keyboard } from '@ionic-native/keyboard';
import { RegistrationPage } from '../pages/registration/registration';
import { ProfileQrPage } from '../pages/profile-qr/profile-qr';
declare var cordova: any;
declare var window;
declare var device: any;
import { FunctProvider } from '../providers/funct/funct';
//import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
//import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Device } from '@ionic-native/device';
import { SingleViewPage } from '../pages/single-view/single-view';
import { UtilProvider } from '../providers/util/util';
import { AccountSecurityPage } from '../pages/account-security/account-security';
import { FaqPage } from '../pages/faq/faq';
import { LocationPage } from '../pages/location/location';
import { ContactUsPage } from '../pages/contact-us/contact-us';
import { SettingsPage } from '../pages/settings/settings';
import { MapPage } from '../pages/map/map';
import { ExchangePage } from '../pages/exchange/exchange';
import { FCM } from '@ionic-native/fcm';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { FirebaseCrashlytics } from '@ionic-native/firebase-crashlytics/ngx';
import { Badge } from '@ionic-native/badge';
import { ShoplistPage } from '../pages/shoplist/shoplist';
import { NewShopPage } from '../pages/new-shop/new-shop';
import { LoginMfi } from '../pages/login-mfi/login-mfi';
import { SchedulePage } from '../pages/schedule/schedule';
import { ShowStatusPage } from '../pages/show-status/show-status';
import { InfoPage } from '../pages/info/info';
import { QPayPage } from '../pages/q-pay/q-pay';
import { StaffLoanInformationPage } from '../pages/staff-loan-information/staff-loan-information';



export const myConst = {
  walletApp: {
    ios: {
      storeUrl: 'itms-apps://itunes.apple.com/us/app/',
      appId: 'mWallet://'
    },
    android: {
      storeUrl: 'https://play.google.com/store/apps/details?id=wallet.NSBmawalletdev',
      appId: 'mWallet'
    }
  }
}
@Component({
  templateUrl: 'app.html',
  providers: [Changefont]
})

export class MyApp {

  imglnk: any;
  photoExist: any;
  profile: any;
  photo: string = '';
  @ViewChild(Nav) nav: Nav;
  lang: string;
  textMyan: any;
  textHeadEng: any = ["Edit"];
  textHeadMyan: any = [" ???????"];
  language: string;
  promo: string = '';
  luck: string = '';
  about: string = '';
  deactivate: string = '';
  font: string = 'eng';
  textEng: any;
  textData: string[] = [];
  appName: string = '';
  rootPage: any;
  fontType: string;
  pages: any;
  token:any;
  storageToken:any;

  //original
  ipaddress: string;
  //cms, wallet, chat, ticket,skynet
  ipaddressApp: string;
  ipaddressCMS: string;
  ipaddressWallet: any;
  ipaddressChat: any;
  ipaddressTicket: any;

  username: string = '';
  phonenumber: string = '';
  userData: any = '';
  location: string = '';
  profileImage: any;

  hautosys: any;
  registerData = {
    syskey: "", phone: "", username: "", realname: "", stateCaption: "",
    photo: "", fathername: "", nrc: "", id: "", passbook: "",
    address: "", dob: "", t1: "", t2: "", t3: "",
    t4: "", t5: "", state: "", status: "", n1: "",
    n2: "", n3: "", n4: "", n5: "", city: ""
  };
  stateList: any = [];
  tempData: any;
  flag: any;
  versionEng:any;
  versionMyan:any;
  version:any=[];

  textMyanfirst: any;
  textEngfirst: any;
  phoneno: any;
  // noticount=1;

  constructor(
    private localNotifications: LocalNotifications,
    private device: Device,
    public funct: FunctProvider,
    private keyboard: Keyboard,
    public platform: Platform,
    public app: App,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public storage: Storage,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public http: Http,
    public sqlite: SQLite,
    public changefont: Changefont,
    public file: File,
    public events: Events,
    public geolocation: Geolocation,
    public locac: LocationAccuracy,
    public geocoder: NativeGeocoder,
    private firebaseAnalytics: FirebaseAnalytics,
    public fcm:FCM,
    private badge:Badge,
    //private transfer: Transfer,
    private transfer: FileTransfer,
    public global: GlobalProvider,
    //private androidPermissions: AndroidPermissions,
    public util: UtilProvider,
    public appCtrl: App,
    private firebaseCrashlytics: FirebaseCrashlytics
  ) { 
    this.flag = false;
    
    this.versionEng=["Version 1.0.16"];
    this.versionMyan=["ဗားရှင်း 1.0.16"];
    this.pages = [{
      "name": "", "data": [
        //{ title: 'Login', component: Login, index: 0, icon: 'ios-log-in' },  
        { title: 'Location', component: MapPage, index: '', icon: 'ios-navigate-outline' },
        { title: 'Exchange Rate', component: ExchangePage, index: '', icon: 'ios-sync-outline',fontType:20 },       
        { title: 'FAQ', component: FaqPage, index: '', icon: 'ios-help' },
        { title: 'Contact Us', component: ContactUsPage, index: '', icon: 'ios-call-outline' }
        // { title: 'Settings', component: SettingsPage, index: '', icon: 'ios-settings-outline' },
        // { title: 'Version1.1.26', component: '', index: '1', icon: 'ios-information-circle-outline' }
      ]
    }];

    events.subscribe('user:loggedin', () => {
      // this.textMyan = ["အကောင့်လုံခြုံရေး", "မှတ်ပုံတင်ခြင်း", "မေးလေ့ရှိသောမေးခွန်းများ", "တည်နေရာ", "ဆက်သွယ်ရန်", 'ပြင်ဆင်ရန်', 'ဗားရှင်း 1.1.40', "ထွက်ရန်"];
      // this.textEng = ["Account Security", "Registration", "FAQ", "Location", "Contact Us", 'Settings', "Version 1.1.40", "Logout"];

      this.textMyan = ["အကောင့်လုံခြုံရေး", 'ပြင်ဆင်ရန်', "တည်နေရာ", "ငွေလဲလှယ်နှုန်း", "မေးလေ့ရှိသောမေးခွန်းများ", "ဆက်သွယ်ရန်","ဆိုင်စာရင်း",,"Staff Loan","ဇယား","မှတ်တမ်း","MerchaZnt Pay","ထွက်ရန်"];
      this.textEng = ["Account Security", 'Settings', "Location", "Exchange Rate", "FAQ", "Contact Us", "Small Loan","Staff Loan","HP Schedule","Loan History","Merchant Pay","Log out"];
      this.flag = true;
      this.pages = [{
        "name": "", "data": [
          /* { title: 'Wallet', component: TabsPage, tabComponent: WalletPage, index: 0, icon: 'ios-home' }, */
          { title: 'Account Security', component: AccountSecurityPage, index: '', icon: 'ios-lock' },
          { title: 'Settings', component: SettingsPage, index: '', icon: 'ios-settings-outline' },
          { title: 'Location', component: MapPage, index: '', icon: 'ios-navigate-outline' },
          { title: 'Exchange Rate', component: ExchangePage, index: '', icon: 'sync' },
          // { title: 'Registration', component: RegistrationPage, index: '', icon: 'ios-person-outline' },
          { title: 'FAQ', component: FaqPage, index: '', icon: 'ios-help' },
          //{ title: 'Version1.1.40', component: '', index: '1', icon: 'ios-information-circle-outline' },
          { title: 'Contact Us', component: ContactUsPage, index: '', icon: 'ios-call-outline' },
          { title: 'HP Loan', component: NewShopPage, index: '', icon: 'ios-cart' },
          { title: 'Money Loan ', component: StaffLoanInformationPage, index: '', icon: 'ios-paper-outline' },
          { title: 'HP Schedule ', component: SchedulePage, index: '', icon: 'calendar' },
          { title: 'Loan History ', component: ShowStatusPage, index: '', icon: 'paper' },
          { title: 'Merchant Pay', component: QPayPage, index: '', icon: 'ios-card' },
          { title: 'Log out', component: Login, index: '', icon: 'ios-log-out' }]

      }];
      this.events.subscribe('changelanguage', lan => {
        if (lan == "" || lan == null) {
          this.storage.set('language', "eng");
          lan = "eng";
        }
        this.changelanguage(lan);
      });
      this.storage.get('language').then((lan) => {
        if (lan == "" || lan == null) {
          this.storage.set('language', "eng");
          lan = "eng";
        }
        this.changelanguage(lan);
      });
    });

    events.subscribe('user:loggedout', () => {
      this.textMyan = ["တည်နေရာ", "ငွေလွှဲနှုန်း","မေးခွန်း","ဆက်သွယ်ရန်", "ဗားရှင်း 1.1.40"];
      this.textEng = ["Location","Exchange Rate","FAQ",  "Contact Us", "Version 1.1.40"];
      this.flag = false;
      this.pages = [{
        "name": "", "data": [
          //{ title: 'Login', component: Login, index: 0, icon: 'ios-log-in' }, 
          { title: 'Location', component: MapPage, index: '', icon: 'ios-navigate-outline' },
          { title: 'Exchange Rate', component: ExchangePage, index: '', icon: 'ios-sync-outline' },        
          { title: 'FAQ', component: FaqPage, index: '', icon: 'ios-help' },
          { title: 'Contact Us', component: ContactUsPage, index: '', icon: 'ios-call-outline' }
          // { title: 'Version1.1.26', component: '', index: '1', icon: 'ios-information-circle-outline' }
        ]
      }];

      this.events.subscribe('changelanguage', lan => {
        if (lan == "" || lan == null) {
          this.storage.set('language', "eng");
          lan = "eng";
        }
        this.changelanguage(lan);
      });
      this.storage.get('language').then((lan) => {
        if (lan == "" || lan == null) {
          this.storage.set('language', "eng");
          lan = "eng";
        }
        this.changelanguage(lan);
      });
    });


    this.imglnk = this.file.cacheDirectory;
    this.initializeApp();
    this.appName = this.global.appName;
    this.getStorageData();
  }

  download(photoAndServerLink, photo) {
    //const fileTransfer: TransferObject = this.transfer.create();
    const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(photoAndServerLink, this.file.cacheDirectory + photo).then((entry) => {
      let abc = entry.toURL();
      this.storage.set('localPhotoAndLink', abc);
      this.events.publish('localPhotoAndLink', abc);
    }, (error) => {

    });
  }

  getStorageData() {
    this.storage.get('localPhotoAndLink').then((photo) => {
      this.photo = photo;
      if (this.photo != '' && this.photo != undefined && this.photo != null && this.photo.includes("/")) {
        this.profile = this.photo;
      } else if (this.photo == 'Unknown.png' || this.photo == 'user-icon.png' || this.photo == '') {
        this.profile = "assets/images/user-icon.png";
      } else {
        this.storage.get('profileImage').then((profileImage) => {
          if (profileImage != undefined && profileImage != null && profileImage != '') {
            this.profileImage = profileImage;
          } else {
            this.profileImage = this.global.digitalMediaProfile;
          }
          this.profile = this.profileImage + this.photo;
          this.download(this.profile, this.photo);
        });
      }
    });
    this.events.subscribe('localPhotoAndLink', photo => {
      this.photo = photo;
      if (this.photo != '' && this.photo != undefined && this.photo != null && this.photo.includes("/")) {
        this.profile = this.photo;
      } else if (this.photo == 'Unknown.png' || this.photo == 'user-icon.png' || this.photo == '') {
        this.profile = "assets/images/user-icon.png";
      } else {
        this.storage.get('profileImage').then((profileImage) => {
          if (profileImage != undefined && profileImage != null && profileImage != '') {
            this.profileImage = profileImage;
          } else {
            this.profileImage = this.global.digitalMediaProfile;
          }
          this.profile = this.profileImage + this.photo;
          this.download(this.profile, this.photo);
        });
      }
    });

    this.storage.get('phonenumber').then((phonenumber) => {
      if (phonenumber !== undefined && phonenumber !== "") {
        this.phonenumber = phonenumber;
      //  this.phoneno=phonenumber.substr(9,4);
     
      }
    });
    this.events.subscribe('phonenumber', phonenumber => {
      if (phonenumber !== undefined && phonenumber !== "") {
        this.phonenumber = phonenumber;
      }
    })
    this.storage.get('username').then((username) => {
      if (username !== undefined && username !== "") {
        this.username = username;
      }
    });
    this.events.subscribe('username', username => {
      if (username !== undefined && username !== "") {
        this.username = username;
      }
    });
    this.storage.get('userData').then((userData) => {
      if (userData !== undefined && userData !== "") {
        this.userData = userData;
      }
    });
    this.events.subscribe('userData', userData => {
      if (userData !== undefined && userData !== "") {
        this.userData = userData;
      }
    });
  }

  initializeApp() {
    this.storage.remove('ip');
    this.storage.remove('ipaddressCMS');
    this.storage.remove('ipaddressWallet');
    this.storage.remove('ipaddressChat');
    this.storage.remove('ipaddressTicket');
    this.storage.remove('ipaddressApp');
    
      // this.keyboard.disableScroll(false);

      this.platform.ready().then(() => {
        //const crashlytics = cordova(this.firebaseCrashlytics, "initialise", { "sync": true }, undefined);
      //   this.fcm.subscribeToTopic('hello');
        
      //   this.fcm.getToken().then(token => {
      //     this.token=token;
      //     // alert(this.token);
      //     console.log('Firebase Token: '+token)
      //     this.storage.set('firebaseToken', this.token);
      //     this.storage.get('firebaseToken').then((firebaseToken) => {
      //       this.storageToken = firebaseToken;
      //       console.log('Storage Token: '+this.storageToken);
      //     });
      //   });
        
      //   this.fcm.onNotification().subscribe(data => {
      //     // this.badge.set(JSON.stringify(data.badge));
      //     console.log('Noti Count: '+this.global.noticount);
      
      //     console.log("Received in foreground", data);
      //     this.localNotifications.schedule([{
      //       id: this.global.noticount,
      //       title: data.title,
      //       text: data.body,
      //      }]);
      //     this.global.noticount++;
      //     console.log('Noti Count: '+this.global.noticount);
      //     this.events.publish('user:created');
      //  // }
      //   });
      //   this.fcm.unsubscribeFromTopic('marketing');
         // this.statusBar.styleDefault();
          this.statusBar.styleDefault();
          this.splashScreen.hide();

      this.storage.get('firstlogin').then((firstTime) => {
        if (firstTime) {
          this.storage.get('phonenumber').then((phone) => {
            if (phone != undefined && phone != null && phone != '') {
              this.rootPage = TabsPage;
            } else {
              this.rootPage = Login;//change yan
            }
          });
        } else {
          this.rootPage = Login;
        }

        this.storage.get('font').then((font) => {
          if (font == undefined || font == null || font == '') {
            this.storage.set('font', 'zg');
            this.events.publish('changeFont', 'zg');
          }
        });
      });
      this.events.subscribe('changelanguage', lan => {
        if (lan == "" || lan == null) {
          this.storage.set('language', "eng");
          lan = "eng";
        }
        this.changelanguage(lan);
      });
      this.storage.get('language').then((lan) => {
        if (lan == "" || lan == null) {
          this.storage.set('language', "eng");
          lan = "eng";
        }
        this.changelanguage(lan);
      });
      // this.ipaddressApp = this.global.ipaddressApp;
      // this.ipaddress = this.global.ipaddress;
      // this.ipaddressCMS = this.global.ipaddressCMS;
      this.storage.get('ipaddress').then((ipaddress) => {
        if (ipaddress == '' || ipaddress == null) {
          this.storage.set('ipaddress', this.global.ipaddress);
          this.ipaddress = this.global.ipaddress;
        } else {
          this.ipaddress = ipaddress;
          //this.getNotiCount();
        }
        this.readVersionMobile();
      });
      this.storage.get('ipaddressApp').then((ipaddress) => {
        if (ipaddress == '' || ipaddress == null) {
          this.storage.set('ipaddressApp', this.global.ipaddressApp);
          this.ipaddress = this.global.ipaddressApp;
        } else {
          this.ipaddress = ipaddress;
        }
      });
      this.storage.get('ipaddress3').then((ipaddress) => {
        if (ipaddress == '' || ipaddress == null) {
          this.storage.set('ipaddress3', this.global.ipaddress3);
          this.ipaddress = this.global.ipaddress3;
        } else {
          this.ipaddress = ipaddress;
        }
      });
      //this.readVersionMobile();
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#000000');
      this.splashScreen.hide();
      this.sqlite.create({
        name: "mw.db",
        location: "default"
      }).then((db: SQLiteObject) => {
      }).catch(e => console.log(e));
    });
  }

  branchInit() {
    if (!this.platform.is('cordova')) { return }
    const Branch = window['Branch'];
    Branch.initSession(data => {
      if (data['+clicked_branch_link']) {
        this.storage.set("deepLinkData", data.custom_string);
      }
    });
  }

  branchInitResume() {
    if (!this.platform.is('cordova')) { return }
    const Branch = window['Branch'];
    Branch.initSession(data => {
      if (data['+clicked_branch_link']) {
        this.nav.setRoot(SingleViewPage, { data: data.custom_string, status: 1 });
      }
    });
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.pages.length; i++) {
        if (this.pages[i].name != '') {
          this.pages[i].name = this.textEng[11];
        }
        for (let j = 0; j < this.pages[i].data.length; j++) {
          this.pages[i].data[j].title = this.textEng[j];
        }
        
      }
      
        this.version[0] = this.versionEng[0];
      
    } else {
      for (let i = 0; i < this.pages.length; i++) {
        if (this.pages[i].name != '') {
          this.pages[i].name = this.textMyan[11];
        }
        for (let j = 0; j < this.pages[i].data.length; j++) {
          this.pages[i].data[j].title = this.textMyan[j];
        }
       
      }
      
        this.version[0] = this.versionMyan[0];
      
    }
  }

  openPage(page) {
    //console.log(`This is menu` + ". " + JSON.stringify(page));
    let params = {};
    if (page.index != undefined) {
      params = { tabIndex: page.index, tabTitle: page.title };
    } else {
      params = page.component;
    }

    // this.rootPage = TabsPage;
    if (page.component != '') {
      // this.nav.setRoot(page.component);
      if (page.index == '') {
        if (page.component == AccountSecurityPage) {
          // this.firebaseAnalytics.logEvent('page_click', { userID: this.userData.userID,Message:"Account Security"})
          //   .then((res: any) => { console.log(res); })
          //   .catch((error: any) => console.error(error));
          this.nav.push(page.component);
        }
        else  if (page.component == NewShopPage) {
          // this.firebaseAnalytics.logEvent('page_click', { userID: this.userData.userID,Message:"Account Security"})
          //   .then((res: any) => { console.log(res); })
          //   .catch((error: any) => console.error(error));
          this.nav.push(page.component);
        }
        // else if (page.component == RegistrationPage) {
        //   this.nav.push(page.component);
        // } 

        else if (page.component == FaqPage) {
          // this.firebaseAnalytics.logEvent('page_click', { userID: this.userData.userID,Message:"FAQ"})
          //   .then((res: any) => { console.log(res); })
          //   .catch((error: any) => console.error(error));
          this.nav.push(page.component);
        } else if (page.component == MapPage) {
          // this.firebaseAnalytics.logEvent('page_click', { userID: this.userData.userID,Message:"Location"})
          //   .then((res: any) => { console.log(res); })
          //   .catch((error: any) => console.error(error));
          this.nav.push(page.component);
        } else if (page.component == ExchangePage) {
          // this.firebaseAnalytics.logEvent('page_click', { userID: this.userData.userID,Message:"Exchange Rate"})
          //   .then((res: any) => { console.log(res); })
          //   .catch((error: any) => console.error(error));
           this.nav.push(page.component);
        } else if (page.component == ContactUsPage) {
          // this.firebaseAnalytics.logEvent('page_click', { userID: this.userData.userID,Message:"Contact Us"})
          //   .then((res: any) => { console.log(res); })
          //   .catch((error: any) => console.error(error));
          this.nav.push(page.component);
        }
        else if (page.component == QPayPage) {
          // this.firebaseAnalytics.logEvent('page_click', { userID: this.userData.userID,Message:"Contact Us"})
          //   .then((res: any) => { console.log(res); })
          //   .catch((error: any) => console.error(error));
          this.nav.push(page.component);//// For Staff Loan
        }
        else if (page.component == StaffLoanInformationPage) {
          // this.firebaseAnalytics.logEvent('page_click', { userID: this.userData.userID,Message:"Contact Us"})
          //   .then((res: any) => { console.log(res); })
          //   .catch((error: any) => console.error(error));
          this.nav.push(page.component);//// For Staff Loan
        }
       
       
        else if (page.component == SchedulePage) {
          // this.firebaseAnalytics.logEvent('page_click', { userID: this.userData.userID,Message:"Contact Us"})
          //   .then((res: any) => { console.log(res); })
          //   .catch((error: any) => console.error(error));
          this.nav.push(page.component);
        }
        else if (page.component == ShowStatusPage) {
          // this.firebaseAnalytics.logEvent('page_click', { userID: this.userData.userID,Message:"Contact Us"})
          //   .then((res: any) => { console.log(res); })
          //   .catch((error: any) => console.error(error));
          this.nav.push(page.component);
        }else if (page.component == Login) {
          // this.firebaseAnalytics.logEvent('page_click', { userID: this.userData.userID,Message:"Logout"})
          //   .then((res: any) => { console.log(res); })
          //   .catch((error: any) => console.error(error));
          this.gologout();
        } else if (page.component == SettingsPage) {
          // this.firebaseAnalytics.logEvent('page_click', { userID: this.userData.userID,Message:"Setting"})
          //   .then((res: any) => { console.log(res); })
          //   .catch((error: any) => console.error(error));
          this.nav.push(page.component);
        }
        else {
          this.nav.setRoot(page.component);
        }
      } else {
        this.nav.setRoot(TabsPage, params).catch((err: any) => {
          //console.log(`Didn't set nav root: ${err}`);
        });
      }
    } else if (page.component == '' && page.index == '1') {
      this.goMarket();
    } else {
      // // this.storage.remove('phonenumber');
      this.storage.remove('username');
      this.storage.remove('userData');
      this.nav.setRoot(Login);
    }
  }

  goMarket() {
    if (this.global.regionCode == '13000000') {
      //Yangon App (13000000)
      window.open('https://play.google.com/store/apps/details?id=dc.digitalyangon', '_system');
    }
    if (this.global.regionCode == '10000000') {
      //Mandalay App (10000000)
      window.open('https://play.google.com/store/apps/details?id=dc.digitalmandalay', '_system');
    }
    if (this.global.regionCode == '00000000') {
      //All App (00000000)
      window.open('https://play.google.com/store/apps/details?id=wallet.NSBmawalletdev', '_system');
    }
  }

  edit() {
    this.nav.push(Profile);
  }

  removeStorage() {
    this.storage.remove('phonenumber');
    this.storage.remove('username');
    this.storage.remove('userData');
    this.storage.remove('firstlogin');
    this.storage.remove('appData');
    this.storage.remove('localPhotoAndLink');
    this.storage.remove("stateList");
    this.storage.remove("uari");
   //ndh
  }

  readVersionMobile() {
    let parameter = {
      version: this.global.version,
      appCode: this.global.appCode,
      t1: "",
      n1: 0
    };
    //console.log("read-version:" + JSON.stringify(parameter));
    this.http.post(this.ipaddress + '/service001/getVersion', parameter).map(res => res.json()).subscribe(result => {

      if (result.code == "0000") {
        if (result.versionStatus == '1') {
          let confirm = this.alertCtrl.create({
            title: result.versionTitle,
            message: result.versionDesc,
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'CANCEL',
                handler: () => {

                }
              },
              {
                text: 'UPDATE',
                handler: () => {
                  window.open(myConst.walletApp.android.storeUrl, '_system');
                }
              }
            ]
          });
          confirm.present();
        }
        else if (result.versionStatus == '999') {
          let confirm = this.alertCtrl.create({
            title: result.versionTitle,
            message: result.versionDesc,
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'UPDATE',
                handler: () => {
                  window.open(myConst.walletApp.android.storeUrl, '_system');
                  this.platform.exitApp();
                }
              }
            ]
          });
          confirm.present();
        }
      }
    },
      error => {

        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 5000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);

      });
  }

  readQr() {
    let pass1 = this.userData.userID;
    //let pass2 = this.username;
    let pass3 = this.userData.sKey;
    let temp = [];
    temp.push("contacts");
    temp.push(pass1);
    //temp.push(pass2);
    temp.push(pass3);
    let dataValue = JSON.stringify(temp);
    console.log('dataValue=' + JSON.stringify(dataValue));
    this.nav.push(ProfileQrPage, {
      data: dataValue,
      user: this.userData
    });
  }

  gologout() {
    this.phoneno="";
    //this.storage.remove('phonenumber');
    this.storage.remove('username');
    this.storage.remove('userData');
  //   this.firebaseAnalytics.logEvent('logout_successful', {log_out:this.username})
  //   .then((res: any) => { console.log(res); })
  //  .catch((error: any) => console.error(error));
    this.appCtrl.getRootNav().setRoot(Login);
  }

}

