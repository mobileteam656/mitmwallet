// module
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, Events, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { SQLite } from '@ionic-native/sqlite';
import { DatePipe } from '@angular/common';
import { MyApp } from './app.component';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { QRCodeModule } from 'angular2-qrcode';
import { Keyboard } from '@ionic-native/keyboard';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
//import { AndroidPermissions } from '@ionic-native/android-permissions';
// provider
import { GlobalProvider } from '../providers/global/global';
import { ChangelanguageProvider } from '../providers/changelanguage/changelanguage';
import { ChangefontProvider } from '../providers/changefont/changefont';
import { UtilProvider } from '../providers/util/util';
import { SuperTabsController } from '../providers/super-tabs-controller';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { FunctProvider } from '../providers/funct/funct';
import { SocialSharing } from '@ionic-native/social-sharing';//ndh
import { Screenshot } from '@ionic-native/screenshot';//ndh
// page
import { AccountTransfer } from '../pages/transfer/transfer';
import { TransferConfirm } from '../pages/transfer-confirm/transfer-confirm';
import { TransferDetails } from '../pages/transfer-details/transfer-details';

import { AccountTransaction } from '../pages/account-transaction/account-transaction';
import { AccountTransactionDetail } from '../pages/account-transaction-detail/account-transaction-detail';
import { MainBlankPage } from '../pages/main-blank-page/main-blank-page';
import { Login } from '../pages/login/login';
import { LogoutPage } from '../pages/logout/logout';
import { Language } from '../pages/language/language';
import { PopOverListPage } from '../pages/pop-over-list/pop-over-list';
import { MyPopOverListPage } from '../pages/my-pop-over-list/my-pop-over-list';
import { Domain } from '../pages/domain/domain';
import { URL } from '../pages/url/url';
import { NotificationPage } from '../pages/notification/notification';
import { QrPayment } from '../pages/qr-payment/qr-payment';
import { Payment } from '../pages/payment/payment';
import { PaymentConfirm } from '../pages/payment-confirm/payment-confirm'
import { QrReceipt } from '../pages/qr-receipt/qr-receipt';
import { Profile } from '../pages/profile/profile';
import { Receipt } from '../pages/receipt/receipt';
import { Chat } from '../pages/chat/chat';
import { ContactPage } from '../pages/contact/contact';
import { MessagePage } from '../pages/message/message';
import { MessageUserPage } from '../pages/message-user/message-user';
import { MessageChooseChannelsPage } from '../pages/message-choose-channels/message-choose-channels';
import { TabsPage } from '../pages/tabs/tabs';
import { TextAvatar } from '../components/text-avatar/text-avatar';
import { AccountList } from '../pages/account-list/account-list';
import { AccountAdd } from '../pages/account-add/account-add';
import { AccountInfo } from '../pages/account-info/account-info';
import { ChooseContactPage } from '../pages/choose-contact/choose-contact';
import { RegistrationPage } from '../pages/registration/registration';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { Network } from '@ionic-native/network';
import { TopUp } from '../pages/top-up/top-up';
import { TopUpDetails } from '../pages/top-up-details/top-up-details';
import { PopoverContactAddPage } from '../pages/popover-contact-add/popover-contact-add';
import { CommentPage } from '../pages/comment/comment';
import { VideodetailPage } from '../pages/videodetail/videodetail';
import { HomePage } from '../pages/home/home';
import { LikepersonPage } from '../pages/likeperson/likeperson';
import { SugvideoPage } from '../pages/sugvideo/sugvideo';
import { PopoverCommentPage } from '../pages/popover-comment/popover-comment';
import { ReplyCommentPage } from '../pages/reply-comment/reply-comment';
import { Clipboard } from '@ionic-native/clipboard';
import { ProfileQrPage } from '../pages/profile-qr/profile-qr';
//import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { ChooseMerchantPage } from '../pages/choose-merchant/choose-merchant';
import { PaymentDetailsPage } from '../pages/payment-details/payment-details';
import { PicsPage } from '../pages/pics/pics';
import { DocumentsPage } from '../pages/documents/documents';
import { LawsPage } from '../pages/laws/laws';
import { PicsChannelPage } from '../pages/pics-channel/pics-channel';
import { BillPage } from '../pages/bill/bill';
import { PicsDetailsPage } from '../pages/pics-details/pics-details';
import { PicsDetailsChannelPage } from '../pages/pics-details-channel/pics-details-channel';
import { FontPopoverPage } from '../pages/font-popover/font-popover';
import { CreatePopoverPage } from '../pages/create-popover/create-popover';
import { ContactInfoPage } from '../pages/contact-info/contact-info';
import { SingleViewPage } from '../pages/single-view/single-view';
import { OnlycommentPage } from '../pages/onlycomment/onlycomment';
import { ViewPhotoMessagePage } from '../pages/view-photo-message/view-photo-message';
import { FbdetailPage } from '../pages/fbdetail/fbdetail';
import { Device } from '@ionic-native/device';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { FileOpener } from '@ionic-native/file-opener';
import { QRValuePage } from '../pages/qr-value/qr-value';
import { SelectParticipantPage } from '../pages/select-participant/select-participant';
import { ViewGroupPage } from '../pages/view-group/view-group';
import { MyPopOverListChatPage } from '../pages/my-pop-over-list-chat/my-pop-over-list-chat';
import { CreatePopoverChatPage } from '../pages/create-popover-chat/create-popover-chat';
import { PopoverMsgPage } from '../pages/popover-msg/popover-msg';
import { MorePage } from '../pages/more/more';
import { WalletPage } from '../pages/wallet/wallet';
import { QrUtilityPage } from '../pages/qr-utility/qr-utility';
import { QrUtilitySuccessPage } from '../pages/qr-utility-success/qr-utility-success';
import { CreatePostPage } from '../pages/create-post/create-post';
import { EditImagesPage } from '../pages/edit-images/edit-images';
import { SelectAlbumPage } from '../pages/select-album/select-album';
import { TagChoosePage } from '../pages/tag-choose/tag-choose';
import { ImagePicker } from '@ionic-native/image-picker';
import { Autosize } from '../components/autosize';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { AccountSecurityPage } from '../pages/account-security/account-security';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { ResetPassword } from '../pages/reset-password/reset-password';
import { TransferPasswordPage } from '../pages/transfer-password/transfer-password';
import { FaqPage } from '../pages/faq/faq';
import { PaymentPasswordPage } from '../pages/payment-password/payment-password';
import { MyPopOverPage } from '../pages/my-pop-over-page/my-pop-over-page';
import { LocationPage } from '../pages/location/location';
import { AllserviceProvider } from '../providers/allservice/allservice';
import { LocationDetail } from '../pages/location-detail/location-detail';
import { TransferFailPage } from '../pages/transfer-fail/transfer-fail';
import { PaymentFailPage } from '../pages/payment-fail/payment-fail';
import { ContactUsPage } from '../pages/contact-us/contact-us';
import { CashPage } from '../pages/cash/cash';
import { CashInPage } from '../pages/cash-in/cash-in';
import { CashOutPage } from '../pages/cash-out/cash-out';
import { AgentPage } from '../pages/agent/agent';
import { BankAccountPage } from '../pages/bank-account/bank-account';
import { NearByPage } from '../pages/near-by/near-by';
import { Locations } from '../providers/locations';
import { GoogleMaps } from '../providers/google-maps';
import { Connectivity } from '../providers/connectivity';
import { MapPage } from '../pages/map/map';
import { MyPopOverLocationPage } from '../pages/my-pop-over-location/my-pop-over-location';
import { PostLikePersonPage } from '../pages/post-like-person/post-like-person';
import { Contacts } from '@ionic-native/contacts';
import { ContactListPage } from '../pages/contact-list/contact-list';
import { SettingsPage } from '../pages/settings/settings';
import { QuickpayPage } from '../pages/quickpay/quickpay';
import { QuickpayConfirmPage } from '../pages/quickpay-confirm/quickpay-confirm';
import { QuickpaySuccessPage } from '../pages/quickpay-success/quickpay-success';
import { AgentTransferPage } from '../pages/agent-transfer/agent-transfer';
import { PaymentTypePage } from '../pages/payment-type/payment-type';
import { CallNumber } from '@ionic-native/call-number';
import { ExchangePage } from '../pages/exchange/exchange';
import { CashoutTransferPage } from '../pages/cashout-transfer/cashout-transfer';
import { ScanPage } from '../pages/scan/scan';
import { MerchantPaymentPage } from '../pages/merchant-payment/merchant-payment';
import { MerchantPaymentSamplePage } from '../pages/merchant-payment-sample/merchant-payment-sample';
import { FCM } from '@ionic-native/fcm';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Badge } from '@ionic-native/badge';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { AccountJoinPage } from '../pages/account-join/account-join';
import { SkynetTopupPage } from '../pages/skynet-topup/skynet-topup';
import { SkynetConfirmPage } from '../pages/skynet-confirm/skynet-confirm';
import { SkynetSuccessPage } from '../pages/skynet-success/skynet-success';
import { MerchantPaymentConfirmPage } from '../pages/merchant-payment-confirm/merchant-payment-confirm';
import { SkynetTopupCardNumListPage } from '../pages/skynet-topup-card-num-list/skynet-topup-card-num-list';
import { UtilityConfirmPage } from '../pages/utility-confirm/utility-confirm';
import { EventLoggerProvider } from '../providers/eventlogger/eventlogger';
import { DefaultPopOver } from '../pages/default-pop-over/default-pop-over';
import { IpchangePage } from '../pages/ipchange/ipchange';
import { FirebaseCrashlytics } from '@ionic-native/firebase-crashlytics/ngx';
import { IonicSelectableModule } from 'ionic-selectable';
import { MobileLoginPage } from '../pages/mobile-login/mobile-login';
import { MobileLoginOtpPage } from '../pages/mobile-login-otp/mobile-login-otp';
import { MobileAccountSummaryPage } from '../pages/mobile-account-summary/mobile-account-summary';
import { TopUpWalletPage } from '../pages/top-up-wallet/top-up-wallet';
import { TopUpWalletConfrimPage } from '../pages/top-up-wallet-confrim/top-up-wallet-confrim';
import { WalletTopupOtpPage } from '../pages/wallet-topup-otp/wallet-topup-otp';

import { MtpTopupSuccessPage } from '../pages/mtp-topup-success/mtp-topup-success';
import { ShoplistPage } from '../pages/shoplist/shoplist';
import { NewShopPage } from '../pages/new-shop/new-shop';
import { ProductlistPage } from '../pages/productlist/productlist';
import { InfoPage } from '../pages/info/info';
import { LoginMfi } from '../pages/login-mfi/login-mfi';
import { PaybillPage } from '../pages/paybill/paybill';
import { ShowStatusPage } from '../pages/show-status/show-status';
import { ShoppingCartInfoPage } from '../pages/shopping-cart-info/shopping-cart-info';
import { SchedulePage } from '../pages/schedule/schedule';
import { LoanListInfoPage } from '../pages/loan-list-info/loan-list-info';
import { ScheduledetailPage } from '../pages/scheduledetail/scheduledetail';
import { QPayPage } from '../pages/q-pay/q-pay';
import { QPayInfoPage } from '../pages/q-pay-info/q-pay-info';
import { QPaySuccessPage } from '../pages/q-pay-success/q-pay-success';
import { QPayConfirmPage } from '../pages/q-pay-confirm/q-pay-confirm';
import { StaffLoanInformationPage } from '../pages/staff-loan-information/staff-loan-information';
import { MptTopupConfirmPage } from '../pages/mpt-topup-confirm/mpt-topup-confirm';
import { MptTopupPage } from '../pages/mpt-topup/mpt-topup';

@NgModule({
  declarations: [
    Autosize,
    MyApp,
    CashoutTransferPage,
    ScanPage,
    WalletPage,
    Login,
    LogoutPage,
    Language,
    AccountTransfer,
    TransferDetails,
    TopUp,
    TopUpDetails,
    TransferConfirm,
    PopOverListPage,
    MainBlankPage,
    MyPopOverListPage,
    Domain,
    URL,
    AccountTransaction,
    AccountTransactionDetail,
    NotificationPage,
    QrPayment,
    Payment,
    QrReceipt,
    Profile,
    Receipt,
    Chat,
    ContactPage,
    MessagePage,
    MessageUserPage,
    MessageChooseChannelsPage,
    TabsPage,
    TextAvatar,
    AccountList,
    AccountAdd,
    AccountInfo,
    RegistrationPage,
    PopoverContactAddPage,
    ChooseContactPage,
    PaymentConfirm,
    HomePage,
    LikepersonPage,
    CommentPage,
    VideodetailPage,
    MerchantPaymentSamplePage,
    SugvideoPage,
    PopoverCommentPage,
    ReplyCommentPage,
    ProfileQrPage,
    ChooseMerchantPage,
    PaymentDetailsPage,
    PicsPage,
    DocumentsPage,
    LawsPage,
    PicsChannelPage,
    BillPage,
    PicsDetailsPage,
    PicsDetailsChannelPage,
    FontPopoverPage,
    CreatePopoverPage,
    ContactInfoPage,
    SingleViewPage,
    OnlycommentPage,
    ViewPhotoMessagePage,
    FbdetailPage,
    QRValuePage,
    SelectParticipantPage,
    ViewGroupPage,
    MyPopOverListChatPage,
    CreatePopoverChatPage,
    PopoverMsgPage,
    MorePage,
    QrUtilityPage,
    QrUtilitySuccessPage,
    CreatePostPage,
    EditImagesPage,
    SelectAlbumPage,
    TagChoosePage,
    ChangePasswordPage,
    AccountSecurityPage,
    SignUpPage,
    ResetPassword,
    TransferPasswordPage,
    FaqPage,
    PaymentPasswordPage,
    MyPopOverPage,
    LocationPage,
    LocationDetail,
    TransferFailPage,
    PaymentFailPage,
    ContactUsPage,
    CashPage,
    CashInPage,
    CashOutPage,
    AgentPage,
    BankAccountPage,
    NearByPage,
    MapPage,
    MyPopOverLocationPage,
    PostLikePersonPage,
    ContactListPage,
    SettingsPage,
    QuickpayPage,
    QuickpayConfirmPage,
    QuickpaySuccessPage,
    AgentTransferPage,
    PaymentTypePage,
   ExchangePage,
   MerchantPaymentPage,
   AccountJoinPage,
   SkynetTopupPage,
   SkynetConfirmPage,
   SkynetSuccessPage,
   MerchantPaymentConfirmPage,
   SkynetTopupCardNumListPage,
   UtilityConfirmPage,
   DefaultPopOver,
   IpchangePage,
   RegistrationPage,
   MobileLoginPage,
   MobileLoginOtpPage,
   MobileAccountSummaryPage,
   TopUpWalletPage,
   TopUpWalletConfrimPage,
   WalletTopupOtpPage,
   MptTopupPage,
   MtpTopupSuccessPage,
   MptTopupConfirmPage,
   ShoplistPage,
   NewShopPage ,
   ProductlistPage,
   InfoPage,
   LoginMfi,
   PaybillPage,
   ShowStatusPage,
   ShoppingCartInfoPage,
   SchedulePage,
   LoanListInfoPage,
   ScheduledetailPage,
   QPayPage,
   QPayInfoPage,
   QPaySuccessPage,
   QPayConfirmPage,
   StaffLoanInformationPage
   // ndh for prepaid topup card

  ],

  imports: [
    BrowserModule,
    HttpModule,
    IonicSelectableModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
      tabsPlacement: 'bottom',
      scrollAssist: false,
      autoFocusAssist: false,
      platforms: {
        android: {
          tabsPlacement: 'bottom'
        },
        ios: {
          tabsPlacement: 'bottom',
          menuType: 'overlay'
        },
        windows:
        {
          tabsPlacement: 'top'
        }
      }
    }),
    IonicStorageModule.forRoot(),
    SlimLoadingBarModule.forRoot(),
    SuperTabsModule.forRoot(),
    QRCodeModule,
    Ng2OrderModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    CashoutTransferPage,
    MyApp,
    ScanPage,
    Login,
    LogoutPage,
    WalletPage,
    Language,
    TopUp,
    MerchantPaymentSamplePage,
    AccountTransfer,
    TransferConfirm,
    TransferDetails,
    TopUpDetails,
    PopOverListPage,
    MainBlankPage,
    MyPopOverListPage,
    Domain,
    URL,
    AccountTransaction,
    AccountTransactionDetail,
    NotificationPage,
    QrPayment,
    Payment,
    QrReceipt,
    Profile,
    Receipt,
    Chat,
    ContactPage,
    MessagePage,
    MessageUserPage,
    MessageChooseChannelsPage,
    TabsPage,
    AccountList,
    AccountAdd,
    AccountInfo,
    RegistrationPage,
    PopoverContactAddPage,
    ChooseContactPage,
    PaymentConfirm,
    HomePage,
    LikepersonPage,
    CommentPage,
    VideodetailPage,
    SugvideoPage,
    PopoverCommentPage,
    ReplyCommentPage,
    ProfileQrPage,
    ChooseMerchantPage,
    PaymentDetailsPage,
    PicsPage,
    DocumentsPage,
    LawsPage,
    PicsDetailsChannelPage,
    BillPage,
    PicsDetailsPage,
    FontPopoverPage,
    CreatePopoverPage,
    ContactInfoPage,
    SingleViewPage,
    OnlycommentPage,
    ViewPhotoMessagePage,
    FbdetailPage,
    PicsChannelPage,
    QRValuePage,
    SelectParticipantPage,
    ViewGroupPage,
    MyPopOverListChatPage,
    CreatePopoverChatPage,
    PopoverMsgPage,
    MorePage,
    QrUtilityPage,
    QrUtilitySuccessPage,
    CreatePostPage,
    EditImagesPage,
    SelectAlbumPage,
    TagChoosePage,
    ChangePasswordPage,
    AccountSecurityPage,
    SignUpPage,
    ResetPassword,
    TransferPasswordPage,
    FaqPage,
    PaymentPasswordPage,
    MyPopOverPage,
    LocationPage,
    LocationDetail,
    TransferFailPage,
    PaymentFailPage,
    ContactUsPage,
    CashPage,
    CashInPage,
    CashOutPage,
    AgentPage,
    BankAccountPage,
    NearByPage,
    MapPage,
    MyPopOverLocationPage,
    PostLikePersonPage,
    ContactListPage,
    SettingsPage,
    QuickpayPage,
    QuickpayConfirmPage,
    QuickpaySuccessPage,
    AgentTransferPage,
    PaymentTypePage,
    ExchangePage,
    MerchantPaymentPage,
    AccountJoinPage,
    SkynetTopupPage,
    SkynetConfirmPage,
    SkynetSuccessPage,
    MerchantPaymentConfirmPage,
    SkynetTopupCardNumListPage,
    UtilityConfirmPage,
    DefaultPopOver,
    IpchangePage,
    RegistrationPage,
    MobileLoginPage,
    MobileLoginOtpPage,
    MobileAccountSummaryPage,
    TopUpWalletPage,
    TopUpWalletConfrimPage,
    WalletTopupOtpPage,
    MptTopupPage,
    MtpTopupSuccessPage,
    MptTopupConfirmPage,
    ShoplistPage,
    NewShopPage,
    ProductlistPage,
    InfoPage,
    LoginMfi,
    PaybillPage,
    ShowStatusPage,
    ShoppingCartInfoPage,
    SchedulePage,
    LoanListInfoPage,
    ScheduledetailPage,
    QPayPage,
    QPayInfoPage,
    QPaySuccessPage,
    QPayConfirmPage,
    StaffLoanInformationPage
  ],
  providers: [
    FirebaseCrashlytics,
    FingerprintAIO,
    FirebaseAnalytics,
    Badge,
    LocalNotifications,
    FCM,
    Screenshot,
    SocialSharing,
    CallNumber,
    FileOpener,
    DocumentViewer,
    Keyboard,
    StatusBar,
    SplashScreen,
    SQLite,
    File,
    DatePipe,
    NativeGeocoder,
    LocationAccuracy,
    Geolocation,
    Keyboard,
    GlobalProvider,
    ChangefontProvider,
    ChangelanguageProvider,
    BarcodeScanner,
    UtilProvider,
    SuperTabsController,
    Camera,
    ImagePicker,
    Network,
    Device,
    FunctProvider,
    Clipboard,
    FileTransfer,
    EventLoggerProvider,
    //AndroidPermissions,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AllserviceProvider, Locations, GoogleMaps, Connectivity, Contacts
  ]
})
export class AppModule { }
