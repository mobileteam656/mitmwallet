import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ToastController, Platform, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Changefont } from '../changefont/changeFont';
import { Storage } from '@ionic/storage';
import { PaybillPage } from '../paybill/paybill';
import { ProductlistPage } from '../productlist/productlist';

/**
 * Generated class for the ShoppingCartInfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-shopping-cart-info',
  templateUrl: 'shopping-cart-info.html',
  providers: [Changefont]

})

export class ShoppingCartInfoPage {
  textdata: string[] = [];
  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  font: string = '';
  ipPhoto = this.global.ipphoto;
  ipaddress: string;
  public loading;
  textMyan: any = ["နောက်သို.", "လျှောက်ထားသည်.ပစ္စည်းများ", "လျှောက်ထားမည်.ပစ္စည်းရွေးပါ"];
  textEng: any = ["Next", "Apply Products", "Please Select Product"];

  chooseItemLst: string[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, public global: GlobalProvider, public events: Events,
    public changefont: Changefont, public toastCtrl: ToastController,
    public platform: Platform, public loadingCtrl: LoadingController) {
    this.storage.get('ip').then((ip) => {

      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
        console.log("not undefined  conditon ");
        console.log("Your ip is", this.ipaddress);
      }
      else {
        console.log(" undefined  conditon ");
        this.ipaddress = this.global.ipaddress;
        console.log("Your ip main is", this.ipaddress);
      }

      console.log("Apply Product List Data is:" + JSON.stringify(this.global.PersonData));
      //Font change
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      })
      this.storage.get('language').then((font) => {
        console.log('Your language is', font);
        this.changelanguage(font);
      });
      console.log("Choose Product Item is:" + JSON.stringify(this.global.PersonData.applyProductList.data));
      for (let i = 0; i < this.global.PersonData.applyProductList.data.length; i++) {
        if (this.global.PersonData.applyProductList.data[i].productprice != "" && this.global.PersonData.applyProductList.data[i].productprice != undefined && this.global.PersonData.applyProductList.data[i].productprice != null) {
          this.global.PersonData.applyProductList.data[i].productprice = this.global.PersonData.applyProductList.data[i].productprice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
      }
      this.chooseItemLst = this.global.PersonData.applyProductList.data;
      console.log("Choose Item List is :" + JSON.stringify(this.chooseItemLst));
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShoppingCartInfoPage');
  }

  removeItem(item) {
    for (var i = 0; i < this.global.PersonData.applyProductList.data.length; i++) {
      console.log('remove ' + this.global.PersonData.applyProductList.data[i] + ':' + item);

      if (this.global.PersonData.applyProductList.data[i].outletProductSyskey == item) {
        this.global.PersonData.applyProductList.data.splice(i, 1);
        break;
      }
      console.log("After Remove Items is >>" + JSON.stringify(this.global.PersonData.applyProductList.data));
    }
    this.global.productcount = this.global.PersonData.applyProductList.data.length;
  }

  comfirm() {
    if (this.global.productcount > 0) {
      console.log("before paybill", this.global.PersonData);
      this.navCtrl.push(PaybillPage,
        {

        });
    } else {
      let toast = this.toastCtrl.create({
        message: this.textdata[2],
        position: 'bottom',
        showCloseButton: true,
        dismissOnPageChange: true,
        closeButtonText: 'OK'
      });
      toast.present(toast);
      this.loading.dismiss();
    }
  }
  gobackmain() {
    this.navCtrl.setRoot(ProductlistPage,
      {

      });

  }
  ionViewDidEnter() {
    this.backButtonExit();
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      this.gobackmain();
    });
  }
}
