// import { ProductlistPage } from '../productlist/productlist';
import { Component } from '@angular/core';
import { Login } from '../login/login';
import { ModalController } from 'ionic-angular';
import { IonicPage, NavController, NavParams, Events, ToastController, LoadingController, Platform, AlertController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Storage } from '@ionic/storage';
import { Changefont } from '../changefont/changeFont';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { WalletPage } from '../wallet/wallet';
import { ProductlistPage } from '../productlist/productlist';
// import { InfoPage } from '../info/info';

// import { ModalPage } from './modal-page';

/**
 * Generated class for the ShoplistPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
  selector: 'page-shoplist',
  templateUrl: 'shoplist.html',
  providers: [Changefont]
})
export class ShoplistPage {

  ipaddress: string;
  regionList: string[] = [];
  townshipList:
    string[] = [];
  regionData: any = '';
  townData: any = '';
  public loading;
  usersyskey: any;

  textMyan: any = ["စျေးဆိုင်များ"];
  textEng: any = ["Shops"];

  textdata: string[] = [];
  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  font: string = '';
  outletSyskey: any;
  data: any = [];
  flagNet: any;

  testRadioOpen: boolean;
  testRadioResult;
  ipPhoto = this.global.ipphoto;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public events: Events,
    public global: GlobalProvider, public changefont: Changefont, public http: Http, public toastCtrl: ToastController,
    public platform: Platform, public util: UtilProvider, public loadingCtrl: LoadingController, public alerCtrl: AlertController
    , public network: Network) {

    this.storage.get('ip').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
        console.log("not undefined  conditon ");
        console.log("Your ip is", this.ipaddress);
      }
      else {
        console.log(" undefined  conditon ");
        this.ipaddress = this.global.ipaddress;
        console.log("Your ip main is", this.ipaddress);
      }
      if (this.global.PersonData.applyProductList.data.length == 0) {
        this.global.outletSyskey = '';
      }

      this.usersyskey = this.global.PersonData.applicantdata.syskey;
      if (this.usersyskey == null) {
        this.getshoplist();
      } else {
        this.getshoplist();
      }
      //Font change
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      })
      this.storage.get('language').then((font) => {
        console.log('Your language is', font);
        this.changelanguage(font);
      });
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
    }
  }

  getshoplist() {
    if (this.global.netWork == "connected") {
      this.loading = this.loadingCtrl.create({
        content: "", spinner: 'circles',
        dismissOnPageChange: true
        //   duration: 3000
      });
      this.loading.present();
      this.http.get(this.global.ipAdressMFI + '/serviceMFI/getShopList').map(res => res.json().data).subscribe(data => {
        if (data != null) {
          this.global.PersonData.shopDataList.shopdata = data;
          console.log("shoplist>>" + JSON.stringify(this.global.PersonData.shopDataList.shopdata));
        } else {
          let toast = this.toastCtrl.create({
            message: "Can't connect right now!",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        }
        this.loading.dismiss();
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    } else {
      let toast = this.toastCtrl.create({
        message: this.util.getErrorMessage("Check your internet connection!"),
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }
  }

  getShopListByApplicantSyskey() {
    //psst added network test
    if (this.global.netWork == "connected") {
      this.loading = this.loadingCtrl.create({
        content: "", spinner: 'circles',
        dismissOnPageChange: true
        //   duration: 3000
      });
      this.loading.present();
      this.http.get(this.global.ipAdressMFI + '/serviceMFI/getShopListByApplicantSyskey?syskey=' + this.usersyskey).map(res => res.json()).subscribe(data => {
        if (data != null) {
          let tempArray = [];
          if (!Array.isArray(data.data)) {
            tempArray.push(data.data);
            data.data = tempArray;
          }
          this.global.PersonData.shopDataList.shopdata = [];
          for (let i = 0; i < data.data.length; i++) {
            this.global.PersonData.shopDataList.shopdata.push(data.data[i]);
          }
          console.log("shoplist>>" + JSON.stringify(this.global.PersonData.shopDataList.shopdata));
          console.log("Shop Data List getShopListByApplicantSyskey array Length >>" + this.global.PersonData.shopDataList.shopdata.length);
        } else {
          let toast = this.toastCtrl.create({
            message: "Can't connect right now!",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        }
        this.loading.dismiss();
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }
  }

  next(shopData) {
    this.outletSyskey = shopData.outletSyskey;
    console.log("this.global.outletSyskey:" + this.global.outletSyskey);
    if (this.global.outletSyskey == this.outletSyskey) {
      console.log("shop list equal::" + this.global.outletSyskey + this.outletSyskey);
      this.global.outletSyskey = shopData.outletSyskey;
      this.navCtrl.setRoot(ProductlistPage,
        {

        });
    }
    else { // updated  for shop alert      
      if (this.global.outletSyskey != '') {
        let confirm = this.alerCtrl.create({
          // title: 'Use this lightsaber?',
          message: 'Are you sure to choose new shop?',
          buttons: [
            {
              text: 'Yes',
              handler: () => {
                console.log("shop list not equal::" + this.global.outletSyskey + this.outletSyskey);
                this.global.outletSyskey = shopData.outletSyskey;
                this.global.PersonData.applyProductList.data = [];
                this.global.productcount = 0;
                console.log('Yes clicked');
                console.log("data" + this.global.PersonData.applyProductList.data);
                this.navCtrl.setRoot(ProductlistPage,
                  {

                  });
                console.log("shop list organizationSyskey::" + JSON.stringify(this.global.outletSyskey));
              }
            }
            ,
            {
              text: 'No',
              handler: () => {
                console.log('No clicked');
              }
            }
          ]
        });
        confirm.present()
      }
      else {
        console.log("shop list not equal::" + this.global.outletSyskey + this.outletSyskey);
        this.global.outletSyskey = shopData.outletSyskey;
        this.global.PersonData.applyProductList.data = [];
        console.log('Yes clicked');
        console.log("data" + this.global.PersonData.applyProductList.data);
        this.navCtrl.setRoot(ProductlistPage,
          {


          });
        console.log("shop list organizationSyskey::" + JSON.stringify(this.global.outletSyskey));
      }
    }
  }
  gobackmain() {
    this.navCtrl.setRoot(WalletPage,
      {

      });

  }
  ionViewDidEnter() {
    this.backButtonExit();
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      this.gobackmain();

    });
  }
}
