import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, MenuController, LoadingController, ToastController, Events, AlertController, PopoverController, Platform, ViewController, App } from 'ionic-angular';
import { Http } from '@angular/http';
import { DatePipe } from '@angular/common';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';
import { UtilProvider } from '../../providers/util/util';
import { GlobalProvider } from '../../providers/global/global';
import { EventLoggerProvider } from '../../providers/eventlogger/eventlogger';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { Storage } from '@ionic/storage';
import { TopUpWalletConfrimPage } from '../top-up-wallet-confrim/top-up-wallet-confrim';
import { MobileLoginPage } from '../mobile-login/mobile-login';
import { WalletPage } from '../wallet/wallet';
@Component({
  selector: 'page-top-up-wallet',
  templateUrl: 'top-up-wallet.html',
  providers: [CreatePopoverPage]
})
export class TopUpWalletPage {
  textEng: any = ["Top up", "Account Number", "Amount","Invalid Amount","Reference","Topup","Account Balance",'Account Type'];
  textMyan: any = ["ငွေဖြည့်ရန်","အကောင့်နံပါတ်" ,"ငွေပမာဏ","Invalid Amount","မှတ်ချက်","ငွေဖြည့်ရန်","ဘဏ်စာရင်းလက်ကျန်ငွေ",'Account Type'];
  textData: string[] = [];
  _obj = {"amount":"","remark":"","syskey":"","userID":"","sessionID":"","fromAccount":"","receiverID":"","fromName":"","toName":""};
   
  public loading;
  userData: any = {};
  mobileData:any= {};
  ipaddress: string;
  keyboardfont: any;
  language: any;
  allaccount:any;
  errormsg1: any;
  b:any=[];
  amount:string;
  amountMsg: any = '';
  amountBal:any='';
  accountBal: any = '';
  status: any;
  fromaccMsg: any = '';
  btnflag: boolean;
  accountType:string='';
  mobileaccount:string;
constructor(
  public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public menuCtrl: MenuController, public modalCtrl: ModalController, public http: Http,
    public datePipe: DatePipe, public loadingCtrl: LoadingController,
    public toastCtrl: ToastController, public sqlite: SQLite,
    public changefont: Changefont, 
    public events: Events, public alertCtrl: AlertController,
    public popoverCtrl: PopoverController, public platform: Platform,
    public util: UtilProvider, public global: GlobalProvider,private eventlog:EventLoggerProvider,
    public viewCtrl: ViewController, private slimLoader: SlimLoadingBarService,
    public appCtrl: App, public createPopover: CreatePopoverPage,
) {
  // this.b=navParams.get("data");
  
 
  this.events.subscribe('changelanguage', lan => {
    this.changelanguage(lan);
  });

  this.storage.get('language').then((lan) => {
    this.changelanguage(lan);
  });

  this.storage.get('font').then((font) => {
    this.keyboardfont = font;
  });

  this.events.subscribe('changeFont', font => {
    this.keyboardfont = font;
  });
  this.storage.get('loginData').then((result) => {
    this.mobileData = result;
    console.log('Mobile User: '+JSON.stringify(this.mobileData));
    
  });
  this.storage.get('userData').then((result) => {
    this.userData = result;
    console.log('Wallet User: '+JSON.stringify(this.userData));
    this.storage.get('ipaddressApp').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressApp;
      }
      this.getAccountSummary();
    }); 
    this.events.subscribe('ipaddressApp', ip => {
      this.ipaddress = ip;
    });
  });
 
  }
  changelanguage(lan) {
    this.language = lan;

    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  goTopup(){
    this.btnflag = true;
    if (this.util.checkInputIsEmpty(this._obj.amount) || this.util.checkNumberOrLetter(this._obj.amount) ||
      this.util.checkAmountIsZero(this._obj.amount) || this.util.checkAmountIsLowerThanZero(this._obj.amount) ||
      this.util.checkStartZero(this._obj.amount) || this.util.checkPlusSign(this._obj.amount)) {
      this.errormsg1 = this.textData[3];
      this.btnflag = false;
    }
    else{
      this.loading = this.loadingCtrl.create({
        content: "Processing",
        dismissOnPageChange: true
        // duration: 3000
      });
      this.loading.present();
      let type = '16';
      let param = { userID: this.mobileData.userID, sessionID: this.mobileData.sessionID, type: type, merchantID: '' };
      this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.btnflag = false;
          this._obj.syskey= data.sKey;
          this._obj.userID=this.mobileData.userID,
          this._obj.sessionID=this.mobileData.sessionID;
          this._obj.fromAccount=this.mobileaccount;
          this._obj.receiverID=this.userData.userID;
          this._obj.fromName= this.mobileData.userName,
          this._obj.toName=this.userData.name;
          this.navCtrl.push(TopUpWalletConfrimPage, {
            data: this._obj, 
            type: type,
            otp: data.rKey,
            sKey: data.sKey,
            
          })   

          this.loading.dismiss();
         
        }
        else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else if (data.code == "0014") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else {
          this.toastInvalidLogin();
          this.loading.dismiss();
        }
      },
        error => {
          this.toastInvalidLogin();
          this.loading.dismiss();
        });
    }
  }
  toastInvalidLogin() {
    let toast = this.toastCtrl.create({
      message: "Connection Error.",
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });

    toast.present(toast);
  }
  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('loginData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(WalletPage, {
            });
            
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }
  getAccountSummary() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let parameter = { userID: this.mobileData.userID, sessionID: this.mobileData.sessionID };
    this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        let tempArray = [];
        
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;  
        }
        this.allaccount = result.dataList;
        console.log('Account Summary: '+JSON.stringify(this.allaccount));
        this.loading.dismiss();
      }
      else if (result.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: result.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(WalletPage, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert',
        })
        confirm.present();
        this.loading.dismiss();
        // this.slimLoader.complete();
      }
      else {
        this.status = 0;
        this.allaccount = [];
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
        // this.slimLoader.complete();
      }
    },
      error => {
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        // this.slimLoader.complete();
        this.loading.dismiss();
      });
  }
  changeAcc(s, account) {
    if (s == 1) {
      this.fromaccMsg = '';
    }
    for (let i = 0; i < this.allaccount.length; i++) {
      if (account == this.allaccount[i].depositAcc) {
        this.amountBal = this.allaccount[i].avlBal;
        this.accountBal = this.allaccount[i].avlBal + " " + this.allaccount[i].ccy;
        this.accountType=this.allaccount[i].accType;
      }
    }
  }
  

    }

