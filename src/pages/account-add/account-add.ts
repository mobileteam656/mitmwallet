import { Component } from '@angular/core';
import { Platform, NavController, ViewController, NavParams, MenuController, ModalController, LoadingController, PopoverController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';

import { GlobalProvider } from '../../providers/global/global';
import { DatePipe } from '@angular/common';
import { UtilProvider } from '../../providers/util/util';
import { AccountList } from '../account-list/account-list';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { App } from 'ionic-angular';
import { CreatePopoverPage } from '../create-popover/create-popover';
declare var window;

@Component({
  selector: 'page-account-add',
  templateUrl: 'account-add.html',
  providers: [CreatePopoverPage],
})

export class AccountAdd {
  textEng: any = ["Add Account", "Account Number", "Account Type", "Cancel", "Submit", "Account number is required!"];
  textMyan: any = ["အကောင့် ထည့်သွင်းခြင်း", "အကောင့်နံပါတ်", "အကောင့်အမျိုးအစား", "ပယ်ဖျက်မည်", "စာရင်းသွင်းမည်", "အကောင့်နံပါတ် ဖြည့်သွင်းပါ"];
  textData: string[] = [];

  accountList: any;
  tempData: any;

  _obj = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
    frominstituteCode: "", toInstitutionCode: "", reference: "", toAcc: "", amount: "",
    bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "",
    fromName: "", toName: "", "wType": "",
    field1: "", field2: ""
  }

  accNumber: string = '';
  accountType: string = '';
  accountTypeCaption: string = '';

  accountTypeList: any;

  errormsg1: string = '';

  font: string = '';
  ipaddress: string = '';
  //temp: any;

  popover: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public http: Http,
    public datePipe: DatePipe,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public sqlite: SQLite,
    public changefont: Changefont,
    public events: Events,
    public alertCtrl: AlertController,
    public popoverCtrl: PopoverController,
    public platform: Platform,
    public util: UtilProvider,
    public global: GlobalProvider,
    public viewCtrl: ViewController,
    private slimLoader: SlimLoadingBarService,
    public appCtrl: App,
    public createPopover: CreatePopoverPage, ) {

    this.storage.get('userData').then((userData) => {
      this._obj = userData;

      this.storage.get('ipaddressWallet').then((ip) => {
        if (ip !== undefined && ip != null && ip !== "") {
          this.ipaddress = ip;
          this.getAccType();
        } else {
          this.ipaddress = this.global.ipaddressWallet;
          this.getAccType();
        }
      });

      // if(userData != null || userData != ''){
      //   this.balance = userData.balance;
      // }
    });

    // let institution = this.util.setInstitution(); 
    // this._obj.frominstituteCode = institution.fromInstitutionCode;
    // this._obj.toInstitutionCode = institution.toInstitutionCode;
    // this._obj.toInstitutionName = institution.toInstitutionName;
    // this.institute.name=this._obj.toInstitutionName;
    // this.institute.code= this._obj.toInstitutionCode;

    // this.storage.get('phonenumber').then((phonenumber) => {
    //   this._obj.userID = phonenumber;
    // });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });

    // this.temp = this.navParams.get('data');
    // if (this.temp != null && this.temp != undefined && this.temp != '') {
    //   this._obj.beneficiaryID = this.temp[0];
    //   this._obj.toInstitutionCode = this.temp[1];
    //   this._obj.toName = this.temp[2];
    // }

    this.storage.get('username').then((username) => {
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    });

    this.events.subscribe('username', username => {
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    })
  }

  getAccType() {
    this.slimLoader.start(() => { });

    let userID1 = this._obj.userID;//this._obj.userID.replace(/\+/g, '');

    let parameter = { userID: userID1, sessionID: this._obj.sessionID };

    this.http.post(this.ipaddress + '/service001/getAccountType', parameter).map(res => res.json()).subscribe(result => {
      if (result != null && result.code != null && result.code == "0000") {
        let tempArray = [];

        if (!Array.isArray(result.accountTypeList)) {
          tempArray.push(result.accountTypeList);
          result.accountTypeList = tempArray;
        }

        this.accountTypeList = result.accountTypeList;
        this.tempData = this.accountTypeList;
        this.accountType = this.accountTypeList[0].value;
        this.accountTypeCaption = this.accountTypeList[0].caption;

        this.slimLoader.complete();
      } else {
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
        this.slimLoader.complete();
      }
    },
      error => {
        /* ionic App error
         ............001) url link worng, not found method (404)
         ........... 002) server not response (500)
         ............003) cross fillter not open (403)
         ............004) server stop (-1)
         ............005) app lost connection (0)
         */
        let code;

        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }

        let msg = "Can't connect right now. [" + code + "]";

        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
        this.slimLoader.complete();
      });
  }

  setAccType(accType) {
    for (let i = 0; i < this.accountTypeList.length; i++) {
      if (accType == this.accountTypeList[i].value) {
        this.accountType = this.accountTypeList[i].value;
        this.accountTypeCaption = this.accountTypeList[i].caption;
      }
    }
  }

  cancel() {
    this.accNumber = '';
    this.accountType = this.accountTypeList[0].value;
    this.errormsg1 = '';
  }

  addAccount() {
    if (this.accNumber == undefined || this.accNumber == null || this.accNumber == '') {
      this.errormsg1 = this.textData[5];
    } else {
      this.slimLoader.start(() => { });

      let userID1 = this._obj.userID;//this._obj.userID.replace(/\+/g, '');

      let parameter = {
        userID: userID1,
        sessionID: this._obj.sessionID,
        accNumber: this.accNumber,
        accountType: this.accountType
      };

      this.http.post(this.ipaddress + '/service001/addNewAccount', parameter).map(res => res.json()).subscribe(result => {
        if (result != null && result.code != null && result.code == "0000") {
          this.navCtrl.setRoot(AccountList);
          this.navCtrl.popToRoot();

          this.slimLoader.complete();
        } else {
          let toast = this.toastCtrl.create({
            message: result.desc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });

          toast.present(toast);
          this.slimLoader.complete();
        }
      },
        error => {
          /* ionic App error
           ............001) url link worng, not found method (404)
           ........... 002) server not response (500)
           ............003) cross fillter not open (403)
           ............004) server stop (-1)
           ............005) app lost connection (0)
           */
          let code;
          if (error.status == 404) {
            code = '001';
          }
          else if (error.status == 500) {
            code = '002';
          }
          else if (error.status == 403) {
            code = '003';
          }
          else if (error.status == -1) {
            code = '004';
          }
          else if (error.status == 0) {
            code = '005';
          }
          else if (error.status == 502) {
            code = '006';
          }
          else {
            code = '000';
          }

          let msg = "Can't connect right now. [" + code + "]";

          let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });

          toast.present(toast);
          this.slimLoader.complete();
        });
    }
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";

      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";

      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";

      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

}
