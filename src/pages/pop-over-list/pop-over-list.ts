import { Component } from '@angular/core';
import { Events, NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import 'rxjs/Rx';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'pop-over-list',
  templateUrl: 'pop-over-list.html'
})
export class PopOverListPage {

  version: string;
  popoverItemList = [
    { name: 'Language', key: 1 },
    { name: 'Font', key: 3 }, { name: 'Version1.0.16', key: 2 }
  ];

  textMyan: any = [
    { name: 'ဘာသာစကား', key: 1 },
    { name: 'ဖောင့်', key: 3 }, { name: 'ဗားရှင်း1.0.16', key: 2 }
  ];
  textEng: any = [
    { name: 'Language', key: 1 },
    { name: 'Font', key: 3 }, { name: 'Version1.0.16', key: 2 }
  ];
  selectedTitle: string;
  showFont: any;
  font: string;
  tempArray: any = [];

  constructor(
    public viewCtrl: ViewController,
    public global: GlobalProvider, public events: Events,
    public storage: Storage
  ) {
    this.selectedTitle = "";
    // this.version = this.global.version;
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      ////console.log('Your language is', font);
      this.changelanguage(font);
    });
  }

  changelanguage(lan) {
    if (lan != 'eng') {
      for (let j = 0; j < this.textMyan.length; j++) {
        this.popoverItemList[j].name = this.textMyan[j].name;
      }
    }
    else {
      this.font = '';
      for (let j = 0; j < this.textEng.length; j++) {
        this.popoverItemList[j].name = this.textEng[j].name;
      }
    }
  }

  setSelectedTitle(selectedItem) {
    this.selectedTitle = selectedItem;
    this.viewCtrl.dismiss(this.selectedTitle);
  }

  showArrow(item) {
    if (item.key != '2') {
      return true;
    }
  }

}
