import { Component } from '@angular/core';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-font-popover',
  templateUrl: 'font-popover.html'
})
export class FontPopoverPage {

  chooseFont: any;
  textMyan: any = ['ဖောင့် ပြင်ဆင်ရန်', 'ယူနီ', 'ဇော်ဂျီ'];
  textEng: any = ['Font Setting', 'Unicode', 'Zawgyi'];
  textData: any = [];
  font: any = '';

  constructor(
    public storage: Storage,
    public events: Events
  ) {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('font').then((font) => {
      this.chooseFont = font;
    });
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad FontPopoverPage');
  }

  changelanguage(lan) {
    if (lan == 'eng') {
      for (let j = 0; j < this.textEng.length; j++) {
        this.textData[j] = this.textEng[j];
      }
    } else {
      for (let j = 0; j < this.textMyan.length; j++) {
        this.textData[j] = this.textMyan[j];
      }
    }
  }

  mcqAnswer(s) {
    this.storage.set('font', s);
    this.events.publish('changeFont', s);
  }

}
