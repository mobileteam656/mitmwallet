import { Component } from '@angular/core';
import 'rxjs/add/operator/map';
import { Chat } from '../chat/chat';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Platform, NavController, NavParams, PopoverController, Events, ToastController, LoadingController, ActionSheetController,AlertController } from 'ionic-angular';
import { PopoverContactAddPage } from '../popover-contact-add/popover-contact-add';
import { App } from 'ionic-angular';
import { UtilProvider } from '../../providers/util/util';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { FunctProvider } from '../../providers/funct/funct';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import { PicsPage } from '../pics/pics';
import { BarcodeScanner, BarcodeScannerOptions } from "@ionic-native/barcode-scanner";
import { Payment } from '../payment/payment';
import { QRValuePage } from '../qr-value/qr-value';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { Contacts,ContactFieldType, ContactFindOptions } from '@ionic-native/contacts';
import { ContactListPage } from '../contact-list/contact-list';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
  providers: [CreatePopoverPage],
})
export class ContactPage {
  rootNavCtrl: NavController;
  cont: any = [];
  groupedContacts: any = [];
  userData: any;
  userDataChat: any;
  qrData: any;
  fromPage: any;
  isLoading: any;
  nores: any;
  UserList: any = [];
  ipaddress: string='';
  regdata: any;
  totalCount: any;
  state: boolean;
  popover: any;
  contactData: any = [];
  ph: any;
  all: any;  
  public loading;
  docData: any;
  from: any;
  profileImageLink: any;
  imageDetail_Contact: any;
  from_Contact: any = { fromName: '' };
  description_Contact: any = { description: '' };
  db: any;
  contact: boolean = false;
  name : any;
  phNo : any;

  options: BarcodeScannerOptions;
  photoName: any;
  loadingIcon: any;
  latitude: any;
  longitude: any;
  location: any;
  buttonText: any;
  scannedText: any;
  userdata: any;
  allContacts: any;

  contactObj :any ={
    code: '',
    desc: '',
    dataList: [
        {
            autokey: '',
            syskey: '',
            createdDate: '',
            modifiedDate: '',
            userID: '',
            sessionID: '',
            contactRef: '',
            phone: '',
            name: '',
            photo: '',
            type: '',
            status: '',
            t1: '',
            t2: '',
            t3: '',
            t4: '',
            t5: '',
            t6: '',
            t7: '',
            t8: '',
            t9: '',
            t10: '',
            t11: '',
            t12: '',
            t13: '',
            t14: '',
            t15: '',
            t16: '',
            t17: '',
            t18: '',
            t19: '',
            t20: '',
            n1: '',
            n2: '',
            n3: '',
            n4: '',
            n5: '',
            n6: '',
            n7: '',
            n8: '',
            n9: '',
            n10: '',
            n11: '',
            n12: '',
            n13: '',
            n14: '',
            n15: '',
            n16: '',
            n17: '',
            n18: '',
            n19: '',
            n20: '',
            channelkey: ''
        }]
      }
  a:any;
  textEng: any = ["Contacts"];
  textMyan: any = ["အဆက်အသွယ်များ"];
  textData: any = [];
  showFont: any = '';
  msgForUser: any;
  constructor(private geolocation: Geolocation, private _barcodeScanner: BarcodeScanner,
    private nativeGeocoder: NativeGeocoder, private file: File,
    private camera: Camera, public popoverCtrl: PopoverController,
    private sqlite: SQLite, public navCtrl: NavController,
    public loadingCtrl: LoadingController, public platform: Platform,
    public storage: Storage, public navParams: NavParams,
    public global: GlobalProvider, public http: Http,
    public events: Events, public toastCtrl: ToastController,
    public util: UtilProvider, public appCtrl: App,
    public createPopover: CreatePopoverPage,
    public alertCtrl: AlertController,
    public funct: FunctProvider, public actionSheetCtrl: ActionSheetController,
    public menuCtrl: MenuController, private contacts: Contacts,
    
  ) {
    this.storage.get('userData').then((data) => {
      this.userdata = data;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {

        }
        else {
          this.ipaddress = result;          
        }
      });
    });  
    this.storage.get('profileImage').then((profileImage) => {
      if (profileImage != undefined && profileImage != null && profileImage != '') {
        this.profileImageLink = profileImage;
      } else {
        this.profileImageLink = this.global.digitalMediaProfile;
      }
    });
    this.docData = this.navParams.get('data');
    //console.log("doc : " + JSON.stringify(this.docData))
    this.from = this.navParams.get('from');
    ///console.log("from : " + JSON.stringify(this.from))
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    }); 

    this.qrData = this.navParams.get('data');
    this.fromPage = this.navParams.get('fromPage');
    //console.log("from sss: " + JSON.stringify(this.fromPage))
    if (this.fromPage === 'QR') {
      this.presentPopoverContact(this.qrData);
    }
    this.storage.get('ip').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
        this.selectData();
        // this.getChatUser();
      }
      else {
        this.ipaddress = this.global.ipaddress;
        this.selectData();
        // this.getChatUser();
      }
    });
  }  

  syncContact(){
      this.navCtrl.push(ContactListPage);
  }   

  sync(){
   this.contacts.pickContact().then((data) => {
      if (data.phoneNumbers.length > 1) {
        this.contact = true;
        this.contactData = data.phoneNumbers;
        //this.phoneMsg = "";
        //console.log("aaa=" + this.contactData);
      
        for (let i = 0; i < this.contactData.length; i++) {
          // this.ph =this.contactData[i].value;
          
          this.contactData[i].value = this.contactData[i].value.toString().replace(/ +/g, "");
          if (this.contactData[i].value.indexOf("7") == 0 && this.contactData[i].value.length == "9") {
            this.contactData[i].value = '+959' + this.contactData[i].value;
          }
          else if (this.contactData[i].value.indexOf("9") == 0 && this.contactData[i].value.length == "9") {
            this.contactData[i].value = '+959' + this.contactData[i].value;
          }
          else if (this.contactData[i].value.indexOf("+") != 0 && this.contactData[i].value.indexOf("7") != 0 && this.contactData[i].value.indexOf("9") != 0 && (this.contactData[i].value.length == "8" || this.contactData[i].value.length == "9" || this.contactData[i].value.length == "7")) {
            this.contactData[i].value = '+959' + this.contactData[i].value;
          }
          else if (this.contactData[i].value.indexOf("09") == 0 && (this.contactData[i].value.length == 10 || this.contactData[i].value.length == 11 || this.contactData[i].value.length == 9)) {
            this.contactData[i].value = '+959' + this.contactData[i].value.substring(2);
          }
          else if (this.contactData[i].value.indexOf("959") == 0 && (this.contactData[i].value.length == 11 || this.contactData[i].value.length == 12 || this.contactData[i].value.length == 10)) {
            this.contactData[i].value = '+959' + this.contactData[i].value.substring(3);
          }
          console.log("contact lists= " + JSON.stringify(this.contactData[i].value));
        }
        //console.log("contact lists= " + JSON.stringify(this.ph));
      } else {
        this.contact = false;
       // this.topupData.phone = data.phoneNumbers[0].value;
      }
    },error=>{
      console.log(error);
    });
  }

  getChatUser() {
    this.storage.get('userData').then((userData) => {
      this.userData = userData;
      let param = {
        "userID": this.userData.userID,
        "sessionID": this.userData.sessionID
      }
      if (this.global.netWork == 'connected') {
        this.http.post(this.ipaddress + "/chatservice/getContact", param).map(res => res.json()).subscribe(result => {
          if (result.dataList == undefined || result.dataList == null || result.dataList == '') {
            this.deleteData();
            this.nores = 0;
          } else {
            let tempArray = [];
            if (!Array.isArray(result.dataList)) {
              tempArray.push(result.dataList);
              result.dataList = tempArray;
            }
            if (result.dataList.length > 0) {
              //  this.groupedContacts = result.data; 
              this.UserList = result.dataList;
              this.deleteData();
              //  this.getGroupData();
              this.nores = 1;
            }
          }
          this.isLoading = false;
        }, error => {
          //this.nores = 0;
          this.getError(error);
        });
      }
    });
  }

  getGroupData() {
    let group_to_values = this.groupedContacts.reduce(function (obj, item) {
      obj[item.t16] = obj[item.t16] || [];
      obj[item.t16].push(item);
      return obj;
    }, {});
    let groups = Object.keys(group_to_values).map(function (key) {
      let name;
      if (key == '')
        name = "User";
      else
        name = "Administrator";
      return { t16: name, data: group_to_values[key] };
    });

    this.UserList = groups;
    if (this.userData.t16 == '') {
      for (let i = 0; i < this.UserList.length; i++) {
        if (this.UserList[i].t16 == "Administrator") {
          let array = [];
          if (this.UserList[i].data.length > 0) {
            this.UserList[i].data[0].t2 = "Admin";
            array.push(this.UserList[i].data[0]);
          }
          this.UserList[i].data = array;
        }
      }
    }

  }


  viewChat(d) {
    this.from_Contact.fromName = 'ContactPage';
    this.description_Contact.description = '';
    this.imageDetail_Contact = null;
    this.navCtrl.push(Chat, {
      data: d,
      sKeyDB: this.userDataChat,
      from: this.from_Contact,
      description: this.description_Contact,
      imageDetail: this.imageDetail_Contact
    });
  }

  ionViewCanEnter() {
    this.menuCtrl.swipeEnable(true);
  } 

  ionViewWillEnter() {
    this.menuCtrl.swipeEnable(true);
    this.storage.get('userData').then((userData) => {
      if (userData != undefined && userData != null && userData != "") {
        this.userDataChat = userData;
        this.getChatUser();
      }
    });
    this.events.subscribe('userData', userData => {
      if (userData != undefined && userData != null && userData != "") {
        this.userDataChat = userData;
        //this.getChatUser();
      }
    });
  }

  doRefresh(refresher) {
    this.getChatUser();
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  presentPopoverContact(ev) {
    this.popover = this.popoverCtrl.create(PopoverContactAddPage, { data: ev }, { cssClass: 'contact-popover' });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != undefined && data != null && data != '') {
        this.contactData = data;
        this.goContactSave();
      }
    });
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: '',
      enableBackdropDismiss: false,
      message: "Please fill Phone no.",
      buttons: [{
        text: 'OK',
        handler: () => {
          ////console.log('OK clicked');
        }
      }]
    });

    alert.present();
  }

  goContactSave() {   
    this.isLoading = true;   
    let removePlusPhone = this.contactData.phone;
    let parameter = {
      userID: this.userDataChat.userID,
      sessionID: this.userDataChat.sessionID,
      phone: removePlusPhone,
      name: '',
      type: '1',
      t3: '0'
    };

    if (this.global.netWork == 'connected') {
      this.http.post(this.ipaddress + '/chatservice/addContact', parameter).map(res => res.json()).subscribe(result => {
        if (result != null && result.code != null && result.code == "0000") {
          let toast = this.toastCtrl.create({
            message: result.desc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.getChatUser();
          this.isLoading = false;
          let parameter = {
            userID: this.contactData.phone,
            sessionID: this.userDataChat.sessionID,
            phone: this.userDataChat.userID,
            name: '',
            type: '1',
            t3: '0'
          };   
        } else {
          //this.showAlert();
          let toast = this.toastCtrl.create({
            message: result.desc,
            // message: "Please fill Phone no.",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);        
          this.isLoading = false;
        }
      }, error => {
        let code;
        this.getError(error);
        this.isLoading = false;     
      }
      );
    }
  }
  insertData() {   
    let c;
    c = this.UserList.length;
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      for (let i = 0; i < c; i++) {
        db.executeSql("INSERT INTO Contact(name) VALUES (?)", [JSON.stringify(this.UserList[i])]).then((data) => {
         
        }, (error) => {
        
        });
      }
    }).catch(e => console.log(e));
  }

  selectData() { 
    this.UserList = [];
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      db.executeSql("SELECT * FROM Contact ", []).then((data) => {     
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {        
            this.UserList.push(JSON.parse(data.rows.item(i).name));
          }      
          this.nores = 1;
          this.isLoading = false;
        }
      }, (error) => {
       
      });
    }).catch(e => console.log(e));
  }

  deleteData() {
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      db.executeSql("DELETE FROM Contact", []).then((data) => {   
        this.insertData();
      }, (error) => {
   
      });
    }).catch(e => console.log(e));
  }

  goCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      targetWidth: 600,
      targetHeight: 600,
    }

    this.camera.getPicture(options).then((imagePath) => {
      this.loadingCCP();    
      let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    }, (error) => {
      this.getError(error);
    });
  }

  loadingCCP() {
    this.loadingIcon = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Processing...',
    });
    this.loadingIcon.present();
  }

  dismiss() {
    this.loadingIcon.dismissAll();
  }
  
  copyFileToLocalDir(namePath, currentName, newFileName) { 
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.docData = this.file.dataDirectory + newFileName;
      this.photoName = newFileName;   
      this.createDB();
    }, error => {
      alert('Error while storing file.' + JSON.stringify(error));
    });
  }

  createDB() {
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {    
      this.db = db;  
      this.db.executeSql('CREATE TABLE IF NOT EXISTS DOCPHOTO(id INTEGER PRIMARY KEY AUTOINCREMENT,PhotoPath TEXT,Name TEXT,Latitude TEXT,Longitude TEXT,Location TEXT,CreateDate TEXT,CreateTime TEXT)', {})
        .then((success) => {
          this.getLocation();         
        }, (error) => {
          
        })
        .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }

  getLocation() {
    let options = {
      timeout: 30000
    };
    this.geolocation.getCurrentPosition(options).then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;    
      this.nativeGeocoder.reverseGeocode(this.latitude, this.longitude)
        .then((result: NativeGeocoderReverseResult[]) => {
          this.location = result[0].locality + ", " + result[0].countryName;
          this.insertPhoto();        
        }, (err) => {
          this.dismiss();
          let toast = this.toastCtrl.create({
            message: "Please take photo again. Please turn on location access.",
            duration: 5000,
            position: 'bottom',       
            dismissOnPageChange: true,          
          });
          toast.present(toast);        
        });    
    }, (err) => {
      this.dismiss();
      let toast = this.toastCtrl.create({
        message: "Please take photo again. Please turn on location access.",
        duration: 5000,
        position: 'bottom',     
        dismissOnPageChange: true,     
      });
      toast.present(toast);   
    });
  }

  insertPhoto() {
    let date = new Date()
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    const weekNames = ["SUN", "MON", "TUE", "WED", "THUR", "FRI", "SAT"];
    const dayNames = [];
    let month = monthNames[date.getMonth()];
    let day = dayNames[date.getUTCDate()];
    let createData = month + "-" + date.getDate() + "-" + date.getFullYear();
    let wDay = weekNames[date.getDay()];
    let hoursMinutes = [date.getHours(), date.getMinutes()];
    let creteTime = wDay + " " + this.formatAMPM(hoursMinutes);
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      db.executeSql("INSERT INTO DOCPHOTO(PhotoPath,Name,Latitude ,Longitude ,Location,CreateDate, CreateTime) VALUES (?,?,?,?,?,?,?)", [this.docData, this.photoName, this.latitude, this.longitude, this.location, createData, creteTime]).then((data) => {    
        this.dismiss();
        this.navCtrl.push(PicsPage);
      }, (error) => {      
      });
    }).catch(e => console.log(e));
  }

  formatAMPM(date) {
    var hours = date[0];
    var minutes = date[1];
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  createFileName() {
    let d = new Date().getTime(); 
    let newFileName = "Doc-" + d + ".jpg";
    return newFileName;
  }

  getError(error) {   
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',  
      dismissOnPageChange: true,    
    });
    toast.present(toast);
    this.isLoading = false;  
  }

  scanQR(param) {
    this.buttonText = "Loading..";
    this.loading = true;
    this.options = {
      formats: "QR_CODE",
      "prompt": " ",
    }
    this._barcodeScanner.scan(this.options).then((barcodeData) => {
      if (barcodeData.cancelled) {     
        this.buttonText = "Scan";
        this.loading = false;
        return false;
      }
      this.scannedText = barcodeData.text;
      if (param == "contact") {
        try {
          let datacheck_scan = JSON.parse(this.scannedText);
          if (datacheck_scan[0] == "contacts") {
            this.navCtrl.push(ContactPage, {
              data: datacheck_scan,
              fromPage: "QR"
            });
          } else {
            this.toastInvalidQR();
          }
        } catch (e) {
          this.toastInvalidQR();
          
        }
      } else if (param == "payment") {
        try {
          let datacheck_scan = JSON.parse(this.scannedText);
          if (datacheck_scan[0] == "payments") {
            this.navCtrl.push(Payment, {
              data: datacheck_scan
            });
          } else {
            this.toastInvalidQR();
          }
        } catch (e) {
          this.toastInvalidQR();
          
        }
      } else if (param == "other") {
        if (this.isURL(this.scannedText)) {
          window.open(this.scannedText, '_system');
        } else {
          this.navCtrl.push(QRValuePage, {
            paramQRValue: this.scannedText
          });
        }
      }
    }, (err) => {      
    });
  }

  chooseQR() {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Add Contact',
          icon: !this.platform.is('ios') ? 'contact' : null,
          handler: () => {
            this.scanQR("contact");
          }
        },
        {
          text: 'Payment',
          icon: !this.platform.is('ios') ? 'cash' : null,
          handler: () => {
            this.scanQR("payment");
          }
        },
        {
          text: 'Other',
          icon: !this.platform.is('ios') ? 'link' : null,
          handler: () => {
            this.scanQR("other");
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {           
          }
        }
      ]
    });
    actionSheet.present();
  }

  toastInvalidQR() {
    let toast = this.toastCtrl.create({
      message: "Invalid QR Type.",
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    toast.present(toast);
  }

  isURL(str) {
    let pattern = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    if (!pattern.test(str)) {
      return false;
    } else {
      return true;
    }
  }

  showMap() {
    window.open('geo://?q=', '_system'); 
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }
}
