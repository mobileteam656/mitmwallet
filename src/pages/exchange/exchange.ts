import { Component } from '@angular/core';
import { NavController, MenuController, NavParams, LoadingController,ToastController, PopoverController, Events, Platform, App } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Login } from '../login/login';
import { Global } from '../global/global';
/**
 * Generated class for the ExchangePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-exchange',
  templateUrl: 'exchange.html',
  providers: [ChangelanguageProvider, Global,CreatePopoverPage]
})
export class ExchangePage {
  textEng: any = ['Foreign Exchange Rate', 'No result found!'];
  textMyan: any = ['နိုင်ငံခြားငွေလွဲနှုန်း', 'အချက်အလက်မရှိပါ'];
  showFont: any = [];
  ipaddress: string;
  status: any;
  ststus: any;
  userdata: any;
  font: string = '';
  public users: any = [];
  public temp: any = [];
  public w: any = [];
  public tempuser:any=[];
  ipaddressApp:string;
  constructor(private appCtrl: App,
    public navCtrl: NavController,

    public menu: MenuController,
    public navParams: NavParams,
    public http: Http,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    public slimLoader:SlimLoadingBarService,
    public popoverCtrl: PopoverController,
    public events: Events,
    public platform: Platform,
    public changeLanguage: ChangelanguageProvider,
    public globalpro: GlobalProvider,
    public global: Global,
    public all: AllserviceProvider,
    public toastCtrl:ToastController,
    public createPopover: CreatePopoverPage,
    public menuCtrl: MenuController) {

      this.ststus=this.navParams.get("ststus");
      this.storage.get('language').then((font) => {
        
        this.font = font;
        this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
        {
          
          this.showFont = data;
          
        });
      });
      this.events.subscribe('changelanguage', lan => {
        this.font = lan.data;
        this.changeLanguage.changelanguage(lan.data, this.textEng,this.textMyan).then(data =>
        {
          
          this.showFont = data;
          
        });
      });
  
  }

  showlist() {
    if (this.users != null || this.users != undefined) {

      for (let j = 0; j < this.users.length; j++) {
        this.temp = this.getcurrencyCode(this.users[j].ccy1);
        this.users[j].currencyCode = this.temp.code;
        this.users[j].flag = this.temp.flag;
      }
    }
  }
  getcurrencyCode(code) {
    let allcurencyCode = this.global.currencyCode;
    for (let i = 0; i < allcurencyCode.length; i++) {
      if (allcurencyCode[i].code == code) {
        this.w.code = allcurencyCode[i].symbolCode;
        this.w.flag = allcurencyCode[i].flag;
        break;
      }
    }
    return this.w;
  }

  ionViewDidLoad() {
    // this.ipaddressApp = this.globalpro.ipaddressApp;
    
    this.storage.get('userData').then((result) => {
      this.userdata = result;
      this.storage.get('ipaddressApp').then((ip) => {
        if (ip !== undefined && ip !== "" && ip != null) {
          this.ipaddress = ip;
        } else {
          this.ipaddress = this.globalpro.ipaddressApp;
        }
        this.getdata();
      }); 
      

    });
    // this.storage.get('ipaddressApp').then((ip) => {
    //   if (ip !== undefined && ip !== "" && ip != null) {
    //     this.ipaddress = ip;
    //   } else {
    //     this.ipaddress = this.globalpro.ipaddressApp;
    //   }
    //   this.getdata();
    // });
  }

  getdata(){
    this.slimLoader.start(() => {
      
    });
        /*this.loading = this.loadingCtrl.create({
          content: "Please wait...",
          dismissOnPageChange :true
          //   duration: 3000
        });
        this.loading.present();*/
   // let param={"userID":this.userdata.userID,"sessionID":this.userdata.sessionID};
    let param={userID:'',sessionID:''};
    
        this.http.post(this.ipaddress+'/service001/getForeignExchangeRates',param).map(res => res.json()).subscribe(response => {
            
            if (response.code == "0000") {
              this.users=response.data;
              this.status = 1;
              this.showlist();
              this.tempuser=this.users;

              this.slimLoader.complete();
            } else {
              this.status = 0;
              let toast = this.toastCtrl.create({
                message:response.desc,
                duration: 3000,
                position: 'bottom',
                //  showCloseButton: true,
                dismissOnPageChange: true,
                // closeButtonText: 'OK'
              });
              toast.present(toast);
              this.slimLoader.complete();
            }
          },
            error => {
              this.status = 0;
              let toast = this.toastCtrl.create({
                message:this.all.getErrorMessage(error),
                duration: 3000,
                position: 'bottom',
                //  showCloseButton: true,
                dismissOnPageChange: true,
                // closeButtonText: 'OK'
              });
              toast.present(toast);
              this.slimLoader.complete();
          });

        //work process
      }

      getItems(ev) {
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
          this.users = this.tempuser.filter((users) => {
            return (users.ccy1.toLowerCase().indexOf(val.toLowerCase()) > -1);
          })
        }
        else{
          this.users = this.tempuser.filter((tempuser) => {
            return (tempuser.ccy1.toLowerCase().indexOf(val.toLowerCase()) > -1);
          })
        }
      }

  ionViewDidLeave(){
    
    this.slimLoader.reset();
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

}
