import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { CallNumber } from '@ionic-native/call-number';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, MenuController, NavController, NavParams, Platform, PopoverController, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
// import { DefaultPopOver } from '../default-pop-over/default-pop-over';
// import { LanguagePopOver } from '../language-pop-over/language-pop-over';
// import { IpchangePage } from '../ipchange/ipchange';
import { GlobalProvider } from '../../providers/global/global';
import { AgentTransferPage } from '../agent-transfer/agent-transfer';
// import { PopoverPage } from '../popover-page/popover-page';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { LocationDetail } from '../location-detail/location-detail';
import { NearByPage } from '../near-by/near-by';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { UtilProvider } from '../../providers/util/util';
declare var window;


@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
  providers: [ChangelanguageProvider,CreatePopoverPage]
})

export class LocationPage {
  qrValue: any = [];
  locationdata: string = '';
  locationstauts:boolean=false;
  mapLocation: any = [];
  location: any;
  alllocation: any=[];
  originalallocation:any=[];
  atmlocation: any = [];
  branchlocation: any = [];
  hasData: boolean = false;
  allocationMessage: string;
  status: any;
  flag=true;
  public loading;
  ipaddress: string;
  popover: any;
  callnumber:string;
  font: string = '';
  showFont: any = [];
  filterLocation:any=[];
  textEng: any = ["Location", "Branch", "No Result Found","Cash In","Cash Out","Agent"];
  textMyan: any = ["တည်နေရာ", "ဘဏ်ခွဲများ", "အချက်အလက်မရှိပါ","ငွေသွင်း", "ငွေထုတ်","ကိုယ်စားလှယ်"];
  ststus:any;
  userdata:any;
  locationType: any;
  originalLocation=[];
  allList = [];
  tempList = [];
  temp:any=[];
  _obj = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "",
    frominstituteCode: "", toInstitutionCode: "", toAcc: "", accountNo:"",balance: "", bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "", field1: "", field2: ""
  };
  
  constructor(public barcodeScanner: BarcodeScanner,public toastCtrl: ToastController,public navCtrl: NavController, public menu: MenuController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public storage: Storage,
    private slimLoader: SlimLoadingBarService,public util: UtilProvider,public popoverCtrl: PopoverController,public alertController: AlertController, public events: Events, public platform: Platform, public changeLanguage: ChangelanguageProvider,
    public global: GlobalProvider,private callNumber: CallNumber, public all: AllserviceProvider,public createPopover: CreatePopoverPage, public menuCtrl: MenuController) {
 
      this.storage.get('userData').then((userData) => {
        this._obj = userData;
        console.log('Location Data: '+JSON.stringify(this._obj));
        this.storage.get('ipaddress').then((ip) => {
          if (ip !== undefined && ip != null && ip !== "") {
            this.ipaddress = ip;
            console.log('IP is: '+this.ipaddress);
           this.getLocation(); 
          } else {
            this.ipaddress = this.global.ipaddress;
            this.getLocation();
          }
        });
       
      });
     
   
    }
    
 

  ionViewDidEnter(){
    this.locationType = this.navParams.get("location");
    console.log("loc param==" + JSON.stringify(this.location));
    
    this.ststus=this.navParams.get("ststus");
    console.log("loc status==" + JSON.stringify(this.ststus));
  }

  getLocation() {
    
    this.slimLoader.start(() => {
    });
    let param = {
      "userID":this._obj.userID,
      "sessionID":this._obj.sessionID
      };

      console.log("request list =" + JSON.stringify(param));
    this.http.post(this.ipaddress + '/service002/getLocation', param).map(res => res.json()).subscribe(data => {
      
      if (data.code == "0012") {
        this.util.SessiontimeoutAlert(data.desc);
      }
      else if (data.code == "0000") {
        this.tempList=[];
        this.allList=[];
        let tempArray = [];
        if (!Array.isArray(data.data)) {
          tempArray.push(data.data);
          data.data = tempArray;
          //this.alllocation.data = data.data;
        }
        
        for(let i=0;i<data.data.length ;i++){
          for(let j=0;j<data.data[i].dataList.length;j++){
            if(data.data[i].locationType=='Agent' || data.data[i].locationType=='A'){
            if(data.data[i].dataList[j]!=undefined && data.data[i].dataList[j]!=null && data.data[i].dataList[j].length!=0){
              this.allList.push(data.data[i].dataList[j]);
            }
          }
        }
        }

        this.tempList=this.allList;

        this.alllocation.data = data.data;
        // this.originalallocation.data=data.data;
       
        // console.log("location==" , JSON.stringify(this.alllocation.data));
        this.status = 1;
        if (this.alllocation.data.length > 0) {
          for(let i=0;i<this.alllocation.data.length ;i++){
            let tempListArray = [];
            if (!Array.isArray(this.alllocation.data[i].dataList)) {
              tempListArray.push(this.alllocation.data[i].dataList);
              this.alllocation.data[i].dataList = tempListArray;
            }
          }

          for (let i = 0; i < this.alllocation.data.length; i++) {
            for (let j = 0; j < this.alllocation.data[i].dataList.length; j++) {
              this.mapLocation.push({ "title": this.alllocation.data[i].dataList[j].name, "address": this.alllocation.data[i].dataList[j].address, "latitude": this.alllocation.data[i].dataList[j].latitude, "longitude": this.alllocation.data[i].dataList[j].longitude, "type":this.alllocation.data[i].dataList[j].locationType});
            }
          }          
          //console.log("location==" , JSON.stringify(this.mapLocation));

          this.slimLoader.complete();
          this.hasData = true;
          //this.location=this.alllocation.data[3].locationType;         
         //console.log("location type==" , JSON.stringify(this.location));
        
        } else {
          this.slimLoader.complete();
          this.status = 0;
        }
      }
      //  else if (data.code == "0012") {
      //   this.util.SessiontimeoutAlert(data.desc);
      // }
      // else {
      //   this.status = 0;
      //   this.all.showAlert('Warning!', data.desc);
      //   this.slimLoader.complete();
      // }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.slimLoader.complete();
      });
  }

  ionViewDidLoad() {
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
        console.log('IP is: '+this.ipaddress);
       this.getLocation(); 
      } else {
        this.ipaddress = this.global.ipaddress;
        this.getLocation();
      }
    });
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  getloaction(data) {
    var parm = {
      lat: data.latitude,
      lng: data.longitude,
      name: data.name,
      phone: data.phone1,
      address: data.address
    }
    this.navCtrl.push(LocationDetail, { data: parm ,show:this.flag})
  }

  goAgent(data) { 
    //console.log('Post Data'+JSON.stringify(data));   
    //this.navCtrl.push(CashoutTransferPage, { phone: data.phone1,name:data.name});    
    this.navCtrl.push(AgentTransferPage, {
       phone: data.phone1,
       data: ''
     });
  }
  goScan(){
    this.barcodeScanner.scan().then((barcodeData) => {
      if (barcodeData.cancelled) {
          return false;        
      } 
      try {      
          this.qrValue = this.all.getDecryptText(this.all.iv, this.all.salt, this.all.dm, barcodeData.text);
          this.qrValue = JSON.parse(this.qrValue);           
          console.log("QR Value=" +this.qrValue);
          this.navCtrl.push(AgentTransferPage, {
            data: this.qrValue,          
          });
      } catch (e) {
        this.toastInvalidQR();
      }
    }, (err) => {
    });
  }
  toastInvalidQR() {
    let toast = this.toastCtrl.create({
      message: "Invalid QR Type.",
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
  
    toast.present(toast);
  }
  ionViewDidLeave() {
    this.slimLoader.reset();    
  }
  setLocation(type){
    this.location=type;
  }
  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }
  nearby(){
    this.navCtrl.push(NearByPage);
  } 
  getcallnumber(phone){
    this.callnumber=phone;
    let alert = this.alertController.create({
      message: 'Are you sure want to call?',
      buttons: [
        {
          text: 'No',
          role: 'No',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.callNumber.callNumber(this.callnumber, true)
            .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
          }
        }
      ]
    });
    alert.present();
  }
  searchUser(ev) {
   var val = ev.target.value;

    if (val == undefined || val.trim() == '') {
      val = '';
    } else { 
      val = val.trim();
    }
    console.log('DataList'+JSON.stringify(this.allList));
    if (val && val.trim() != '') {
      this.tempList=this.allList.filter(
        (fl) => {
          return (fl.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        }
      );
    }
    else{
      this.tempList=this.allList;
    }
  }
}



