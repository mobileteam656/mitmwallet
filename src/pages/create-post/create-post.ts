import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, AlertController, Events, ToastController, LoadingController, Platform } from 'ionic-angular';
import { TagChoosePage } from "../tag-choose/tag-choose";
import { SelectAlbumPage } from '../select-album/select-album';
import { IonPullUpFooterState } from 'ionic-pullup';
import { ImagePicker } from '@ionic-native/image-picker';
import { DatePipe } from '@angular/common';
import { EditImagesPage } from "../edit-images/edit-images";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { Http } from "@angular/http";
import { FunctProvider } from "../../providers/funct/funct";
//import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Storage } from "@ionic/storage";
import { Geolocation } from '@ionic-native/geolocation';
import { GlobalProvider } from '../../providers/global/global';
import { Changefont } from '../changefont/changeFont';
import { UtilProvider } from '../../providers/util/util';
import { CreatePopoverPage } from '../create-popover/create-popover';

@Component({
  selector: 'page-create-post',
  templateUrl: 'create-post.html',
  providers: [CreatePopoverPage]
})
export class CreatePostPage {

  textEng: any = ["Create Post", "What's on your mind?", "Share"];
  textMyan: any = ["Create Post", "What's on your mind?", "Share"];
  textData: string[] = [];
  showFont: any;
  keyboardfont: any;
  language: any;

  ipaddress: any;

  @ViewChild('myInput') myInput: ElementRef;

  photos: any = [];
  photosUpload: any = [];
  Privacies: any = [
    { syskey: 0, name: 'Public', icon: 'md-globe', des: 'Anyone on or off Digital Connect' },
    { syskey: 1, name: 'Friends', icon: 'md-people', des: 'Your friends on Digital Connect' },
    // /*{syskey: 2, name: 'Specific friends' , icon: 'md-contact', des: 'Only show to some friends'},*/
    { syskey: 2, name: 'Only me', icon: 'md-lock', des: 'Only me' },

  ];
  isLoading: any;
  footerState: IonPullUpFooterState;
  registerData: any;
  loading: any;
  location: any;
  postText: any;
  privacy: any;
  privacyName: any = 'Public';
  privacyIcon: any = 'md-globe';
  profile: any;
  photo: string = '';
  profileImage: any;
  userName: any;
  postSyskey: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    private imagePicker: ImagePicker,
    private file: File,
    public datePipe: DatePipe,
    private camera: Camera,
    public http: Http,
    public funct: FunctProvider,
    //private transfer: Transfer,
    private transfer: FileTransfer,
    public toastCtrl: ToastController,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    private geolocation: Geolocation,
    public global: GlobalProvider,
    public events: Events,
    public changefont1: Changefont,
    public platform: Platform,
    public util: UtilProvider,
    public changefont: Changefont,
    public createPopover: CreatePopoverPage
  ) {
    this.showFont = 'uni';
    this.ipaddress = this.global.ipaddressCMS;

    this.registerData = this.navParams.get("data");
    this.userName = this.registerData.t3;

    this.postSyskey = this.navParams.get("postSyskey");

    if (this.postSyskey != '0') {
      this.getPost(this.postSyskey);
    } else {
      this.privacyIcon = this.Privacies[1].icon;
      this.privacyName = this.Privacies[1].name;
      this.privacy = this.Privacies[1].syskey;
    }

    this.storage.get('localPhotoAndLink').then((photo) => {
      ////console.log('localPhotoAndLink in app.component constructor get:', photo);
      this.photo = photo;
      //this.checkLocalPhotoExists(this.imglnk, this.photo);

      if (this.photo != '' && this.photo != undefined && this.photo != null && this.photo.includes("/")) {
        this.profile = this.photo;
        ////console.log('file cache directory and photo(e.g.xxx.jpg):', this.profile);
      } else if (this.photo == 'Unknown.png' || this.photo == 'user-icon.png' || this.photo == '') {
        this.profile = "assets/images/user-icon.png";
        ////console.log('Unknown.png or user-icon.png or blank string:', this.profile);
      } else {
        this.storage.get('profileImage').then((profileImage) => {
          if (profileImage != undefined && profileImage != null && profileImage != '') {
            this.profileImage = profileImage;
          } else {
            this.profileImage = this.global.digitalMediaProfile;
          }
          this.profile = this.profileImage + this.photo;
          this.download(this.profile, this.photo);
          ////console.log('server link and photo(e.g.xxx.jpg):', this.profile);
        });
      }
    });
    this.events.subscribe('localPhotoAndLink', photo => {
      ////console.log('localPhotoAndLink in app.component constructor subscribe:', photo);
      this.photo = photo;
      //this.checkLocalPhotoExists(this.imglnk, this.photo);

      if (this.photo != '' && this.photo != undefined && this.photo != null && this.photo.includes("/")) {
        this.profile = this.photo;
        ////console.log('file cache directory and photo(e.g.xxx.jpg):', this.profile);
      } else if (this.photo == 'Unknown.png' || this.photo == 'user-icon.png' || this.photo == '') {
        this.profile = "assets/images/user-icon.png";
        ////console.log('Unknown.png or user-icon.png or blank string:', this.profile);
      } else {
        this.storage.get('profileImage').then((profileImage) => {
          if (profileImage != undefined && profileImage != null && profileImage != '') {
            this.profileImage = profileImage;
          } else {
            this.profileImage = this.global.digitalMediaProfile;
          }
          this.profile = this.profileImage + this.photo;
          this.download(this.profile, this.photo);
          ////console.log('server link and photo(e.g.xxx.jpg):', this.profile);
        });
      }
    });

    this.footerState = IonPullUpFooterState.Collapsed;
    ////console.log("this.footerState income=" + this.footerState);

    this.currentLocation();

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('font').then((font) => {
      this.keyboardfont = font;

      if (font == "zg" && this.language != "eng") {
        this.textData[1] = this.changefont.UnitoZg(this.textMyan[1]);
      } else if (font == "uni" && this.language != "eng") {
        this.textData[1] = this.textMyan[1];
      }
    });

    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;

      if (font == "zg" && this.language != "eng") {
        this.textData[1] = this.changefont.UnitoZg(this.textMyan[1]);
      } else if (font == "uni" && this.language != "eng") {
        this.textData[1] = this.textMyan[1];
      }
    });
  }

  changelanguage(lan) {
    this.language = lan;

    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }

      if (this.keyboardfont == 'zg') {
        this.textData[1] = this.changefont.UnitoZg(this.textMyan[1]);
      }
    }
  }

  download(photoAndServerLink, photo) {
   // const fileTransfer: TransferObject = this.transfer.create();
   const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(photoAndServerLink, this.file.cacheDirectory + photo).then((entry) => {
      ////console.log('photo download success: ' + entry.toURL());
      let abc = entry.toURL();
      this.storage.set('localPhotoAndLink', abc);
      this.events.publish('localPhotoAndLink', abc);
    }, (error) => {
      ////console.log('photo download error: ' + JSON.stringify(error));
    });
  }

  footerExpanded() {
    ////console.log('Footer expanded!' + this.footerState);
  }

  footerCollapsed() {
    ////console.log('Footer collapsed!' + this.footerState);
  }

  toggleFooter() {
    this.footerState = this.footerState == IonPullUpFooterState.Collapsed ? IonPullUpFooterState.Expanded : IonPullUpFooterState.Collapsed;
    ////console.log("this.footerState=" + this.footerState);
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad CreatePostPage');
  }

  ionViewDidEnter() {
   // //console.log('ionViewDidEnter CreatePostPage');
    /*let allphotos = [];
    allphotos = this.navParams.get("allphoto");
    //console.log('ionViewDidEnter allphotos' + JSON.stringify(allphotos));
    if(allphotos != undefined || allphotos != null || allphotos != []){
      this.photos = allphotos;
    }*/

    // this.privacy = this.navParams.get("privacy");
    ////console.log('ionViewDidEnter photos' + JSON.stringify(this.photos));
    ////console.log('ionViewDidEnter privacy' + this.privacy);

    // for (let i = 0; i < this.Privacies.length; i++) {
    //   if (this.privacy == this.Privacies[i].syskey) {
    //     this.privacyIcon = this.Privacies[i].icon;
    //     this.privacyName = this.Privacies[i].name;
    //   }
    // }
    ////console.log('iVDEnter privacyName=' + this.privacyName);
    ////console.log('iVDEnter privacyIcon=' + this.privacyIcon);
  }

  goTagChoose() {
    this.navCtrl.push(TagChoosePage, {
      valueTagChoose: this.privacy
    });
  }

  goAlbumChoose() {
    let toast = this.toastCtrl.create({
      message: "This feature will be included in next version.",
      duration: 5000,
      position: 'bottom',
      //showCloseButton: true,
      dismissOnPageChange: true,
      //closeButtonText: 'OK'
    });
    toast.present(toast);
    //this.navCtrl.push(SelectAlbumPage);
  }

  goMenu() {

  }
  imageCount: any;
  openImagePicker() {
    if (this.photos.length > 0) {
      this.imageCount = 15 - this.photos.length;
    } else {
      this.imageCount = 15;
    }
    ////console.log("before this.footerState in gallery=" + this.footerState);

    /*if(IonPullUpFooterState.Collapsed){
      this.footerState =  IonPullUpFooterState.Expanded;
    }else{
      this.footerState =  IonPullUpFooterState.Collapsed;
    }
    //console.log("this.footerState in gallery="+ this.footerState);*/

    if (this.photos.length >= 15) {
      let confirm = this.alertCtrl.create({
        message: 'No more than 15 images!',
        buttons: [
          {
            text: 'Yes',
            handler: () => {
              ////console.log('Yes clicked');
            }
          }
        ]
      });
      confirm.present();
    }
    else {
      let options = {
        maximumImagesCount: this.imageCount
      }

      this.imagePicker.getPictures(options)
        .then((results) => {
          ////console.log("results image picker=" + JSON.stringify(results))

          for (let i = 0; i < results.length; i++) {
            ////console.log('Image URI: ' + results[i]);
            this.updateUserImage(results[i])
          }

        }, (err) => { console.log(err) });
    }
  }

  updateUserImage(imageData) {
    //   this.isLoading = true;
    let currentName = '';
    let correctPath = '';
    if (imageData.indexOf('file:///') > -1) {
      if (imageData.indexOf('?') > -1) {
        currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
        correctPath = imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      }
      else {
        currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
        correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
      }
    }
    else {
      if (imageData.indexOf('?') > -1) {
        currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
        correctPath = 'file:///' + imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      }
      else {
        currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
        correctPath = 'file:///' + imageData.substr(0, imageData.lastIndexOf('/') + 1);
      }
    }

    ////console.log("currentName == " + currentName);
    ////console.log("currentPath == " + correctPath);
    this.copyFileToLocalDir(correctPath, currentName, this.createFileName(currentName)); // this.createImageName()
  }

  private copyFileToLocalDir(namePath, currentName, newFileName) {
    ////console.log("namePaht == " + namePath + "   //// currentNmae == " + currentName + "   ////  newFileName == " + newFileName);
    ////console.log("this.file.datadirectory == " + this.file.dataDirectory);

    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.photos.push({
        name: this.file.dataDirectory + newFileName,
        desc: ''
      });
      ////console.log("Select photos=" + JSON.stringify(this.photos));
    }, error => {
      alert('Error while storing file.' + JSON.stringify(error));
    });
  }

  private createFileName(currentName) {
    let splitName = currentName.split(".")
    let imgType = splitName[1].trim();
    let d = new Date();
    let date = this.datePipe.transform(d, 'yyyyMMdd');
    let t = d.getTime();

    let newFileName = "IMG" + "_" + date + "_" + t + "." + imgType; //  Update All_20160812_1470985197167.docx

    return newFileName;
  }


  public pathForFile(img) {
    if (img === null) {
      return '';
    } else {
      return this.file.dataDirectory + img;
    }
  }

  viewImage(photos) {
    ////console.log("viewImages=" + JSON.stringify(photos))
    this.navCtrl.push(EditImagesPage, { photos: photos });
  }


  goCamera() {
    if (this.photos.length >= 15) {
      let confirm = this.alertCtrl.create({
        message: 'No more than 15 images!',
        buttons: [
          {
            text: 'Yes',
            handler: () => {
              ////console.log('Yes clicked');
            }
          }
        ]
      });
      confirm.present();
    }
    else {
      ////console.log("before this.footerState in camera=" + this.footerState);

      if (IonPullUpFooterState.Collapsed) {
        this.footerState = IonPullUpFooterState.Expanded;
      } else {
        this.footerState = IonPullUpFooterState.Collapsed;
      }
      ////console.log("this.footerState in camera=" + this.footerState);

      ////console.log('Camera');
      const options: CameraOptions = {
        quality: 100,
        targetWidth: 600,
        targetHeight: 600,
        sourceType: this.camera.PictureSourceType.CAMERA,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        correctOrientation: true
      }

      this.camera.getPicture(options).then((imageData) => {
        ////console.log('Camera success');
        //this.updateUsreImage(imageData);
        this.copyFile(imageData);
      }, (error) => {
        //console.error("Unable to open database", error);
      });
    }
  }

  copyFile(result) {
    let dmyhmssk = this.util.getImageName();
    let tmpAry = result.split('/').pop().split(".");
    let newfileName = tmpAry[0] + "_" + dmyhmssk + "." + tmpAry[1];
    let sourceDirectory = result.substring(0, result.lastIndexOf('/') + 1);
    let sourceFileName = result.substring(result.lastIndexOf('/') + 1, result.length);

    this.copyFileToLocalDir(sourceDirectory, sourceFileName, this.createFileName(newfileName));
  }

  // updateUsreImage(imageData) {
  //   //   this.isLoading = true;
  //   let currentName = '';
  //   let correctPath = '';
  //   if (imageData.indexOf('file:///') > -1) {
  //     if (imageData.indexOf('?') > -1) {
  //       currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
  //       correctPath = imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
  //     }
  //     else {
  //       currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
  //       correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
  //     }
  //   }
  //   else {
  //     if (imageData.indexOf('?') > -1) {
  //       currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
  //       correctPath = 'file:///' + imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
  //     }
  //     else {
  //       currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
  //       correctPath = 'file:///' + imageData.substr(0, imageData.lastIndexOf('/') + 1);
  //     }
  //   }


  //   //console.log("currentName == " + currentName);
  //   //console.log("currentPath == " + correctPath);
  //   this.copyFileToLocalDir(correctPath, currentName, this.createFileName(currentName));
  // }

  currentLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coordthis.longitude
      this.location = resp.coords.latitude + "/" + resp.coords.longitude;
     // //console.log('this.location=', this.location);
    }).catch((error) => {
      ////console.log('Error getting location', error);
    });
  }

  savePost() {
    this.postText = this.postText.trim();

    if (this.photos.length != 0 || this.postText != '') {
      this.loading = this.loadingCtrl.create({
        content: 'Posting...',
        dismissOnPageChange: true
      });

      this.loading.present();
      // let photoList = [];

      // photoList = this.photos;

      // for (let iIndex = 0; iIndex < this.photos.length; iIndex++) {
      //   if (this.photos[iIndex].url != undefined && this.photos[iIndex].url != null && this.photos[iIndex].url != '') {
      //     photoList.splice(iIndex, 1);
      //     iIndex--;
      //   }
      // }

      //this.isLoading = true;
      let params = {
        'syskey': this.postSyskey,
        'n5': this.registerData.syskey,//this.registerData.syskey,
        'userId': this.registerData.t1,//this.registerData.t1,
        'userName': this.registerData.t3,//this.registerData.t3,
        't2': this.postText,
        't4': this.location,
        't9': this.privacy, //0,1,2 public or friends only or only me
        'n4': 8,// 'user post',
        'n7': 5,// 'publish'
        'uploadlist': this.photos,  //[{name,desc}] or [{image:,caption:}]
        'domain': this.global.domainCMS
      }

      ////console.log("request savePost=" + this.ipaddress + '/servicePost/savePost?imgurl=' + this.global.digitalMedia + ", params=" + JSON.stringify(params));
      this.http.post(this.ipaddress + '/servicePost/savePost?imgurl=' + this.global.digitalMedia, params).map(res => res.json()).subscribe(result => {
        ////console.log("response savePost=" + JSON.stringify(result));

        if (result.state) {
          this.loading.dismissAll();
          this.navCtrl.pop();
          //this.presentToast('Your post was successful.')
        }
        else {
          this.loading.dismissAll();
          this.presentToast('Posting failed.')
        }

        //this.isLoading = false;
      }, error => {
        this.loading.dismissAll();
        ////console.log("signin error=" + error.status);
        this.isLoading = false;
        this.getError(error);
      });
    } else {
      this.presentToast('Your post is empty.')
    }
  }

  /*uploadImge*/
  createPost() {
    if (this.photos.length > 0) {
      this.loading = this.loadingCtrl.create({
        content: 'Uploading...',
        dismissOnPageChange: true
      });

      this.loading.present();
      this.removeUploadPhoto();
    } else {
      this.savePost();
    }
  }

  removeUploadPhoto() {
    for (let iIndex = 0; iIndex < this.photos.length; iIndex++) {
      if (this.photos[iIndex].url != undefined && this.photos[iIndex].url != null && this.photos[iIndex].url != '') {
      } else {
        this.photosUpload.push(this.photos[iIndex]);
      }

      if (iIndex == (this.photos.length - 1)) {
        if (this.photosUpload.length != 0)
          this.uploadImage(0);
        else
          this.savePost();
      }
    }
  }

  uploadImage(count) {
    if (count < this.photosUpload.length) {
      // if (this.photosUpload[count].url != undefined && this.photosUpload[count].url != null && this.photosUpload[count].url != '') {
      //   count++;
      //   if (count == this.photosUpload.length) {
      //     this.loading.dismissAll();
      //     this.savePost();
      //   } else {
      //     this.uploadImage(count);
      //   }
      // } else {
      let fileName = this.photosUpload[count].name.split("/").pop();
      let url = this.ipaddress + '/servicePost/fileupload?f=upload&fn=' + fileName + '&imgUrl=' + this.global.digitalMedia;
      var options = {
        fileKey: "file",
        fileName: fileName,
        chunkedMode: false,
        mimeType: "multipart/form-data",
      };

      //const fileTransfer: TransferObject = this.transfer.create();
      const fileTransfer: FileTransferObject = this.transfer.create();
      fileTransfer.upload(this.photosUpload[count].name, url, options).then((data) => {
        if (data.responseCode === 200) {
          let noOfPhoto = count + 1;
          if (noOfPhoto != this.photosUpload.length) {
            let resultResponse = JSON.parse(data.response);
            this.photosUpload[count].name = resultResponse.url;
            count++;
            this.uploadImage(count);
          } else if (noOfPhoto == this.photosUpload.length) {
            let resultResponse = JSON.parse(data.response);
            this.photosUpload[count].name = resultResponse.url;
            this.loading.dismissAll();
            this.savePost();
          }
        }
      }, (err) => {
        ////console.log("Error upload=" + JSON.stringify(err));
        this.presentToast('Oops! Something is wrong. Please check connection.');
        this.loading.dismissAll();
      });
      // }
    }
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 5000,
      dismissOnPageChange: true,
      position: 'bottom'
    });
    toast.present();
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //showCloseButton: true,
      dismissOnPageChange: true,
      //closeButtonText: 'OK'
    });
    toast.present(toast);
    this.isLoading = false;
    ////console.log("Oops!");
  }

  getPost(sysKeyParam) {
    this.loading = this.loadingCtrl.create({
      content: 'Loading...',
      dismissOnPageChange: true
    });

    this.loading.present();

    this.http.get(this.ipaddress + '/serviceCMS/readBySyskey?key=' + sysKeyParam).map(res => res.json()).subscribe(result => {
      ////console.log("response getOneContent=" + JSON.stringify(result));

      if (result != undefined && result != null && result != '') {
        this.postText = result.t2;

        for (let i = 0; i < this.Privacies.length; i++) {
          if (result.t9 == this.Privacies[i].syskey) {
            this.privacyIcon = this.Privacies[i].icon;
            this.privacyName = this.Privacies[i].name;
            this.privacy = this.Privacies[i].syskey;
          }
        }

        if (result.uploadlist.length != 0) {
          let tempArray = [];
          if (!Array.isArray(result.uploadlist)) {
            tempArray.push(result.uploadlist);
            result.uploadlist = tempArray;
          }

          for (let iIndex = 0; iIndex < result.uploadlist.length; iIndex++) {
            result.uploadlist[iIndex].name = result.uploadlist[iIndex].url;
          }

          this.photos = result.uploadlist;
          ////console.log("this.photos.push(result.uploadlist);" + JSON.stringify(this.photos));
          this.loading.dismissAll();
        } else {
          this.loading.dismissAll();
        }
      } else {
        this.loading.dismissAll();

        let toast = this.toastCtrl.create({
          message: "Selected failed!",
          duration: 5000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
      }

      //this.isLoading = false;
    }, error => {
      this.loading.dismissAll();

      ////console.log("signin error=" + error.status);
      //this.isLoading = false;
      this.getError(error);
    });
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  ionViewCanEnter() {
    ////console.log("ionViewCanEnter");
  }

  ionViewWillEnter() {
    this.privacy = this.navParams.get('userSelectedPrivacy');

    if (this.privacy != undefined && this.privacy != null && this.privacy != '') {
      for (let i = 0; i < this.Privacies.length; i++) {
        if (this.privacy == this.Privacies[i].syskey) {
          this.privacyIcon = this.Privacies[i].icon;
          this.privacyName = this.Privacies[i].name;
        }
      }
    } else {
      this.privacyIcon = this.Privacies[1].icon;
      this.privacyName = this.Privacies[1].name;
      this.privacy = this.Privacies[1].syskey;
    }

    ////console.log("ionViewWillEnter value of privacy=" + this.privacy);
  }

}


