import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, ToastController, AlertController, Events, Platform, PopoverController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { GlobalProvider } from '../../providers/global/global';
import { WalletPage } from '../wallet/wallet';
import { AllserviceProvider } from '../../providers/allservice/allservice';

import { Changefont } from '../changefont/changeFont';
import { SocialSharing } from '@ionic-native/social-sharing';//ndh
import { Screenshot } from '@ionic-native/screenshot';//ndh


@Component({
  selector: 'page-receipt',
  templateUrl: 'receipt.html',
})
export class Receipt {
  textEng: any = [" QR scan to pay me", "MMK","Receive","Cash In"];
  textMyan: any = ["မိမိထံ ငွေပေးချေရန် QR scan လုပ်ပါ။", "MMK","ငွေလက်ခံရန်","ငွေသွင်း"];
  textData: string[] = [];
  font: string = '';

  dataValue: any = [];
  qrValue: any = [];
  popover: any;
  selectedTitle: any;
  screen: any;
  state: boolean = false;
  flag=false;
  amount2:any;
  amount:any;
  amountformat:any;
  name:any;
  phoneno:any;
  type:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public events: Events, public barcodeScanner: BarcodeScanner, public alertCtrl: AlertController,private screenshot: Screenshot,private socialSharing: SocialSharing,
    public global: GlobalProvider, public viewCtrl: ViewController,public changefont: Changefont, public toastCtrl: ToastController,
    public platform: Platform, public popoverCtrl: PopoverController, private all: AllserviceProvider) {
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      });
      this.storage.get('language').then((font) => {
        this.changelanguage(font);
        ////console.log("state data=" + JSON.stringify(this.textData));
      });
    this.dataValue = this.navParams.get('data');
    this.amount2=this.navParams.get('param');
    this.type=this.navParams.get('type');
    console.log("Type:"+this.type);
    this.amount=this.amount2[0];
    this.name=this.amount2[1];
    this.phoneno=this.amount2[2];
    this.phoneno=this.phoneno.substr(9,4);
    this.amountformat=this.global.format(this.amount);
    console.log('QR balance  rrr'+this.dataValue);
    this.dataValue = this.all.getEncryptText(this.all.iv, this.all.salt, this.all.dm, this.dataValue);
    
    ////console.log("request dataValue >> " + JSON.stringify(this.dataValue));
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad QrPage');
  }
  goClose() {
    this.navCtrl.setRoot(WalletPage);
  }
  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }
  goSaveImage(){
    this.flag=true;
    this.screenshot.URI(80)
    .then((res) => {
      this.screen=res.URI;
      console.log('Screen Shot '+res.URI);
      this.socialSharing.share(null,'',res.URI, null)
       .then(() => {},
         () => { 
       alert('SocialSharing failed');
         });
       },
      () => {
      alert('Screenshot failed');
      });
    }
  
  
  
}
