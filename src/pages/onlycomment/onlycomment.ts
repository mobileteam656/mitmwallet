import { Component, ViewChild } from '@angular/core';
import { Events, Content, TextInput } from 'ionic-angular';
import {
  IonicPage, NavController, NavParams, PopoverController, Platform, LoadingController, ViewController,
  ToastController, ActionSheetController, Navbar, AlertController
} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SQLite } from '@ionic-native/sqlite';
import { FunctProvider } from '../../providers/funct/funct';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { DatePipe } from '@angular/common';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Clipboard } from '@ionic-native/clipboard';

import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ChangefontProvider } from '../../providers/changefont/changefont';

import { ReplyCommentPage } from '../reply-comment/reply-comment';
import { LikepersonPage } from '../likeperson/likeperson';
import { PopoverCommentPage } from '../popover-comment/popover-comment';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'page-onlycomment',
  templateUrl: 'onlycomment.html',
  providers: [ChangelanguageProvider]
})
export class OnlycommentPage {

  @ViewChild(Content) content: Content;

  textMyan: any = ["မှတ်ချက်များ", "မှတ်ချက် မရှိပါ", "မှတ်ချက်ရေးရန်"];
  textEng: any = ["Replies", "No Replies", "Write a comment ....."];
  textdata: string[] = [];
  font: string = '';
  replyData: any = [];
  isLoading: any;
  comment: any = '';
  popover: any;
  registerData: any = { t3: '' };
  nores: any;
  photoLink: any;
  textData: any = [];
  status: any = false;
  textFont: any = '';
  passData: any;
  loading: any;
  inputFont: any;
  ipaddress: string = '';

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, public storage: Storage, public alert: AlertController,
    public datePipe: DatePipe, public http: Http, public toastCtrl: ToastController, public changeLanguage: ChangelanguageProvider,
    public popoverCtrl: PopoverController, public platform: Platform, public funct: FunctProvider, public sanitizer: DomSanitizer,
    public clipboard: Clipboard, public changefont: ChangefontProvider,
    public global: GlobalProvider) {
    this.storage.get('ipaddressCMS').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressCMS;
      }
    });

    this.photoLink = this.global.digitalMediaProfile;
    this.passData = this.navParams.get("data");
    this.storage.get('language').then((font) => {
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then((data) => {
        this.textData = data;
        if (font != "zg")
          this.font = 'uni';
        else
          this.font = font;
        for (let i = 0; i < this.replyData.length; i++) {
          // this.changeLanguage.changelanguageText(this.font, this.replyData[i].t2).then((res) => {
          //   this.replyData[i].t2 = res;
          // })
        }
        ////console.log("data language=" + JSON.stringify(data));
      });
    });
    this.storage.get('textInput').then((font) => {
      this.inputFont = font;
      //  this.changeLanguage.changelanguageText( this.inputFont, this.textMyan[2]).then((res) => {
      //     this.textMyan[2] = res;
      //  })
    });
  }

  ionViewDidEnter() {
    this.storage.get('appData').then((data) => {
      this.registerData = data;
      ////console.log("registerData = " + JSON.stringify(this.registerData));
      this.getCommentData();
    });

    this.storage.get('changelan').then((result) => {
      ////console.log('Your language is', result);
      this.textFont = result;
      if (this.textFont == 'zg') {
        this.textData[2] = this.changefont.UnitoZg(this.textData[2]);
      }
      else {
        this.textData[2] = this.textData[2];
      }
    });

    this.backButtonExit();
  }

  getCommentData() {     //tmn
    this.http.get(this.ipaddress + '/serviceQuestion/getCommentmobile?id=' + this.passData.syskey + '&userSK=' + this.registerData.syskey).map(res => res.json()).subscribe(result => {
      ////console.log("return Commentfunt data = " + JSON.stringify(result));
      this.isLoading = true;
      if (result.data.length > 0) {
        this.nores = 1;
        for (let i = 0; i < result.data.length; i++) {
          result.data[i].differenceTime = this.funct.getTimeDifference(result.data[i].modifiedDate, result.data[i].modifiedTime);

          result.data[i].modifiedDate = this.funct.getTransformDate(result.data[i].modifiedDate);

          result.data[i].like = "Like";

          /* for(let j = 0;j<result.data[i].commentLike.length;j++){
             if(result.data[i].commentLike[j].recordStatus == 1){
               result.data[i].like = "UnLike";
             }
             else if(result.data[i].commentLike[j].recordStatus == 4)
             {
               result.data[i].like = "Like";
             }

           }*/

          if (result.data[i].person.length > 0) {
            for (let k = 0; k < result.data[i].person.length; k++) {
              if (result.data[i].person[k].syskey == this.registerData.syskey) {
                result.data[i].like = "UnLike";
                break;
              }
            }
          }

          result.data[i].likeCount = result.data[i].person.length;
          result.data[i].replyCount = result.data[i].n3;
          if (result.data[i].n3 == 1) {
            result.data[i].textreply = "reply";
          }
          else if (result.data[i].n3 > 1) {
            result.data[i].textreply = "replies";
          }
        }
        this.replyData = result.data;
        //this.scrollToBottom();
        ////console.log("replyData data = " + JSON.stringify(this.replyData));
      }
      else {
        this.nores = 0;
      }
      this.isLoading = false;
    },
      error => {
        this.getError(error);
      });
  }

  onFocus() {
    this.scrollToBottom();
  }

  saveComment() {

    if (this.comment != '') {
      this.status = true;
      let parameter = {
        t1: "answer",
        t2: this.comment,
        n1: this.passData.syskey,
        n5: this.registerData.syskey

      }
      //   this.postcmt = {t3: this.registerData.t3 , t2: this.comment};
      ////console.log("requst saveComment=" + JSON.stringify(parameter));
      //  this.replyData.push(this.postcmt);
      this.scrollToBottom();
      this.http.post(this.ipaddress + '/serviceQuestion/saveAnswer', parameter).map(res => res.json()).subscribe(result => {
       ////console.log("return saveComment data = " + JSON.stringify(result));
        if (result.state && result.data.length > 0) {
          this.nores = 1;

          for (let i = 0; i < result.data.length; i++) {
            result.data[i].differenceTime = this.funct.getTimeDifference(result.data[i].modifiedDate, result.data[i].modifiedTime);

            result.data[i].modifiedDate = this.funct.getTransformDate(result.data[i].modifiedDate);

            result.data[i].like = "Like";
            if (result.data[i].person.length > 0) {
              for (let k = 0; k < result.data[i].person.length; k++) {
                if (result.data[i].person[k].syskey == this.registerData.syskey) {
                  result.data[i].like = "UnLike";
                  break;
                }
              }
            }

            result.data[i].likeCount = result.data[i].person.length;
            result.data[i].replyCount = result.data[i].n3;
            if (result.data[i].n3 == 1) {
              result.data[i].textreply = "reply";
            }
            else if (result.data[i].n3 > 1) {
              result.data[i].textreply = "replies";
            }
          }


          this.replyData = result.data;
          this.scrollToBottom();
          this.comment = '';
          this.passData.n3 = this.passData.n3 + 1;
          //this.passData.commentCount = this.funct.getChangeCount(this.passData.n3);
          this.passData.commentCount = this.passData.n3;
        }
        else {
          if (this.replyData.length > 0)
            this.nores = 1;
          else
            this.nores = 0;
        }
        this.status = false;
        this.isLoading = false;
        this.content.resize();
        this.scrollToBottom();
      },
        error => {
          this.status = false;
          this.comment = '';
          if (this.replyData.length > 0)
            this.nores = 1;
          else
            this.nores = 0;
          this.getError(error);
        });
    }
  }

  presentPopover(ev, d) {
    let st;
    if (d.n5 != this.registerData.syskey) {
      st = 0;
    } else {
      st = 1;
    }
    this.popover = this.popoverCtrl.create(PopoverCommentPage, {
      data: st
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      ////console.log("popover dismissed");
      ////console.log("Selected Item is " + data);
      if (data == "1") {
        let copytext = this.changefont.UnitoZg(d.t2);
        this.clipboard.copy(copytext);
      }
      else if (data == "2") {
        this.getDeleteMsg(d);
      }
    });

  }

  getDeleteMsg(d) {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    ////console.log("request d.syskey =" + d.syskey);
    this.http.get(this.ipaddress + "/serviceQuestion/deleteComment?syskey=" + d.syskey, ).map(res => res.json()).subscribe(result => {
     // //console.log("response process msg =" + JSON.stringify(result));
      if (result.state) {
        this.getCommentData();
        this.passData.n3 = this.passData.n3 - 1;
        //this.passData.commentCount = this.funct.getChangeCount(this.passData.n3);
        this.passData.commentCount = this.passData.n3;
        this.loading.dismiss();
      }
      else {
        let toast = this.toastCtrl.create({
          message: 'Delete Failed! Please try again.',
          duration: 5000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
      this.loading.dismiss();
    });
  }

  changeLike(skey, index) {
    this.replyData[index].likeCount = this.replyData[index].likeCount - 1;
    let parameter = {
      n1: this.registerData.syskey,
      n2: skey
    }
    //   this.postcmt = {t3: this.registerData.t3 , t2: this.comment};
    ////console.log("requst saveComment=" + JSON.stringify(parameter));
    //  this.replyData.push(this.postcmt);
    this.scrollToBottom();
    this.http.post(this.ipaddress + '/serviceQuestion/saveCommentLike', parameter).map(res => res.json()).subscribe(data => {
      ////console.log("return likecomment data = " + JSON.stringify(data));
      ////console.log("data length == " + data.data.length);
      if (data.state && data.data.length > 0) {
        for (let i = 0; i < data.data.length; i++) {
          if (data.data[i].person.length > 0) {
            for (let k = 0; k < data.data[i].person.length; k++) {
              if (data.data[i].person[k].syskey == this.registerData.syskey) {
                this.replyData[index].like = "UnLike";
                break;
              }
              else {
                this.replyData[index].like = "Like";
              }
            }

          }
          else {
            this.replyData[index].like = "Like";
          }

          this.replyData[index].likeCount = data.data[i].person.length;
          this.replyData[index].person = data.data[i].person;
        }
      }
    },
      error => {
        this.getError(error);
      });

  }

  likePerson(cmt) {
    this.navCtrl.push(LikepersonPage, {
      data: cmt
    });
  }

  ClickReply(cmt) {
    this.navCtrl.push(ReplyCommentPage, {
      data: cmt
    });
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });
    toast.present(toast);
    this.isLoading = false;
    // this.loading.dismiss();
    ////console.log("Oops!");
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll) this.content.scrollToBottom(0);
    }, 400)
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
     // //console.log("Active Page=" + this.navCtrl.getActive().name);
      this.navCtrl.pop();
    });
  }

}
