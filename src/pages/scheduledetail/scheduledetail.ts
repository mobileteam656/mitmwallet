import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Events, NavParams, ToastController, MenuController, AlertController, LoadingController, Platform } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Http, JSONPBackend, Jsonp } from '@angular/http';
import { UtilProvider } from '../../providers/util/util';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { Changefont } from '../changefont/changeFont';
import { PopoverController } from 'ionic-angular';

// import * as   Highcharts from 'highcharts';


/**
 * Generated class for the ScheduledetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-scheduledetail',
  templateUrl: 'scheduledetail.html',
  providers: [Changefont]
})
export class ScheduledetailPage {
  @ViewChild('doughnutCanvas') doughnutCanvas;

  ipaddress: string;
  public loading;
  loanHistory: any = [];

  textMyan: any = ["အသေးစိတ်အရစ်ကျပြန်ဆပ်စာရင်း"];
  textEng: any = ["Loan Schedule Details"];
  textdata: string[] = [];
  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  font: string = '';
  // ExitData: boolean;
  PersonData = this.global.PersonData;
  count: number = 1;
  doughnutChart: any;
  totalAmt: any;
  remainAmt: any;
  repayAmt: any;
  flag: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public events: Events,
    public global: GlobalProvider, public changefont: Changefont, public http: Http, public toastCtrl: ToastController,
    public util: UtilProvider, public platform: Platform, public loadingCtrl: LoadingController) {

    this.storage.get('ip').then((ip) => {
      console.log('Your ip is', ip);
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
        console.log("Your ip is", this.ipaddress);
      }
      else {
        console.log(" undefined  conditon ");
        this.ipaddress = global.ipaddress;
        console.log("Your ip main is", this.ipaddress);
      }


      // this.ExitData = false;

      //Font 
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      })
      this.storage.get('language').then((font) => {
        this.changelanguage(font);
      });
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
    this.PersonData = this.navParams.get("PersonDataDetail");
    this.storage.get('ip').then((ip) => {
      console.log('Your ip is', ip);
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
        console.log("Your ip is", this.ipaddress);
      }
      else {
        console.log(" undefined  conditon ");
        this.ipaddress = this.global.ipaddress;
        console.log("Your ip main is", this.ipaddress);
      }
      if (this.loanHistory.length == 0)
        this.LoanHistory();
    });
  }

  //dougnut chart 
  LoanHistory() {
    //added network test
    if (this.global.netWork == "connected") {
      this.loading = this.loadingCtrl.create({
        content: "", spinner: 'circles',
        dismissOnPageChange: true
        //   duration: 3000
      });
      this.loading.present();
      this.http.get(this.global.ipAdressMFI + '/serviceMFI/loadRepaymentInstallmentlistByLoansyskey?loansyskey=' + this.PersonData.loandata.loanSyskey).map(res => res.json()).subscribe(data => {
        var bgColor = [];
        var bdColor = [];
        var repay: any;
        var remain: any;
        if (data != null) {
          this.flag = 1;
          this.totalAmt = data.totalAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          this.repayAmt = data.repaymentAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          this.remainAmt = data.outstandingAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          repay = data.repaymentAmount;
          remain = data.outstandingAmount;
          // this.ExitData = true;
          console.log("Loan List History is  >>" + JSON.stringify(data));
          // this.doughnutCanvas = Highcharts.chart('container', {
          //   chart: { plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false, type: 'pie' },
          //   title: { text: '' },
          //   tooltip: { pointFormat: '</b>{point.percentage:.2f}%</b>' },
          //   plotOptions: {
          //     pie: {
          //       allowPointSelect: true,
          //       cursor: 'pointer',
          //       dataLabels: {
          //         enabled: true,
          //         format: '{point.percentage:.2f}%',
          //         style: { color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black' }
          //       },
          //       showInLegend: true
          //     }
          //   },
          //   credits: { enabled: false },
          //   series: [{
          //     name: 'Percentage',
          //     colorByPoint: false,
          //     color: '#FFFF00',
          //     size: '75%',
          //     innerSize: '70%',
          //     data: [{
          //       name: 'Repayment Amount',
          //       y: repay,
          //       //y: 500,
          //       color: '#9ACD32',
          //     }, {
          //       name: 'Remaining Amount',
          //       y: remain,
          //       //y: 500,
          //       color: '#ce972d'
          //     }]
          //   }]
          // }
         // );
        }
        this.loading.dismiss();
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }
  }

  ionViewDidEnter() {
    this.backButtonExit();
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
    });
  }
}
