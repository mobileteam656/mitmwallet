import { Component } from '@angular/core';
import {  NavController, NavParams, MenuController, ModalController, LoadingController, ToastController, Events, Platform, AlertController, App, PopoverController } from 'ionic-angular';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { Http } from '@angular/http';
import { DatePipe } from '@angular/common';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';
import { EventLoggerProvider } from '../../providers/eventlogger/eventlogger';
import { UtilProvider } from '../../providers/util/util';
import { GlobalProvider } from '../../providers/global/global';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { Storage } from '@ionic/storage';
import { TransferDetails } from '../transfer-details/transfer-details';
import { WalletPage } from '../wallet/wallet';

import { WalletTopupOtpPage } from '../wallet-topup-otp/wallet-topup-otp';

@Component({
  selector: 'page-top-up-wallet-confrim',
  templateUrl: 'top-up-wallet-confrim.html',
  providers: [CreatePopoverPage]
})
export class TopUpWalletConfrimPage {
  textEng: any = ["Confirmation", "Name", "Balance", "Account Number", "Institution", "Amount",
    "Remark", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "Cancel",
    "Confirm", "Reference", "Type"];
  textMyan: any = ["အတည်ပြုခြင်း", "အမည်", "လက်ကျန်ငွေ", "ဖုန်းနံပါတ်", "အော်ပရေတာ", "ငွေပမာဏ",
    "မှတ်ချက်", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "ပယ်ဖျက်မည်",
    "လုပ်ဆောင်မည်", "အကြောင်းအရာ", "အမျိုးအစား"];
  textData: string[] = [];
  font: string = '';
  remark_flag: boolean = false;
  ammount:any;
  ipaddress: string;
  dateString:string;
  resData: any;
  passOtp: any;
  passSKey: any;
  mobileData:any= {};
  public loading;
  String
  _obj = {"amount":"","remark":"","syskey":"","userID":"","sessionID":"","fromAccount":"","receiverID":"","fromName":"","toName":"","merchantID":""};
  constructor(
    public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public menuCtrl: MenuController, public modalCtrl: ModalController, public http: Http,
    public datePipe: DatePipe, public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public sqlite: SQLite, public changefont: Changefont,private eventlog:EventLoggerProvider,
    public global: GlobalProvider, public events: Events, public util: UtilProvider, public platform: Platform,
    public alertCtrl: AlertController, private all: AllserviceProvider, public createPopover: CreatePopoverPage, 
    public appCtrl: App,
     private firebase: FirebaseAnalytics, public popoverCtrl: PopoverController
  ) {
   
    this._obj = this.navParams.get('data');
    console.log("Transfer Confrim Mobile Data: " + JSON.stringify(this._obj));
    this.passOtp = this.navParams.get("otp");
    console.log('OTP Page: '+this.passOtp);
    this.passSKey = this.navParams.get('sKey');
    if (this._obj.remark != '' && this._obj.remark != undefined) {
      this.remark_flag = true;
    }
    this.ammount = this.util.formatAmount(this._obj.amount);
    console.log('Amount: '+this.ammount);
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('ipaddressApp').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressApp;
      }
    }); 
    this.events.subscribe('ipaddressApp', ip => {
      this.ipaddress = ip;
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    this.storage.get('loginData').then((result) => {
      this.mobileData = result;
      console.log('Mobile User: '+JSON.stringify(this.mobileData));
      
    });

  }
  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }
  goBack() {
    this.navCtrl.pop();
  }
  goConfrim(){
    if (this.passOtp == 'true') {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      let param = { userID: this.mobileData.userID, sessionID: this.mobileData.sessionID, type: 16, merchantID: this._obj.merchantID, sKey: this.passSKey };
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          this.navCtrl.push(WalletTopupOtpPage, {
            dataOTP: data,
            data: this._obj,
            sKey: this.passSKey
          })
        }
        else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 5000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    }
    else {
      this.goTransfer();
    }
  }
  goTransfer() {
    this.loading = this.loadingCtrl.create({
      content: "Processing...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    let parameter = {
      userID: this._obj.userID,
      sessionID: this._obj.sessionID,
      fromAccount: this._obj.fromAccount,
      toAccount: "",
      merchantID: "",
      bankCharges: "",
      refNo: this._obj.receiverID,
      sKey: this.passSKey,
      amount: this._obj.amount,
      narrative: this._obj.remark,
      field1: "2",
      field2: "",
      fromName: this._obj.fromName,
      toName: this._obj.toName
    };
    // console.log("request gopayment =" + JSON.stringify(parameter))
    this.http.post(this.ipaddress + '/service003/goTopupToWallet', parameter).map(res => res.json()).subscribe(res => {
      this.dateString=res.transactionDate.substr(6,4);
      this.dateString+=res.transactionDate.substr(3,2);
      this.dateString+=res.transactionDate.substr(0,2);
      
      let resdata = {
        "bankRefNo": res.bankRefNumber,
        "code": res.code,
        "desc": res.desc,
        // "transDate": this.util.getRDatePickerDate(res.transactionDate)
        "transDate": this.dateString
      };
      if (res.code == "0000") {
        this.resData = res;
        this.appCtrl.getRootNav().setRoot(TransferDetails, {
          data: resdata,
          data1: this._obj.amount,
          data2: this._obj.toName
        });
        this.loading.dismiss();
      }

      else if (res.code == "0016") {
        this.logoutAlert(res.desc);
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', res.desc);
        this.loading.dismiss();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.loading.dismiss();
      });
  }
  // goPost(){
  //   this.loading = this.loadingCtrl.create({
  //     content: "Processing",
  //     dismissOnPageChange: true
  //     // duration: 3000
  //   });
  //   this.loading.present();
  //   let parameter = {
  //             userID: this._obj.userID,
  //             sessionID: this._obj.sessionID,
  //             fromAccount: this._obj.fromAccount,
  //             receiverID: this._obj.receiverID,
  //             sKey: this._obj.syskey,
  //             amount: this._obj.amount,
  //             narrative: this._obj.remark,
  //             //field1: "2",
  //             //field2: "",
  //             fromName: this._obj.fromName,
  //             toName: this._obj.toName
  //           };
  //           this.http.post(this.ipaddress + '/service003/goAgentToWallet', parameter).map(res => res.json()).subscribe(res => {
  //             this.dateString=res.transactionDate.substr(6,4);
  //             this.dateString+=res.transactionDate.substr(3,2);
  //             this.dateString+=res.transactionDate.substr(0,2);
  //             console.log('Date String: '+this.dateString);
  //             let resdata = {
  //               "bankRefNo": res.bankRefNumber,
  //               "code": res.code,
  //               "desc": res.desc,
  //               // "transDate": this.util.getRDatePickerDate(res.transactionDate)
  //               "transDate": this.dateString
  //             };
  //             console.log('Response Data: '+resdata);
  //             let code = res.code;
  //             if (res.code == "0000") {
  //               this.resData = res;
  //               this.appCtrl.getRootNav().setRoot(TransferDetails, {
  //                 data: resdata,
  //                 data1: this._obj.amount,
  //                 data2: this._obj.toName
  //               });
  //               this.loading.dismiss();
  //             }
  //             else if (res.code == "0016") {
  //               this.logoutAlert(res.desc);
  //               this.loading.dismiss();
  //             }
  //             else if (res.code == "0014") {
  //               this.logoutAlert(res.desc);
  //               this.loading.dismiss();
  //             }
  //             else {
  //               this.toastInvalidLogin();
  //               this.loading.dismiss();
  //             }

  //           },

  //     error => {
  //       ////console.log("error=" + error.status);
  //       let toast = this.toastCtrl.create({
  //         duration: 3000,
  //         position: 'bottom',
  //         dismissOnPageChange: true,
  //       });
  //       toast.present(toast);
  //       this.loading.dismiss();
  //     }
  //   );

  // }
  toastInvalidLogin() {
    let toast = this.toastCtrl.create({
      message: "Connection Error.",
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });

    toast.present(toast);
  }
  
  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('loginData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(WalletPage, {
            });
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }



}
