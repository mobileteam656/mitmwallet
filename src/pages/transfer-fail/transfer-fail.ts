import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Changefont } from '../changefont/changeFont';
import { Storage } from '@ionic/storage';
//import { TabsPage } from '../tabs/tabs';
import { AccountTransfer } from '../transfer/transfer';
import { ChooseContactPage } from '../choose-contact/choose-contact';
import { WalletPage } from '../wallet/wallet';

/**
 * Generated class for the TransferFailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-transfer-fail',
  templateUrl: 'transfer-fail.html',
})
export class TransferFailPage {
  // "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!",

  textEng: any = ["Transferred Fail", "Transaction Date", "Sender Information", "Receiver Information", "Close"];
  textMyan: any = ["ငွေလွှဲခြင်းမအောင်မြင်ပါ", "လုပ်ဆောင်ခဲ့သည့် ရက်", "ပေးပို့သူ အချက်အလက်", "လက်ခံသူ အချက်အလက်", "ပိတ်မည်"];
  textdata: string[] = [];
  ipaddress: string;
  font: string = '';
  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;

  error: any;
  transDate: any;
  sender: any;
  receiver: any;
  data: any;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage,
    public events: Events,
    public global: GlobalProvider,
    public changefont: Changefont

  ) {
    this.data = this.navParams.get('data');
    this.error = this.navParams.get('desc');
    this.transDate = this.navParams.get('transDate');
    this.sender = this.navParams.get('fromName');
    this.receiver = this.navParams.get('toName');


    this.storage.get('ipaddressWallet').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressWallet;
      }
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      });
      this.storage.get('language').then((lan) => {
        this.changelanguage(lan);
      });

    });

  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      this.language = this.englanguage;
      for (let i = 0; i < this.textEng.length; i++)
        this.textdata[i] = this.textEng[i];
    } else if (font == "zg") {
      this.font = "zg";
      this.language = this.changefont.UnitoZg(this.myanlanguage);
      for (let i = 0; i < this.textMyan.length; i++)
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
    } else {
      this.font = "uni";
      this.language = this.myanlanguage;
      for (let i = 0; i < this.textMyan.length; i++)
        this.textdata[i] = this.textMyan[i];
    }
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad TransferFailPage');
  }

  goBackTransaction() {
    //let params = { tabIndex: 0, tabTitle: 'Wallet' };
    this.navCtrl.setRoot(WalletPage);
  }

 // goBack(){
   // this.navCtrl.push(ChooseContactPage);
  //} 

}
