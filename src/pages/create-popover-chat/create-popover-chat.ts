import { Component } from '@angular/core';
import { LoadingController, ToastController, App, NavController, NavParams, PopoverController, Platform, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FontPopoverPage } from '../font-popover/font-popover';
import { Language } from '../language/language';
import { Login } from '../login/login';
import { MainBlankPage } from '../main-blank-page/main-blank-page';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { MyPopOverListChatPage } from '../my-pop-over-list-chat/my-pop-over-list-chat';
import { ViewGroupPage } from '../view-group/view-group';

@Component({
  selector: 'page-create-popover-chat',
  templateUrl: 'create-popover-chat.html',
})
export class CreatePopoverChatPage {

  popover: any;
  db: any;
  loading: any;
  uList: any;
  userData: any;
  alertPopup: any;
  updateGroupName: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public popoverCtrl: PopoverController, public platform: Platform,
    public storage: Storage, public events: Events,
    public appCtrl: App, public loadingCtrl: LoadingController,
    public alertCtrl: AlertController, public sqlite: SQLite,
    public global: GlobalProvider, public toastCtrl: ToastController,
    public http: Http, public alert: AlertController) {
    this.storage.get('userData').then((userData) => {
      ////console.log('Your userData is', userData);
      if (userData !== undefined && userData !== "") {
        this.userData = userData;
      }
    });
    this.events.subscribe('userData', userData => {
      ////console.log('Your userData is', userData);
      if (userData !== undefined && userData !== "") {
        this.userData = userData;
      }
    });
  }

  ionViewWillEnter() {
    this.storage.get('userData').then((userData) => {
      ////console.log('Your userData is', userData);
      if (userData !== undefined && userData !== "") {
        this.userData = userData;
      }
    });
    this.events.subscribe('userData', userData => {
      ////console.log('Your userData is', userData);
      if (userData !== undefined && userData !== "") {
        this.userData = userData;
      }
    });
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad CreatePopoverPage');
  }

  addPerson(){

  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(MyPopOverListChatPage, {});
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      if (data == "2") {
        this.goLogout();
      }
      if (data == "4") {
        this.presentPopoverFont(ev);
      }

      if (data == "5") {
        this.changeGruopName();
      }
      else if (data == "6") {
        this.addPerson();
      }
      else if (data == "7") {
        this.navCtrl.push(ViewGroupPage, {
          data: this.uList
        });
      }
      else if (data == "8"){
        this.alertPopup = this.alert.create({
          cssClass:'uni',
          message:'Are you sure want to leave group?',
          enableBackdropDismiss : false,
          buttons: [{
            text: "Cancel",
            role: 'cancel',
            handler: () => {
            }
          },{
            text: "OK",
            handler: () => {
              //leave group
              this.leaveGroup();
            }
          }]
        })
        this.alertPopup.present();
        let doDismiss = () => this.alertPopup.dismiss();
        let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
        this.alertPopup.onDidDismiss(unregBackButton);
      }
    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(Language, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      ////console.log("Selected Item is " + data);
    });
  }

  presentPopoverFont(ev) {
    this.popover = this.popoverCtrl.create(FontPopoverPage, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      ////console.log("Selected Item is " + data);
    });
  }


  presentPopoverCloud(ev) {
    this.popover = this.popoverCtrl.create(MainBlankPage, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      ////console.log("popover dismissed");
      ////console.log("Selected Item is " + data);
      if (data != null) {
        this.storage.set('settings', data);
      }
    });

  }

  goLogout() {
    let alert = this.alertCtrl.create({
      title: 'Are you sure you want to exit?',
      enableBackdropDismiss: false,
      message: '',
      buttons: [{
        text: 'No',
        handler: () => {
        }
      },
      {
        text: 'Yes',
        handler: () => {
          // this.storage.remove('phonenumber');
          this.storage.remove('username');
          this.storage.remove('userData');
          this.storage.remove('firstlogin');
          this.appCtrl.getRootNav().setRoot(Login);
          this.deleteLocalSQLiteDB();
        }
      }]
    });
    alert.present();
  }

  deleteLocalSQLiteDB() {
    /* 
    this.db.executeSql("DELETE FROM channel", []).then((data) => {
      //console.log("Delete data successfully FOR LOGOUT", data);
    }, (error) => {
      console.error("Unable to delete data FOR LOGOUT", error);
    });

    this.db.executeSql("DELETE FROM Contact", []).then((data) => {
      //console.log("Delete data successfully FOR LOGOUT", data);
    }, (error) => {
      console.error("Unable to delete data FOR LOGOUT", error);
    }); */

    this.sqlite.deleteDatabase({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success drop database");
    }, (error) => {
      //console.error("Unable to open database", error);
    });
  }
  leaveGroup() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
  
    let url = this.global.ipaddress3 + 'serviceChat/deleteGroup?sysKey=' + this.uList.channelkey
    + '&registerKey=' + this.userData.sKey 
    + '&Group=1';

    ////console.log("url=" + url);

    this.http.get(url).map(res => res.json()).subscribe(data => {
      ////console.log("response leave group== " + JSON.stringify(data));

      if (data.state) {
        this.presentToast(data.msgDesc);
        this.navCtrl.pop();
      }

      this.loading.dismiss();
    }, error => {
      this.presentToast('Deleted Fail!');
      ////console.log("Leave group error==" + error.status);
      this.loading.dismiss();
      this.getError(error);
    });
  }

  changeGruopName() {
    let prompt = this.alertCtrl.create({
      title: this.uList.t2,
      //  message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'groupName',
          placeholder: 'Change Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
           // //console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            ////console.log('Saved clicked');
            this.updateGroupName = data.groupName;
            this.changeName();
          }
        }
      ]
    });
    prompt.present();
  }

  changeName() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    let params = {
      autokey: this.uList.channelkey,
      groupName: this.updateGroupName
    }
    ////console.log("request changeChannelName =" + JSON.stringify(params));
    this.http.post(this.global.ipaddress3 + "serviceChat/changeChannelName", params).map(res => res.json()).subscribe(result => {
      ////console.log("response changeChannelName =" + JSON.stringify(result));
      if (result.state) {
        this.uList.t2 = this.updateGroupName
      }
      else {
        let toast = this.toastCtrl.create({
          message: 'Updated Failed! Please try again.',
          duration: 5000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
      }
      this.loading.dismiss();
    }, error => {
      ////console.log("signin error=" + error.status);
      this.loading.dismiss();
      this.getError(error);
    });
  }

  presentToast(str){
    let toast = this.toastCtrl.create({
      message: str,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });
    toast.present(toast);
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = "Can't connect right now. [" + code + "]";
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });
    toast.present(toast);
    this.loading.dismiss();
    //this.isLoading = false;
    //this.dismiss();
    ////console.log("Oops!");
  }

}
