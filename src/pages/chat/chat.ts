//import { CameraPage } from '../camera-page/camera-page';
import { Component, ViewChild, OnInit } from '@angular/core';
import { App, NavController, NavParams, PopoverController, LoadingController, ToastController, AlertController, ActionSheetController, Platform, Navbar } from 'ionic-angular';
import { Events, Content, TextInput } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
//import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';
import { TabsPage } from '../tabs/tabs';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import { Language } from '../language/language';
import { Login } from '../login/login';
import { MessagePage } from '../message/message';
import { FontPopoverPage } from '../font-popover/font-popover';
import { FunctProvider } from '../../providers/funct/funct';
import { ContactInfoPage } from '../contact-info/contact-info';
import { PicsDetailsChannelPage } from '../pics-details-channel/pics-details-channel';
import { ViewPhotoMessagePage } from '../view-photo-message/view-photo-message';
import { PicsChannelPage } from '../pics-channel/pics-channel';
import { CreatePopoverChatPage } from '../create-popover-chat/create-popover-chat';
import { MyPopOverListChatPage } from '../my-pop-over-list-chat/my-pop-over-list-chat';
import { ViewGroupPage } from '../view-group/view-group';
import { SelectParticipantPage } from '../select-participant/select-participant';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { PopoverMsgPage } from '../popover-msg/popover-msg';
import { Clipboard } from '@ionic-native/clipboard';

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
  providers: [CreatePopoverChatPage, CreatePopoverPage],
})
export class Chat {

  textEng: any = ["Write message here...", "No message found!"];
  textMyan: any = ["ဤနေရာတွင်စာရိုက်ပါ", "No message found!"];
  textData: string[] = [];
  showFont: any;
  keyboardfont: any;
  language: any;

  ipaddress: string = '';
  ipaddressCS: string = '';

  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: TextInput;
  @ViewChild(Navbar) navBar: Navbar;
  photos: any = [];
  name: any;
  userst: any;
  syskey: any;
  msgList: any = [];
  conversionList: any = [];
  start: any = 0;
  end: any = 0;
  postcmt = { t2: "", t8: "", t3: "" };
  postMsg: any = '';
  nores: any;
  time: any;
  hour: any;
  minute: any;
  isLoading: any;
  status: any;
  usersyskey: any;
  role: any;
  passData: any = {};
  channelSyskey: any;
  channelName: any;
  groupnames: any = "";
  groupname: any = "";
  updateGroupName: any = "";
  isgroup: any = 0;
  popover: any;
  loading: any;
  imageLinkFolder: any;
  clickopenMenu: boolean = false;
  img: any = '';
  showEmojiPicker = false;
  editorMsg = '';
  emojiArr = [];
  showRow: boolean = false;
  emo: any = [];
  showEmo: boolean = false;
  count: number = 1;
  hardwareBackBtn: any = true;
  modal: any;
  exit: boolean = false;

  userDataChat: any;

  files: any = [];
  fileName: any;
  chatphoto: any;
  hautosys: any;
  imageLink: any;
  keyboard_font: any;

  latitude: any;
  longitude: any;
  location: any = '';
  createdDate: any;
  createdTime: any;
  docData: any;
  photoName: any;
  chatPhoto: any = { id: '', PhotoPath: '', name: '', latitude: '', longitude: '', location: '', CreateDate: '', CreateTime: '' };

  imageDetail_Chat: any;
  from_Chat: any = { fromName: '' };
  description_Chat: any = { description: '' };

  docDetail: any = { id: '', PhotoPath: '', name: '', latitude: '', longitude: '', location: '', CreateDate: '', CreateTime: '' };
  profileImageLink: any;
  mySyskey: any;
  photoSize: string = '';
  isUserTakenPhoto: string = '';
  slimHeight: string = '0px';
  stopBackBtn: string = '';
  db: any;
  secondTime: string = '';
  msgParam: string = '';
  msgSend: string = '';
  msgCarry: string = '';

  alertPopup: any;
  checkdorefresh: boolean = false;

  constructor(
    public http: Http,
    public sqlite: SQLite,
    public toastCtrl: ToastController,
    public storage: Storage,
    public events: Events,
    public appCtrl: App,
    //private transfer: Transfer,
    private transfer: FileTransfer,
    private file: File,
    public camera: Camera,
    public global: GlobalProvider,
    public navParams: NavParams,
    public navCtrl: NavController,
    public popoverCtrl: PopoverController,
    public changefont: Changefont,
    public platform: Platform,
    public util: UtilProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public createPopoverChat: CreatePopoverChatPage,
    public actionSheetCtrl: ActionSheetController,
    public funct: FunctProvider,
    public app: App,
    public createPopover: CreatePopoverPage,
    private clipboard: Clipboard
  ) {
    this.ipaddress = this.global.ipaddressChat;
    this.ipaddressCS = this.global.ipaddress3;
    this.showFont = 'uni';

    this.storage.get('profileImage').then((profileImage) => {
      ////console.log("this.storage.get('profileImage') constructor=" + JSON.stringify(profileImage));

      if (profileImage != undefined && profileImage != null && profileImage != '') {
        this.profileImageLink = profileImage;
        ////console.log("this.profileImageLink constructor=" + JSON.stringify(this.profileImageLink));
      } else {
        this.profileImageLink = this.global.digitalMediaProfile;
        ////console.log("this.profileImageLink constructor=" + JSON.stringify(this.profileImageLink));
      }
    });

    // this.profileImageLink = this.funct.profileImage;
    //first is upload/image/
    this.imageLink = this.global.imglink + "/uploads/smallImage/"; //+ "upload/image";
    //this.fileLink = this.global.imglink + "upload/image/";

    this.passData = this.navParams.get("data");
    this.from_Chat = this.navParams.get("from");
    this.docDetail = this.navParams.get("docData");
    this.description_Chat = this.navParams.get("description");
    this.imageDetail_Chat = this.navParams.get("imageDetail");
    this.userDataChat = this.navParams.get("sKeyDB");
    this.msgCarry = this.navParams.get('msg');
    console.log("this.passData constructor=" + JSON.stringify(this.passData));
    console.log("this.fromPage constructor=" + JSON.stringify(this.from_Chat));
    console.log("this.docDetail constructor=" + JSON.stringify(this.docDetail));
    console.log("this.description constructor=" + JSON.stringify(this.description_Chat));

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('font').then((font) => {
      this.keyboardfont = font;

      if (font == "zg" && this.language != "eng") {
        this.textData[0] = this.changefont.UnitoZg(this.textMyan[0]);
      } else if (font == "uni" && this.language != "eng") {
        this.textData[0] = this.textMyan[0];
      }
    });

    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;

      if (font == "zg" && this.language != "eng") {
        this.textData[0] = this.changefont.UnitoZg(this.textMyan[0]);
      } else if (font == "uni" && this.language != "eng") {
        this.textData[0] = this.textMyan[0];
      }
    });
  }

  changelanguage(lan) {
    this.language = lan;

    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }

      if (this.keyboardfont == 'zg') {
        this.textData[0] = this.changefont.UnitoZg(this.textMyan[0]);
      }
    }
  }

  selectData() {
    this.msgList = [];

    this.db.executeSql("SELECT * FROM ChatDetail WHERE channelkey = ?", [this.passData.channelkey]).then(
      (data) => {
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            this.msgList.push(JSON.parse(data.rows.item(i).chatData));
          }
          ////console.log("registerData = " + JSON.stringify(this.msgList));
          this.nores = 1;
          this.getConversion(this.start, '');
        } else {
          this.getConversion(this.start, '');
        }
      }, (error) => {
        //console.error("Unable to select data", error);
      });
  }

  insertData(data1) {
    for (let i = 0; i < data1.length; i++) {
      this.db.executeSql("INSERT INTO ChatDetail(channelkey, chatData) VALUES (?, ?)", [this.passData.channelkey, JSON.stringify(data1[i])]).then((data) => {
        ////console.log("Insert data successfully", data);
      }, (error) => {
        //console.error("Unable to insert data", error);
      });
    }
  }

  deleteData() {
    this.db.executeSql("DELETE FROM ChatDetail WHERE channelkey = ?", [this.passData.channelkey]).then((data) => {
      // //console.log("Delete data successfully", data);
      this.insertData(this.msgList);
    }, (error) => {
      //console.error("Unable to delete data", error);
    });
  }

  ionViewCanEnter() {
    this.storage.get('userData').then((userData) => {
      this.userDataChat = userData;
      this.storage.get('hautosys').then((data) => {
        this.hautosys = data.hautosys;

        if (this.from_Chat.fromName == 'MessageChooseChannelsPage' && this.docDetail != undefined) {
          this.centerCircle();
          this.stopBack();
          this.createDir(this.docDetail.PhotoPath);
        }
        if (this.from_Chat.fromName == 'PicsDetailsChannelPage' && this.imageDetail_Chat != undefined) {
          if (this.msgCarry != undefined && this.msgCarry != null && this.msgCarry != '' && this.msgCarry.trim() != '') {
            this.postMsg = this.msgCarry;
            this.msgCarry = '';
          }
          this.centerCircle();
          this.stopBack();
          this.createDir(this.imageDetail_Chat.PhotoPath);
        }
        //this.userData = this.userDataChat.sKey;
        ////console.log("mitChatData = " + JSON.stringify(this.userData));
        this.passData = this.navParams.get("data");
        this.groupname = "";
        this.groupnames = "";
        // if (this.passData.person == null) {
        //   this.passData.channelkey = 0;
        // }
        // else 
        if (this.passData.person != null && this.passData.person.length > 2) {
          this.isgroup = 1;
          for (let i = 0; i < this.passData.person.length; i++) {
            if (this.passData.person[i].syskey != this.userDataChat.sKey) {
              let arr = [];
              arr = this.passData.person[i].t2.split(" ");
              if (this.groupname == "")
                this.groupname = arr[0];
              else
                this.groupname = this.groupname + ", " + arr[0];
            }
          }
          //console.log("groupname=" + JSON.stringify(this.groupname))
        }

        if (this.groupname.length > 25) {
          this.groupnames = this.groupname.substring(0, 22) + "...";
        } else {
          this.groupnames = this.groupname;
        }

        //one to one
        this.role = this.passData.syskey + "-" + this.userDataChat.sKey;
        this.channelName = this.userDataChat.sKey + "-" + this.passData.syskey;
        this.usersyskey = '0';
        //to check
        this.userst = this.userDataChat.sKey;
        this.syskey = this.passData.syskey;
        this.start = 0;
        this.end = 0;
        this.conversionList = [];
        this.checkdorefresh = false;
        this.isLoading = true;
        //this.getConversion(this.start);

        ////console.log("ionViewCanEnter chat");

        this.sqlite.create({
          name: "mw.db",
          location: "default"
        }).then((db: SQLiteObject) => {
          ////console.log("success create or open database");
          this.db = db;
          this.db.executeSql('CREATE TABLE IF NOT EXISTS ChatDetail(channelkey TEXT, chatData TEXT)', {}).then(() =>
            console.log('Executed SQL'))
            .catch(e => console.log(e));
          this.selectData();
        }).catch(e => console.log(e));

      });
    });
  }

  getConversion(start, refresher) {
    if (this.checkdorefresh) {
      this.end = this.end + 10;
    } else {
      this.end = this.end + 4;
    }
    let msgparams = {
      start: start,
      end: this.end
    }

    // //console.log("request msg =" + JSON.stringify(msgparams));
    ////console.log("channelGroup=" + this.role + "&syskey=" + this.syskey + "&channelSyskey=" + this.passData.channelkey + "&channelName=" + this.channelName);

    this.http.post(this.ipaddressCS + "serviceChat/getConversationForAll?"
      + "channelGroup=" + this.role
      + "&syskey=" + this.syskey
      + "&channelSyskey=" + this.passData.channelkey
      + "&channelName=" + this.channelName
      + "&userSyskey=" + this.hautosys, msgparams).map(res => res.json()).subscribe(
        result => {
          ////console.log("response getConversationForAll =" + JSON.stringify(result));

          if (result.data.length > 0) {
            if (result.data[0].autokey != 0 && this.passData.channelkey == 0) {
              this.passData.channelkey = result.data[0].autokey;
              let syskeyOne = this.passData.syskey;
              let syskeyTwo = this.hautosys;
              let phoneOne = this.passData.phone;
              let phoneTwo = this.userDataChat.userID;
              let channelkey = this.passData.channelkey;
              let paramss = {
                syskeyOne: syskeyOne,
                syskeyTwo: syskeyTwo,
                phoneOne: phoneOne,
                phoneTwo: phoneTwo,
                channelkey: this.passData.channelkey
              }

              this.http.post(this.ipaddress + "/chatservice/updateChannelKey", paramss).map(res => res.json()).subscribe(result => {
                ////console.log(JSON.stringify(result));
              }, error => {
                ////console.log(JSON.stringify(error));

                this.getError(error);
              });
            }

            this.end = this.start + result.data.length;
            this.conversionList = result.data;
            this.getGroupData();
            this.nores = 1;
          }
          else {

            if (this.end > 10) {
              this.end = this.start - 1;
              if (refresher != '') {
                refresher.complete();
              }
              if (this.msgList.length > 0) {
                this.nores = 1;
              }
            }
            else {
              this.nores = 0;
            }

          }

          if (refresher != '') {
            ////console.log("refresher exist>>" + refresher);
            //this.events.publish('msgsend','msgsend')
            refresher.complete();
          } else {
            ////console.log("refresher not exist>>" + refresher);
            //this.scrollToBottom();
          }
          if (!this.checkdorefresh) {
            this.scrollToBottom();
          }
          this.isLoading = false;
        }, error => {
          this.nores = 0;
          if (this.msgList.length > 0)
            this.nores = 1;
          else
            this.nores = 0;
          if (refresher != '') {
            refresher.complete();
          }

          this.getError(error);

        });
  }

  sendMsg() {
    let from, to, sendsys, photo, userID, userName;
    if (this.postMsg != undefined && this.postMsg != null && this.postMsg != '' && this.postMsg.trim() != '') {
      this.status = true;
      this.scrollToBottom();
      this.postMsg = this.postMsg.trim();
      if (this.isgroup == 1) {
        from = this.userDataChat.sKey;
        to = this.passData.syskey;
        sendsys = this.userDataChat.sKey;
        this.usersyskey = '0';
      } else {
        // one to one
        from = this.userDataChat.sKey;
        to = this.passData.syskey;
        sendsys = this.passData.syskey;
      }
      let msgparams = {
        t2: this.postMsg, //message
        t3: '', //photo
        t5: from, //from (reg sykey)
        t6: to, // to
        t9: sendsys, // reg sykey //in most condition it is other people syskey
        t11: '',
        t12: this.global.type,
        t16: '',
        t17: '',
        t19: '',
        t20: 0,
        n1: this.usersyskey,
        n20: this.passData.channelkey,
        isGroup: this.isgroup,
        groupName: this.passData.t2,
        userSyskey: from
      }
      this.postMsg = '';
      ////console.log("request sendmsg =" + JSON.stringify(msgparams));
      this.http.post(this.ipaddressCS + "serviceChat/sendMessage1", msgparams).map(res => res.json()).subscribe(result => {
        // //console.log("response sendmsg =" + JSON.stringify(result));       
        if (result.data.length > 0) {
          this.conversionList = result.data;
          // //console.log("this.conversionList-sendMessageOne-Res:" + JSON.stringify(this.conversionList));
          if (this.msgList.length == 0) {
            this.getGroupData();
          } else {
            this.msgList[this.msgList.length - 1].data.push(this.conversionList[result.data.length - 1]);
          }
          this.content.resize();
          this.scrollToBottom();
          this.nores = 1;
          this.end = result.data.length;
          //this.postMsg = '';
          this.photos = [];
        } else {
          this.nores = 0;
          let toast = this.toastCtrl.create({
            message: 'Send failed! Please try again.',
            duration: 5000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
        }

        this.status = false;
      }, error => {
        ////console.log("signin error=" + error.status);
        this.status = false;
        this.nores = 0;
        this.getError(error);
      });
    }
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 400)
  }

  getQuotes(str) {
    if (str.indexOf("''") > -1) {
      let re = /''/gi;
      str = str.replace(re, "'");
    }
    return str;
  }

  getTransformDate(date) {
    let tranDate;
    let getdate;
    let month;
    let year;
    let day;

    year = date.slice(0, 4);
    month = date.slice(4, 6);
    day = date.slice(6, 8);

    switch (month) {
      case '01':
        month = 'Jan'
        break;
      case '02':
        month = 'Feb'
        break;
      case '03':
        month = 'Mar'
        break;
      case '04':
        month = 'Apr'
        break;
      case '05':
        month = 'May'
        break;
      case '06':
        month = 'Jun'
        break;
      case '07':
        month = 'Jul'
        break;
      case '08':
        month = 'Aug'
        break;
      case '09':
        month = 'Sep'
        break;
      case '10':
        month = 'Oct'
        break;
      case '11':
        month = 'Nov'
        break;
      case '12':
        month = 'Dec'
        break;
      default:

    }

    tranDate = month + " " + day + ", " + year;
    return tranDate;
  }

  getGroupData() {
    let group_to_values = this.conversionList.reduce(function (obj, item) {
      obj[item.t7] = obj[item.t7] || [];
      obj[item.t7].push(item);
      return obj;
    }, {});
    let groups = Object.keys(group_to_values).map(function (key) {
      return { t7: key, data: group_to_values[key] };
    });

    for (let i = 0; i < groups.length; i++) {
      groups[i].t7 = this.getTransformDate(groups[i].t7);
      for (let j = 0; j < groups[i].data.length; j++) {
        if (groups[i].data[j].t3.indexOf("''") > -1) {
          groups[i].data[j].t3 = this.getQuotes(groups[i].data[j].t3);
        }
      }
    }
    this.msgList = groups;
    this.deleteData();
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = "Can't connect right now. [" + code + "]";
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });
    //toast.present(toast);
    this.isLoading = false;
    this.dismiss();
    ////console.log("Oops!");
  }

  onFocus() {
    this.content.resize();
    this.scrollToBottom();
  }

  doRefresh(refresher) {
    ////console.log('Begin async operation', refresher);
    ////console.log('this.start=' + this.start);
    this.start = 0;
    this.checkdorefresh = true;
    this.getConversion(this.start, refresher);
    setTimeout(() => {
      //console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  getMenu(val) {
    this.clickopenMenu = true;
    this.count++;
    if (this.count == 2) {
      this.clickopenMenu = true;
    }
    else if (this.count % 2 == 0) {
      this.clickopenMenu = true;
      this.showEmojiPicker = false;
    }
    else if (this.count % 2 != 0) {
      this.clickopenMenu = false;
      this.showEmojiPicker = false;
    }
    else {

    }
  }

  //if success do copyFile()
  //comment=
  // Save photo in WalletChatPhoto directory when
  // 1. user takes photo via channel
  // 2. photo comes from gallery via channel
  // Save photo in file.dataDirectory (it is invisible by searching manually in phone) in order to show in Pics Page
  createDir(result) {
    let targetPath1;
    if (this.platform.is('ios')) {
      targetPath1 = this.file.documentsDirectory;
    } else {
      targetPath1 = this.file.externalApplicationStorageDirectory;
    }
    //console.log("targetPath1 = " + targetPath1);
    //in android
    //targetPath1 = "file:///storage/emulated/0/Android/data/dc.xxx/"
    this.file.checkDir(targetPath1, 'WalletChatPhoto').then(() => {
      //console.log('WalletChatPhoto Directory exists');

      this.copyFile(result, targetPath1);
    }, (error) => {
      //console.log('WalletChatPhoto Directory does not exist');

      this.file.createDir(targetPath1, "WalletChatPhoto", true).then(() => {
        //console.log("WalletChatPhoto Directory created");

        this.copyFile(result, targetPath1);
      }, (error) => {
        //console.log("WalletChatPhoto Directory creating failed" + JSON.stringify(error));
      });
    });
  }

  //if success do uploadImage()
  copyFile(result, targetPath1) {
    let dmyhmssk = this.util.getImageName();
    let tmpAry = result.split('/').pop().split(".");
    this.fileName = tmpAry[0] + "_" + dmyhmssk + "." + tmpAry[1];
    let sourceDirectory = result.substring(0, result.lastIndexOf('/') + 1);
    let sourceFileName = result.substring(result.lastIndexOf('/') + 1, result.length);
    //console.log("this.fileName = " + this.fileName);
    //console.log("sourceDirectory = " + sourceDirectory);
    //console.log("sourceFileName = " + sourceFileName);
    //console.log("this.file.copyFile=" + sourceDirectory + ", " + sourceFileName + ", " + targetPath1 + 'WalletChatPhoto/' + ", " + this.fileName);
    this.file.copyFile(sourceDirectory, sourceFileName, targetPath1 + 'WalletChatPhoto/', this.fileName).then(file => {
      this.chatphoto = targetPath1 + 'WalletChatPhoto/' + this.fileName;
      this.photos.push(this.chatphoto);
      //console.log("copied photo in WalletChatPhoto Directory" + JSON.stringify(file));
      //console.log("photos=" + JSON.stringify(this.photos));

      if (this.photos.length > 0) {
        this.uploadImage();
        this.clickopenMenu = false;
      }
    }, (err) => {
      //console.log("fail copied photo in WalletChatPhoto Directory" + JSON.stringify(err));
    });
  }

  //if success do sendPhoto()
  /*upload image*/
  //url = "https://chatting.azurewebsites.net/api/v1/" + 'file/mobileupload'
  //chat service
  //this.ipaddress3 = "https://chatting.azurewebsites.net/api/v1/";
  uploadImage() {
    //console.log("orimage=" + this.photos[0]);
    var fileName = this.photos[0].split("/").pop();
    var url = this.ipaddressCS + 'file/fileupload?f=uploads&fn=' + fileName;
    //const fileTransfer: TransferObject = this.transfer.create();
    const fileTransfer: FileTransferObject = this.transfer.create();
    var options = {
      fileKey: "file",
      fileName: fileName,
      chunkedMode: false,
      mimeType: "multipart/form-data",
    };
    //console.log("go UrL photos=" + JSON.stringify(this.photos) + "upload url=" + url + "options=" + JSON.stringify(options));
    fileTransfer.upload(this.photos[0], url, options).then((data) => {
      this.imageLinkFolder = JSON.parse(data.response);
      if (data.responseCode === 200) {
        if (this.from_Chat.fromName == 'PicsDetailsChannelPage' && this.imageDetail_Chat != undefined && this.passData.t16 == "admin") {
          this.msgParam = this.description_Chat.description;
          this.docDetail = this.imageDetail_Chat;
          this.sendPhoto();
        }
        if (this.from_Chat.fromName == 'MessageChooseChannelsPage' && this.docDetail != undefined && this.passData.t16 == "admin") {
          this.msgParam = this.description_Chat.description;
          this.sendPhoto();
        }
        if (this.passData.t16 != "admin" && this.isUserTakenPhoto == "yes") {
          this.sendPhoto();
        }
      }
    }, (error) => {
      //console.log("upload done with error");
      //console.log("signin error=" + JSON.stringify(error));
      this.getError(error);
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 5000,
      dismissOnPageChange: true,
      position: 'top'
    });
    toast.present();
  }

  sendPhoto() {
    this.storage.get('hautosys').then((data) => {
      this.storage.get('uari').then((results) => {
        //console.log("this.storage.get('hautosys') constructor=" + JSON.stringify(data));
        if (data.hautosys != undefined && data.hautosys != null && data.hautosys != '' && data.hautosys.length != 0) {
          this.hautosys = data.hautosys;
          let from = this.hautosys;
          let senderPhone, senderName, latAndLong, shootDate, shootTime, msgForPhoto;
          if (this.passData.t16 == "admin") {
            senderPhone = results.phone;
            senderName = results.username;
            latAndLong = this.docDetail.latitude + "/" + this.docDetail.longitude;
            shootDate = this.docDetail.CreateDate;
            shootTime = this.docDetail.CreateTime;
            msgForPhoto = this.msgParam;
          }
          //console.log("this.hautosys=" + JSON.stringify(this.hautosys));
          let d = this.util.getImageName();
          if (!this.clickopenMenu && this.from_Chat.fromName != 'MessageChooseChannelsPage') {
            //this.messageInput.setFocus();
          }//wkk
          this.status = true;
          let to, sendsys, photo;
          if (this.isgroup == 1) {
            //console.log("if (this.isgroup == 1)=" + this.isgroup);
            to = this.passData.syskey;//fcm old
            //to = this.passData.syskey;//new
            sendsys = this.hautosys;
            this.usersyskey = '0';

            //console.log('if (this.isgroup == 1) to=' + to);
            //console.log('if (this.isgroup == 1) sendsys=' + sendsys);
          } else {
            if (this.passData.t16 == "admin") {
              //console.log("if (this.passData.t16 == admin)=" + JSON.stringify(this.passData));
              to = this.passData.syskey;
              sendsys = this.passData.syskey;
              //console.log('else (this.passData.t16 == doublecodedoublecode) to=' + to);
              //console.log('else (this.passData.t16 == doublecodedoublecode) sendsys=' + sendsys);
            } else {
              //console.log("else (this.passData.t16 == admin)=" + JSON.stringify(this.passData));

              if (this.passData.t16 == "") {
                to = this.passData.syskey;
                sendsys = this.passData.syskey;
                //console.log('else (this.passData.t16 == admin) if (this.passData.t16 == doublecodedoublecode) to=' + to);
                //console.log('else (this.passData.t16 == admin) if (this.passData.t16 == doublecodedoublecode) sendsys=' + sendsys);
              }
            }
          }
          let t20 = "";
          if (this.passData.t16 == "admin") {
            t20 = "1";
          } else {
            t20 = "0";
          }
          /*wkk*/
          let msgparams;
          if (this.photos.length > 0) {
            let t16 = "";
            let t17 = "";
            let t19 = "";
            if (this.passData.t16 == "admin") {
              if (this.docDetail != undefined && this.docDetail != null) {
                t16 = this.docDetail.latitude + "/" + this.docDetail.longitude;
                t17 = this.docDetail.CreateDate;
                t19 = this.docDetail.CreateTime;
              }
              this.msgSend = this.msgParam;
            } else {
              this.msgSend = '';
            }
            //console.log(this.imageLinkFolder.filePath);
            msgparams = {
              t2: this.msgSend, //message
              t3: this.imageLinkFolder.filePath,//this.photos[0].substr(this.photos[0].lastIndexOf('/') + 1), //photo
              t5: from, //from (reg sykey) //user's syskey but may be R0001 in one case
              t6: to, // to //in most condition it is other people syskey
              t9: sendsys, // reg sykey //in most condition it is other people syskey
              t11: d,//e.g.1452018143321
              t12: "android",
              t15: "",
              t16: t16,//=>latitude/longitude
              t17: t17,//CreateDate
              t19: t19,//CreateTime
              t20: t20,
              n1: this.usersyskey,
              isGroup: this.isgroup,
              n20: this.passData.channelkey,
              groupName: this.passData.t2,
              // userId: this.util.removePlus(this.userDataChat.userID),
              // userName: this.userDataChat.name
            }
            //console.log("request sendmsg =" + JSON.stringify(msgparams));
          }

          this.http.post(this.ipaddressCS + "serviceChat/sendMessage1", msgparams).map(res => res.json()).subscribe(result => {
            /* if (this.passData.t16 == "admin") {
              var url = this.ipaddress + '/serviceTicket/mobileupload';
              var fileName = this.photos[0].split("/").pop();

              if (fileName != undefined && fileName != '' && fileName != null) {
                let options: FileUploadOptions = {
                  fileKey: 'file',
                  fileName: fileName,
                  chunkedMode: false,
                }

                const fileTransfer: TransferObject = this.transfer.create();

                fileTransfer.upload(this.photos[0], url, options).then(
                  (data) => {
                    //console.log("uploadPhoto upload success" + JSON.stringify(data));
                    let parameter = {
                      t2: this.global.region,//region
                      t3: from,//sender
                      t4: to,//receiver
                      t5: senderName,//senderName
                      t6: this.passData.channelkey,//channelKey
                      t7: msgForPhoto,//message
                      t8: latAndLong,//location
                      t9: senderPhone,//senderPhone
                      t11: result.syskey,//messageSyskey
                      t12: this.global.regionCode,//regionCode
                      t13: fileName,//photoName
                      t14: data.response,//photoPath
                      date: result.data[0].t7,//date
                      time: result.data[0].t8,//time
                      shootdate: shootDate,//shootDate
                      shoottime: shootTime//shootTime
                    };

                    this.http.post(this.ipaddress + '/serviceTicket/insertTicket', parameter).map(res => res.json()).subscribe(
                      result => {
                        //console.log("response for insert ticket:" + JSON.stringify(result));

                        if (result != null && result.messageCode != null && result.messageCode == "0000") {
                          let from1, to1, sendsys1;
                          if (this.isgroup == 1) {
                            from1 = this.passData.syskey;
                            to1 = this.userDataChat.sKey;
                            sendsys1 = this.passData.syskey;
                            this.usersyskey = '0';
                          } else {
                            // one to one
                            from1 = this.passData.syskey;
                            to1 = this.userDataChat.sKey;
                            sendsys1 = this.userDataChat.sKey;
                          }
                          let msgparams = {
                            t2: 'Ticket Number : '+result.t1, //message
                            t3: '', //photo
                            t5: from1, //from (reg sykey)
                            t6: to1, // to
                            t9: sendsys1, // reg sykey //in most condition it is other people syskey
                            t11: '',
                            t12: this.global.type,
                            t16 : '',
                            t17 : '',
                            t19 : '',
                            t20 : 0,
                            n1: this.usersyskey,
                            n20: this.passData.channelkey,
                            isGroup: this.isgroup,
                            groupName: this.passData.t2,
                            userSyskey : from1
                          }
                          //console.log("request sendmsg =" + JSON.stringify(msgparams));

                          this.http.post(this.ipaddressCS + "serviceChat/sendMessage1", msgparams).map(res => res.json()).subscribe(result => {
                            //console.log("response sendmsg =" + JSON.stringify(result));

                            if (result.totalCount != "0") {
                              let tempArray = [];
                              if (!Array.isArray(result.data)) {
                                tempArray.push(result.data);
                                result.data = tempArray;
                              }
                              if (result.data.length > 0) {
                                this.conversionList = result.data;
                                //console.log("this.conversionList-sendMessageOne-Res:" + JSON.stringify(this.conversionList));
                                this.msgList = [];
                                this.postMsg = '';
                                this.photos = [];
                                this.content.resize();
                                this.getGroupData();
                                this.nores = 1;
                                this.end = result.data.length;
                                this.getConversion(0);
                                this.end = 0;
                                this.scrollToBottom();
                              }
                            } else {
                              this.nores = 0;
                              //console.log("send fail");
                            }
                    
                            this.status = false;
                          }, error => {
                            //console.log("signin error=" + JSON.stringify(error));
                            this.status = false;
                            this.nores = 0;
                            this.getError(error);
                          });
                        }
                      },
                      error => {
                        //console.log("error for insert ticket:" + JSON.stringify(error));
                        this.getError(error);
                      }
                    );
                  },
                  error => {
                    //console.log("uploadPhoto upload error" + JSON.stringify(error));
                  }
                );
              } else {
                //console.log("uploadPhoto upload error" + "Upload failed.");
              }
            } */

            this.clearFrm();
            this.from_Chat.fromName = '';
            this.openBack();
            //console.log("response sendmsg =" + JSON.stringify(result));
            if (result.data.length > 0) {
              this.imageDetail_Chat = '';
              this.isUserTakenPhoto = "";
              this.docDetail = '';
              this.photos = [];
              this.files = [];

              this.getConversion(this.start, '');
              /* this.conversionList = result.data;
              this.msgList[this.msgList.length - 1].data.push(this.conversionList[result.data.length - 1]);
              this.content.resize();
              this.scrollToBottom();
              this.nores = 1;
              this.end = result.data.length; */

              // this.getGroupData();
              //  this.getConversion(0);
              // this.getGroupData();

              /*  this.end = result.data.length;
              this.getConversion(0);
              this.end = 0;
              this.scrollToBottom(); */
            } else {
              let toast = this.toastCtrl.create({
                message: 'Send Failed! Please try again.',
                duration: 5000,
                position: 'bottom',
                //  showCloseButton: true,
                dismissOnPageChange: true,
                // closeButtonText: 'OK'
              });
              toast.present(toast);
            }

            this.status = false;
            this.dismiss();
          }, error => {
            this.clearFrm();
            this.from_Chat.fromName = '';
            this.openBack();
            this.dismiss();
            this.imageDetail_Chat = '';
            //this.description_Chat.description = '';
            this.isUserTakenPhoto = "";
            //console.log("signin error=" + error.status);
            this.status = false;
            this.nores = 0;
            this.getError(error);
          });
        } else {
          //console.log("error in getting syskey hkey autokey");
        }
      });
    });
  }

  clearFrm() {
    this.msgParam = '';
    this.msgSend = '';
  }

  deletePhoto(index) {
    this.photos.splice(index, 1);
    //console.log("signin error=" + JSON.stringify(this.photos));
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    //comment=creating directory is file:///storage/emulated/0/Android/data/dc.xxx/(targetPath1)
    //existing directory may be file:///storage/emulated/0/Android/data/dc.xxx/WalletChatPhoto
    let targetPath1;
    if (this.platform.is('ios')) {
      targetPath1 = this.file.documentsDirectory;
    } else {
      targetPath1 = this.file.externalApplicationStorageDirectory;
    }
    //console.log("targetPath1 = " + targetPath1);
    this.file.checkDir(targetPath1, 'WalletChatPhoto').then(() => {
      //console.log('WalletChatPhoto Directory exists in Normal User.');
    }, (error) => {
      //console.log('WalletChatPhoto Directory does not exist in Normal User.', JSON.stringify(error));

      this.file.createDir(targetPath1, "WalletChatPhoto", true).then(() => {
        //console.log("WalletChatPhoto Directory created in Normal User.");
      }, (error) => {
        //console.log('WalletChatPhoto Directory while creating in Normal User.', JSON.stringify(error));
      });
    });

    //comment=copy photo from gallery from user's phone in this directory
    //this directory is file:///storage/emulated/0/Android/data/dc.xxx/WalletChatPhoto
    this.file.copyFile(namePath, currentName, targetPath1 + 'WalletChatPhoto/', newFileName).then(file => {
      this.chatphoto = targetPath1 + 'WalletChatPhoto/' + newFileName;
      this.photos.push(this.chatphoto);
      //console.log("copied photo from gallery from user's phone in WalletChatPhoto Directory" + JSON.stringify(file));
      //console.log("photos=" + JSON.stringify(this.photos));

      if (this.photos.length > 0) {
        this.uploadImage();
        this.clickopenMenu = false;
      }
    }, (err) => {
      //console.log("fail copied photo from gallery from user's phone in WalletChatPhoto Directory" + JSON.stringify(err));
    });
  }

  createPhotoName() {
    let photoName = this.util.getImageName() + this.hautosys + ".jpg";
    this.photoName = this.util.getImageName() + this.hautosys + ".jpg";
    return photoName;
  }

  insertPhoto() {
    let date = new Date();
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"];
    const weekNames = ["SUN", "MON", "TUE", "WED", "THUR", "FRI", "SAT"];
    let month = monthNames[date.getMonth()];
    this.createdDate = month + "-" + date.getDate() + "-" + date.getFullYear();
    let wDay = weekNames[date.getDay()];
    let hoursMinutes = [date.getHours(), date.getMinutes()];
    this.createdTime = wDay + " " + this.formatAMPM(hoursMinutes);
    //console.log("time " + this.createdTime);
    this.db.executeSql("INSERT INTO DOCPHOTO(PhotoPath,Name,Latitude,Longitude,Location,CreateDate,CreateTime) VALUES (?,?,?,?,?,?,?)",
      [this.docData, this.photoName, this.latitude, this.longitude, this.location, this.createdDate, this.createdTime]).then((data) => {
        //console.log("Insert data successfully", data);
        this.docDetail = { PhotoPath: this.docData, name: this.photoName, latitude: this.latitude, longitude: this.longitude, location: this.location, CreateDate: this.createdDate, CreateTime: this.createdTime };
        this.goPicDetailChannelPage(this.docDetail);
      }, (error) => {
        console.error("Unable to insert data", error);
      });
  }

  formatAMPM(date) {
    var hours = date[0];
    var minutes = date[1];
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  getLocation() {
    let options = {
      timeout: 30000
    };

    this.geolocation.getCurrentPosition(options).then((resp) => {
      //console.log(' getting latitude ', JSON.stringify(resp));
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      //console.log(' getting latitude ', resp.coords.latitude);
      //console.log(' getting longitude ', resp.coords.longitude);
      this.nativeGeocoder.reverseGeocode(this.latitude, this.longitude)
        .then((result: NativeGeocoderReverseResult[]) => {
          this.location = result[0].locality + ", " + result[0].countryName;
          //console.log(JSON.stringify(result))
        }, (err) => {
          this.dismiss();
          let toast = this.toastCtrl.create({
            message: "Please take photo again. Please turn on location access.",
            duration: 5000,
            position: 'bottom',
            //showCloseButton: true,
            dismissOnPageChange: true,
            //closeButtonText: 'OK'
          });
          toast.present(toast);
          //console.log('Error getting location', JSON.stringify(err));
        });
      // )
      //   .catch((error: any) => //console.log(error));
    }, (err) => {
      this.dismiss();
      let toast = this.toastCtrl.create({
        message: "Please take photo again. Please turn on location access.",
        duration: 5000,
        position: 'bottom',
        //showCloseButton: true,
        dismissOnPageChange: true,
        //closeButtonText: 'OK'
      });
      toast.present(toast);
      //console.log('Error getting location', JSON.stringify(err));
    });/* )
        .catch((error: any) => //console.log(error));
    }).catch((error) => {
      //console.log('Error getting location', error);
    }); */
  }

  getLocationSecondTime() {
    let options = {
      timeout: 30000
    };

    this.geolocation.getCurrentPosition(options).then((resp) => {
      //console.log(' getting latitude ', JSON.stringify(resp));
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      //console.log(' getting latitude ', resp.coords.latitude);
      //console.log(' getting longitude ', resp.coords.longitude);
      this.nativeGeocoder.reverseGeocode(this.latitude, this.longitude)
        .then((result: NativeGeocoderReverseResult[]) => {
          this.location = result[0].locality + ", " + result[0].countryName;
          this.insertPhoto();
          //console.log(JSON.stringify(result))
        }, (err) => {
          this.dismiss();
          let toast = this.toastCtrl.create({
            message: "Please take photo again. Please turn on location access.",
            duration: 5000,
            position: 'bottom',
            //showCloseButton: true,
            dismissOnPageChange: true,
            //closeButtonText: 'OK'
          });
          toast.present(toast);
          //console.log('Error getting location', JSON.stringify(err));
        });
      // )
      //   .catch((error: any) => //console.log(error));
    }, (err) => {
      this.dismiss();
      let toast = this.toastCtrl.create({
        message: "Please take photo again. Please turn on location access.",
        duration: 5000,
        position: 'bottom',
        //showCloseButton: true,
        dismissOnPageChange: true,
        //closeButtonText: 'OK'
      });
      toast.present(toast);
      //console.log('Error getting location', JSON.stringify(err));
    });
    /* })
    .catch((error: any) => //console.log(error));
}).catch((error) => {
  //console.log('Error getting location', error);
}); */
  }

  createFileNameForPicListPage() {
    let d = new Date().getTime();
    // let n = d.getTime();
    let newFileName = "Doc-" + d + ".jpg";
    return newFileName;
  }

  /* presentPopover(ev) {
    let title = this.passData.name || this.passData.t2;

    if (title == "Group") {
      this.createPopoverChat.presentPopover(ev);
    } else {
      this.createPopoverChat.presentPopover(ev);
    }
  } */

  addPerson() {
    this.navCtrl.push(SelectParticipantPage, {
      data: this.passData
    });
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(MyPopOverListChatPage, {}, { cssClass: 'custom-popover' })
    //this.popover = this.popoverCtrl.create(PopoverForGroupPage, {});
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      //console.log("popover dismissed");
      //console.log("Selected Item is " + data);
      if (data == "0") {
        this.changeGruopName();
      }
      else if (data == "1") {
        this.addPerson();
      }
      else if (data == "2") {
        this.navCtrl.push(ViewGroupPage, {
          data: this.passData
        });
      }
      else if (data == "3") {
        this.alertPopup = this.alertCtrl.create({
          cssClass: 'uni',
          message: 'Are you sure want to leave group?',
          enableBackdropDismiss: false,
          buttons: [{
            text: "Cancel",
            role: 'cancel',
            handler: () => {
            }
          }, {
            text: "OK",
            handler: () => {
              //leave group
              this.leaveGroup();
            }
          }]
        })
        this.alertPopup.present();
        let doDismiss = () => this.alertPopup.dismiss();
        let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
        this.alertPopup.onDidDismiss(unregBackButton);
      } if (data == "4") {
        this.presentPopoverLanguage(ev);
      }
      if (data == "5") {
        this.presentPopoverFont(ev);
      }
      if (data == "6") {
        this.goLogout();
      }
    });
  }

  goLogout() {
    let alert = this.alertCtrl.create({
      title: 'Are you sure you want to exit?',
      enableBackdropDismiss: false,
      message: '',
      buttons: [{
        text: 'No',
        handler: () => {
        }
      },
      {
        text: 'Yes',
        handler: () => {
          // this.storage.remove('phonenumber');
          this.storage.remove('username');
          this.storage.remove('userData');
          this.storage.remove('firstlogin');
          this.appCtrl.getRootNav().setRoot(Login);
          this.deleteLocalSQLiteDB();
        }
      }]
    });
    alert.present();
  }

  deleteLocalSQLiteDB() {
    this.sqlite.deleteDatabase({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      //console.log("success drop database");
    }, (error) => {
      console.error("Unable to open database", error);
    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(Language, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      //console.log("Selected Item is " + data);
    });
  }

  presentPopoverFont(ev) {
    this.popover = this.popoverCtrl.create(FontPopoverPage, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      //console.log("Selected Item is " + data);
    });
  }

  leaveGroup() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();

    let url = this.ipaddressCS + 'serviceChat/deleteGroup?sysKey=' + this.passData.channelkey
      + '&registerKey=' + this.hautosys
      + '&Group=1';

    //console.log("url=" + url);

    this.http.get(url).map(res => res.json()).subscribe(data => {
      //console.log("response leave group== " + JSON.stringify(data));

      if (data.state) {
        this.presentToast(data.msgDesc);
        this.navCtrl.pop();
      }

      this.loading.dismiss();
    }, error => {
      this.presentToast('Deleted Fail!');
      //console.log("Leave group error==" + error.status);
      this.loading.dismiss();
      this.getError(error);
    });
  }

  changeGruopName() {
    let prompt = this.alertCtrl.create({
      title: this.passData.t2,
      //  message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'groupName',
          placeholder: 'Change Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            //console.log('Saved clicked');
            this.updateGroupName = data.groupName;
            this.changeName();
          }
        }
      ]
    });
    prompt.present();
  }

  changeName() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    let params = {
      autokey: this.passData.channelkey,//this.msgList.channelkey,
      groupName: this.updateGroupName
    }
    //console.log("request changeChannelName =" + JSON.stringify(params));
    this.http.post(this.ipaddressCS + "serviceChat/changeChannelName", params).map(res => res.json()).subscribe(result => {
      //console.log("response changeChannelName =" + JSON.stringify(result));
      if (result.state) {
        this.passData.t2 = this.updateGroupName
      }
      else {
        let toast = this.toastCtrl.create({
          message: 'Updated Failed! Please try again.',
          duration: 5000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
      }
      this.loading.dismiss();
    }, error => {
      //console.log("signin error=" + error.status);
      this.loading.dismiss();
      this.getError(error);
    });
  }

  ionViewDidLoad() {
    let params = { tabIndex: 1, tabTitle: MessagePage };
    if (this.from_Chat.fromName == 'MessageChooseChannelsPage') {
      this.navBar.backButtonClick = (e: UIEvent) => {
        // todo something
        //this.navCtrl.setRoot(TabsPage, params);
        //this.navCtrl.pop();
        this.appCtrl.getRootNav().setRoot(TabsPage, params);
      }
    }

    if (this.from_Chat.fromName == 'PicsDetailsChannelPage') {
      this.navBar.backButtonClick = (e: UIEvent) => {
        // todo something
        //this.navCtrl.setRoot(TabsPage, params);
        this.appCtrl.getRootNav().setRoot(TabsPage, params);
        //this.navCtrl.pop();
      }
    }
  }

  goGoogleMap(location) {
    let string = location.replace(/,/g, '/');
    window.open('geo://?q=' + string + '(' + location + ')', '_system');
  }

  viewImage(msg) {
    let imageLinkOne = this.imageLink + msg;
    this.navCtrl.push(ViewPhotoMessagePage, {
      data: imageLinkOne,
      contentImg: "singlePhoto"
    });
  }

  info() {
    let profileimg = '';
    let type = '';
    if (this.passData.t16 == 'admin' && (this.passData.t18 == '')) {
      profileimg = 'assets/channelAdmin.png';
      type = 'admin';
    } else {
      profileimg = this.passData.t18;
      type = 'user';
    }
    let contactInfo = {
      syskey: this.passData.syskey,
      phone: this.passData.t1 || this.passData.phone,
      name: this.passData.t2 || this.passData.name,
      image: profileimg,
      type: type
    };
    this.navCtrl.push(ContactInfoPage, {
      data: contactInfo
    });
  }

  url(string) {
    var re = new RegExp("^(http|https)://", "i");
    // var re = new RegExp("(?:(?:(?:ht|f)tp)s?://)?[\\w_-]+(?:\\.[\\w_-]+)+([\\w.,@?^=%&:/~+#-]*[\\w@?^=%&/~+#-])?");
    return re.test(string);
  }

  goBrowser(url) {

  }

  showMessageForNotAllowMoreThanOnePhoto() {
    let confirm = this.alertCtrl.create({
      title: 'Warning',
      message: 'You can upload one photo.',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            //console.log('Yes clicked');
            this.clickopenMenu = false;
          }
        }
      ]
    });

    confirm.present();
  }

  goCameraUI() {
    if (this.photos.length > 0 || this.files.length > 0) {
      this.showMessageForNotAllowMoreThanOnePhoto();
    } else {

      if (this.passData.t16 != 'admin') {
        this.isUserTakenPhoto = "yes";
      } else {
        this.getLocation();
      }

      this.goCamera();
    }
  }

  goGalleryUI() {
    if (this.photos.length > 0 || this.files.length > 0) {
      this.showMessageForNotAllowMoreThanOnePhoto();
    } else {
      if (this.passData.t16 == 'admin') {
        this.goGalleryAdmin();
      } else {
        this.isUserTakenPhoto = "yes";
        this.goGalleryUser();
      }
    }
  }

  loadingCCP() {
    this.loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Processing...',
    });

    this.loading.present();
  }

  goCamera() {
    const options: CameraOptions = {
      quality: 100,
      //allowEdit: true,
      targetWidth: 600,
      targetHeight: 600,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    this.camera.getPicture(options).then((imageData) => {
      //console.log('Camera success>>' + imageData);

      if (this.passData.t16 == 'admin') {
        this.loadingCCP();
        this.savePhotoInPicsList(imageData);
      } else {
        this.centerCircle();
        this.stopBack();
        this.createDir(imageData);
      }

    }, (err) => {
      console.error("Unable to take picture!", err);
      //this.presentToast('Error while taking image!');
    });
  }

  savePhotoInPicsList(imageData) {
    let sourceDirectory = imageData.substring(0, imageData.lastIndexOf('/') + 1);
    let sourceFileName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.length);
    /* comment=the following means if user takes photo via channel, save photo for pic list page */
    this.photoName = this.createFileNameForPicListPage();
    this.file.copyFile(sourceDirectory, sourceFileName, this.file.dataDirectory, this.photoName).then(success => {
      //console.log("save photo for pic list page success=" + JSON.stringify(success));
      this.docData = this.file.dataDirectory + this.photoName;
      this.createDBForPICLIST();
    }, error => {
      //console.log("save photo for pic list page fail=" + JSON.stringify(error));
    });
  }

  createDBForPICLIST() {
    this.db.executeSql('CREATE TABLE IF NOT EXISTS DOCPHOTO(id INTEGER PRIMARY KEY AUTOINCREMENT,PhotoPath TEXT,Name TEXT,Latitude TEXT,Longitude TEXT,Location TEXT,CreateDate TEXT,CreateTime TEXT)', {})
      .then((success) => {
        if (this.latitude != undefined && this.latitude != '' && this.longitude != undefined && this.longitude != '') {
          this.insertPhoto();
        } else {
          this.getLocationSecondTime();
        }
        //console.log('Executed SQL success')
      }, (error) => {
        console.error("Unable to create database", error);
      })
  }

  goGalleryAdmin() {
    this.imageDetail_Chat = null;
    this.from_Chat.fromName = 'ChatPage';
    this.description_Chat.description = '';
    if (this.postMsg != undefined && this.postMsg != null && this.postMsg != '' && this.postMsg.trim() != '') {
      this.msgCarry = this.postMsg.trim();
    } else {
      this.msgCarry = '';
    }

    this.navCtrl.push(PicsChannelPage, {
      imageDetail: this.imageDetail_Chat,
      from: this.from_Chat,
      description: this.description_Chat,
      data: this.passData,
      msg: this.msgCarry
    });
  }

  goPicDetailChannelPage(imageDetailParam) {
    //in order to be that data in chat is equal data in mydocs-detail-channel
    this.imageDetail_Chat = imageDetailParam;
    this.from_Chat.fromName = 'ChatPage';
    this.description_Chat.description = '';
    //we can set null into this.docDetail
    //this.docDetail=null;
    this.dismiss();
    this.navCtrl.push(PicsDetailsChannelPage, {
      imageDetail: this.imageDetail_Chat,
      from: this.from_Chat,
      description: this.description_Chat,
      data: this.passData
    });
  }

  openBack() {
    this.stopBackBtn = '';
    let navbar = this.navBar;
    let backbutton = navbar.getNativeElement().querySelector('.back-button');
    if (backbutton !== undefined && backbutton !== null) {
      backbutton.removeAttribute("disabled");
    }
  }

  stopBack() {
    this.stopBackBtn = 'yes';
    let navbar = this.navBar;
    let backbutton = navbar.getNativeElement().querySelector('.back-button');
    if (backbutton !== undefined && backbutton !== null) {
      backbutton.setAttribute("disabled", true);
    }
  }

  goGalleryUser() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      //allowEdit: true,
      targetWidth: 600,
      targetHeight: 600,
    }

    this.camera.getPicture(options).then((imageData) => {
      this.centerCircle();
      this.stopBack();
      //console.log("goGalleryUser getPicture success:", JSON.stringify(imageData));

      /* acmy */
      let currentName = '';
      let correctPath = '';

      if (imageData.includes("?")) {
        currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
        correctPath = imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      } else {
        currentName = imageData.substring(imageData.lastIndexOf('/') + 1);
        correctPath = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      }

      //console.log("result of copyFileToLocalDir:" + correctPath + "//" + currentName + "//" + this.createPhotoName());
      this.copyFileToLocalDir(correctPath, currentName, this.createPhotoName());
      //this.moveFile(imageData);
    }, (error) => {
      //console.log("goGallery getPicture error:", JSON.stringify(error));

      let toast = this.toastCtrl.create({
        message: "Choosing photo is not successful.",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
    });
  }

  centerCircle() {
    this.loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Sending...',
    });

    this.loading.present();
  }

  circleDelete() {
    this.loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Deleting...',
    });

    this.loading.present();
  }

  dismiss() {
    this.loading.dismissAll();
  }

  doDelete(msg, j, z) {
    this.circleDelete();
    let msgparams = {
      t1: this.hautosys,
      syskey: msg.syskey,
      autokey: msg.autokey
    }
    //console.log("request deletemsg =" + JSON.stringify(msgparams));

    this.http.post(this.ipaddressCS + "serviceChat/messageDelete", msgparams).map(res => res.json()).subscribe(result => {
      //console.log("response deletemsg =" + JSON.stringify(result));

      if (result.state) {
        this.msgList[j].data.splice(z, 1);
        this.dismiss();
      } else {
        this.dismiss();
        let toast = this.toastCtrl.create({
          message: 'Deleted failed! Please try again.',
          duration: 5000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
      }
    }, error => {
      this.dismiss();
      let toast = this.toastCtrl.create({
        message: 'Delete failed! Please try again.',
        duration: 5000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
      //console.log("deletemsg error=" + JSON.stringify(error));
      this.getError(error);
    });
  }

  onPress(ev, msg, j, z) {
    this.popover = this.popoverCtrl.create(PopoverMsgPage, {
      data: msg,
      group: this.isgroup
    });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      //console.log("popover dismissed");
      //console.log("Selected Item is " + data);
      if (data == "1") {
        this.clipboard.copy(msg.t2);
      }
      else if (data == "2") {
        this.delMsg(msg, j, z);
      }
      else if (data == "3") {
        this.viewImage(msg.t3);
      }
      else if (data == "4") {
        this.DownloadIMG(msg.t3);
      }
    });
  }

  DownloadIMG(image) {
    image = this.imageLink + image;
    //console.log("dowload image == " + JSON.stringify(image));
    this.loading = this.loadingCtrl.create({
      content: 'Saving...',
    });
    this.loading.present();
    //console.log("this.file.externalRootDirectory == " + this.file.externalRootDirectory);
    this.file.checkDir(this.file.externalRootDirectory + 'DCIM/', 'NSBPay').then((data) => {
      //console.log('Directory exists');
      this.imageDownload(image);

    }, (error) => {
      //console.log('Directory doesnt exist');
      this.file.createDir(this.file.externalRootDirectory + 'DCIM/', 'NSBPay', false).then((res) => {
        //console.log("file create success");
        this.imageDownload(image);
      }, (error) => {
        //console.log("file create error");
      });
    });
  }

  imageDownload(image) {
    let url = image;
    let photo = image.substring(image.lastIndexOf('/') + 1, image.length);
    //let url = image;
    //const fileTransfer: TransferObject = this.transfer.create();
    const fileTransfer: FileTransferObject = this.transfer.create();
    var targetPath = this.file.externalRootDirectory + 'DCIM/NSBPay/' + photo;
    fileTransfer.download(url, targetPath).then((entry) => {
      //console.log('download complete: ' + entry.toURL());
      this.presentToast("Saving completed");
      this.loading.dismissAll();
      //this.viewFile(image);

    }, (error) => {
      this.presentToast("Download Fail!Check your file extension name.")
      //console.log('download error: ' + JSON.stringify(error));
      this.loading.dismissAll();
    });
  }

  delMsg(msg, j, z) {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Do you want to delete?',
          icon: !this.platform.is('ios') ? 'trash' : null,
          handler: () => {
            this.doDelete(msg, j, z);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            //console.log('Cancel clicked');
          }
        }
      ]
    });

    actionSheet.present();
  }

  presentPopoverOne(ev) {
    this.createPopover.presentPopover(ev);
  }

}
