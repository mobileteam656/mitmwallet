import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Changefont } from '../changefont/changeFont';
import { Login } from '../login/login';
import { GlobalProvider } from '../../providers/global/global';
import { App } from 'ionic-angular';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html',
})
export class LogoutPage {
  passtemp: any;
  showFont: string[] = [];
  textEng: any = ['Thanks for using mBanking 360!', 'Last Time Log In', 'Last Time Log out', 'Duration', 'Activity History', 'mBanking 360'];
  textMyan: any = ['ကျေးဇူးတင်ပါသည်', 'နောက်ဆုံးဝင်ရောက်ခဲ့သည့်အချိန်', 'နောက်ဆုံးထွက်ခဲ့သည့်အချိန်', 'လုပ်ဆောင်ခဲ့သည့်အချိန်', 'လုပ်ဆောင်ခဲ့သည့်အကြောင်းအရာ', "mBanking 360"];
  textData: string[] = [];
  font: string;
  constructor(private firebase: FirebaseAnalytics,public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, public events: Events, public changefont: Changefont
    , public global: GlobalProvider,public appCtrl: App) {
    //this.events.publish('user:loggedout');
    this.passtemp = this.navParams.get('data');
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    }) //...
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });    
  }
  changelanguage(font) {    
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  getlogout() {
    // this.firebase.logEvent('log_out', {message:"Logout successful"})
    // .then((res: any) => { console.log(res); })
    // .catch((error: any) => console.error(error));
    this.storage.remove('firstlogin');
    // this.storage.remove('phonenumber');
    this.storage.remove('username');
    this.storage.remove('userData');
    this.appCtrl.getRootNav().setRoot(Login);
 
  }


}
