import { Component } from '@angular/core';
import { IonicPage, Events, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { Changefont } from '../changefont/changeFont';
import { CreatePopoverPage } from '../create-popover/create-popover';

@Component({
  selector: 'page-edit-images',
  templateUrl: 'edit-images.html',
  providers: [CreatePopoverPage]
})
export class EditImagesPage {


  textEng: any = ["Edit", "Add a caption..."];
  textMyan: any = ["Edit", "Add a caption..."];
  textData: string[] = [];
  showFont: any;
  keyboardfont: any;
  language: any;

  passImages: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public events: Events,
    public storage: Storage,
    public changefont: Changefont,
    public createPopover: CreatePopoverPage
  ) {
    this.showFont = 'uni';

    this.passImages = this.navParams.get("photos");
    ////console.log("editImages=" + JSON.stringify(this.passImages));

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('font').then((font) => {
      this.keyboardfont = font;

      if (font == "zg" && this.language != "eng") {
        this.textData[1] = this.changefont.UnitoZg(this.textMyan[1]);
      } else if (font == "uni" && this.language != "eng") {
        this.textData[1] = this.textMyan[1];
      }
    });

    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;

      if (font == "zg" && this.language != "eng") {
        this.textData[1] = this.changefont.UnitoZg(this.textMyan[1]);
      } else if (font == "uni" && this.language != "eng") {
        this.textData[1] = this.textMyan[1];
      }
    });
  }

  changelanguage(lan) {
    this.language = lan;

    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }

      if (this.keyboardfont == 'zg') {
        this.textData[1] = this.changefont.UnitoZg(this.textMyan[1]);
      }
    }
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad EditImagesPage');
  }

  ionViewWillLeave() {
    if (this.passImages.length > 0) {
      ////console.log('willleave editImages=' + JSON.stringify(this.passImages));
      this.navCtrl.getPrevious().data.allphoto = this.passImages;
    }
  }

  Done() {
    if (this.keyboardfont == 'zg') {
      for (let iIndex = 0; iIndex < this.passImages.length; iIndex++) {
        if (this.passImages[iIndex].desc != undefined && this.passImages[iIndex].desc != null && this.passImages[iIndex].desc != '') {
          this.passImages[iIndex].desc = this.changefont.ZgtoUni(this.passImages[iIndex].desc.trim());
        }
      }
    }

    this.navCtrl.pop();
  }

  deletePhoto(index) {
    let confirm = this.alertCtrl.create({
      message: 'Are you sure want to delete?',
      buttons: [
        {
          text: 'NO',
          handler: () => {
            ////console.log('Disagree clicked');
          }
        }, {
          text: 'OK',
          handler: () => {
            ////console.log('Agree clicked');
            this.passImages.splice(index, 1);
          }
        }
      ]
    });
    confirm.present();
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

}
