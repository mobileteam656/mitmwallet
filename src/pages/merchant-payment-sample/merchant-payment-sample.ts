import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, ViewController, NavParams, MenuController, ModalController, LoadingController, PopoverController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';
import { GlobalProvider } from '../../providers/global/global';
import { DatePipe } from '@angular/common';
import { PaymentConfirm } from '../payment-confirm/payment-confirm';
import { UtilProvider } from '../../providers/util/util';
import { App } from 'ionic-angular';
//import { CreatePopoverPage } from '../create-popover/create-popover';
import { PaymentDetailsPage } from '../payment-details/payment-details';
import { PaymentFailPage } from '../payment-fail/payment-fail';
import { AllserviceProvider } from '../../providers/allservice/allservice';


@Component({
  selector: 'page-merchant-payment-sample',
  templateUrl: 'merchant-payment-sample.html',
})
export class MerchantPaymentSamplePage {
  textEng: any = ["Payment", "Name", "Balance", "Payee", "Institution", "Amount",
    "Remark", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "Pay",
    "Mobile Wallet", "Type", "Payment To", "Reference", "Date",
    "Invalid Amount", "Merchant Name", "Phone Number", "Name", "Commission"];
  textMyan: any = ["ငွေပေးသွင်းရန်", "အမည်", "လက်ကျန်ငွေ", "ဖုန်းနံပါတ်/ စာရင်းနံပါတ်", "အော်ပရေတာ", "ငွေပမာဏ",
    "မှတ်ချက်", "မှားယွင်းနေပါသည်", "Does not match! Please try again.", "Please insert name field!", "ပေးသွင်းမည်",
    "Mobile Wallet", "အမျိုးအစား", "Payment To", "အကြောင်းအရာ", "နေ့စွဲ",
    "ငွေပမာဏ ရိုက်ထည့်ပါ", "Merchant Name", "Phone Number", "Name", "Commission"];

  textData: string[] = [];
  errormsg2 = "";
  type: any = { name: "mWallet", code: "mWallet" };
  institute: any = { name: "", code: "" };
  address: string = '';
  userID: string = '';
  balance: string = '';
  loading: any;
  beneficiaryID: string = '';
  toInstitutionCode: string = '';
  amount: string = '';
  remark: string = '';
  ipaddress: string = '';
  font: string = '';
  lan: any;
  popover: any;
  remark_flag: boolean = false;
  _obj = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
    frominstituteCode: "", toInstitutionCode: "", reference: "", toAcc: "", amount: "",
    bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "",
    fromName: "", toName: "", "wType": "",
    field1: "", field2: "", tosKey: "", datetime: "", accountNo: "", balance: ""
  }
  errormsg1: any;
  temp: any;
  normalizePhone: any;
  readOnly: any;
  merchant: any;
  pass: boolean = true;
  public alertPresented: any;
  resData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, public menuCtrl: MenuController,
    public modalCtrl: ModalController, public http: Http,
    public datePipe: DatePipe, public loadingCtrl: LoadingController,
    public toastCtrl: ToastController, public sqlite: SQLite,
    public changefont: Changefont,
    public events: Events, public alertCtrl: AlertController,
    public popoverCtrl: PopoverController, public platform: Platform,
    public util: UtilProvider, public global: GlobalProvider,
    public viewCtrl: ViewController, public appCtrl: App, public all: AllserviceProvider) {
    //public createPopover: CreatePopoverPage) {
    this.readOnly = 0;
    //this.remark_flag = true;
    this.storage.get('userData').then((userData) => {
      this._obj = userData;
      if (userData != null || userData != '') {
        this.balance = userData.balance;
      }
    });

    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    this._obj.toInstitutionName = institution.toInstitutionName;
    this.institute.name = this._obj.toInstitutionName;
    this.institute.code = this._obj.toInstitutionCode;
    this.storage.get('phonenumber').then((phonenumber) => {
      this._obj.userID = phonenumber;
    });
    this.temp = this.navParams.get('param');
    this.storage.get('ipaddressWallet').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressWallet;
      }
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    })
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    this.merchant = this.navParams.get('param');
    if (this.merchant != null && this.merchant != undefined && this.merchant != '') {
      this._obj.accountNo = this.merchant.accountNo;
      this._obj.beneficiaryID = this.merchant.merchantID;
      this._obj.toName = this.merchant.merchantName;
    }
    this.temp = this.navParams.get('data');
    console.log("Payment temp: " + this.temp);
    if (this.temp != null && this.temp != undefined && this.temp != '') {
      this._obj.beneficiaryID = this.temp[1];
      this.getName(this._obj.beneficiaryID);
      this._obj.toInstitutionCode = this.temp[2];
      this._obj.amount = this.temp[3];
      this._obj.tosKey = this.temp[6];
      if (this._obj.amount != "" && this._obj.amount != "0" && this._obj.amount != "0.0" && this._obj.amount != "0.00") {
        this.readOnly = 1;
      }
      this._obj.remark = this.temp[4];
      if (this._obj.remark != '' && this._obj.remark != undefined) {
        this.remark_flag = true;
      }
      this._obj.datetime = this.temp[5];
    }

    this.storage.get('username').then((username) => {
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    });
    this.events.subscribe('username', username => {
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    });
  }

  getName(userID) {
    this.storage.get('ipaddressWallet').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressWallet;
      }
      let param = { "userID": userID, "name": "", "nrc": "", "sessionID": "", "institutionCode": "", "sKey": "", "field1": "", "field2": "" };
      this.http.post(this.ipaddress + '/service001/getName', param).map(res => res.json()).subscribe(data => {
        if (data.messageCode == '0000') {
          this._obj.toName = data.name;
        } else {
          this._obj.toName = '';
        }
      }, error => {
        this.getError(error);
      });
    });
  }

  getError(error) {
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = "Can't connect right now. [" + code + "]";
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
  }

  ionViewWillEnter() {
    if (this.merchant.merchantID != undefined || this.merchant != '' || this.merchant != null) {
      this._obj.beneficiaryID = this.merchant.merchantID;
      this._obj.toName = this.merchant.merchantName;
    }
  }

  goNext() {
    if (this.temp == null || this.temp == "" || this.temp == undefined) {
      this.goManual();
    }
    else {
      this.goQR();
    }
  }

  goManual() {
    if (this.util.checkInputIsEmpty(this._obj.amount) || this.util.checkNumberOrLetter(this._obj.amount) ||
      this.util.checkAmountIsZero(this._obj.amount) || this.util.checkAmountIsLowerThanZero(this._obj.amount) ||
      this.util.checkStartZero(this._obj.amount) || this.util.checkPlusSign(this._obj.amount)) {
      this.errormsg1 = this.textData[16];
    }
    else {
      this._obj.field1 = "2";
      this._obj.wType = this.type.name;
      let flag = false;
      if (this._obj.beneficiaryID != null && this._obj.beneficiaryID != '' && this._obj.beneficiaryID != undefined) {

      }
      else if (this.merchant != null && this.merchant != undefined && this.merchant != '') {
        this._obj.accountNo = this.merchant.accountNo;
        this._obj.toName = this.merchant.merchantName;
        this._obj.beneficiaryID = this.merchant.merchantID;
      }
      let institution = this.util.setInstitution();
      this._obj.frominstituteCode = institution.fromInstitutionCode;
      this._obj.toInstitutionCode = institution.toInstitutionCode;
      this._obj.toInstitutionName = institution.toInstitutionName;
      this.navCtrl.push(PaymentConfirm, {
        data: this._obj,

      })
    }
  }

  goQR() {
    console.log("my amount is : " + this._obj.amount);
    this._obj.field1 = "2";
    this._obj.wType = this.type.name;
    let flag = false;
    if (this.temp != null && this.temp != undefined && this.temp != '') {
      this._obj.beneficiaryID = this.temp[1];
      if (this.temp[3] != "" && this.temp[3] != null && this.temp[3] != "0" && this.temp[3] != "0.00" && this.temp[3] != undefined) {
        this._obj.amount = this.temp[3];
      }
      if (this.temp[4] != "" && this.temp[4] != null && this.temp[4] != undefined) {
        this._obj.remark = this.temp[4];
      }
      this._obj.tosKey = this.temp[6];
    }

    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    this._obj.toInstitutionName = institution.toInstitutionName;
    this.normalizePhone = this.util.normalizePhone(this._obj.beneficiaryID);
    if (this.normalizePhone.flag == true) {
      flag = true;
      console.log("normalizePhone.phone>>" + this.normalizePhone.phone);
      this._obj.beneficiaryID = this.normalizePhone.phone;
      console.log("this._obj.beneficiaryID>>" + this._obj.beneficiaryID);
      if (flag) {
        this.navCtrl.push(PaymentConfirm, {
          data: this._obj,
        })
      }
    } else {
      flag = false;
      let toast = this.toastCtrl.create({
        message: "Invalid. Please try again!",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
  }


  goQ() {
    this._obj.field1 = "2";
    this._obj.wType = this.type.name;
    let flag = false;
    if (this.temp != null && this.temp != undefined && this.temp != '') {
      this._obj.beneficiaryID = this.temp[1];
      if (this.temp[3] != "" && this.temp[3] != null && this.temp[3] != "0" && this.temp[3] != "0.00" && this.temp[3] != undefined) {
        this._obj.amount = this.temp[3];
      }
      if (this.temp[4] != "" && this.temp[4] != null && this.temp[4] != undefined) {
        this._obj.remark = this.temp[4];
      }
      //if (this._obj.remark != '' && this._obj.remark != undefined) {
      //  this.remark_flag = true;
      // }
      this._obj.tosKey = this.temp[6];
    }
    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    this._obj.toInstitutionName = institution.toInstitutionName;
    this.normalizePhone = this.util.normalizePhone(this._obj.beneficiaryID);
    if (this.normalizePhone.flag == true) {
      flag = true;
      console.log("normalizePhone.phone>>" + this.normalizePhone.phone);
      this._obj.beneficiaryID = this.normalizePhone.phone;
      console.log("this._obj.beneficiaryID>>" + this._obj.beneficiaryID);
      if (flag) {

        let tempdata: any;
        // this.slimLoader.start(() => { });
        let userID1 = this._obj.userID;
        let param = { userID: userID1, sessionID: this._obj.sessionID, type: '12', merchantID: '' };
        this.http.post(this.ipaddress + '/service002/readMessageSetting', param).map(res => res.json()).subscribe(data => {
          if (data.code == "0000") {
            tempdata = data;
            console.log("tempdata is : " + JSON.stringify(tempdata));
            // this.slimLoader.complete();
            this.navCtrl.push(PaymentConfirm, {
              data: this._obj,
              //tosKey: this.temp.syskey,            
              //messageParam:param ,
              //psw:tempdata.rKey 

            })
          } else {
            this.showAlert('Warning!', data.desc);
            //this.slimLoader.complete();
          }
        });


        /*this.navCtrl.push(PaymentConfirm, {
          data: this._obj,
          tosKey: this._obj.tosKey,            
         //ss messageParam:param ,
          psw: this.pass 
        })*/
      }
    } else {
      flag = false;
      let toast = this.toastCtrl.create({
        message: "Invalid. Please try again!",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
  }

  @ViewChild('myInput') myInput;
  ionViewDidLoad() {
    console.log("ionViewDidLoad");
    setTimeout(() => {
      this.myInput.setFocus();
    }, 500);
  }


  typeChange(s) {
    this.type = s;
    this.type.name = s.name;
    this.type.code = s.code;
  }

  onChange(s, i) {
    if (i == "09") {
      this._obj.beneficiaryID = "+959";
    }
    else if (i == "959") {
      this._obj.beneficiaryID = "+959";
    }
  }

  instituteChange(s) {
    this._obj.toInstitutionName = s.toInstitutionName;
    this._obj.toInstitutionCode = s.toInstitutionCode;
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  showAlert(titleText, subTitleText) {
    if (this.alertPresented == undefined) {
      let alert = this.alertCtrl.create({
        title: titleText,
        subTitle: subTitleText,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              this.alertPresented = undefined;
            }
          }
        ],
        enableBackdropDismiss: false
      });

      this.alertPresented = alert.present();
      setTimeout(() => alert.dismiss(), 2000 * 60);
    }
  }

  goPayment() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
      // duration: 3000
    });
    this.loading.present();
    let payeePhone = this._obj.userID;//this.util.removePlus(this._obj.userID);
    // let benePhone = this.util.removePlus(this._obj.beneficiaryID);
    let amount = this.util.formatToDouble(this._obj.amount);
    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;

    let param = {
      "userID": payeePhone, "sessionID": this._obj.sessionID,
      "amount": amount,
      "beneficiaryID": this._obj.accountNo,
      "payeeID": payeePhone,
      "fromInstitutionCode": '',
      "toInstitutionCode": '',
      "refNo": 110,
      "fromName": '',
      "toName": '',
      "paymentType": this.merchant.merchantName + ' Payment',
      "merchantID": this.merchant.merchantID
    }
    let msgparam = {
      "userID": payeePhone,
      "sessionID": this._obj.sessionID,
      "type": 1,
      "merchantID": this.merchant.merchantID
    }
    let transferParam = { "messageRequest": msgparam, "transferOutReq": param }
    console.log("request for payment" + JSON.stringify(transferParam));
    this.http.post(this.ipaddress + '/payment/payMerchant', transferParam).map(res => res.json()).subscribe(data => {

      let resdata = {
        "bankRefNo": data.bankRefNo,
        "code": data.code,
        "desc": data.desc,
        "field1": "",
        "field2": "",
        "transDate": data.transDate
      };
      console.log("response for payment in ad=" + JSON.stringify(data));
      let code = data.code;
      if (code == '0000') {
        this.resData = data;
        this.appCtrl.getRootNav().setRoot(PaymentDetailsPage, {
          data: resdata,
          data1: this._obj.amount,
          data2: this._obj.toName
        });
        this.loading.dismiss();
      }
      else {
        this.resData = [];
        this.navCtrl.push(PaymentFailPage, {
          desc: data.desc,
          data: this._obj
        })

        this.loading.dismiss();
      }
    },
      error => {
        let toast = this.toastCtrl.create({
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }
  paymentV2() {
    if (this.util.checkInputIsEmpty(this._obj.amount) || this.util.checkNumberOrLetter(this._obj.amount) ||
      this.util.checkAmountIsZero(this._obj.amount) || this.util.checkAmountIsLowerThanZero(this._obj.amount) ||
      this.util.checkStartZero(this._obj.amount) || this.util.checkPlusSign(this._obj.amount)) {
      this.errormsg1 = this.textData[16];
    }
    else {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      let amount = this.util.formatToDouble(this._obj.amount);
      let parameter = {
        userID: this._obj.userID,
        sessionID: this._obj.sessionID,
        fromAccount: "",
        toAccount: " ",
        amount: amount,
        bankCharges: 0,
        refNo: 110,
        field1: '2',
        merchantID: this.merchant.merchantID,
        fromName: this._obj.fromName,
        toName: this.merchant.merchantName,
        trprevBalance: this._obj.balance
      };
      this.http.post(this.ipaddress + '/payment/goMerchantPaymentTransferDeposit', parameter).map(res => res.json()).subscribe(data => {
        let resdata = {
          "bankRefNo": data.bankRefNumber,
          "code": data.code,
          "desc": data.desc,
          "transDate": data.transactionDate
        };
        console.log("response for payment in ad=" + JSON.stringify(data));
        let code = data.code;
        if (code == '0000') {
          this.resData = data;
          this.appCtrl.getRootNav().setRoot(PaymentDetailsPage, {
            data: resdata,
            data1: this._obj.amount,
            data2: this.merchant.merchantName,
          });
          this.loading.dismiss();
        }
        else {
          this.resData = [];
          this.navCtrl.push(PaymentFailPage, {
            desc: data.desc,
            data: this._obj
          })
          this.loading.dismiss();
        }
      },
        error => {
          let toast = this.toastCtrl.create({
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    }
  }
  calculate() {
    let flag3 = false;
    if (this._obj.amount != '' && this._obj.amount != null && this._obj.amount != undefined) {
      flag3 = true;
      this.errormsg2 = '';
    } else {
      flag3 = false;
      this.errormsg2 = this.textData[16];
    }
    if (flag3) {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      let param =
      {
        userID: this._obj.userID,
        sessionID: this._obj.sessionID,
        amount: this._obj.amount,
        merchantID: this.merchant.merchantID
      };
      console.log("Calculate Commission param>>" + JSON.stringify(param))
      this.http.post(this.ipaddress + '/payment/calculateComm', param).map(res => res.json()).subscribe(data => {
        this.loading.dismiss();
        let code = data.code;
        if (code == '0000') {

        } else if (data.code == "0016") {

        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 5000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    }
  }

}
