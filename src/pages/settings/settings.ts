import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, Events } from 'ionic-angular';
import { PopOverListPage } from '../pop-over-list/pop-over-list';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { Storage } from '@ionic/storage';
import { Language } from '../language/language';
import { FontPopoverPage } from '../font-popover/font-popover';

/**
 * Generated class for the SettingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
  providers: [CreatePopoverPage]
})
export class SettingsPage {

  popover: any;

  textMyan: any = ["ဘာသာစကား", "ဖောင့်", "ပြင်ဆင်ရန်"];
  textEng: any = ['Language', 'Font', 'Settings'];
  textData: string[] = [];
  languageData: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public createPopover: CreatePopoverPage,
    public events: Events,
    public storage: Storage) {

    this.events.subscribe('changelanguage', lan => {
      if (lan == "" || lan == null) {
        this.storage.set('language', "eng");
        lan = "eng";
      }
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      if (lan == "" || lan == null) {
        this.storage.set('language', "eng");
        lan = "eng";
      }
      this.changelanguage(lan);
    });
  }

  changelanguage(lan) {
    this.languageData = lan;
    ////console.log('language' + this.languageData);
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  clickchange(i) {
    if (i == 0) {
      this.navCtrl.push(Language);
    } else if (i == 1) {
      this.navCtrl.push(FontPopoverPage);
    }
  }
}
