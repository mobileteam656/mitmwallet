import { Component } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Chat } from '../chat/chat';
import { FunctProvider } from '../../providers/funct/funct';
import { GlobalProvider } from '../../providers/global/global';
import { Platform, NavController, NavParams, PopoverController, Events, ToastController, LoadingController, ActionSheetController } from 'ionic-angular';
import { ContactPage } from '../contact/contact';//contact
import { App } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
//import { Transfer } from '@ionic-native/transfer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { BarcodeScanner, BarcodeScannerOptions } from "@ionic-native/barcode-scanner";
import { File } from '@ionic-native/file';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { PicsPage } from '../pics/pics';
import { Payment } from '../payment/payment';
import { QRValuePage } from '../qr-value/qr-value';
import { SelectParticipantPage } from '../select-participant/select-participant';
@Component({
  selector: 'page-message-user',
  templateUrl: 'message-user.html',
  providers: [CreatePopoverPage],
})
export class MessageUserPage {
  rootNavCtrl: NavController;
  chatList: any = [];
  isLoading: any;
  userData: any;
  nores: any;
  start: any = 0;
  end: any = 0;
  ipaddress: string;
  userDataChat: any;
  popover: any;
  tabBarElement: any;
  channelData: any = [];
  //  db: any;
  profile: any;
  photo: any;
  docData: any;
  from: any;
  description: string = '';
  photoName: any;
  latitude: any;
  longitude: any;
  location: any;
  buttonText: any;
  loading: boolean;
  scannedText: any;
  profileImageLink: any;
  options: BarcodeScannerOptions;

  loadingIcon: any;
  from_Chat: any = { fromName: '' };
  description_Chat: any = { description: '' };
  textEng: any = [
    "Messages"
  ];
  textMyan: any = [
    "မက်ဆေ့ချ်များ"
  ];
  textData: any = [];
  showFont: any = '';
  db: any;
  constructor(private _barcodeScanner: BarcodeScanner, public platform: Platform,
    private transfer: FileTransfer, public popoverCtrl: PopoverController,
    public navCtrl: NavController, public navParams: NavParams,
    public http: Http, public toastCtrl: ToastController,
    public global: GlobalProvider, private file: File,
    public sqlite: SQLite, public funct: FunctProvider,
    public events: Events, public storage: Storage,
    public appCtrl: App, public createPopover: CreatePopoverPage,
    private camera: Camera, private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder, public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    
  ) {
    this.storage.get('ipaddress3').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
        console.log('IP Chatting is :'+this.ipaddress);
      } else {
        this.ipaddress = this.global.ipaddress3;
      }
    });

    this.storage.get('profileImage').then((profileImage) => {
      if (profileImage != undefined && profileImage != null && profileImage != '') {
        this.profileImageLink = profileImage;
      } else {
        this.profileImageLink = this.global.digitalMediaProfile; 
        console.log("my profile image is here : " + JSON.stringify(this.profileImageLink.t18));
      }
    });
    // this.profile = this.funct.imglink+"upload/image/userProfile";
    ////console.log("my profile image is here : " + JSON.stringify(this.profile));
    this.rootNavCtrl = navParams.get('rootNavCtrl');
    this.docData = this.navParams.get('data');
    this.from = this.navParams.get('from');
    this.description = this.navParams.get('description');
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
   this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success create or open database");
      this.db = db;
      this.db.executeSql("CREATE TABLE IF NOT EXISTS messageUser (id INTEGER PRIMARY KEY AUTOINCREMENT,username TEXT,messagecontent TEXT)", {}).then((data) => {
        ////console.log("TABLE CREATED: ", data);
        this.selectData();
      }, (error) => {
       // console.error("Unable to execute sql", error);
      });
    }).catch(e => console.log(e));
  }

  ionViewCanEnter() {
    this.storage.get("phonenumber").then((data) => {
      ////console.log("ionViewCanEnter this.storage.get(phonenumber) in message.ts = " + JSON.stringify(data));
      this.userData = data;
      this.start = 0;
      this.end = 0;
      this.storage.get('ipaddress3').then((ip) => {
        if (ip !== undefined && ip !== "" && ip != null) {
          this.ipaddress = ip;
          console.log('IP Chatting is :'+this.ipaddress);
        } else {
          this.ipaddress = this.global.ipaddress3;
        }
      });

      //this.ipaddress = this.global.ipaddressChat;
    });
  }

  insertData() {
    ////console.log("channel data length = ", this.chatList.length);

    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success create or open database");
      for (let i = 0; i < this.chatList.length; i++) {
        db.executeSql("INSERT INTO messageUser(messagecontent) VALUES (?)", [JSON.stringify(this.chatList[i])]).then((data) => {
          ////console.log("Insert data successfully", data);
        }, (error) => {
          //console.error("Unable to insert data", error);
        });
      }
    }).catch(e => console.log(e));
  }

  deleteData() {
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success create or open database");

      db.executeSql("DELETE FROM messageUser", []).then((data) => {
      console.log("Delete data successfully", data);
        this.insertData();
      }, (error) => {
       // console.error("Unable to delete data", error);
      });
    }).catch(e => console.log(e));
  }

  selectData() {
    ////console.log("select data from sqlite");
    this.chatList = [];

    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success create or open database");
      db.executeSql("SELECT * FROM messageUser ", []).then((data) => {
        ////console.log("length == " + data.rows.length);
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            console.log("data.rows.item(i).allData == " + data.rows.item(i).messagecontent);
            this.channelData.push(JSON.parse(data.rows.item(i).messagecontent));
            ////console.log("Channel Data " + JSON.stringify(this.channelData));
            this.chatList = this.channelData;
          }
          this.nores = 1;
        }
      });
    }).catch(e => console.log(e));
  }

  getchatUserList(start) {
    // this.isLoading = true;
    this.end = this.end + 10;
    let msgparams = {
      start: start,
      end: this.end
    }
    //console.log("request con=" + this.userDataChat.sKey);
    //searchChatList
    if (this.global.netWork == 'connected') {
      this.http.post(this.ipaddress + "serviceChat/searchChatList1?syskey=" + this.userDataChat.sKey + "&role=" + "&domain=" + this.global.domainChat + "&appId=" + this.global.appIDChat, msgparams).map(res => res.json()).subscribe(result => {
       // console.log("response chatList =>" + JSON.stringify(result));
        if (result.data == undefined || result.data == null || result.data == '') {
          this.deleteData();
          this.nores = 0;
        } else {
          let tempArray = [];
          if (!Array.isArray(result.data)) {
            tempArray.push(result.data);
            result.data = tempArray;
          }

          for (let i = 0; i < result.data.length; i++) {
            if (result.data[i].t1.includes("MCDC") || result.data[i].t1.includes("YCDC") || result.data[i].t1 == "general@dc.com") {
              result.data.splice(i, 1);
              i--;
            }
          }

          if (result.data.length > 0) {
            this.chatList = result.data;
            //console.log("My Chat List is = " + JSON.stringify(this.chatList));
            this.getGroupData();
            this.deleteData();
            this.nores = 1;
            //   //this.deleteTable();
          } else {
            this.nores = 0;
          }
        }
        this.isLoading = false;
      }, error => {
        ////console.log("signin error=" + error.status);
        //this.nores = 0;
        this.getError(error);
      });
    }
  }

  viewChat(d) {
    this.from_Chat.fromName = 'MessageUserPage';
    ////console.log("pass data (MessageUserPage 2 MessageUserPage)=" + JSON.stringify(d));
    this.navCtrl.push(Chat, {
      data: d,
      from: this.from_Chat,
      sKeyDB: this.userDataChat.sKey
    });
  }

  doRefresh(refresher) {
    ////console.log('Begin async operation', refresher);
    this.start = this.start + 11;
    this.getchatUserList(this.start);
    setTimeout(() => {
      ////console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  goParticipant(uList) {
    this.navCtrl.push(SelectParticipantPage);
  }

  getGroupData() {
    let monthNames = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    for (let i = 0; i < this.chatList.length; i++) {
      let modifiedDateStr = this.chatList[i].modifiedDate;

      // if (this.isToday(modifiedDateStr)) {
      //   this.chatList[i].modifiedDate = time;
      // } else {
      let monthStr = modifiedDateStr.substring(4, 6);

      let monthInt = parseInt(monthStr);

      let month = monthNames[monthInt];

      let dateStr = modifiedDateStr.substring(6);

      let date = parseInt(dateStr);

      this.chatList[i].modifiedDate = date + " " + month;
      // }
    }

    ////console.log("chatList=" + JSON.stringify(this.chatList));

    let sortedContacts = this.chatList.sort(function (a, b) {
      if (a.t16 < b.t16) return 1;
      if (a.t16 > b.t16) return -1;
      return 0;
    });

    let group_to_values = sortedContacts.reduce(function (obj, item) {
      obj[item.t16] = obj[item.t16] || [];
      obj[item.t16].push(item);
      return obj;
    }, {});

    let groups = Object.keys(group_to_values).map(function (key) {
      ////console.log("group_to_values" + JSON.stringify(group_to_values));

      return { t16: key, data: group_to_values[key] };
    });

    this.chatList = groups;

    ////console.log("allcontacts List=" + JSON.stringify(this.chatList));
  }

  ionViewWillEnter() {
    this.storage.get('userData').then((userData) => {
     // //console.log('Your userData is message.ts in getchatUserList', userData);
      if (userData !== undefined && userData !== "") {
        this.userDataChat = userData;
        this.start = this.start + 11;
        this.getchatUserList(this.start);
      }
    });
  }

  isToday(yyyymmdd) {
    /* let yyyyParam = yyyymmdd.substring(0, 4);
    let mmParam = yyyymmdd.substring(4, 6);
    let ddParam = yyyymmdd.substring(6); */

    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let yyyy = today.getFullYear();

    let todayString = "" + yyyy + mm + dd;

    if (todayString == yyyymmdd) {
      return true;
    } else {
      return false;
    }
  }

  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return this.file.dataDirectory + img;
    }
  }

  goCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      //allowEdit: true,
      targetWidth: 600,
      targetHeight: 600,
    }

    this.camera.getPicture(options).then((imagePath) => {
      this.loadingCCP();
      ////console.log('Camera success' + imagePath);
      let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());

    }, (error) => {
      this.getError(error);
    });
  }

  loadingCCP() {
    this.loadingIcon = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Processing...',
    });

    this.loadingIcon.present();
  }

  dismiss() {
    this.loadingIcon.dismissAll();
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    ////console.log("namePaht == " + namePath + "   //// currentNmae == " + currentName + "   ////  newFileName == " + newFileName);
    ////console.log("this.file.datadirectory == " + this.file.dataDirectory);
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.docData = this.file.dataDirectory + newFileName;
      this.photoName = newFileName;
      ////console.log("photos=" + JSON.stringify(this.docData));
      this.createDB();
    }, error => {
      alert('Error while storing file.' + JSON.stringify(error));
    });
  }

  createDB() {
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success create or open database");
      this.db = db;
      this.db.executeSql('CREATE TABLE IF NOT EXISTS DOCPHOTO(id INTEGER PRIMARY KEY AUTOINCREMENT,PhotoPath TEXT,Name TEXT,Latitude TEXT,Longitude TEXT,Location TEXT,CreateDate TEXT,CreateTime TEXT)', {})
        .then((success) => {
          this.getLocation();
          ////console.log('Executed SQL success')
        }, (error) => {
         // console.error("Unable to create database", error);
        })
        .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }

  getLocation() {
    let options = {
      timeout: 30000
    };

    this.geolocation.getCurrentPosition(options).then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      ////console.log(' getting latitude ', resp.coords.latitude);
      ////console.log(' getting longitude ', resp.coords.longitude);
      this.nativeGeocoder.reverseGeocode(this.latitude, this.longitude)
        .then((result: NativeGeocoderReverseResult[]) => {
          this.location = result[0].locality + ", " + result[0].countryName;
          this.insertPhoto();
          ////console.log(JSON.stringify(result))
        }, (err) => {
          this.dismiss();
          let toast = this.toastCtrl.create({
            message: "Please take photo again. Please turn on location access.",
            duration: 5000,
            position: 'bottom',
            //showCloseButton: true,
            dismissOnPageChange: true,
            //closeButtonText: 'OK'
          });
          toast.present(toast);
          ////console.log('Error getting location', JSON.stringify(err));
        });
      // )
      //   .catch((error: any) => //console.log(error));
    }, (err) => {
      this.dismiss();
      let toast = this.toastCtrl.create({
        message: "Please take photo again. Please turn on location access.",
        duration: 5000,
        position: 'bottom',
        //showCloseButton: true,
        dismissOnPageChange: true,
        //closeButtonText: 'OK'
      });
      toast.present(toast);
      ////console.log('Error getting location', JSON.stringify(err));
    });
    /* })
    .catch((error: any) => //console.log(error));
}).catch((error) => {
  //console.log('Error getting location', error);
}); */
  }

  insertPhoto() {
    let date = new Date()
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    const weekNames = ["SUN", "MON", "TUE", "WED", "THUR", "FRI", "SAT"];
    const dayNames = [];
    let month = monthNames[date.getMonth()];
    let day = dayNames[date.getUTCDate()];
    let createData = month + "-" + date.getDate() + "-" + date.getFullYear();
    let wDay = weekNames[date.getDay()];
    let hoursMinutes = [date.getHours(), date.getMinutes()];
    let creteTime = wDay + " " + this.formatAMPM(hoursMinutes);
    ////console.log("time " + creteTime);

    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success create or open database");

      db.executeSql("INSERT INTO DOCPHOTO(PhotoPath,Name,Latitude ,Longitude ,Location,CreateDate, CreateTime) VALUES (?,?,?,?,?,?,?)", [this.docData, this.photoName, this.latitude, this.longitude, this.location, createData, creteTime]).then((data) => {
        ////console.log("Insert data successfully", data);
        this.dismiss();
        this.navCtrl.push(PicsPage);
      }, (error) => {
        console.error("Unable to insert data", error);
      });
    }).catch(e => console.log(e));
  }

  formatAMPM(date) {
    var hours = date[0];
    var minutes = date[1];
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  createFileName() {
    let d = new Date().getTime();
    // let n = d.getTime();
    let newFileName = "Doc-" + d + ".jpg";
    return newFileName;
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //showCloseButton: true,
      dismissOnPageChange: true,
      //closeButtonText: 'OK'
    });
    toast.present(toast);
    this.isLoading = false;
    ////console.log("Oops!");
  }

  scanQR(param) {
    this.buttonText = "Loading..";
    this.loading = true;
    this.options = {
      formats: "QR_CODE",
      "prompt": " ",
    }

    this._barcodeScanner.scan(this.options).then((barcodeData) => {
      if (barcodeData.cancelled) {
        ////console.log("User cancelled the action!");
        this.buttonText = "Scan";
        this.loading = false;

        return false;
      }

      this.scannedText = barcodeData.text;
      ////console.log("scan result=" + this.scannedText);

      if (param == "contact") {
        try {
          let datacheck_scan = JSON.parse(this.scannedText);

          if (datacheck_scan[0] == "contacts") {
            this.navCtrl.push(ContactPage, {
              data: datacheck_scan,
              fromPage: "QR"
            });
          } else {
            this.toastInvalidQR();
          }
        } catch (e) {
          this.toastInvalidQR();
        
        }
      } else if (param == "payment") {
        try {
          let datacheck_scan = JSON.parse(this.scannedText);

          if (datacheck_scan[0] == "payments") {
            this.navCtrl.push(Payment, {
              data: datacheck_scan
            });
          } else {
            this.toastInvalidQR();
          }
        } catch (e) {
          this.toastInvalidQR();
          
        }
      } else if (param == "other") {
        if (this.isURL(this.scannedText)) {
          window.open(this.scannedText, '_system');
        } else {
          this.navCtrl.push(QRValuePage, {
            paramQRValue: this.scannedText
          });
        }
      }
    }, (err) => {
      ////console.log(err);
    });
  }

  chooseQR() {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Add Contact',
          icon: !this.platform.is('ios') ? 'contact' : null,
          handler: () => {
            this.scanQR("contact");
          }
        },
        {
          text: 'Payment',
          icon: !this.platform.is('ios') ? 'cash' : null,
          handler: () => {
            this.scanQR("payment");
          }
        },
        {
          text: 'Other',
          icon: !this.platform.is('ios') ? 'link' : null,
          handler: () => {
            this.scanQR("other");
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            ////console.log('Cancel clicked');
          }
        }
      ]
    });

    actionSheet.present();
  }

  toastInvalidQR() {
    let toast = this.toastCtrl.create({
      message: "Invalid QR Type.",
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });

    toast.present(toast);
  }

  isURL(str) {
    let pattern = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    ////console.log("pattern test = " + pattern.test(str));

    if (!pattern.test(str)) {
      return false;
    } else {
      return true;
    }
  }

  showMap() {
    window.open('geo://?q=', '_system');  // android
    // window.open('http://maps.apple.com/?q=','_system') //ios
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

}