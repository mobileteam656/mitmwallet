import 'rxjs/add/operator/map';
import { GlobalProvider } from '../../providers/global/global';
import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { AlertController, Events, LoadingController, NavController, NavParams, ToastController } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { QPayConfirmPage } from '../q-pay-confirm/q-pay-confirm';
import { Login } from '../login/login';

@Component({
  selector: 'page-q-pay-info',
  templateUrl: 'q-pay-info.html',
})
export class QPayInfoPage {
  merchantId: any;
  merchantName: any;
  merchantlist: any;

  textMyan: any = ["Bill Aggregator Confirm", "Merchant Name", "Customer(Ref No)", "Company/Customer Name", "ငွေပမာဏ", "ပယ်ဖျက်မည်", "ပေးမည်", "အကောင့်ရွေးချယ်ပါ", "နေရပ်လိပ်စာ", "အမည်", "ငွေပမာဏ", "မှတ်ပုံတင်", "ဖုန်းနံပါတ်", "ကျေးဇူးပြု၍ အချက်အလက်များ ဖြည့်စွက်ပါ။", "အကြောင်းအရာ", "အကောင့်နံပါတ် မှ"];
  textEng: any = ["Bill Aggregator Confirm", "Merchant Name", "Customer(Ref No)", "Company/Customer Name", "Amount", "Cancel", "Pay", "Select Account", "Address", "Name", "Amount", "NRC", "Phone No", "Please fill this field.", "Narrative", "From Account"];
  showFont: string[] = [];
  billerid = "";

  userData: any;
  errormsg: any;
  refNo: any;
  name: any;
  amount: any;
  selectAccount: any;
  userdata: any;
  ipaddress: any;
  passTemp: any;
  passType: any;
  logoutAlert: any;
  transferData: any = {};
  transferData1: any = {};
  fromaccMsg: any = '';
  toaccMsg: any = '';
  amountcMsg: any = '';
  fromaccMsg1: any = '';
  toaccMsg1: any = '';
  amountcMsg1: any = '';
  fromAccountlist: any = [];
  accountBal: any;
  amountBal: any;
  popover: any;
  txnType: any;
  value: any;
  _data = {
    refNo: "P0003", name: "", amount: "", account: "", waveAcc: "", nrc: "", phoneNo: "", description: ""
  }
  font: any = '';
  data: any;
  loading: any;
  flag: string;
  detail: any;
  passOtp: any;
  passSKey: any;
  fromPage: any;
  nameMsg: any;
  amountMsg: any; 
  isChecked: boolean;
  _obj = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
    frominstituteCode: "", toInstitutionCode: "", reference: "", toAcc: "", amount: "",
    bankCharges: "", commissionCharges: "", remark: "",   transferType: "2", sKey: "",
    fromName: "", toName: "", "wType": "",
    field1: "", field2: "", tosKey: "", datetime: "", accountNo: ""
  }
  balance: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public http: Http, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public network: Network, private storage: Storage,
    public alertCtrl: AlertController,public global: GlobalProvider, public all: AllserviceProvider, public util: UtilProvider, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider, public changefont: Changefont) {
     
      this.merchantlist = this.navParams.get("merchantlist");
      this.merchantId = this.merchantlist.merchantID;
      this.merchantName = this.merchantlist.merchantName;      
      console.log("merchantlist>>>",this.merchantlist);
      console.log("merchantId>>>",this.merchantId);
      this.storage.get('userData').then((userData) => {
        this._obj = userData;   
        if (userData != null || userData != '') {
          this.balance = userData.balance;
        }
      });  

      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      });
      this.storage.get('language').then((lan) => {
        this.changelanguage(lan);
      });
      this.billerid = this.navParams.get("billerid");
      // this.storage.get('userData').then((data) => {
      //   this.userdata = data;
  
      //   this.storage.get('ipaddress').then((result) => {
  
      //     if (result == null || result == '') {
  
      //     }
      //     else {
      //       this.ipaddress = result;
      //     }
      //     this.getAccountSummary();
      //   });
      // });
      this.storage.get('ipaddress').then((ip) => {
        if (ip !== undefined && ip !== "" && ip != null) {
          this.ipaddress = ip;
        } else {
          this.ipaddress = this.global.ipaddress;
        }
      });
    }
    changelanguage(lan) {
      if (lan == "eng") {
        for (let i = 0; i < this.textEng.length; i++) {
          this.showFont[i] = this.textEng[i];
        }
      } else {
        for (let i = 0; i < this.textMyan.length; i++) {
          this.showFont[i] = this.textMyan[i];
        }
      }
    }
  
    ionViewDidLoad() {
      // console.log('ionViewDidLoad QuickpayPage');
    }
  
    goCancel() {
      this.navCtrl.pop();
    }
  
    goPay() {
      if (this._data.refNo == "" || this._data.name == "" || this._data.amount == "") {
        this.errormsg = this.showFont[13];
      } else {
        this.errormsg = "";
        this.navCtrl.push(QPayConfirmPage, {
          billerid: this.billerid,
          data: this._data
        });
      }
  
      // this.navCtrl.push(QuickpaySuccessPage);
    }

    transfer() {   

      let f1 = false, f2 = false, f3 = false;
      // if (this._data.account != null && this._data.account != '' && this._data.account != undefined) {
      //   this.fromaccMsg = '';
      //   f1 = true;
      // }
      // else {
      //   this.fromaccMsg = "Please select account.";
      //   f1 = false;
      // }
      if (this._data.name != null && this._data.name != '' && this._data.name != undefined) {
        this.nameMsg = '';
        f2 = true;
      }
      else {
        this.nameMsg = "Please fill name.";
        f2 = false;
      }
      if (this._data.amount != null && this._data.amount != '' && this._data.amount != undefined) {
        this.amountMsg = '';
        f3 = true;
      }
      else {
        this.amountMsg = "Please fill amount.";
        f3 = false;
      }
      if (f2 && f3) {
         this.loading = this.loadingCtrl.create({
              dismissOnPageChange: true
          });
          this.loading.present();

          // let param = {
          //     userID: this.userdata.userID,
          //     sessionID: this.userdata.sessionID,
          //     type: '1',
          //     merchantID: 'M00003'
          // };

          this.navCtrl.push(QPayConfirmPage, {
            merchantlist: this.merchantlist,
            data: this._data,
            otp: this._obj.userID,
            //sKey: data.sKey
        });

      //     this.http.post(this.ipaddress + '/service001/checkOTPSetting', param).map(res => res.json()).subscribe(
      //         data => {
      //             if (data.code == "0000") {
      //                 this.loading.dismiss();                   
      //                // let parameter: any;
      //                 // let commAmt = 0;
      //                 // let totalAmt = parseFloat(this._data.amount) + commAmt;
      //                 // let parameter = {
      //                 //   userID: this.userdata.userID, sessionID: this.userdata.sessionID, fromAccount: this.transferData.fromAcc,
      //                 //   toAccount: "", merchantID: this.merchantId, merchantName: this.merchantName, amount: this._data.amount,
      //                 //   bankCharges: 0, totalAmount: totalAmt, narrative: "", refNo: '',
      //                 //   field1: '2', field2: '', sKey: "", paidBy: "mBanking", fromName: "", toName: "", receiverID: ""
      //                 // };             
                                  
      //                 this.navCtrl.push(QPayConfirmPage, {
      //                     merchantlist: this.merchantlist,
      //                     data: this._data,
      //                     otp: this._obj.userID,
      //                     sKey: data.sKey
      //                 });
      //                 //this.payment(data.sKey);
      //             }
      //             else if (data.code == "0016") {
      //                 let confirm = this.alertCtrl.create({
      //                     title: 'Warning!',
      //                     enableBackdropDismiss: false,
      //                     message: data.desc,
      //                     buttons: [{
      //                         text: 'OK',
      //                         handler: () => {
      //                             this.storage.remove('userData');
      //                             this.events.publish('login_success', false);
      //                             this.events.publish('lastTimeLoginSuccess', '');
      //                             this.navCtrl.setRoot(Login, {});
      //                             this.navCtrl.popToRoot();
      //                         }
      //                     }],
      //                     cssClass: 'warningAlert',
      //                 })
      //                 confirm.present();
      //                 this.loading.dismiss();
      //             }
      //             else {
      //                 this.all.showAlert('Warning!', data.desc);
      //                 this.loading.dismiss();
      //             }
      //         },
      //         error => {
      //             this.all.showAlert('Warning!', this.all.getErrorMessage(error));
      //             this.loading.dismiss();
      //         }
      //     );
       }
  }

  
    payment() {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      let commAmt = 0;
      let totalAmt = parseFloat(this._data.amount) + commAmt;
      let parameter = {
        userID: this.userdata.userID, sessionID: this.userdata.sessionID, fromAccount: this.transferData.fromAcc,
        toAccount: "", merchantID: this.merchantId, merchantName: this.merchantName, amount: this._data.amount,
        bankCharges: 0, totalAmount: totalAmt, narrative: "", refNo: '',
        field1: '2', field2: '', sKey: "", paidBy: "mBanking", fromName: "", toName: "", receiverID: ""
      };
      console.log("M-PayReq>>>",parameter);
      this.http.post(this.ipaddress + '/service003/goMerchantTransfer', parameter).map(res => res.json()).subscribe(data1 => {
        console.log("M-Pay>>>",data1);
        if (data1.code == "0000") {
          this.loading.dismiss();
          /* this.navCtrl.setRoot(AccountTransferDetail, {
            data: data1,
            detail: this.passTemp,
            type: this.passType,
            fromPage: this.fromPage
          }) */
          // this.navCtrl.push(QPayConfirmPage, {
          //   billerid: this.billerid,
          //   data: this._data
          // });
        }
        else if (data1.code == "0016") {
          this.logoutAlert(data1.desc);
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', data1.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    }
  
    getAccountSummary() {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      let parameter = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
      this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
        if (result.code == "0000") {
          let tempArray = [];
          if (!Array.isArray(result.dataList)) {
            tempArray.push(result.dataList);
            result.dataList = tempArray;
          }
          this.fromAccountlist = result.dataList;
          this.loading.dismiss();
        }
        else if (result.code == "0016") {
          this.logoutAlert('Warning!', result.desc);        
          this.loading.dismiss();
        }
        else {
          this.all.showAlert('Warning!', result.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    }
  
    // changeAcc(s, account) {
    //   if (s == 1) {
    //     this.fromaccMsg = '';
    //   }
      
    //   for (let i = 0; i < this.fromAccountlist.length; i++) {
    //     if (account == this.fromAccountlist[i].depositAcc) {
    //       this.amountBal = this.fromAccountlist[i].avlBal;
    //       this.accountBal = this.fromAccountlist[i].avlBal + " " + this.fromAccountlist[i].ccy
    //     }
    //   }
    //   this._data.account = account;
    // }

    changeAcc(s, account) {
      if (s == 1) {
        this.fromaccMsg = '';
      }
      else if (s == 2) {
        this.toaccMsg = '';
      }
      for (let i = 0; i < this.fromAccountlist.length; i++) {
        if (account == this.fromAccountlist[i].depositAcc) {
          this.amountBal = this.fromAccountlist[i].avlBal;
          this.accountBal = this.fromAccountlist[i].avlBal + " " + this.fromAccountlist[i].ccy
        }
      }
      this._data.account = account;
    }

}
