import { Component } from '@angular/core';
import { MenuController, ModalController, NavController, NavParams, LoadingController, PopoverController, ToastController, AlertController, Events, ActionSheetController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';

import { DatePipe } from '@angular/common';
import { TransferDetails } from '../transfer-details/transfer-details';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { App } from 'ionic-angular';
import { TransferPasswordPage } from '../transfer-password/transfer-password';
import { Login } from '../login/login';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { PaymentFailPage } from '../payment-fail/payment-fail';
import { PaymentDetailsPage } from '../payment-details/payment-details';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { EventLoggerProvider } from '../../providers/eventlogger/eventlogger';

@Component({
  selector: 'page-merchant-payment-confirm',
  templateUrl: 'merchant-payment-confirm.html',
  providers: [CreatePopoverPage]
})
export class MerchantPaymentConfirmPage {

  textEng: any = ["Confirmation", "Name", "Balance", "MerchantID", "Institution", "Amount",
    "Remark", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "Cancel",
    "Confirm", "Reference", "Type"];
  textMyan: any = ["အတည်ပြုခြင်း", "အမည်", "လက်ကျန်ငွေ", "ဖုန်းနံပါတ်", "အော်ပရေတာ", "ငွေပမာဏ",
    "မှတ်ချက်", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "ပယ်ဖျက်မည်",
    "လုပ်ဆောင်မည်", "အကြောင်းအရာ", "အမျိုးအစား"];
  textData: string[] = [];
  font: string = '';

  userID: string = '';
  beneficiaryID: string = '';
  toInstitutionCode: string = '';
  amount: any;
  remark: string = '';
  ipaddress: string;
  public loading;
  storageToken:any;
  popover: any;
  token:any;
  password:any;
  errormsg1: any;
  _obj: any = {
    "userID": "", "sessionID": "", "payeeID": "", "beneficiaryID": "",
    "fromInstitutionCode": "", "toInstitutionCode": "", "reference": "", "toAcc": "",
    "amount": "", "bankCharges": "", "commissionCharges": "", "remark": "", "transferType": "2",
    "sKey": "", fromName: "", toName: "", "wType": "",
    "field1": "", "field2": ""
  }
  
  resData: any;
  tosKey: any;
  passPassword: any;
  passTemp: any;
  passSKey: any;
  fromPage: any;
  passType: any;
  param: any;
  ammount: any;
  remark_flag: boolean = false;
  merchant: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public menuCtrl: MenuController, public modalCtrl: ModalController, public http: Http,public platform: Platform,
    public datePipe: DatePipe, public loadingCtrl: LoadingController, public toastCtrl: ToastController,
    public sqlite: SQLite, public changefont: Changefont, public createPopover: CreatePopoverPage,
    public global: GlobalProvider, public events: Events, public util: UtilProvider,public popoverCtrl: PopoverController,
    public alertCtrl: AlertController, public appCtrl: App,public all: AllserviceProvider,private eventlog:EventLoggerProvider) {
    this._obj = this.navParams.get('data');
    console.log('My Test Data is NDH: '+JSON.stringify(this._obj));
    this._obj.amount=this.navParams.get('amount');
    this.passPassword = this.navParams.get("psw");
    console.log("password >>" + JSON.stringify(this.passPassword));
    console.log("Transfer Confrim" + JSON.stringify(this._obj));
    if (this._obj.remark != '' && this._obj.remark != undefined) {
      this.remark_flag = true;
    }
    this.tosKey = this.navParams.get('tosKey');
    this.ammount = this.util.formatAmount(this._obj.amount);
    this.param = this.navParams.get("messageParam");
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
        
      } else {
        this.ipaddress = this.global.ipaddress;
        
      }
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    this.storage.get('firebaseToken').then((firebaseToken) => {
      this.storageToken = firebaseToken;
      console.log('Storage Login Token: '+this.storageToken);
    });
   
  }

//------------------------//

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  goBack() {
    
    this.navCtrl.pop();
  }
  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }
  presentPopoverContact(ev) {
    //this.buttonColor = '#D8D8D8';
    this.popover = this.popoverCtrl.create(TransferPasswordPage, { data: ev }, { cssClass: 'contact-popover' });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss,5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != undefined && data != null && data != '') {
        //this.buttonColor='';
        this.password=data;
        console.log('Confrim Password Log: '+this.password);
        this.paymentV2();
      }
      else{
        //this.buttonColor='';
      }
    });
  }
  paymentV2() {
    // this.btnflag = true;
    if (this.util.checkInputIsEmpty(this._obj.amount) || this.util.checkNumberOrLetter(this._obj.amount) ||
      this.util.checkAmountIsZero(this._obj.amount) || this.util.checkAmountIsLowerThanZero(this._obj.amount) ||
      this.util.checkStartZero(this._obj.amount) || this.util.checkPlusSign(this._obj.amount)) {
      this.errormsg1 = this.textData[16];
      // this.btnflag = false;
     
    }
    else{
      
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let amount = this.util.formatToDouble(this._obj.amount); 
    let iv = this.all.getIvs();
    let dm = this.all.getIvs();
    let salt = this.all.getIvs(); 
    let parameter = {
      token: this._obj.sessionID,
      senderCode: this._obj.userID,
      merchantID: this._obj.beneficiaryID,
      fromName:this._obj.fromName,
      toName: this._obj.merchantName,
      amount: amount, 
      prevBalance: this._obj.balance,
      "password": this.all.getEncryptText(iv, salt, dm, this.password),
      "iv": iv, "dm": dm, "salt": salt
      //bankCharges: 0, 
      //refNo: 110,
      //field1: '',
      //merchantID:this.merchant.merchantID,
      //trprevBalance: this._obj.balance
    };
    this.http.post(this.ipaddress + '/payment/goMerchantPayment', parameter).map(res => res.json()).subscribe(data => {
      let resdata = {
        "bankRefNo": data.bankRefNumber,
        "code": data.code,
        "desc": data.desc,
        "transDate": data.transactionDate
      };
      console.log("response for payment in ad=" + JSON.stringify(data));
      let code = data.code;
      if (code == '0000') {
        this.eventlog.fbevent('merchant_payment_success',{ userID: this._obj.userID,Message:data.desc});
        if(this.storageToken!=null){
          this.token=this.storageToken;
        }
        else{
          this.token="";
        }
        this.resData = data;
        this.appCtrl.getRootNav().setRoot(PaymentDetailsPage, {
          data: resdata,
          data1: this._obj.amount,
          data2: this._obj.toName,
        });
        this.loading.dismiss();
      }
      else if(code == '0015'){
        this.eventlog.fbevent('merchant_payment_fail',{ userID: this._obj.userID,Message:data.desc});
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      }
      else {
        this.eventlog.fbevent('merchant_payment_fail',{ userID: this._obj.userID,Message:data.desc});
        this.resData = [];
        this.navCtrl.push(PaymentFailPage, {
          desc: data.desc,
          data: this._obj
        })
        this.loading.dismiss();
      }
    },
    error => {
      this.eventlog.fbevent('connection_error',{ userID: this._obj.userID,Message:'Connection Error'});
      let toast = this.toastCtrl.create({
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
      this.loading.dismiss();
    });
  }
  }
goConfrim(){    // this.btnflag = true;
    if (this.util.checkInputIsEmpty(this._obj.amount) || this.util.checkNumberOrLetter(this._obj.amount) ||
      this.util.checkAmountIsZero(this._obj.amount) || this.util.checkAmountIsLowerThanZero(this._obj.amount) ||
      this.util.checkStartZero(this._obj.amount) || this.util.checkPlusSign(this._obj.amount)) {
      this.errormsg1 = this.textData[16];
      // this.btnflag = false;
     
    }
    else{
      
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let amount = this.util.formatToDouble(this._obj.amount); 
    let iv = this.all.getIvs();
    let dm = this.all.getIvs();
    let salt = this.all.getIvs(); 
    let parameter = {
      token: this._obj.sessionID,
      senderCode: this._obj.userID,
      merchantID: this._obj.beneficiaryID,
      fromName:this._obj.fromName,
      toName: this._obj.merchantName,
      amount: amount, 
      prevBalance: this._obj.balance,
      "password": '',
      "iv": iv, "dm": dm, "salt": salt
      //bankCharges: 0, 
      //refNo: 110,
      //field1: '',
      //merchantID:this.merchant.merchantID,
      //trprevBalance: this._obj.balance
    };
    this.http.post(this.ipaddress + '/payment/goMerchantPayment', parameter).map(res => res.json()).subscribe(data => {
      let resdata = {
        "bankRefNo": data.bankRefNumber,
        "code": data.code,
        "desc": data.desc,
        "transDate": data.transactionDate
      };
      console.log("response for payment in ad=" + JSON.stringify(data));
      let code = data.code;
      if (code == '0000') {
        this.eventlog.fbevent('merchant_payment_success',{ userID: this._obj.userID,Message:data.desc});
        if(this.storageToken!=null){
          this.token=this.storageToken;
        }
        else{
          this.token="";
        }
        this.resData = data;
        this.appCtrl.getRootNav().setRoot(PaymentDetailsPage, {
          data: resdata,
          data1: this._obj.amount,
          data2: this._obj.toName,
        });
        this.loading.dismiss();
      }
      else if(code == '0015'){
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      }
      else {
        this.eventlog.fbevent('merchant_payment_fail',{ userID: this._obj.userID,Message:data.desc});
        this.resData = [];
        this.navCtrl.push(PaymentFailPage, {
          desc: data.desc,
          data: this._obj
        })
        this.loading.dismiss();
      }
    },
    error => {
      let toast = this.toastCtrl.create({
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
      this.loading.dismiss();
    });
  }
  }
 
  
  

 
  


}
