import { Component } from '@angular/core';
import { App, NavController, NavParams, PopoverController, Platform, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MyPopOverListPage } from '../my-pop-over-list/my-pop-over-list';
import { FontPopoverPage } from '../font-popover/font-popover';
import { Language } from '../language/language';
import { Login } from '../login/login';
import { MainBlankPage } from '../main-blank-page/main-blank-page';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../../providers/global/global';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
  selector: 'page-create-popover',
  templateUrl: 'create-popover.html',
})
export class CreatePopoverPage {

  popover: any;
  db: any;
  createPopover: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public platform: Platform,
    public storage: Storage,
    public events: Events,
    public appCtrl: App,
    private firebase: FirebaseAnalytics,
    public alertCtrl: AlertController, public sqlite: SQLite, public global: GlobalProvider) {
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad CreatePopoverPage');
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(MyPopOverListPage, {});
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      if (data == "2") {
        this.goLogout();
      }
      if (data == "4") {
        this.presentPopoverFont(ev);
      }
    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(Language, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      ////console.log("Selected Item is " + data);
    });
  }

  presentPopoverFont(ev) {
    this.popover = this.popoverCtrl.create(FontPopoverPage, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      // //console.log("Selected Item is " + data);
    });
  }


  presentPopoverCloud(ev) {
    this.popover = this.popoverCtrl.create(MainBlankPage, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      // //console.log("popover dismissed");
      ////console.log("Selected Item is " + data);
      if (data != null) {
        this.storage.set('settings', data);
      }
    });

  }

  goLogout() {
    let alert = this.alertCtrl.create({
      title: 'Are you sure you want to exit?',
      enableBackdropDismiss: false,
      message: '',
      buttons: [{
        text: 'No',
        handler: () => {
        }
      },
      {
        text: 'Yes',
        handler: () => {
          // this.storage.remove('phonenumber');
          // this.firebase.logEvent('log_out', {log_out:'logout-user'});
          this.storage.remove('username');
          this.storage.remove('userData');
          this.storage.remove('firstlogin');
          this.appCtrl.getRootNav().setRoot(Login);
          this.deleteLocalSQLiteDB();
        }
      }]
      
    });
    alert.present();
  }
  logoutConfirm() {
    let logoutalert = this.alertCtrl.create({
      title: 'Log Out',
      enableBackdropDismiss: false,
      message: 'Your account is logged on other device',
      buttons:[
        {
          text: 'Confirm',
          handler: () => {
            // this.firebase.logEvent('log_out', {log_out:'logout-user'});
            this.storage.remove('username');
            this.storage.remove('userData');
            this.storage.remove('firstlogin');
            this.appCtrl.getRootNav().setRoot(Login);
           // this.createPopover.deleteLocalSQLiteDB();
          }
        }
      ]
     });
     logoutalert.present();
  }

  deleteLocalSQLiteDB() {
    /* 
    this.db.executeSql("DELETE FROM channel", []).then((data) => {
      //console.log("Delete data successfully FOR LOGOUT", data);
    }, (error) => {
      console.error("Unable to delete data FOR LOGOUT", error);
    });

    this.db.executeSql("DELETE FROM Contact", []).then((data) => {
      //console.log("Delete data successfully FOR LOGOUT", data);
    }, (error) => {
      console.error("Unable to delete data FOR LOGOUT", error);
    }); */

    this.sqlite.deleteDatabase({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success drop database");
    }, (error) => {
      //console.error("Unable to open database", error);
    });
  }
}
