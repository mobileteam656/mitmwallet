import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, PopoverController, Content, NavParams, ToastController, LoadingController, Events, Platform, AlertController } from 'ionic-angular';
import { ShoplistPage } from '../shoplist/shoplist';
import { GlobalProvider } from '../../providers/global/global';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { UtilProvider } from '../../providers/util/util';
import { Login } from '../login/login';
import { PopOverListPage } from '../pop-over-list/pop-over-list';
import { MainBlankPage } from '../main-blank-page/main-blank-page';
import { Language } from '../language/language';
import { ProductlistPage } from '../productlist/productlist';
import { MyPopOverListPage } from '../my-pop-over-list/my-pop-over-list';
import { FontPopoverPage } from '../font-popover/font-popover';
import { LogoutPage } from '../logout/logout';
import { Network } from '@ionic-native/network';
import { Changefont } from '../changefont/changeFont';
import { Keyboard } from '@ionic-native/keyboard';
import { WalletPage } from '../wallet/wallet';


/**
 * Generated class for the NewShopPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-new-shop',
  templateUrl: 'new-shop.html',
  providers: [Changefont]
})
export class NewShopPage {
  ipaddress: string;
  regionList: string[] = [];
  public loading;
  popover: any;
  accountList: any;
  tempData: any;

  searchData: string = '';

  orgsyskey: any;

  defaultreg = this.getDefaultObj();
  getDefaultObj() {
    return { RegionRefSyskey: 1024 };
  }

  items: any;
  region: string = '1024';
  flagNet: any;
  ipPhoto = this.global.ipphoto;

  textMyan: any = ["စျေးဆိုင်များ", "ဒေတာမရှိပါ", "တိုင်းဒေသ"];
  textEng: any = ["Shops", "No Result Found !!!", "Region"];


  textdata: string[] = [];
  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  font: string = '';
  outletSyskey: any;

  count: number = 0;  // shop alert control
  public tempShops: any = [];
  public shops: any = [];

  @ViewChild(Content) content: Content;

  constructor(public navCtrl: NavController, public navParams: NavParams, public global: GlobalProvider,
    public storage: Storage, public http: Http, public util: UtilProvider, public toastCtrl: ToastController,
    public events: Events, public platform: Platform, public loadingCtrl: LoadingController, public popoverCtrl: PopoverController,
    public network: Network, public changefont: Changefont, public alertCtrl: AlertController,
    private keyboard: Keyboard) {
    this.storage.get('ip').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      }
      else {
        this.ipaddress = this.global.ipAdressMFI;
      }
      //clean data for not to show blank img 
      this.global.PersonData.shopDataList.shopdata = [];
      console.log("Shop List length>> " + JSON.stringify(this.global.PersonData.shopDataList.shopdata.length));
      console.log("Shop List Data >> " + JSON.stringify(this.global.PersonData.shopDataList.shopdata));

      this.getRegion();

      //Font change
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      })
      this.storage.get('language').then((font) => {
        this.changelanguage(font);
      });
    });
  }

  ionViewDidEnter() {
    if (this.ipaddress !== undefined && this.ipaddress !== "" && this.ipaddress != null && this.global.PersonData.shopDataList.shopdata.length == 1) {
      this.global.PersonData.shopDataList.shopdata = [];
      this.getRegion();
    }
    this.backButtonExit();

  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      console.log("Active Page=" + this.navCtrl.getActive().name);
      this.platform.exitApp();
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
    }
  }

  //original
  // getItems(ev: any) {
  //   let val = ev.target.value;
  //   if (val == undefined || val.trim() == '')
  //     this.searchData = "";
  //   else
  //     this.searchData = val;
  //   this.getData(this.region, this.searchData);
  // }

  //keyboard search bar test 10.8
  getItems(ev) {
    //this.shops = this.tempShops;
    this.global.PersonData.shopDataList.shopdata = this.tempShops;
    var val = ev.target.value;
    this.searchData = ev.target.value;

    if (val && val.trim() != '') {
      this.global.PersonData.shopDataList.shopdata = this.global.PersonData.shopDataList.shopdata.filter((item) => {
        return (item.outletName.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
      console.log("Search result data >>" + JSON.stringify(this.global.PersonData.shopDataList.shopdata));
    }



    // if (val == undefined || val.trim() == '') {
    //   val = '';
    // } else {
    //   val = val.trim();
    // }
    // if the value is an empty string don't filter the items
    // if (val && val.trim() != '') {
    //   this.shops = this.shops.filter((item) => {
    //     return (item.outletName.toLowerCase().indexOf(val.toLowerCase()) > -1);
    //   })
    //   console.log("Search result data >>" + JSON.stringify(this.shops));
    // }
    // else {
    //   this.getData(this.region, val);
    // }
  }

  getRegion() {
    if (this.global.netWork == "connected") {
      this.loading = this.loadingCtrl.create({
        content: "",
        spinner: 'circles',
        dismissOnPageChange: true,
        duration: 300
      });
      this.loading.present();
      this.http.get(this.global.ipAdressMFI+ '/serviceMFI/getRegionList').map(res => res.json()).subscribe(data => {
        if (data.data != null) {
          this.regionList = data.data;
        } else {
          let toast = this.toastCtrl.create({
            message: "Can't connect right now!",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        }
        this.onSelectChange(this.region);
        this.loading.dismiss();
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        });
      this.loading.dismiss();
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }
  }

  onSelectChange(selectedValue: any) {
    console.log('Selected', selectedValue);
    this.region = selectedValue;
    this.getData(this.region, "");
  }

  getData(reg, ser) {
    // if (this.global.flagNet == "success") {
    //   this.loading = this.loadingCtrl.create({
    //     content: "",
    //     spinner: 'circles',
    //     dismissOnPageChange: true
    //     //   duration: 3000
    //   });
    this.http.get(this.global.ipAdressMFI+ '/serviceMFI/getShopListByRegionSyskeyandSearchData?regionsyskey=' + reg
      + '&searchdata=' + ser).map(res => res.json().data).subscribe(data => {
        if (data != null) {
          this.global.PersonData.shopDataList.shopdata = data;
          this.tempShops = this.global.PersonData.shopDataList.shopdata;
          //psst test for searchbar 
          //this.shops = data;
          //this.tempShops = this.shops;

          //this.getItems(this.searchData);
          if (this.searchData && this.searchData.trim() != '') {
            this.global.PersonData.shopDataList.shopdata = this.global.PersonData.shopDataList.shopdata.filter((item) => {
              return (item.outletName.toLowerCase().indexOf(this.searchData.toLowerCase()) > -1);
            })
          }

          console.log("shoplist>>" + JSON.stringify(this.global.PersonData.shopDataList.shopdata));
        } else {
          let toast = this.toastCtrl.create({
            message: "Can't connect right now!",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        }
        this.loading.dismiss();
        this.getOrganizationSyskey();
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    // } else {
    //   let toast = this.toastCtrl.create({
    //     message: "Check your internet connection!",
    //     duration: 3000,
    //     position: 'bottom',
    //     //  showCloseButton: true,
    //     dismissOnPageChange: true,
    //     // closeButtonText: 'OK'
    //   });
    //   toast.present(toast);
    // }
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(MyPopOverListPage, {}, { cssClass: 'contact-popover' });
    this.popover.present({
      ev: ev
    });
    this.popover.onWillDismiss(data => {

      if (data == 0) {
        this.navCtrl.push(MainBlankPage, {
          data: ''
        });
      }
      else if (data == 1) {
        this.navCtrl.push(Language, {
          data: ''
        });
      }
      else if (data == 2) {
        this.navCtrl.push(FontPopoverPage, {
          data: ''
        });
      }
      // else if (data == 3) {
      //   this.navCtrl.push(LogoutPage, {
      //     data: ''
      //   });
      // }
    });
  }

  //org 6.8
  // move(shopData) {
  //   this.global.outletSyskey = shopData.outletSyskey;
  //   this.global.startshop = 1;
  //   this.navCtrl.push(ProductlistPage,
  //     {
  //       obj: this.PersonData

  //     });
  // }

  move(shopData) {
    this.outletSyskey = shopData.outletSyskey;
    this.global.startshop = 1;
    console.log("this.global.outletSyskey:" + this.global.outletSyskey);
    if (this.global.outletSyskey == this.outletSyskey) {
      console.log("shop list equal::" + this.global.outletSyskey + this.outletSyskey);
      this.global.outletSyskey = shopData.outletSyskey;
      this.navCtrl.push(ProductlistPage,
        {

        });
    }
    else { //psst updated  for shop alert      
      if (this.global.outletSyskey != '' || this.global.outletSyskey != undefined) {

        this.global.outletSyskey = shopData.outletSyskey;
        this.global.PersonData.applyProductList.data = [];
        this.global.productcount = 0;
        console.log('Yes clicked');
        console.log("data" + this.global.PersonData.applyProductList.data);
        this.navCtrl.push(ProductlistPage,
          {
          });
      }
      else {
        console.log("shop list not equal::" + this.global.outletSyskey + this.outletSyskey);
        this.global.outletSyskey = shopData.outletSyskey;
        this.global.PersonData.applyProductList.data = [];
        this.global.productcount = 0;
        console.log('Yes clicked');
        console.log("data" + this.global.PersonData.applyProductList.data);
        this.navCtrl.push(ProductlistPage,
          {

          });
        console.log("shop list organizationSyskey::" + JSON.stringify(this.global.outletSyskey));
      }
    }

  }

  next() {
    this.global.outletSyskey = '';
    this.global.startshop = 0;
    this.navCtrl.push(Login,
      {
      });
  }

  getOrganizationSyskey() {
    // if (this.global.flagNet == "success") {
    //   this.loading = this.loadingCtrl.create({
    //     content: "",
    //     spinner: 'circles',
    //     // //dismissOnPageChange: true
    //     duration: 300
    //   });
    this.http.get(this.global.ipAdressMFI + '/serviceMFI/getOrganizationSyskey').map(res => res.json()).subscribe(data => {
      if (data != null) {
        this.orgsyskey = data.syskey;

        this.storage.get('orgsyskey').then((result) => {
          if (result == null || result == '') {
            this.storage.set('orgsyskey', this.orgsyskey);
            this.events.publish('orgsyskey', this.orgsyskey);
          }
          else {
            this.storage.remove('orgsyskey');
            this.storage.set('orgsyskey', this.orgsyskey);
          }
        });
      } else {
        let toast = this.toastCtrl.create({
          message: data.msgCode,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      }
      this.loading.dismiss();
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.util.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
    // } else {
    //   let toast = this.toastCtrl.create({
    //     message: "Check your internet connection!",
    //     duration: 3000,
    //     position: 'bottom',
    //     //  showCloseButton: true,
    //     dismissOnPageChange: true,
    //     // closeButtonText: 'OK'
    //   });
    //   toast.present(toast);
    // }
  }


  // tested for hide keyboard text for no data available
  // tab-button   tab-highlight    tabbar  tabbar show-tabbar  tabs tabs-md
  ionViewCanEnter() {
    this.keyboard.onKeyboardShow()
      .subscribe(data => {
        let tab = this.content;
        let controlTab = tab.getNativeElement().querySelector('.tabs');
        if (controlTab !== undefined && controlTab !== null) {
          controlTab.setAttribute("style", 'display: none');
        }
      });

    this.keyboard.onKeyboardHide()
      .subscribe(data => {
        let tab = this.content;
        let controlTab = tab.getNativeElement().querySelector('.tabs');
        if (controlTab !== undefined && controlTab !== null) {
          controlTab.removeAttribute("style");
        }
      });
  }



  // psst tested hide keyboard 
  //   platform.ready().then(() => {
  //     Keyboard.onKeyboardShow().subscribe(() => {
  //         document.body.classList.add('keyboard-is-open');
  //     });

  //     Keyboard.onKeyboardHide().subscribe(() => {
  //         document.body.classList.remove('keyboard-is-open');
  //     });
  // });

  backButton() {
    this.navCtrl.setRoot(WalletPage);
  }
}



