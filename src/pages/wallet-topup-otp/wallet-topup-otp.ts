import 'rxjs/add/operator/map';

import {
    AlertController, Events, LoadingController, NavController, NavParams, Platform, ToastController
} from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';

import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
//import { SMS } from '@ionic-native/sms';
import { Storage } from '@ionic/storage';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Login } from '../login/login';

import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { TransferDetails } from '../transfer-details/transfer-details';
import { WalletPage } from '../wallet/wallet';
import { GlobalProvider } from '../../providers/global/global';

declare var window: any;
@Component({
  selector: 'page-wallet-topup-otp',
  templateUrl: 'wallet-topup-otp.html',
})
export class WalletTopupOtpPage {
  textMyan: any = ["အတည်ပြုသည်", "လူကြီးမင်းဖုန်းသို့ အတည်ပြုနံပါတ်ပို့ထားပြီးပါပြီ", "ခေတ္တစောင့်ဆိုင်းပေးပါ", "နံပါတ်ပြန်လည်ရယူမည်", "အတည်ပြု ကုဒ်နံပါတ်", "အတည်ပြုသည်"];
  textEng: any = ["CONFIRM", "Confirmation code has been sent.", "Please wait.", "Resend", "Confirmation Code", "Verify"];
  showFont: string[] = [];
  font: string;
  otpcode: any = '';
  regdata: any;
  smsArived: any;
  userData: any;
  passtemp1: any;
  dateString:string;
  _walletObj: any;
  passtType: any;
  public loading;
  flag: string;
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  ipaddress: string;
  fromaccMsg: any = '';
  toaccMsg: any = '';
  amountcMsg: any = '';
  fromaccMsg1: any = '';
  toaccMsg1: any = '';
  amountcMsg1: any = '';
  fromAccountlist: any = [];
  passSKey: any;
  fromPage: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public toastCtrl: ToastController, public all: AllserviceProvider,
    public storage: Storage, public alertCtrl: AlertController, public global: GlobalProvider, public events: Events, public network: Network, public platform: Platform, private slimLoader: SlimLoadingBarService, public changeLanguage: ChangelanguageProvider,
    private firebaseAnalytics: FirebaseAnalytics) {
   /*  this.slimLoader.start(() => {
    }); */
    this.passtemp1 = this.navParams.get('dataOTP');
    this._walletObj = this.navParams.get('data');
    console.log('Wallet Top up Data is: '+JSON.stringify(this._walletObj));
    this.passSKey = this.navParams.get('sKey');
   // this.fromPage = this.navParams.get('fromPage');
    this.storage.get('ipaddressApp').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressApp;
      }
    }); 
    this.events.subscribe('ipaddressApp', ip => {
      this.ipaddress = ip;
    });
    this.storage.get('loginData').then((userData) => {
      this.userData = userData;
    });
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  getOTP() {
    this.checkNetwork();
    if (this.flag == "success") {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, type: 16, merchantID: '', sKey: this.passSKey };
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.passtemp1.rKey = data.rKey;
          this.loading.dismiss();
        }
        else if (data.code == "0016") {
          this.logoutAlert(data.desc);
          this.loading.dismiss();
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
    // // this.idleWatch();
    this.network.onDisconnect().subscribe(data => {
      this.flag = 'none';
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }, error => console.error(error));
  }

  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }

  confirm() {
    this.checkNetwork();
    if (this.flag == "success") {
      this.loading = this.loadingCtrl.create({
        content: "Processing...",
        dismissOnPageChange: true
        //   duration: 3000
      });
      this.loading.present();
      let param = { userID: this.userData.userID, sessionID: this.userData.sessionID, rKey: this.passtemp1.rKey, otpCode: this.otpcode, sKey: this.passSKey };
      this.http.post(this.ipaddress + '/service001/checkOTP', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.goTransfer();
        }
        else if (data.code == "0016") {
          this.loading.dismiss();
          this.logoutAlert(data.desc);
        }
        else {
          this.loading.dismiss();
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
        }
      },
        error => {
          this.loading.dismiss();
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        });
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
  }

  goTransfer() {
    this._walletObj.amountTotal = this._walletObj.amount;
    let parameter = {
      userID: this.userData.userID,
      sessionID: this.userData.sessionID,
      fromAccount: this._walletObj.fromAccount,
      toAccount: "",
      merchantID: "",
      bankCharges: "",
      refNo: this._walletObj.receiverID,
      sKey: this.passSKey,
      amount: this.formatToDouble(this._walletObj.amount),
      narrative: this._walletObj.narrative,
      field1: "2",
      field2: "",
      fromName: this._walletObj.fromName,
      toName: this._walletObj.toName
    };
    this.http.post(this.ipaddress + '/service003/goTopupToWallet', parameter).map(res => res.json()).subscribe(res => {
      this.dateString=res.transactionDate.substr(6,4);
      this.dateString+=res.transactionDate.substr(3,2);
      this.dateString+=res.transactionDate.substr(0,2);
      let resdata = {
        "bankRefNo": res.bankRefNumber,
        "code": res.code,
        "desc": res.desc,
        // "transDate": this.util.getRDatePickerDate(res.transactionDate)
        "transDate": this.dateString
      };
      if (res.code == "0000") {
        this.navCtrl.setRoot(TransferDetails, {
          data: resdata,
          data1: this._walletObj.amount,
          data2: this._walletObj.toName
        });
        this.loading.dismiss();
      }
      else if (res.code == "0016") {
        this.logoutAlert(res.desc);
        this.loading.dismiss();
      }
      else {
        this.all.showAlert('Warning!', res.desc);
        this.loading.dismiss();
      }
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.all.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
        ////this.slimLoader.complete();
      });
  }

  formatToDouble(amount) {
    return amount.replace(/[,]/g, '');
  }

  formatAmount(n) {
    return (+n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }

  ionViewDidLoad() {
  }

  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.remove('userData');
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(WalletPage, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }
}
