import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
@Component({
  selector: 'page-mtp-topup-success',
  templateUrl: 'mtp-topup-success.html',
})
export class MtpTopupSuccessPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MtpTopupSuccessPage');
  }

}
