import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, ModalController, LoadingController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { DatePipe } from '@angular/common';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { TabsPage } from '../tabs/tabs';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
declare var window: any;

@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
  providers: [CreatePopoverPage]
})

export class ResetPassword {
  textEng: any = ["Reset Password", "One Time Password", "New Password", "Confirm Password", "Reset","Please fill Reset Password Code.","Please fill New Password.","Please fill Confirm Password."];
  textMyan: any = ["လျှို့ဝှက်နံပါတ် သတ်မှတ်မည်", "တစ်ခါသုံးလျှို့ဝှက်နံပါတ်ရိုက်ပါ", "လျှို့ဝှက်နံပါတ်အသစ်", "လျှို့ဝှက်နံပါတ်အသစ်အတည်ပြု", "ပြန်လည်သတ်မှတ်ပါ","ကျေးဇူးပြုပြီး Reset Password Code ကိုဖြည့်ပါ","ကျေးဇူးပြုပြီး လျှို့ဝှက်နံပါတ်အသစ် ကိုဖြည့်ပါ","ကျေးဇူးပြုပြီး လျှို့ဝှက်နံပါတ်အသစ်အတည်ပြု ကိုဖြည့်ပါ"];
  textData: string[] = [];
  language: any;
  showFont: any;
  errormsg: any;
  ipaddress: string;
  phone: any;
  userdata: any;
  public loading;
  oldPassword: string = '';
  newPassword: string = '';
  passwordConfirm: string = '';
  getotp: any;
  msgForUser: any;
  smsArived: any;
  otpcode: any;
  pdata: any = {};

  constructor(
    private firebase: FirebaseAnalytics,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public http: Http,
    public datePipe: DatePipe,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public sqlite: SQLite,
    public events: Events,
    public util: UtilProvider,
    public alertCtrl: AlertController,
    public global: GlobalProvider,
    public createPopover: CreatePopoverPage,
    public slimLoader: SlimLoadingBarService, private all: AllserviceProvider,
  ) {
    this.showFont = "uni";   
    this.userdata = navParams.get("userData");
    this.phone = this.userdata.userID;
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddress;
      }
      this.getOTP();
      this.readPasswordPolicy();

    });
    
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
  }

  changelanguage(lan) {
    this.language = lan;
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  checkUndefinedOrNullOrBlank(param) {
    if (param == undefined || param == null || param == "") {
      return true;
    } else {
      return false;
    }
  }

  checkBlankAfterTrim(param) {
    if (param == "") {
      return true;
    } else {
      return false;
    }
  }

  updatePassword() {
    let isValid = true;
    if (this.checkUndefinedOrNullOrBlank(this.oldPassword)) {
      isValid = false;
      this.msgForUser = "Invalid One Time Password.";
      this.showAlert();
    }
    else if (this.checkUndefinedOrNullOrBlank(this.newPassword)) {
      isValid = false;
      this.msgForUser = "Invalid New Password.";
      this.showAlert();
    }
    else if (this.checkUndefinedOrNullOrBlank(this.passwordConfirm)) {
      isValid = false;
      this.msgForUser = "Invalid Confirm Password.";
      this.showAlert();
    }
    if (isValid) {
      this.oldPassword = this.oldPassword.trim();
      this.newPassword = this.newPassword.trim();
      this.passwordConfirm = this.passwordConfirm.trim();
      if (this.checkBlankAfterTrim(this.oldPassword)) {
        this.msgForUser = "Invalid OTP.";
        this.showAlert();
      } else if (this.checkBlankAfterTrim(this.newPassword)) {
        this.msgForUser = "Invalid New Password.";
        this.showAlert();
      } else if (this.checkBlankAfterTrim(this.passwordConfirm)) {
        this.msgForUser = "Invalid Confirm Password.";
        this.showAlert();
      } else if (this.newPassword != this.passwordConfirm) {
        this.msgForUser = "New Password and Confirm Password should be same.";
        this.showAlert();
      } else {
        this.changePassword();
      }
    }
  }

  changePassword() {
    let iv = this.all.getIvs();
    let dm = this.all.getIvs();
    let salt = this.all.getIvs(); 
    let changePasswordParams = {
      newpassword: this.all.getEncryptText(iv, salt, dm, this.newPassword),
      userid: this.phone,
      sessionid: this.userdata.sessionID,
      "iv": iv, "dm": dm, "salt": salt
    };
    this.http.post(this.ipaddress + '/service001/resetPassword', changePasswordParams).map(res => res.json()).subscribe(
      data => {
        this.loading.dismiss();   
        // this.firebase.logEvent('updatepwd', { userID: this.newPassword })
        // .then((res: any) => { console.log(res); })
        // .catch((error: any) => console.error(error));     
        if (data.msgCode == '0000') {
          this.msgForUser = "Your password reset  successfully.";
          this.showAlert();         
          this.oldPassword = '';
          this.newPassword = '';
          this.passwordConfirm = '';
          let params = { tabIndex: 0, tabTitle: "Home" };
          this.navCtrl.setRoot(TabsPage, params).catch((err: any) => {
          });    
        } else {
          this.msgForUser = data.msgDesc;
          this.showAlert();
        }
      }, error => {
        this.loading.dismiss();
        // this.firebase.logEvent('updatepwd-error', { userID: this.navParams})
        // .then((res: any) => { console.log(res); })
        // .catch((error: any) => console.error(error));
        this.msgForUser = "Your password reset  fails.";
        this.showAlert();
        this.getError(error);
      }
    );
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: '',
      enableBackdropDismiss: false,
      message: this.msgForUser,
      buttons: [{
        text: 'OK',
        handler: () => {  
               
        }
      }]
    });
    alert.present();
  }

  getError(error) {   
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    } else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    //toast.present(toast);
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  getOTP() {
    if (this.global.netWork == 'connected') {
      this.slimLoader.start(() => {       
      });
      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, type: '7' };    
      this.http.post(this.ipaddress + '/service001/getOTP', param).map(res => res.json()).subscribe(data => {         
        this.slimLoader.complete();
        if (data.code == "0000") {          
          this.getotp = data;
          window.SMS.startWatch(function () {           
          }, function () {         
          });
          setTimeout(() => {         
          }, 6000);
          window.document.addEventListener('onSMSArrive', res => {
            this.smsArived = res;
            let arr = this.smsArived.data.body.split(' ');
            this.otpcode = arr[3];   //got otp
            if (this.otpcode != undefined) {
              this.slimLoader.complete();
            }
          });
        }
        else {
          this.getError(data.desc);
          this.slimLoader.complete();
        }
      },
        error => {
          this.slimLoader.complete();
          // this.firebase.logEvent('Description', {updatePassword:this.otpcode })
          // .then((res: any) => { console.log(res); })
          // .catch((error: any) => console.error(error));
        });
    }
    else {
      this.slimLoader.complete();
      // this.firebase.logEvent('Error-message', { passwordConfirm:this.oldPassword })
      // .then((res: any) => { console.log(res); })
      // .catch((error: any) => console.error(error));
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
  }

  reset() {      
     if(this.otpcode == '' || this.otpcode == null || this.otpcode == undefined){
       this.errormsg = this.textData[5];
     }else if(this.newPassword == '' || this.newPassword == null || this.newPassword == undefined){
      this.errormsg = this.textData[6];
     }else if(this.passwordConfirm == '' || this.passwordConfirm == null || this.passwordConfirm == undefined){
      this.errormsg = this.textData[7];
     }
     else{ 
      this.loading = this.loadingCtrl.create({
        content: "Processing",
        dismissOnPageChange: true
      });
      this.loading.present();
      let checkotpreq = { userID: this.userdata.userID, sessionID: this.userdata.sessionID, rKey: this.getotp.rKey, otpCode: this.otpcode };
      console.log("reset-password-checkOTP-req" + JSON.stringify(checkotpreq));
      this.http.post(this.ipaddress + '/service001/checkOTP', checkotpreq).map(res => res.json()).subscribe(data => {
      console.log("reset-password-checkOTP-res" + JSON.stringify(data));
        if (data.code == "0000") {
          this.changePassword();         
        }
        else {
          this.getError(data.desc);
          this.loading.dismiss();
        }
      },
        error => {
          this.loading.dismiss();
        }); 
  } /*else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }*/
  }

  readPasswordPolicy() {
    let param = {
      "userID": this.userdata.userID,
      "sessionID": this.userdata.sessionID,
      "pswminlength": "",
      "pswmaxlength": "",
      "spchar": "",
      "upchar": "",
      "lowerchar": "",
      "pswno": "",
      "msgCode": "",
      "msgDesc": ""
    };
    if (this.global.netWork == 'connected') {
      this.http.post(this.ipaddress + '/service001/readPswPolicy', param).map(res => res.json()).subscribe(response => {

        if (response.msgCode == "0000") {
          this.pdata = response;
        }
      },
        error => {
          this.getError(error);
        });
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
  }
}