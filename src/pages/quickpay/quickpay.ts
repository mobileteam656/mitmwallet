import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { QuickpayConfirmPage } from '../quickpay-confirm/quickpay-confirm';


@Component({
  selector: 'page-quickpay',
  templateUrl: 'quickpay.html',
})
export class QuickpayPage {

  textMyan: any = ["ဘေလ်ပေးသွင်းရန်", "Payment Type", "ငွေပေးချေမှုရွေးရန်", "လုပ်ဆောင်မည်", "ပြန်စမည်","ကျေးဇူးပြု၍ ငွေပေးချေမှု ရွေးချယ်ပါ",];
  textEng: any = ["Bill Payment", "Payment Type", "Select Biller", "Submit", "Reset","Please select biller"];
  showFont: string[] = [];
  billerType  = ["ABC Telecomm","One Stop Mart","Wave Money","BNF Express"];
  billerid = "";
  errormsg = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage) {

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QuickpayPage');
  }

  goPay1() {
    if(this.billerid ==""){
      this.errormsg = this.showFont[5];
    }else{
      this.errormsg = "";
      this.navCtrl.push(QuickpayConfirmPage,{
      billerid : this.billerid    
      });
    }   
  }

  goPay(type) {
    this.navCtrl.push(QuickpayConfirmPage,{
      billerid : type   
      });
  }

  goCancel() {
    this.billerid = "";
  }
}
