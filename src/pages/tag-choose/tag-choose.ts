import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Navbar, Events } from 'ionic-angular';
import { ChangelanguageProvider } from "../../providers/changelanguage/changelanguage";
import { Storage } from "@ionic/storage";

@Component({
  selector: 'page-tag-choose',
  templateUrl: 'tag-choose.html',
})
export class TagChoosePage {

  textMyan: any = ["Select Privacy", 'Done'];
  textEng: any = ["Select Privacy", 'Done'];
  textData: any = [];
  showFont: any;

  @ViewChild(Navbar) navBar: Navbar;

  Privacies: any = [
    { syskey: 0, name: 'Public', icon: 'md-globe', des: 'Anyone on or off Digital Connect', select: false },
    { syskey: 1, name: 'Friends', icon: 'md-people', des: 'Your friends on Digital Connect', select: false },
    // /*{syskey: 2, name: 'Specific friends' , icon: 'md-person', des: 'Only show to some friends', select: false},*/
    { syskey: 2, name: 'Only me', icon: 'md-lock', des: 'Only me', select: false },
  ];

  privacy: any = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public changeLanguage: ChangelanguageProvider,
    public events: Events
  ) {
    this.showFont = 'uni';
    this.privacy = this.navParams.get("valueTagChoose");

    if (this.privacy == undefined || this.privacy == null || this.privacy == '')
      this.privacy = 1;

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  doneEvent() {
    this.navCtrl.getPrevious().data.userSelectedPrivacy = this.privacy;
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    this.navBar.backButtonClick = (e: UIEvent) => {
      this.navCtrl.getPrevious().data.userSelectedPrivacy = this.privacy;
      this.navCtrl.pop();
    }
  }

}
