import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ToastController } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { Http, Jsonp } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { Network } from '@ionic-native/network';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { CreatePopoverPage } from '../create-popover/create-popover';
/**
 * Generated class for the FaqPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
  providers: [GlobalProvider, CreatePopoverPage]

})
export class FaqPage {

  textMyan: any = ["မေးလေ့ရှိသောမေးခွန်းများ"];
  textEng: any = ["FAQ"];
  textData: any = [];
  showFont: any;
  ques: any;
  ans: any;
  languageData: any;
  faqData: any = [{ "questionEng": "", "questionUni": "", "answerEng": "", "answerUni": "", "selected": false }];
  ipaddress: string = '';
  flag: any;
  public isLoading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public storage: Storage,
    public http: Http, public menuCtrl: MenuController,
    public global: GlobalProvider,
    public toastCtrl: ToastController,
    public network: Network,
    public createPopover: CreatePopoverPage) {

    // this.menuCtrl.swipeEnable(false);
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddress;
      }
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      });
      this.storage.get('language').then((lan) => {
        this.changelanguage(lan);
      });
      this.getFaqList();
      this.isLoading = 0;

    });

  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad FaqPage');

  }

  changelanguage(lan) {
    this.languageData = lan;
    ////console.log('language' + this.languageData);
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  changeIcon(a) {
    ////console.log("Select index : " + a);
    if (this.faqData[a].selected)
      this.faqData[a].selected = false;
    else
      this.faqData[a].selected = true;
  }

  getFaqList() {
    if (this.global.netWork == 'connected') {

      this.http.get(this.ipaddress + '/service002/getFAQList').map(res => res.json()).subscribe(data => {
        if (data != null && data.code != null && data.code == "0000") {
          this.flag = 1;
          let tempArray = [];
          if (!Array.isArray(data.data)) {
            tempArray.push(data.data);
            data.data = tempArray;
          }
          this.faqData = data.faqData;
        } else
          this.flag = 0;
        this.isLoading = 1;
      },
        error => {
          /* ionic App error
           ............001) url link worng, not found method (404)
           ........... 002) server not response (500)
           ............003) cross fillter not open (403)
           ............004) server stop (-1)
           ............005) app lost connection (0)
           */
          let code;
          if (error.status == 404) {
            code = '001';
          }
          else if (error.status == 500) {
            code = '002';
          }
          else if (error.status == 403) {
            code = '003';
          }
          else if (error.status == -1) {
            code = '004';
          }
          else if (error.status == 0) {
            code = '005';
          }
          else if (error.status == 502) {
            code = '006';
          }
          else {
            code = '000';
          }

          let msg = "Can't connect right now. [" + code + "]";
          let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.isLoading = 1;
        });
    } else {
      let toast = this.toastCtrl.create({
        message: "Please check internet connection!",
        duration: 5000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

}
