// import { SuccessCallback } from 'ionic-native/dist/es5';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ToastController, Platform, LoadingController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Storage } from '@ionic/storage';
import { Changefont } from '../changefont/changeFont';
import { Http } from '@angular/http';
import { PaybillPage } from '../paybill/paybill';

//psst 
import { Badge } from '@ionic-native/badge';
import { ShoppingCartInfoPage } from '../shopping-cart-info/shopping-cart-info';
import { ParseSourceFile } from '@angular/compiler';
import { Network } from '@ionic-native/network';
import { NewShopPage } from '../new-shop/new-shop';
import { ShoplistPage } from '../shoplist/shoplist';

@Component({
  selector: 'page-productlist',
  templateUrl: 'productlist.html',
  providers: [Changefont]

})
export class ProductlistPage {

  prolist = [];
  price: any;
  ipPhoto = this.global.ipphoto;

  textMyan: any = ["ပစ္စည်းများ", "နောက်သို.", "ပစ္စည်းရွေးပါ"];
  textEng: any = ["Products", "Next", "Please Select Product"];
  textdata: string[] = [];
  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  font: string = '';
  ipaddress: string;
  public loading;
  flagNet: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public events: Events,
    public global: GlobalProvider, public changefont: Changefont, public http: Http, public toastCtrl: ToastController,
    public util: UtilProvider, public platform: Platform, public loadingCtrl: LoadingController, private badge: Badge, public network: Network
  ) {

    this.storage.get('ip').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
        console.log("not undefined  conditon ");
        console.log("Your ip is", this.ipaddress);
      }
      else {
        console.log(" undefined  conditon ");
        this.ipaddress = this.global.ipaddress;
        console.log("Your ip main is", this.ipaddress);
      }
      this.storage.set('itemCount', this.global.productcount);
      this.events.publish('itemCount', this.global.productcount);
      this.storage.set('amount', this.global.totalamount);
      this.events.publish('amount', this.global.totalamount);
      //Font change
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      })
      this.storage.get('language').then((font) => {
        console.log('Your language is', font);
        this.changelanguage(font);
      });
      console.log("Newshop data is:" + JSON.stringify(this.global.PersonData));
      console.log('apply product length', this.global.PersonData.applyProductList.data.length);
      this.bindProductList();
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductlistPage');
  }

  bindProductList() {
    console.log("Global outsyskey is >> " + JSON.stringify(this.global.outletSyskey));
    if (this.global.netWork== "connected") {
      this.loading = this.loadingCtrl.create({
        content: "", spinner: 'circles',
        dismissOnPageChange: true
        //   duration: 3000
      });
      this.loading.present();
      this.http.get(this.global.ipAdressMFI + '/serviceMFI/getProductListByOutletSyskey?outletsyskey=' + this.global.outletSyskey).map(res => res.json()).subscribe(data => {
        if (data != null) {
          let tempArray = [];
          if (!Array.isArray(data.data)) {
            tempArray.push(data.data);
            data.data = tempArray;
          }
          this.prolist = [];
          for (let i = 0; i < data.data.length; i++) {
            if (data.data[i].productprice != "" && data.data[i].productprice != undefined && data.data[i].productprice != null) {
              data.data[i].productprice = data.data[i].productprice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
            data.data[i].productImagePath = this.ipPhoto + data.data[i].productImagePath;

            this.prolist.push(data.data[i]);
          }
          console.log("productList data>>" + JSON.stringify(data));
        } else {
          let toast = this.toastCtrl.create({
            message: "Can't connect right now!",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        }
        this.loading.dismiss();
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }
  }

  next() {
    if (this.global.productcount == 0) {
      let toast = this.toastCtrl.create({
        message: this.textdata[2],
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    } else {
      this.navCtrl.push(ShoppingCartInfoPage,
        {


        });
    }
  }

  addCart(obj) {
    this.global.productcount = this.global.productcount + 1;
    this.global.totalamount += parseInt(obj.price);
    this.global.PersonData.applyProductList.data.push({
      "outletProductSyskey": obj.outletProductLinksyskey, "productprice": obj.price,
      "productdescription": obj.productdescription, "category": obj.category,
      "productImagePath": obj.productImagePath
    });
    console.log("Apply Product list is : " + JSON.stringify(this.global.PersonData.applyProductList));
    console.log("Global Product Count is : " + JSON.stringify(this.global.productcount));
    console.log("Global Total Amt is : " + JSON.stringify(this.global.totalamount));
  }

  gobackmain() {
    if (this.global.startshop == 1) {
      this.navCtrl.setRoot(NewShopPage,
        {

        });

    } else {
      this.navCtrl.setRoot(ShoplistPage,
        {

        });
    }

  }

  ionViewDidEnter() {
    this.backButtonExit();
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      this.gobackmain();

    });
  }
}
