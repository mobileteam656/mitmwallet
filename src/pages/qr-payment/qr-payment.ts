import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Storage } from '@ionic/storage';
import { AlertController, App, Events, NavController, NavParams, Platform, PopoverController, ToastController, ViewController } from 'ionic-angular';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { GlobalProvider } from '../../providers/global/global';
import { Changefont } from '../changefont/changeFont';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { Payment } from '../payment/payment';
@Component({
  selector: 'page-qr-payment',
  templateUrl: 'qr-payment.html',
  providers: [CreatePopoverPage],
})

export class QrPayment {

  textEng: any = ["Payment", "Scan QR", "Manual"];
  textMyan: any = ["ငွေပေးသွင်းရန်", "QR ဖတ်မည်", "Manual"];
  textData: string[] = [];
  font: string = '';
  userData: any;
  qrValue: any = [];
  popover: any;
  selectedTitle: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public events: Events, public barcodeScanner: BarcodeScanner, public alertCtrl: AlertController,
    public global: GlobalProvider, public viewCtrl: ViewController, public changefont: Changefont,
    public platform: Platform, public appCtrl: App, public popoverCtrl: PopoverController,
    public createPopover: CreatePopoverPage, public toastCtrl: ToastController, private all: AllserviceProvider) {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
      ////console.log("state data=" + JSON.stringify(this.textData));
    });

    this.storage.get('userData').then((userData) => {
      if (userData != null) {
        this.userData = userData;
      }
    });

    this.events.subscribe('userData', userData => {
      if (userData != null) {
        this.userData = userData;
      }
    });
  }

  scanQR() {
    this.barcodeScanner.scan().then((barcodeData) => {
      if (barcodeData.cancelled) {
          return false;
      } 
      try {
          this.qrValue = this.all.getDecryptText(this.all.iv, this.all.salt, this.all.dm, barcodeData.text);
          this.qrValue = JSON.parse(this.qrValue)   
          this.navCtrl.push(Payment, {
            data: this.qrValue
          });
      } catch (e) {
        this.toastInvalidQR();
        
      }
    }, (err) => {
    });
  }

  toastInvalidQR() {
    let toast = this.toastCtrl.create({
      message: "Invalid QR Type.",
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });

    toast.present(toast);
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }


  formatToDouble(amount) {
    return amount.replace(/[,]/g, '');
  }

  formatAmount(n) {
    return (+n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }
}
