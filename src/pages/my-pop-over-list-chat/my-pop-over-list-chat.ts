import { Component } from '@angular/core';
import { Events, NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import 'rxjs/Rx';
import { Changefont } from '../changefont/changeFont';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'my-pop-over-list-chat',
  templateUrl: 'my-pop-over-list-chat.html',
  providers: [Changefont]
})
export class MyPopOverListChatPage {

  headeritem = "Settings";
  popoverItemList: any[] = [{ name: '', key: 0 }, { name: '', key: 1 }
    , { name: '', key: 2 }, { name: '', key: 3 }
    , { name: '', key: 4 }, { name: '', key: 5 }
    , { name: '', key: 6 }, { name: '', key: 7 }];

  textMyan: any = [{ name: 'Change Group Name', key: 0 }, { name: 'Add Person', key: 1 }
    , { name: 'View Group', key: 2 }, { name: 'Leave Group', key: 3 }
    , { name: 'ဘာသာစကား', key: 4 }, { name: 'ဖောင့်', key: 5 }
    , { name: 'ထွက်မည်', key: 6 }, { name: 'ဗားရှင်း1.1.48', key: 7 }
  ];
//၁၂၃၄၅၆၇၈၉၁၀
  textEng: any = [{ name: 'Change Group Name', key: 0 }, { name: 'Add Person', key: 1 }
    , { name: 'View Group', key: 2 }, { name: 'Leave Group', key: 3 }
    , { name: 'Language', key: 4 }, { name: 'Font', key: 5 }
    , { name: 'Logout', key: 6 }, { name: 'Version1.1.48', key: 7 }
  ];

  showFont: any;
  font: string;
  tempArray: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController, 
    public changefont: Changefont,
    public events: Events, 
    public storage: Storage,
    public global: GlobalProvider
  ) {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      ////console.log('Your language is', font);
      this.changelanguage(font);
    });
  }

  changelanguage(lan) {
    if (lan != 'eng') {
      for (let j = 0; j < this.textMyan.length; j++) {
        this.popoverItemList[j].name = this.textMyan[j].name;
      }
    }    
    else {
      this.font = '';
      for (let j = 0; j < this.textEng.length; j++) {
        this.popoverItemList[j].name = this.textEng[j].name;
      }
    }
  }

  dismissIt(s) {
    this.viewCtrl.dismiss(s);
  }

  goMarket() {
    if (this.global.regionCode == '13000000') {
      //Yangon App (13000000)
      window.open('https://play.google.com/store/apps/details?id=dc.digitalyangon', '_system');
    }
    if (this.global.regionCode == '10000000') {
      //Mandalay App (10000000)
      window.open('https://play.google.com/store/apps/details?id=dc.digitalmandalay', '_system');
    }
    if (this.global.regionCode == '00000000') {
      //All App (00000000)
      window.open('https://play.google.com/store/apps/details?id=wallet.NSBmawallet', '_system');
    }
  }

}
