import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, Events, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { PopOverListPage } from '../pop-over-list/pop-over-list';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { CreatePopoverPage } from '../create-popover/create-popover';

import { Changefont } from '../changefont/changeFont';
import { Storage } from '@ionic/storage';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { Http } from '@angular/http';
import { App } from 'ionic-angular';
import { TransferDetails } from '../transfer-details/transfer-details';
import { PaymentDetailsPage } from '../payment-details/payment-details';
import { PaymentFailPage } from '../payment-fail/payment-fail';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Login } from '../login/login';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
@Component({
  selector: 'page-payment-password',
  templateUrl: 'payment-password.html',
  providers: [Changefont, CreatePopoverPage]
})
export class PaymentPasswordPage {
  popover: any;
  textEng: any = ['Enter Password', "Enter valid password", "Password", "Confirm"];
  textMyan: any = ['လျှို့ဝှက်နံပါတ်ကိုရိုက်ထည့်ပါ', "မှန်ကန်သောလျှို့ဝှက်နံပါတ်ကိုရိုက်ထည့်ပါ", "လျှို့ဝှက်နံပါတ်", "အတည်ပြုပါ"];
  textdata: any = [];

  font: string = '';

  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  errormsg: string = '';
  ipaddress: string;
  ipaddressCMS: string;
  public alertPresented: any;
  isLoading: any;
  loading: any;
  resData: any;
  password: any;
  userData: any;
  _obj: any = {
    "userID": "", "sessionID": "", "payeeID": "", "beneficiaryID": "",
    "fromInstitutionCode": "", "toInstitutionCode": "", "reference": "", "toAcc": "",
    "amount": "", "bankCharges": "", "commissionCharges": "", "remark": "", "transferType": "2",
    "sKey": "", fromName: "", toName: "", "wType": "",
    "field1": "", "field2": ""
  }
  tosKey: any;
  msgparam: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController,
    public global: GlobalProvider, public util: UtilProvider, public events: Events, public toastCtrl: ToastController,
    public createPopover: CreatePopoverPage, public storage: Storage, public changefont: Changefont, public loadingCtrl: LoadingController,
    private slimLoader: SlimLoadingBarService, public alertCtrl: AlertController, public http: Http
    , public appCtrl: App, private all: AllserviceProvider,
    private firebase: FirebaseAnalytics) {


    //this.tosKey = this.navParams.get('tosKey');  
    this._obj = this.navParams.get('data');   
    this.msgparam = this.navParams.get('messageParam');   
    console.log("message param", JSON.stringify(this.msgparam)); 
    this.userData = this.navParams.get('userdata');

    this.storage.get('ip').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddress;
      }
    });
    this.events.subscribe('ip', ip => {
      this.ipaddress = ip;
    });

    this.storage.get('ipaddressCMS').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddressCMS = ip;
      } else {
        this.ipaddressCMS = this.global.ipaddressCMS;
      }
    });
    this.events.subscribe('ipaddressCMS', ip => {
      this.ipaddressCMS = ip;
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });   
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      this.language = this.englanguage;
      for (let i = 0; i < this.textEng.length; i++)
        this.textdata[i] = this.textEng[i];
    } else if (font == "zg") {
      this.font = "zg";
      this.language = this.changefont.UnitoZg(this.myanlanguage);
      for (let i = 0; i < this.textMyan.length; i++)
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
    } else {
      this.font = "uni";
      this.language = this.myanlanguage;
      for (let i = 0; i < this.textMyan.length; i++)
        this.textdata[i] = this.textMyan[i];
    }
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopOverListPage, {}, { cssClass: 'login-popover' });
    this.popover.present({
      ev: ev
    });

    this.popover.onWillDismiss(data => {
      if (data.key == 0) {
        this.createPopover.presentPopoverCloud(ev);
      } else if (data.key == 1) {
        this.createPopover.presentPopoverLanguage(ev);
      } else if (data.key == 3) {
        this.createPopover.presentPopoverFont(ev);
      }
    });
  }

  getconfirm() {
    this.slimLoader.start(() => { });
    let userID1 = this.userData.userID;
    let param = { userID: userID1, sessionID: this.userData.sessionID, type: '', merchantID: '' };
    if (this.password == undefined || this.password == null || this.password == "") {
      this.showAlert('Warning!', this.textdata[0]);
    }
    else {
      // this.msgparam.password = this.password;

      this.goPay();
    }

  }

  goPay() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
      // duration: 3000
    });
    this.loading.present();
    let payeePhone = this.userData.userID;
    // let benePhone = this.util.removePlus(this._obj.beneficiaryID);
    let amount = this.util.formatToDouble(this._obj.amount);
    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    this._obj.beneficiaryID = this._obj.toAcc;
    let iv = this.all.getIvs();
    let dm = this.all.getIvs();
    let salt = this.all.getIvs(); 
    let param = {
      "userID": payeePhone, "sessionID": this.userData.sessionID,
      "payeeID": payeePhone, "beneficiaryID": this._obj.beneficiaryID,
      "fromInstitutionCode": this.userData.frominstituteCode, "toInstitutionCode": this.userData.toInstitutionCode,
      "toAcc": this._obj.toAcc, "amount": this.formatToDouble(this._obj.amount),
      "bankCharges": this._obj.bankCharges, "commissionCharges": this._obj.commissionCharges, "remark": this._obj.remark, "transferType": "1",
      "sKey": this._obj.sKey,
      "tosKey": this._obj.tosKey,
      "fromName": this.userData.fromName,
      "toName": "",
      "wType": this._obj.wType,
      "field1": "", "field2": "",
      "refNo": this._obj.refNo,
      "billId": this._obj.billId,
      "cusName": this._obj.cusName,
      "ccyCode": this._obj.ccy,
      "t1": this._obj.t1,
      "t2": this._obj.t2,
      "dueDate": this._obj.dueDate,
      "deptName": this._obj.deptName,
      "taxDesc": this._obj.taxDesc,
      "paidBy": "mWallet",
      "vendorCode": this._obj.vendorCode,
      "password": this.all.getEncryptText(iv, salt, dm, this.password),
      "iv": iv, "dm": dm, "salt": salt,
      "paymentType":"Utility Pay"
    };
    let transferParam = { "messageRequest": this.msgparam, "transferOutReq": param }
    console.log("utility-payment-req:" + JSON.stringify(transferParam));
    this.http.post(this.ipaddress + '/payment/payMerchant', transferParam).map(res => res.json()).subscribe(data => {
      let resdata = {
        "bankRefNo": data.bankRefNo,
        "code": data.code,
        "desc": data.desc,
        "field1": "",
        "field2": "",
        "transDate": data.transDate
      };
      console.log('resdata'+JSON.stringify(resdata));
      let code = data.code;
      if (code == '0000') {
        // this.firebase.logEvent('utility_payment', {bankRefNo:data.backRefNo,Message:data.desc,Code:data.code })
        // .then((res: any) => { console.log(res); })
        // .catch((error: any) => console.error(error));
        this.resData = data;
        this.appCtrl.getRootNav().setRoot(PaymentDetailsPage, {
          data: resdata,
          data1: this._obj.amount,
          data2: this._obj.cusName
        });
        //  this.navCtrl.setRoot(TransferDetails, {
        //    data: resdata
        //  }); 
        this.loading.dismiss();
      }else if (data.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('phonenumber');
                this.storage.remove('username');
                this.storage.remove('userData');
                this.storage.remove('firstlogin');
                this.appCtrl.getRootNav().setRoot(Login);
              }
            }
          ]
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        // this.firebase.logEvent('utility_payment_fail', {bankRefNo:data.backRefNo,Message:data.desc,Code:data.code })
        // .then((res: any) => { console.log(res); })
        // .catch((error: any) => console.error(error));
        this.resData = [];
        this.navCtrl.push(PaymentFailPage, {
          desc: data.desc,
          data: this._obj,
        })

        this.loading.dismiss();
      }
    },
      error => {
        let toast = this.toastCtrl.create({
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }  
  
  formatToDouble(amount) {
    return amount.replace(/[,]/g, '');
  }


  showAlert(titleText, subTitleText) {
    if (this.alertPresented == undefined) {
      let alert = this.alertCtrl.create({
        title: titleText,
        subTitle: subTitleText,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              this.alertPresented = undefined;
            }
          }
        ],
        enableBackdropDismiss: false
      });

      this.alertPresented = alert.present();
      setTimeout(() => alert.dismiss(), 2000 * 60);
    }
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = "Can't connect right now. [" + code + "]";
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    toast.present(toast);
    this.isLoading = false;
  }
}
