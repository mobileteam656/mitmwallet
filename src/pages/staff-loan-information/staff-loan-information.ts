import { Component, NgModule, ViewChild } from '@angular/core';
import { IonicPage, ActionSheetController, Platform, NavController, NavParams, Navbar, ToastController, MenuController, AlertController, LoadingController, Events, Content, TextInput } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { UtilProvider } from '../../providers/util/util';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { Changefont } from '../changefont/changeFont';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Slides } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { TabsPage } from '../tabs/tabs';
import { Network } from '@ionic-native/network';
import { FileOpener } from '@ionic-native/file-opener';
import { App } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { WalletPage } from '../wallet/wallet';

/**
 * Generated class for the InfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-staff-loan-information',
  templateUrl: 'staff-loan-information.html',
  providers: [Changefont]
})

export class StaffLoanInformationPage {
  @ViewChild(Slides) slides: Slides;
  @ViewChild(Content) content: Content;
  testRadioOpen: boolean;
  testRadioResult;
  title: any;
  callGuran: boolean = true;

  // Gdata = {
  //   'member_name': '', 'nrc': '', 'dob': '',
  //   'current_address_region': '', 'current_address_township': '',
  //   'current_address_street': '', 'current_address_houseno': '',
  //   'mobile': '', 'email': '', 'company_name': '', 'department': '',
  //   'current_position': '', 'years_of_service': '', 'monthly_basic_income': '',
  //   'company_ph_no': '', 'creditForOhters': '0', 'creditAmount': '', 'syskey': '', "front_nrc_image_path": "", "back_nrc_image_path": ""
  // }
  Gdata() {
    return {
      "mobile": "",
      "guarantorName": "",
      "guarantorNrc": "",
      "department": "",
      "currentAddressTownship": "",
      "currentAddressRegion": "",
      "currentPosition": "",
      "currentAddressStreet": "",
      "currentAddressHouseNo": "",
      "backNRCImagePath": "",
      "frontNRCImagePath": "",
      "guarantorSyskey": '0',
      "guarantorPDFImagePath": "",
      "guarantorSignImagePath": ""
    };
  }


  GuranInfo = [{ 'guarantorSyskey': '', 'guarantorName': '' }];

  textMyan: any = ["အမည်", "မှတ်ပုံတင်", "တိုင်းဒေသ", "မြို့နယ်", "လိပ်စာ", "ဖုန်းနံပါတ်",
    "လက်ရှိရာထူး", "လစဉ်ဝင်ငွေ(ကျပ်)", "လက်ရှိတာဝန်ထမ်းဆောင်သည်.ဌာန", "လျှောက်ထားသူ၏ဓါတ်ပုံ",
    "မှတ်ပုံတင်မိတ္တူအရှေ.ဘက်", "မှတ်ပုံတင်မိတ္တူအနောက်ဘက်", "ဌာနအကြီးအကဲထောက်ခံချက်", "ဝန်ထမ်းကတ်မိတ္တူ",
    "ဝန်ဆောင်စရိတ်(ရာခိုင်နှုန်း)", "ဘဏ်တိုးနှုန်း(ရာခိုင်နှုန်း)", "လျှောက်မည်", "နောက်သို.", "ငွေပေးချေရမည်.ကာလကိုရွေးချယ်ပါ", "ချေးငွေပမာဏ",
    "အိမ်ထောင်စုဇယား(ရှေ.)", "အိမ်ထောင်စုဇယား(နောက်)", "အာမခံသူ(၁)ရွေးပါ", "အာမခံသူ(၂)ရွေးပါ",
    //24 - 27
    "လျှောက်ထားသူ", "အာမခံ(၁)", "အာမခံ(၂)", "ကာလ",
    // 28 - 63
    "လျှောက်ထားသူအမည်ကိုရိုက်ထည့်ပါ", "လျှောက်ထားသူမှတ်ပုံတင်ကိုရိုက်ထည့်ပါ", "လျှောက်ထားသူ​ဒေသကိုရွေးချယ်ပါ",
    "လျှောက်ထားသူမြို့နယ်ကိုရွေးချယ်ပါ", "လျှောက်ထားသူလိပ်စာကိုရိုက်ထည့်ပါ", "လျှောက်ထားသူ​ဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ",
    "လျှောက်ထားသူဌာနကိုရိုက်ထည့်ပါ", "လျှောက်ထားသူလက်ရှိရာထူးကိုရိုက်ထည့်ပါ", "လျှောက်ထားသူလစဉ်ဝင်ငွေကိုရိုက်ထည့်ပါ",
    "လျှောက်ထားသူမှတ်ပုံတင်မိတ္တူအရှေ.ဘက်ရွေးပါ", "လျှောက်ထားသူမိတ္တူမှတ်ပုံတင်အနောက်ဘက်ရွေးပါ",
    "လျှောက်ထားသူ၏ဓါတ်ပုံရွေးပါ", "ဝန်ထမ်းကတ်မိတ္တူရွေးပါ",
    "အိမ်ထောင်စုဇယား(ရှေ.)ရွေးပါ", "အိမ်ထောင်စုဇယား(နောက်)ရွေးပါ", "ဌာနအကြီးအကဲထောက်ခံချက်ရွေးပါ",

    "အာမခံသူ(၁)​အမည်ကိုရိုက်ထည့်ပါ", "အာမခံသူ(၁)​မှတ်ပုံတင်ကိုရိုက်ထည့်ပါ", "အာမခံသူ(၁)​ဒေသကိုရွေးချယ်ပါ",
    "အာမခံသူ(၁)​မြို့နယ်ကိုရွေးချယ်ပါ", "အာမခံသူ(၁)​လိပ်စာကိုရိုက်ထည့်ပါ", "အာမခံသူ(၁)​ဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ",
    "အာမခံသူ(၁)​ဌာနကိုရိုက်ထည့်ပါ", "အာမခံသူ(၁)​လက်ရှိရာထူးကိုရိုက်ထည့်ပါ",
    "အာမခံသူ(၁)မှတ်ပုံတင်မိတ္တူအရှေ.ဘက်ရွေးပါ", "အာမခံသူ(၁)မိတ္တူမှတ်ပုံတင်အနောက်ဘက်ရွေးပါ",

    "အာမခံသူ(၂)အမည်ကိုရိုက်ထည့်ပါ", "အာမခံသူ(၂)​မှတ်ပုံတင်ကိုရိုက်ထည့်ပါ", "အာမခံသူ(၂)​ဒေသကိုရွေးချယ်ပါ",
    "အာမခံသူ(၂)​မြို့နယ်ကိုရွေးချယ်ပါ", "အာမခံသူ(၂)​လိပ်စာကိုရိုက်ထည့်ပါ", "အာမခံသူ(၂)​ဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ",
    "အာမခံသူ(၂)​ဌာနကိုရိုက်ထည့်ပါ", "အာမခံသူ(၂)​လက်ရှိရာထူးကိုရိုက်ထည့်ပါ",
    "အာမခံသူ(၂)မှတ်ပုံတင်မိတ္တူအရှေ.ဘက်ရွေးပါ", "အာမခံသူ(၂)မိတ္တူမှတ်ပုံတင်အနောက်ဘက်ရွေးပါ",
    //format 64 - 70
    // "လျှောက်ထားသူမှန်ကန်သောမှတ်ပုံတင်ကိုရိုက်ထည့်ပါ", "လျှောက်ထားသူ​မှန်ကန်သောဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ",
    // "အာမခံသူ(၁)မှန်ကန်သောမှတ်ပုံတင်ကိုရိုက်ထည့်ပါ", "အာမခံသူ(၁)​မှန်ကန်သောဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ",
    // "အာမခံသူ(၂)မှန်ကန်သောမှတ်ပုံတင်ကိုရိုက်ထည့်ပါ", "အာမခံသူ(၂)မှန်ကန်သောဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ",

    "လျှောက်ထားသူမှတ်ပုံတင်မှားယွင်းနေပါသည်", "လျှောက်ထားသူဖုန်းနံပါတ်မှားယွင်းနေပါသည်",
    "အာမခံသူ(၁)မှတ်ပုံတင်မှားယွင်းနေပါသည်", "အာမခံသူ(၁)​ဖုန်းနံပါတ်မှားယွင်းနေပါသည်",
    "အာမခံသူ(၂)မှတ်ပုံတင်မှားယွင်းနေပါသည်", "အာမခံသူ(၂)ဖုန်းနံပါတ်မှားယွင်းနေပါသည်",
    "ချေးငွေပမာဏကိုရိုက်ထည့်ပါ",

    /// 71,72
    "လျှောက်ထားရန်", "Staff Loan လျှောက်နိုင်သောစာမျက်နှာ"];

  textEng: any = ['Name', 'NRC', 'Region', 'Township', "Current Address", "Phone No.", "Current Position",
    "Monthly Basic Income (MMK)", "Current Department", "Applicant's Photo", "Front NRC Copy ", "Back NRC Copy",
    "Department Chief Recommendation", "Staff ID Card Copy",
    "Service Charges(Percent)", "Bank Charges(Percent)", "Apply Now", "Next", 'Choose Payment Terms', 'Loan Amount(MMK)',
    "Front Family Recommendation", "Back Family Recommendation", "Choose Guarantor 1", "Choose Guarantor 2",
    //24 - 27

    "APPL:", "GUAR:1", "GUAR:2", "TERMS",


    "Enter Applicant Name", "Enter Applicant NRC", "Choose Applicant Address Region", "Choose Applicant Address Township",
    "Enter Applicant Current Address", "Enter Applicant Phone No.", "Enter Applicant Current Department",
    "Enter Applicant Current Position", "Enter Applicant Monthly Basic Income",

    "Choose Applicant's Front NRC copy", "Choose Applicant's Back NRC copy",
    "Choose Applicant's Photo Copy", "Choose Staff ID Card Copy",
    "Choose Front Family Recommendation", "Choose Back Family Recommendation", "Choose Department Chief Recommendation",

    "Enter Guarantor1's Name", "Enter Guarantor1's NRC", "Choose Guarantor1's Address Region",
    "Choose Guarantor1's Address Township", "Enter Guarantor1's Current Address", "Enter Guarantor1's Phone No.",
    "Enter Guarantor1's Current Department", "Enter Guarantor1's Current Position",
    "Choose Guarantor1's Front NRC copy", "Choose Guarantor1's Back NRC copy",

    "Enter Guarantor2's Name", "Enter Guarantor2's NRC", "Choose Guarantor2's Address Region",
    "Choose Guarantor2's Address Township", "Enter Guarantor2's Current Address", "Enter Guarantor2's Phone No.",
    "Enter Guarantor2's Current Department", "Enter Guarantor2's Current Position",
    "Choose Guarantor2's Front NRC copy", "Choose Guarantor2's Back NRC copy",

    "Invalid Applicant NRC No!", "Invalid Applicant Phone No.!",
    "Invalid Gaurantor1 NRC No!", "Invalid Gaurantor1 Phone No.!",
    "Invalid Gaurantor2 NRC No!", "Invalid Gaurantor2 Phone No.!",

    "Enter Loan Amount", "Submit", "Staff Loan Apply Page"];

  textdata: string[] = [];
  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  font: string = '';
  flag: string;
  errormessagetext: string;
  shop: any[];
  regionList: string[] = [];
  regionListGua1: string[] = [];
  regionListGua2: string[] = [];
  townshipList: string[] = [];
  townshipListGua1: string[] = [];
  townshipListGua2: string[] = [];
  regionData: any = '';
  townData: any = '';
  ipaddress: string;
  dob: any;
  usersyskey: any;
  public loading;
  isToggled: boolean;
  isGuaToggled: boolean;

  //photo upload
  profileImage: any;
  imglnk: any;
  photoExist: any;
  photo: any;
  localStoragePhoto: string = '';
  syskey: any;
  photoName: any;
  profileImg: any;
  profile: any;
  successMsg: any;
  imname: any;
  photos: any = [];
  chatphoto: any;
  DECIMAL_SEPARATOR = ".";
  GROUP_SEPARATOR = ",";

  //test New Guarantor1
  ExitData: boolean = false;
  Newguran1: boolean = false;
  Newguran2: boolean = false;

  newgran = { 'guarantorSyskey': '0', 'guarantorName': 'New Guarantor' };



  getDefaultImgObj() {
    return { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  }
  flagNet: any;
  changeFont: any;

  @ViewChild(Navbar) navBar: Navbar;

  infoType: any;
  loanTermList: any;
  loanTermData: any;


  db: any;
  loanlist: any = [];
  loanlist1: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public global: GlobalProvider
    , public http: Http, public util: UtilProvider, public loadingCtrl: LoadingController, public storage: Storage, public alertCtrl: AlertController,
    public events: Events, public toastCtrl: ToastController, public changefont: Changefont, private file: File,
    public actionSheetCtrl: ActionSheetController, private transfer: FileTransfer, private camera: Camera, public platform: Platform,
    public alerCtrl: AlertController, public fileOpener: FileOpener, public menuCtrl: MenuController, public datepipe: DatePipe, private keyboard: Keyboard
    , public network: Network, private app: App, public sqlite: SQLite) {
    this.infoType = 'app'; //default Segment
    this.menuCtrl.swipeEnable(true);

    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      }
      else {
        this.ipaddress = this.global.ipaddress;
      }
      this.storage.get('phonenumber').then((phonenumber) => {
        this.global.staffData.staffLoanApplicant.mobile = phonenumber;
        console.log('My Phone no is:'+this.global.staffData.staffLoanApplicant.mobile);
      });
      this.storage.get('username').then((username) => {
        this.global.staffData.staffLoanApplicant.staffName = username;
        console.log( this.global.staffData.staffLoanApplicant.staffName);
      });
      this.storage.get('applicantsyskey').then((applicantsyskey) => {
        this.global.applicantsyskey=applicantsyskey;
        console.log('Global syskey: '+this.global.applicantsyskey);
       });
      this.global.staffData.staffLoanApplicant.staffSyskey = this.global.applicantsyskey; //ndh
      //this.global.staffData.staffLoanApplicant.staffSyskey = this.global.loansyskey;   // for new Member 
      console.log("Test for StaffLoansyskey >>  " + this.global.staffData.staffLoanApplicant.staffSyskey);

      //test for scroll
      // app.setScrollDisabled(true);


      //Font change
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      })
      this.storage.get('language').then((font) => {
        this.changelanguage(font);
      });

      this.events.subscribe('changeFont', lan => {
        this.changeFont = lan;
        console.log("font data is " + this.changeFont);
        this.ExitGurantor();
        this.BindPaymentTerm();
        this.selectDataLoan();

      })
      this.storage.get('font').then((font) => {
        this.changeFont = font;
        console.log("font data get is " + this.changeFont);
        this.ExitGurantor();
        this.BindPaymentTerm();
        this.selectDataLoan();
      });

      //sql Lite

      this.sqlite.create({
        name: "smidb.db",
        location: "default"
      }).then((db: SQLiteObject) => {
        console.log("success create or open database");

        db.executeSql("CREATE TABLE IF NOT EXISTS LOAN (allData TEXT,appfrontNrc TEXT,appbackNrc TEXT,appPhoto TEXT,staffIDPhoto TEXT,familyFront TEXT,familyBack TEXT,deptRecommend TEXT,gua1FrontNrc TEXT,gua1BackNrc TEXT,gua2FrontNrc TEXT,gua2BackNrc Text)", []).then((data) => {
          console.log("TABLE CREATED: ", data);
          //this.selectDataLoan();
        }, (error) => {
          console.error("Unable to execute sql", error);
        });

      }).catch(e => console.log(e));


      //photo upload 

      this.imglnk = this.file.cacheDirectory;
      this.getRegion();
    });


  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
    }
  }

  validateInput() {
    let flag = true;
    if (this.global.staffData.staffLoanApplicant.staffName == undefined || this.global.staffData.staffLoanApplicant.staffName == null || this.global.staffData.staffLoanApplicant.staffName == '') {
      flag = false;
      this.errormessagetext = this.textdata[28];
    } else if (this.global.staffData.staffLoanApplicant.staffNrc == undefined || this.global.staffData.staffLoanApplicant.staffNrc == null || this.global.staffData.staffLoanApplicant.staffNrc == '') {
      flag = false;
      this.errormessagetext = this.textdata[29];
    }
    else if (this.global.staffData.staffLoanApplicant.currentAddressRegion == undefined || this.global.staffData.staffLoanApplicant.currentAddressRegion == null || this.global.staffData.staffLoanApplicant.currentAddressRegion == '') {
      flag = false;
      this.errormessagetext = this.textdata[30];
    } else if (this.global.staffData.staffLoanApplicant.currentAddressTownship == undefined || this.global.staffData.staffLoanApplicant.currentAddressTownship == null || this.global.staffData.staffLoanApplicant.currentAddressTownship == '') {
      flag = false;
      this.errormessagetext = this.textdata[31];
    } else if (this.global.staffData.staffLoanApplicant.currentAddressStreet == undefined || this.global.staffData.staffLoanApplicant.currentAddressStreet == null || this.global.staffData.staffLoanApplicant.currentAddressStreet == '') {
      flag = false;
      this.errormessagetext = this.textdata[32];
    } else if (this.global.staffData.staffLoanApplicant.mobile == undefined || this.global.staffData.staffLoanApplicant.mobile == null || this.global.staffData.staffLoanApplicant.mobile == '') {
      flag = false;
      this.errormessagetext = this.textdata[33];
    } else if (this.global.staffData.staffLoanApplicant.department == undefined || this.global.staffData.staffLoanApplicant.department == null || this.global.staffData.staffLoanApplicant.department == '') {
      flag = false;
      this.errormessagetext = this.textdata[34];
    } else if (this.global.staffData.staffLoanApplicant.currentPosition == undefined || this.global.staffData.staffLoanApplicant.currentPosition == null || this.global.staffData.staffLoanApplicant.currentPosition == '') {
      flag = false;
      this.errormessagetext = this.textdata[35];
    } else if (this.global.staffData.staffLoanApplicant.monthlyBasicIncome == undefined || this.global.staffData.staffLoanApplicant.monthlyBasicIncome == null || this.global.staffData.staffLoanApplicant.monthlyBasicIncome == '') {
      flag = false;
      this.errormessagetext = this.textdata[36];
    }
    //photo upload 
    else if (this.global.appNrcFrontData.profile == undefined || this.global.appNrcFrontData.profile == null || this.global.appNrcFrontData.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[37];
    } else if (this.global.appNrcBackData.profile == undefined || this.global.appNrcBackData.profile == null || this.global.appNrcBackData.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[38];
    } else if (this.global.appPhotoData.profile == undefined || this.global.appPhotoData.profile == null || this.global.appPhotoData.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[39];
    } else if (this.global.staffIdData.profile == undefined || this.global.staffIdData.profile == null || this.global.staffIdData.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[40];
    } else if (this.global.familyRegFrontData.profile == undefined || this.global.familyRegFrontData.profile == null || this.global.familyRegFrontData.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[41];
    } else if (this.global.familyRegBackData.profile == undefined || this.global.familyRegBackData.profile == null || this.global.familyRegBackData.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[42];
    } else if (this.global.deptChRecomData.profile == undefined || this.global.deptChRecomData.profile == null || this.global.deptChRecomData.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[43];
    }

    else if (this.global.staffData.staffLoanGuarantor1.guarantorSyskey == undefined || this.global.staffData.staffLoanGuarantor1.guarantorSyskey == null || this.global.staffData.staffLoanGuarantor1.guarantorSyskey == '') {
      flag = false;
      this.errormessagetext = this.textdata[22];
    }
    else if (this.global.staffData.staffLoanGuarantor1.guarantorName == undefined || this.global.staffData.staffLoanGuarantor1.guarantorName == null || this.global.staffData.staffLoanGuarantor1.guarantorName == '') {
      flag = false;
      this.errormessagetext = this.textdata[44];
    } else if (this.global.staffData.staffLoanGuarantor1.guarantorNrc == undefined || this.global.staffData.staffLoanGuarantor1.guarantorNrc == null || this.global.staffData.staffLoanGuarantor1.guarantorNrc == '') {
      flag = false;
      this.errormessagetext = this.textdata[45];
    } else if (this.global.staffData.staffLoanGuarantor1.currentAddressRegion == undefined || this.global.staffData.staffLoanGuarantor1.currentAddressRegion == null || this.global.staffData.staffLoanGuarantor1.currentAddressRegion == '') {
      flag = false;
      this.errormessagetext = this.textdata[46];
    } else if (this.global.staffData.staffLoanGuarantor1.currentAddressTownship == undefined || this.global.staffData.staffLoanGuarantor1.currentAddressTownship == null || this.global.staffData.staffLoanGuarantor1.currentAddressTownship == '') {
      flag = false;
      this.errormessagetext = this.textdata[47];
    } else if (this.global.staffData.staffLoanGuarantor1.currentAddressStreet == undefined || this.global.staffData.staffLoanGuarantor1.currentAddressStreet == null || this.global.staffData.staffLoanGuarantor1.currentAddressStreet == '') {
      flag = false;
      this.errormessagetext = this.textdata[48];
    } else if (this.global.staffData.staffLoanGuarantor1.mobile == undefined || this.global.staffData.staffLoanGuarantor1.mobile == null || this.global.staffData.staffLoanGuarantor1.mobile == '') {
      flag = false;
      this.errormessagetext = this.textdata[49];
    } else if (this.global.staffData.staffLoanGuarantor1.department == undefined || this.global.staffData.staffLoanGuarantor1.department == null || this.global.staffData.staffLoanGuarantor1.department == '') {
      flag = false;
      this.errormessagetext = this.textdata[50];
    } else if (this.global.staffData.staffLoanGuarantor1.currentPosition == undefined || this.global.staffData.staffLoanGuarantor1.currentPosition == null || this.global.staffData.staffLoanGuarantor1.currentPosition == '') {
      flag = false;
      this.errormessagetext = this.textdata[51];
    } else if (this.global.gua1NrcFrontData.profile == undefined || this.global.gua1NrcFrontData.profile == null || this.global.gua1NrcFrontData.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[52];
    } else if (this.global.gua1NrcBackData.profile == undefined || this.global.gua1NrcBackData.profile == null || this.global.gua1NrcBackData.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[53];
    }

    // else if (this.global.staffData.staffLoanGuarantor2.guarantorSyskey == undefined || this.global.staffData.staffLoanGuarantor2.guarantorSyskey == null || this.global.staffData.staffLoanGuarantor2.guarantorSyskey == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[23];
    // }
    // else if (this.global.staffData.staffLoanGuarantor2.guarantorName == undefined || this.global.staffData.staffLoanGuarantor2.guarantorName == null || this.global.staffData.staffLoanGuarantor2.guarantorName == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[54];
    // } else if (this.global.staffData.staffLoanGuarantor2.guarantorNrc == undefined || this.global.staffData.staffLoanGuarantor2.guarantorNrc == null || this.global.staffData.staffLoanGuarantor2.guarantorNrc == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[55];
    // } else if (this.global.staffData.staffLoanGuarantor2.currentAddressRegion == undefined || this.global.staffData.staffLoanGuarantor2.currentAddressRegion == null || this.global.staffData.staffLoanGuarantor2.currentAddressRegion == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[56];
    // } else if (this.global.staffData.staffLoanGuarantor2.currentAddressTownship == undefined || this.global.staffData.staffLoanGuarantor2.currentAddressTownship == null || this.global.staffData.staffLoanGuarantor2.currentAddressTownship == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[57];
    // } else if (this.global.staffData.staffLoanGuarantor2.currentAddressStreet == undefined || this.global.staffData.staffLoanGuarantor2.currentAddressStreet == null || this.global.staffData.staffLoanGuarantor2.currentAddressStreet == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[58];
    // } else if (this.global.staffData.staffLoanGuarantor2.mobile == undefined || this.global.staffData.staffLoanGuarantor2.mobile == null || this.global.staffData.staffLoanGuarantor2.mobile == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[59];
    // } else if (this.global.staffData.staffLoanGuarantor2.department == undefined || this.global.staffData.staffLoanGuarantor2.department == null || this.global.staffData.staffLoanGuarantor2.department == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[60];
    // } else if (this.global.staffData.staffLoanGuarantor2.currentPosition == undefined || this.global.staffData.staffLoanGuarantor2.currentPosition == null || this.global.staffData.staffLoanGuarantor2.currentPosition == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[61];
    // } else if (this.global.gua2NrcFrontData.profile == undefined || this.global.gua2NrcFrontData.profile == null || this.global.gua2NrcFrontData.profile == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[62];
    // } else if (this.global.gua2NrcBackData.profile == undefined || this.global.gua2NrcBackData.profile == null || this.global.gua2NrcBackData.profile == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[63];
    // } 
    else if (this.global.staffData.staffLoanData.loanAmount == undefined || this.global.staffData.staffLoanData.loanAmount == null || this.global.staffData.staffLoanData.loanAmount == '') {
      flag = false;
      this.errormessagetext = this.textdata[70];
    }
    else if (this.global.staffData.staffLoanData.loanTerm == undefined || this.global.staffData.staffLoanData.loanTerm == null || this.global.staffData.staffLoanData.loanTerm == '') {
      flag = false;
      this.errormessagetext = this.textdata[18];
    }
    return flag;
  }

  // hide keyboard text 
  ionViewCanEnter() {
    this.keyboard.onKeyboardShow()
      .subscribe(data => {
        let conte = this.content;
        let abc = conte.getNativeElement().querySelector('.swiper-pagination');
        if (abc !== undefined && abc !== null) {
          abc.setAttribute("style", 'display: none');
        }
      });

    this.keyboard.onKeyboardHide()
      .subscribe(data => {
        let conte = this.content;
        let abc = conte.getNativeElement().querySelector('.swiper-pagination');
        if (abc !== undefined && abc !== null) {
          abc.removeAttribute("style");
        }
      });
  }

  validateFormat() {
    let flag = true;
    if (this.global.staffData.staffLoanApplicant.mobile.length < 9) {
      this.errormessagetext = this.textdata[65];
      flag = false;
    } else if (this.global.staffData.staffLoanGuarantor1.mobile.length < 9) {
      this.errormessagetext = this.textdata[67];
      flag = false;
    }
    // } else if (this.global.staffData.staffLoanGuarantor2.mobile.length < 9) {
    //   this.errormessagetext = this.textdata[69];
    //   flag = false;
    // }
    return flag;
  }

  BindApplicantData() {

    console.log("font type :" + this.changeFont);
    if (this.global.applicantsyskey != null &&  this.global.staffData.staffLoanApplicant.staffName=="" && this.global.staffData.staffLoanApplicant.staffNrc=="") {
    // if (this.global.loansyskey != null &&  this.global.staffData.staffLoanApplicant.staffName=="" && this.global.staffData.staffLoanApplicant.staffNrc=="") {
      this.usersyskey = this.global.applicantsyskey;
      // "http://localhost:8080/MFIWebService/module001/service002/getLoanApplicantData?syskey=582141"
      this.http.get(this.ipaddress + '/serviceSMI/getLoanApplicantData?syskey=' + this.usersyskey).map(res => res.json()).subscribe(data => {
        if (data != null && (data.staffName != "" || data.staffNrc != '')) {
          this.global.staffData.staffLoanApplicant = data;

          console.log('Staff loan data information ndh:'+JSON.stringify( this.global.staffData.staffLoanApplicant));

          //chkmange font 
          if (this.changeFont == "zg") {
            this.global.staffData.staffLoanApplicant.staffName = this.changefont.UnitoZg(this.global.staffData.staffLoanApplicant.staffName);
            this.global.staffData.staffLoanApplicant.staffNrc = this.changefont.UnitoZg(this.global.staffData.staffLoanApplicant.staffNrc);
            this.global.staffData.staffLoanApplicant.currentPosition = this.changefont.UnitoZg(this.global.staffData.staffLoanApplicant.currentPosition);
            this.global.staffData.staffLoanApplicant.department = this.changefont.UnitoZg(this.global.staffData.staffLoanApplicant.department);
            this.global.staffData.staffLoanGuarantor1.guarantorName = this.changefont.UnitoZg(this.global.staffData.staffLoanGuarantor1.guarantorName);
            this.global.staffData.staffLoanGuarantor1.guarantorNrc = this.changefont.UnitoZg(this.global.staffData.staffLoanGuarantor1.guarantorNrc);
            this.global.staffData.staffLoanGuarantor1.currentPosition = this.changefont.UnitoZg(this.global.staffData.staffLoanGuarantor1.currentPosition);
            this.global.staffData.staffLoanGuarantor1.department = this.changefont.UnitoZg(this.global.staffData.staffLoanGuarantor1.department);
            this.global.staffData.staffLoanGuarantor2.guarantorName = this.changefont.UnitoZg(this.global.staffData.staffLoanGuarantor2.guarantorName);
            this.global.staffData.staffLoanGuarantor2.guarantorNrc = this.changefont.UnitoZg(this.global.staffData.staffLoanGuarantor2.guarantorNrc);
            this.global.staffData.staffLoanGuarantor2.currentPosition = this.changefont.UnitoZg(this.global.staffData.staffLoanGuarantor2.currentPosition);
            this.global.staffData.staffLoanGuarantor2.department = this.changefont.UnitoZg(this.global.staffData.staffLoanGuarantor2.department);
          }
          this.global.appNrcFrontData.edit = "0";
          this.global.appNrcFrontData.profile = this.global.ipphoto + this.global.staffData.staffLoanApplicant.frontNRCImagePath;
          this.global.appNrcBackData.edit = "0";
          this.global.appNrcBackData.profile = this.global.ipphoto + this.global.staffData.staffLoanApplicant.backNRCImagePath;

          this.global.staffData.staffLoanApplicant.monthlyBasicIncome = this.format(this.global.staffData.staffLoanApplicant.monthlyBasicIncome);
          this.global.staffData.staffLoanData.loanAmount = this.format(this.global.staffData.staffLoanData.loanAmount);
        } else {

        }
        this.loading.dismiss();

      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    }

  }

  getRegion() {

    //added network test
    // if (this.global.flagNet == "success") {
    //   this.loading = this.loadingCtrl.create({
    //     content: "Please wait...",
    //     dismissOnPageChange: true
    //     //   duration: 3000
    //   });
    //   this.loading.present();

    this.http.get(this.ipaddress+ '/serviceMFI/getRegionList').map(res => res.json()).subscribe(data => {
      if (data.data != null) {
        this.regionList = data.data;
        if (this.global.staffData.staffLoanApplicant.currentAddressRegion != '' && this.global.staffData.staffLoanApplicant.currentAddressRegion != undefined && this.global.staffData.staffLoanApplicant.currentAddressRegion != null) {
          console.log("Test for StaffLoansyskey before township >>  " + this.global.staffData.staffLoanApplicant.staffSyskey);
          this.getTownship(this.global.staffData.staffLoanApplicant.currentAddressRegion);
        }
        this.regionListGua1 = data.data;
        if (this.global.staffData.staffLoanGuarantor1.currentAddressRegion != '' && this.global.staffData.staffLoanGuarantor1.currentAddressRegion != undefined && this.global.staffData.staffLoanGuarantor1.currentAddressRegion != null) {
          this.getTownshipGua1(this.global.staffData.staffLoanGuarantor1.currentAddressRegion);
        }
        this.regionListGua2 = data.data;
        if (this.global.staffData.staffLoanGuarantor1.currentAddressRegion != '' && this.global.staffData.staffLoanGuarantor1.currentAddressRegion != undefined && this.global.staffData.staffLoanGuarantor1.currentAddressRegion != null) {
          this.getTownshipGua2(this.global.staffData.staffLoanGuarantor2.currentAddressRegion);
        }
        // *** reopen >>> need to add gau2 
      } else {
        let toast = this.toastCtrl.create({
          message: "Can't connect right now!",
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      }
      this.loading.dismiss();
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.util.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
    // }
    // else {
    //   let toast = this.toastCtrl.create({
    //     message: this.textdata[16],
    //     duration: 3000,
    //     position: 'bottom',
    //     //  showCloseButton: true,
    //     dismissOnPageChange: true,
    //     // closeButtonText: 'OK'
    //   });
    //   toast.present(toast);
    //   this.loading.dismiss();
    // }
  }

  RegionChange(r) {
    if (r != null && r != undefined && r != '')
      this.getTownship(r);
  }

  RegionChangeGua1(r) {
    if (r != null && r != undefined && r != '')
      this.getTownshipGua1(r);
  }

  RegionChangeGua2(r) {
    if (r != null && r != undefined && r != '')
      this.getTownshipGua2(r);
  }

  getTownship(r) {
    console.log("Test for StaffLoansyskey in get towndhip >>  " + this.global.staffData.staffLoanApplicant.staffSyskey);
    console.log("Test for StaffLoansyskey in get towndhip >>  " + this.global.applicantsyskey);//ndh
    this.global.staffData.staffLoanApplicant.staffSyskey = this.global.applicantsyskey;//ndh
    this.http.get(this.ipaddress+ '/serviceMFI/getTownshipList?regionRefSyskey=' + r).map(res => res.json()).subscribe(result => {
      if (result.data != null) {
        this.townshipList = result.data;
      } else {
        let toast = this.toastCtrl.create({
          message: "Can't connect right now!",
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      }
      this.loading.dismiss();
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.util.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }

  getTownshipGua1(r) {
    console.log("Test for StaffLoansyskey in get towndhip >>  " + this.global.staffData.staffLoanApplicant.staffSyskey);
    if (r != undefined && r != null && r != '' && r != "") {
      this.http.get(this.ipaddress+ '/serviceMFI/getTownshipList?regionRefSyskey=' + r).map(res => res.json()).subscribe(result => {
        if (result.data != null) {
          this.townshipListGua1 = result.data;
        } else {
          let toast = this.toastCtrl.create({
            message: "Can't connect right now!",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);  //temporary ps 
        }
        this.loading.dismiss();
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    }
  }
  getTownshipGua2(r) {
    console.log("Test for StaffLoansyskey in get towndhip >>  " + this.global.staffData.staffLoanApplicant.staffSyskey);
    if (r != undefined && r != null && r != '' && r != "") {
      this.http.get(this.ipaddress+ '/serviceSMI/getTownshipList?regionRefSyskey=' + r).map(res => res.json()).subscribe(result => {
        if (result.data != null) {
          this.townshipListGua2 = result.data;
        } else {
          let toast = this.toastCtrl.create({
            message: "Can't connect right now!",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);  //temporary ps 
        }
        this.loading.dismiss();
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    }
  }

  inputChange(data) {
    this.errormessagetext = "";
  }

  //guarantor InfoPage
  ExitGurantor() {
    // if (this.global.loansyskey != null) {
    //   this.usersyskey = this.global.loansyskey;
    if (this.global.applicantsyskey != null) {
      this.usersyskey = this.global.applicantsyskey;
      // test for new gauran combo
      if (this.usersyskey == 0) {
        this.GuranInfo = [];
        this.GuranInfo.push(this.newgran);
      }
      else {
        console.log("guatantor info length " + this.GuranInfo.length);
        if (this.GuranInfo.length == 1 && this.GuranInfo[0].guarantorName == "") {
          // http://localhost:8080/MFIWebService/module001/service002/checkLoanGuarantor?syskey=582141
          console.log("Call checkloan " + this.usersyskey);
          this.http.get(this.ipaddress+ '/serviceSMI/checkLoanGuarantor?syskey=' + this.usersyskey).map(res => res.json()).subscribe(data => {
            console.log("Call checkloan " + JSON.stringify(data));
            if (data != null && (data.guarantorName != "" || data.guarantorNrc != '')) {
              let tempArray = [];
              if (!Array.isArray(data.data)) {
                tempArray.push(data.data);
                data.data = tempArray;
              }
              this.GuranInfo = [];
              this.GuranInfo.push(this.newgran);
              for (let i = 0; i < data.data.length; i++) {
                this.GuranInfo.push(data.data[i]);
              }
            } else {
              let toast = this.toastCtrl.create({
                message: data.msgDesc,
                duration: 3000,
                position: 'bottom',
                dismissOnPageChange: true,
              });
              toast.present(toast);
            }
            this.loading.dismiss();
          },
            error => {
              let toast = this.toastCtrl.create({
                message: this.util.getErrorMessage(error),
                duration: 3000,
                position: 'bottom',
                dismissOnPageChange: true,
              });
              toast.present(toast);
              this.loading.dismiss();
            });
        }
      }
    }
  }

  //photo Upload 
  imageEdit(name) {
    let actionSheet = this.actionSheetCtrl.create({
      // title: 'Languages',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Take new picture',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.goCamera(name);
          }
        },
        {
          text: 'Select new from gallery',
          icon: !this.platform.is('ios') ? 'images' : null,
          handler: () => {
            this.goGallery(name);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }

  goCamera(name) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: true,
      allowEdit: false,
    }

    this.camera.getPicture(options).then((imageData) => {
      if (name == "AppPhoto") {
        this.global.appPhotoData.profileImg = imageData;
        this.global.appPhotoData.profile = imageData;
      }
      else if (name == "AppNRCFront") {
        this.global.appNrcFrontData.profileImg = imageData; //UI 
        this.global.appNrcFrontData.profile = imageData;  // upload 

      } else if (name == "AppNRCBack") {
        this.global.appNrcBackData.profileImg = imageData;
        this.global.appNrcBackData.profile = imageData;

      } else if (name == "DeptChRecom") {
        this.global.deptChRecomData.profileImg = imageData;
        this.global.deptChRecomData.profile = imageData;

      } else if (name == "FamilyRegFront") {
        this.global.familyRegFrontData.profileImg = imageData;
        this.global.familyRegFrontData.profile = imageData;

      } else if (name == "FamilyRegBack") {
        this.global.familyRegBackData.profileImg = imageData;
        this.global.familyRegBackData.profile = imageData;

      } else if (name == "StaffID") {
        this.global.staffIdData.profileImg = imageData;
        this.global.staffIdData.profile = imageData;
      }
      else if (name == "Gua1NRCFront") {
        this.global.gua1NrcFrontData.profileImg = imageData; //UI 
        this.global.gua1NrcFrontData.profile = imageData;  // upload 

      } else if (name == "Gua1NRCBack") {
        this.global.gua1NrcBackData.profileImg = imageData;
        this.global.gua1NrcBackData.profile = imageData;

      } else if (name == "Gua2NRCFront") {
        this.global.gua2NrcFrontData.profileImg = imageData; //UI 
        this.global.gua2NrcFrontData.profile = imageData;  // upload 

      } else if (name == "Gua2NRCBack") {
        this.global.gua2NrcBackData.profileImg = imageData;
        this.global.gua2NrcBackData.profile = imageData;
      }
      let currentName = '';
      let correctPath = '';
      if (imageData.indexOf('?') > -1) {
        currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
        correctPath = imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      }
      else {
        currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
        correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
      }
      this.copyFileToLocalDir(name, correctPath, currentName, this.createPhotoName(name));
    }, (error) => {
      let toast = this.toastCtrl.create({
        message: "Taking photo is not successful.",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
    });
  }

  goGallery(name) {
    ''
    const options: CameraOptions = {

      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      correctOrientation: true
    }
    this.camera.getPicture(options).then((imageData) => {
      if (name == "AppPhoto") {
        this.global.appPhotoData.profileImg = imageData;
        this.global.appPhotoData.profile = imageData;
      }
      else if (name == "AppNRCFront") {
        this.global.appNrcFrontData.profileImg = imageData;
        this.global.appNrcFrontData.profile = imageData;

      } else if (name == "AppNRCBack") {
        this.global.appNrcBackData.profileImg = imageData;
        this.global.appNrcBackData.profile = imageData;

      } else if (name == "DeptChRecom") {
        this.global.deptChRecomData.profileImg = imageData;
        this.global.deptChRecomData.profile = imageData;

      } else if (name == "FamilyRegFront") {
        this.global.familyRegFrontData.profileImg = imageData;
        this.global.familyRegFrontData.profile = imageData;

      } else if (name == "FamilyRegBack") {
        this.global.familyRegBackData.profileImg = imageData;
        this.global.familyRegBackData.profile = imageData;

      } else if (name == "StaffID") {
        this.global.staffIdData.profileImg = imageData;
        this.global.staffIdData.profile = imageData;
      }
      else if (name == "Gua1NRCFront") {
        this.global.gua1NrcFrontData.profileImg = imageData;
        this.global.gua1NrcFrontData.profile = imageData;
      } else if (name == "Gua1NRCBack") {
        this.global.gua1NrcBackData.profileImg = imageData;
        this.global.gua1NrcBackData.profile = imageData;
      } else if (name == "Gua2NRCFront") {
        this.global.gua2NrcFrontData.profileImg = imageData;
        this.global.gua2NrcFrontData.profile = imageData;
      } else if (name == "Gua2NRCBack") {
        this.global.gua2NrcFrontData.profileImg = imageData;
        this.global.gua2NrcBackData.profile = imageData;
      }


      let correctPath = '';
      let currentName = '';
      if (imageData.indexOf('?') > -1) {
        currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
        correctPath = imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      }
      else {
        currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
        correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
      }
      this.copyFileToLocalDir(name, correctPath, currentName, this.createPhotoName(name));
    }, (error) => {
      let toast = this.toastCtrl.create({
        message: "Choosing photo is not successful.",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
    });
  }


  createPhotoName(name) {
    let photoName = name + this.util.getImageName() + this.usersyskey + ".jpg";
    this.photoName = name + this.util.getImageName() + this.usersyskey + ".jpg";
    if (name == 'AppPhoto') {
      this.global.appPhotoData.photoName = this.photoName;
      this.global.appPhotoData.edit = "1";
    }
    else if (name == 'AppNRCFront') {
      this.global.appNrcFrontData.photoName = this.photoName;
      this.global.appNrcFrontData.edit = "1";
    } else if (name == "AppNRCBack") {
      this.global.appNrcBackData.photoName = this.photoName;
      this.global.appNrcBackData.edit = "1";
    } else if (name == "DeptChRecom") {
      this.global.deptChRecomData.photoName = this.photoName;
      this.global.deptChRecomData.edit = "1";
    } else if (name == "FamilyRegFront") {
      this.global.familyRegFrontData.photoName = this.photoName;
      this.global.familyRegFrontData.edit = "1";
    } else if (name == "FamilyRegBack") {
      this.global.familyRegBackData.photoName = this.photoName;
      this.global.familyRegBackData.edit = "1";
    } else if (name == "StaffID") {
      this.global.staffIdData.photoName = this.photoName;
      this.global.staffIdData.edit = "1";
    }
    else if (name == "Gua1NRCFront") {
      this.global.gua1NrcFrontData.photoName = this.photoName;
      this.global.gua1NrcFrontData.edit = "1";
    } else if (name == "Gua1NRCBack") {
      this.global.gua1NrcBackData.photoName = this.photoName;
      this.global.gua1NrcBackData.edit = "1";
    } else if (name == "Gua2NRCFront") {
      this.global.gua2NrcFrontData.photoName = this.photoName;
      this.global.gua2NrcFrontData.edit = "1";
    } else if (name == "Gua2NRCBack") {
      this.global.gua2NrcBackData.photoName = this.photoName;
      this.global.gua2NrcBackData.edit = "1";
    }
    return photoName;
  }

  openProcessingLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true,
      duration: 3000
    });
    this.loading.present();
  }

  closeProcessingLoading() {
    this.loading.dismiss();
  }

  findAndRemove(array, property, value) {
    array.forEach(function (result, index) {
      if (result[property] === value) {
        //Remove from array
        array.splice(index, 1);
      }
    });
  }

  copyFileToLocalDir(name, namePath, currentName, newFileName) {
    let targetPath1;
    if (this.platform.is('ios')) {
      targetPath1 = this.file.documentsDirectory;
    } else {
      targetPath1 = this.file.externalApplicationStorageDirectory;
    }
    this.file.checkDir(targetPath1, 'MFIPhoto').then(() => {
    }, (error) => {
      this.file.createDir(targetPath1, "MFIPhoto", true).then(() => {

      }, (error) => {

      });
    });

    this.file.copyFile(namePath, currentName, targetPath1 + 'MFIPhoto/', newFileName).then(file => {
      this.chatphoto = targetPath1 + 'MFIPhoto/' + newFileName;
      // this.photos.push(this.chatphoto);
      if (name == "AppPhoto") {
        this.global.appPhotoData.profileImg = this.chatphoto;
        this.global.appPhotoData.profile = this.chatphoto;
      }
      else if (name == "AppNRCFront") {
        this.global.appNrcFrontData.profileImg = this.chatphoto;
        this.global.appNrcFrontData.profile = this.chatphoto;
      } else if (name == "AppNRCBack") {
        this.global.appNrcBackData.profileImg = this.chatphoto;
        this.global.appNrcBackData.profile = this.chatphoto;
      } else if (name == "DeptChRecom") {
        this.global.deptChRecomData.profileImg = this.chatphoto;
        this.global.deptChRecomData.profile = this.chatphoto;
      } else if (name == "FamilyRegFront") {
        this.global.familyRegFrontData.profileImg = this.chatphoto;
        this.global.familyRegFrontData.profile = this.chatphoto;
      } else if (name == "FamilyRegBack") {
        this.global.familyRegBackData.profileImg = this.chatphoto;
        this.global.familyRegBackData.profile = this.chatphoto;
      } else if (name == "StaffID") {
        this.global.staffIdData.profileImg = this.chatphoto;
        this.global.staffIdData.profile = this.chatphoto;
      }
      else if (name == "Gua1NRCFront") {
        this.global.gua1NrcFrontData.profileImg = this.chatphoto;
        this.global.gua1NrcFrontData.profile = this.chatphoto;
      } else if (name == "Gua1NRCBack") {
        this.global.gua1NrcBackData.profileImg = this.chatphoto;
        this.global.gua1NrcBackData.profile = this.chatphoto;
      } else if (name == "Gua2NRCFront") {
        this.global.gua2NrcFrontData.profileImg = this.chatphoto;
        this.global.gua2NrcFrontData.profile = this.chatphoto;
      } else if (name == "Gua2NRCBack") {
        this.global.gua2NrcBackData.profileImg = this.chatphoto;
        this.global.gua2NrcBackData.profile = this.chatphoto;
      }





      //this.savephoto();
    }, (err) => {

    });
  }

  BindPhotoData() {
    // this.imglink=this.global.ipphoto+"upload/image/userProfile/";
    // this.frontnrc=this.imglink+this.PersonData.applicantdata.front_nrc_image_path;
    // this.backnrc=this.imglink+this.PersonData.applicantdata.back_nrc_image_path;
    // this.guafrontnrc=this.imglink+this.PersonData.guarantordata.front_nrc_image_path;
    // this.guabacknrc=this.imglink+this.PersonData.guarantordata.front_nrc_image_path;
  }

  // download(photoAndServerLink, photo) {
  //   const fileTransfer: TransferObject = this.transfer.create();
  //   fileTransfer.download(photoAndServerLink, this.file.cacheDirectory + photo).then((entry) => {
  //     let abc = entry.toURL();
  //     this.storage.set('localPhotoAndLink', abc);
  //     this.events.publish('localPhotoAndLink', abc);
  //     //this.closeProcessingLoading();
  //   }, (error) => {

  //     //this.closeProcessingLoading();
  //   });
  // }

  getMIMEtype(extn) {
    let ext = extn.toLowerCase();
    let MIMETypes = {
      'txt': 'text/plain',
      'docx': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'doc': 'application/msword',
      'pdf': 'application/pdf',
      'jpg': 'image/jpeg',
      'bmp': 'image/bmp',
      'png': 'image/png',
      'xls': 'application/vnd.ms-excel',
      'xlsx': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'rtf': 'application/rtf',
      'ppt': 'application/vnd.ms-powerpoint',
      'pptx': 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
    }
    return MIMETypes[ext];
  }



  download(downloadlink, name) {

    const fileTransfer: FileTransferObject = this.transfer.create();

    let path = null;
    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else if (this.platform.is('android')) {
      path = this.file.externalRootDirectory + '/Download/';
    }

    let fName = "";
    fName = name;

    this.global.ipaddress = this.global.ipphoto + downloadlink;

    // let link = this.global.ipaddress.substr(0, this.global.ipaddress.lastIndexOf('/') + 1);
    let link = this.global.ipaddress.substr(0, this.global.ipaddress.lastIndexOf('/') + 1);

    let fileExtn = fName.split('.').reverse()[0];
    let fileMIMEType = this.getMIMEtype(fileExtn);


    const url = link + fName;
    fileTransfer.download(link + fName, path + fName).then((entry) => {
      console.log('download complete: ' + entry.toURL());
      //this.fileOpener.open(path + fName, fileMIMEType);
    }, (error) => {
      // handle error
    });

  }






  LoanApply() {

    console.log("font type in applyloan :" + this.changeFont);
    console.log("Test for StaffLoansyskey in loan apply global >>  " + this.global.applicantsyskey);
    console.log("Test for StaffLoansyskey in loan apply >>  " + this.global.staffData.staffLoanApplicant.staffSyskey);

    //this.global.staffData.staffLoanApplicant.staffSyskey = this.global.loansyskey;
    this.global.staffData.staffLoanApplicant.staffSyskey = this.global.applicantsyskey;

    this.global.staffData.imagealldata.imageparsedata = [];
    if (this.global.appPhotoData.edit == "1") {
      this.imname = 'AppPhoto';
      this.global.appPhotoData.imgname = this.imname;

      this.global.staffData.imagealldata.imageparsedata.push(this.global.appPhotoData);
      this.global.staffData.staffLoanApplicant.photoPath = this.global.appPhotoData.photoName;
    } else {
      this.global.staffData.staffLoanApplicant.photoPath = this.global.appPhotoData.profile.substr(this.global.appPhotoData.profile.lastIndexOf('/') + 1);
    }
    if (this.global.appNrcFrontData.edit == "1") {
      this.imname = 'AppNRCFront';
      this.global.appNrcFrontData.imgname = this.imname;

      this.global.staffData.imagealldata.imageparsedata.push(this.global.appNrcFrontData);
      this.global.staffData.staffLoanApplicant.frontNRCImagePath = this.global.appNrcFrontData.photoName;
    }
    else {
      this.global.staffData.staffLoanApplicant.frontNRCImagePath = this.global.appNrcFrontData.profile.substr(this.global.appNrcFrontData.profile.lastIndexOf('/') + 1);
    }
    if (this.global.appNrcBackData.edit == "1") {
      this.imname = 'AppNRCBack';
      this.global.appNrcBackData.imgname = this.imname;

      this.global.staffData.imagealldata.imageparsedata.push(this.global.appNrcBackData);
      this.global.staffData.staffLoanApplicant.backNRCImagePath = this.global.appNrcBackData.photoName;
    }
    else {
      this.global.staffData.staffLoanApplicant.backNRCImagePath = this.global.appNrcBackData.profile.substr(this.global.appNrcBackData.profile.lastIndexOf('/') + 1);
    }
    if (this.global.deptChRecomData.edit == "1") {
      this.imname = 'DeptChRecom';
      this.global.deptChRecomData.imgname = this.imname;

      this.global.staffData.imagealldata.imageparsedata.push(this.global.deptChRecomData);
      this.global.staffData.staffLoanData.departmentChiefRecommendation = this.global.deptChRecomData.photoName;
    }
    else {
      this.global.staffData.staffLoanData.departmentChiefRecommendation = this.global.deptChRecomData.profile.substr(this.global.deptChRecomData.profile.lastIndexOf('/') + 1);
    }

    if (this.global.familyRegFrontData.edit == "1") {
      this.imname = 'FamilyRegFront';
      this.global.familyRegFrontData.imgname = this.imname;

      this.global.staffData.imagealldata.imageparsedata.push(this.global.familyRegFrontData);
      this.global.staffData.staffLoanData.frontFamilyRegImagePath = this.global.familyRegFrontData.photoName;
    } else {
      this.global.staffData.staffLoanData.frontFamilyRegImagePath = this.global.familyRegFrontData.profile.substr(this.global.familyRegFrontData.profile.lastIndexOf('/') + 1);
    }

    if (this.global.familyRegBackData.edit == "1") {
      this.imname = 'FamilyRegBack';
      this.global.familyRegBackData.imgname = this.imname;

      this.global.staffData.imagealldata.imageparsedata.push(this.global.familyRegBackData);
      this.global.staffData.staffLoanData.backFamilyRegImagePath = this.global.familyRegBackData.photoName;
    } else {
      this.global.staffData.staffLoanData.backFamilyRegImagePath = this.global.familyRegBackData.profile.substr(this.global.familyRegBackData.profile.lastIndexOf('/') + 1);
    }

    if (this.global.staffIdData.edit == "1") {
      this.imname = 'StaffID';
      this.global.staffIdData.imgname = this.imname;

      this.global.staffData.imagealldata.imageparsedata.push(this.global.staffIdData);
      this.global.staffData.staffLoanData.staffIDCardImagePath = this.global.staffIdData.photoName;
    } else {
      this.global.staffData.staffLoanData.staffIDCardImagePath = this.global.staffIdData.profile.substr(this.global.staffIdData.profile.lastIndexOf('/') + 1);
    }

    if (this.global.gua1NrcFrontData.edit == "1") {
      this.imname = 'Gua1NRCFront';
      this.global.gua1NrcFrontData.imgname = this.imname;

      this.global.staffData.imagealldata.imageparsedata.push(this.global.gua1NrcFrontData);
      this.global.staffData.staffLoanGuarantor1.frontNRCImagePath = this.global.gua1NrcFrontData.photoName;
    }
    else {
      this.global.staffData.staffLoanGuarantor1.frontNRCImagePath = this.global.gua1NrcFrontData.profile.substr(this.global.gua1NrcFrontData.profile.lastIndexOf('/') + 1);
    }
    if (this.global.gua1NrcBackData.edit == "1") {
      this.imname = 'Gua1NRCBack';
      this.global.gua1NrcBackData.imgname = this.imname;

      this.global.staffData.imagealldata.imageparsedata.push(this.global.gua1NrcBackData);
      this.global.staffData.staffLoanGuarantor1.backNRCImagePath = this.global.gua1NrcBackData.photoName;
    }
    else {
      this.global.staffData.staffLoanGuarantor1.backNRCImagePath = this.global.gua1NrcBackData.profile.substr(this.global.gua1NrcBackData.profile.lastIndexOf('/') + 1);
    }

    // if (this.global.gua2NrcFrontData.edit == "1") {
    //   this.imname = 'Gua2NRCFront';
    //   this.global.gua2NrcFrontData.imgname = this.imname;

    //   this.global.staffData.imagealldata.imageparsedata.push(this.global.gua2NrcFrontData);
    //   this.global.staffData.staffLoanGuarantor2.frontNRCImagePath = this.global.gua2NrcFrontData.photoName;
    // }
    // else {
    //   this.global.staffData.staffLoanGuarantor2.frontNRCImagePath = this.global.gua2NrcFrontData.profile.substr(this.global.gua2NrcFrontData.profile.lastIndexOf('/') + 1);
    // }
    // if (this.global.gua2NrcBackData.edit == "1") {
    //   this.imname = 'Gua2NRCBack';
    //   this.global.gua2NrcBackData.imgname = this.imname;

    //   this.global.staffData.imagealldata.imageparsedata.push(this.global.gua2NrcBackData);
    //   this.global.staffData.staffLoanGuarantor2.backNRCImagePath = this.global.gua2NrcBackData.photoName;
    // }
    // else {
    //   this.global.staffData.staffLoanGuarantor2.backNRCImagePath = this.global.gua2NrcBackData.profile.substr(this.global.gua2NrcBackData.profile.lastIndexOf('/') + 1);
    // }

    if (this.validateInput() && this.validateFormat()) {
      this.deleteDataLoan(1);
      this.global.comeHome = true;
      // *** reopen
      this.global.staffData.staffLoanApplicant.monthlyBasicIncome = this.global.staffData.staffLoanApplicant.monthlyBasicIncome.toString().replace(/,/g, "");
      this.global.staffData.staffLoanData.loanAmount = this.global.staffData.staffLoanData.loanAmount.toString().replace(/,/g, "");

      this.global.staffData.staffLoanData.serviceCharges = this.global.staffData.staffLoanData.serviceCharges.toString().replace(/%/g, "");
      this.global.staffData.staffLoanData.bankInterestRate = this.global.staffData.staffLoanData.bankInterestRate.toString().replace(/%/g, "");
      //change font 
      if (this.changeFont == "zg") {
        this.global.staffData.staffLoanApplicant.staffName = this.changefont.ZgtoUni(this.global.staffData.staffLoanApplicant.staffName);
        this.global.staffData.staffLoanApplicant.staffNrc = this.changefont.ZgtoUni(this.global.staffData.staffLoanApplicant.staffNrc);
        this.global.staffData.staffLoanApplicant.currentPosition = this.changefont.ZgtoUni(this.global.staffData.staffLoanApplicant.currentPosition);
        this.global.staffData.staffLoanApplicant.department = this.changefont.ZgtoUni(this.global.staffData.staffLoanApplicant.department);
        this.global.staffData.staffLoanGuarantor1.guarantorName = this.changefont.ZgtoUni(this.global.staffData.staffLoanGuarantor1.guarantorName);
        this.global.staffData.staffLoanGuarantor1.guarantorNrc = this.changefont.ZgtoUni(this.global.staffData.staffLoanGuarantor1.guarantorNrc);
        this.global.staffData.staffLoanGuarantor1.currentPosition = this.changefont.ZgtoUni(this.global.staffData.staffLoanGuarantor1.currentPosition);
        this.global.staffData.staffLoanGuarantor1.department = this.changefont.ZgtoUni(this.global.staffData.staffLoanGuarantor1.department);
        this.global.staffData.staffLoanGuarantor2.guarantorName = this.changefont.ZgtoUni(this.global.staffData.staffLoanGuarantor2.guarantorName);
        this.global.staffData.staffLoanGuarantor2.guarantorNrc = this.changefont.ZgtoUni(this.global.staffData.staffLoanGuarantor2.guarantorNrc);
        this.global.staffData.staffLoanGuarantor2.currentPosition = this.changefont.ZgtoUni(this.global.staffData.staffLoanGuarantor2.currentPosition);
        this.global.staffData.staffLoanGuarantor2.department = this.changefont.ZgtoUni(this.global.staffData.staffLoanGuarantor2.department);
      }

      console.log("Test for StaffLoansyskey in loan apply >>  " + JSON.stringify(this.global.staffData));
      this.http.post(this.ipaddress+ '/serviceSMI/applyLoan', this.global.staffData).map(res => res.json()).subscribe(result => {

        if (result.msgCode == "0000") {
          for (var i = 0; i < this.global.staffData.imagealldata.imageparsedata.length; i++) {
            console.log("image parse data" + JSON.stringify(this.global.staffData.imagealldata.imageparsedata[i]));
            if (this.global.staffData.imagealldata.imageparsedata[i].edit == "1") {
              this.uploadPhoto(this.global.staffData.imagealldata.imageparsedata[i].imgname, this.global.staffData.imagealldata.imageparsedata[i].profileImg, this.global.staffData.imagealldata.imageparsedata[i].photoName, this.global.staffData.imagealldata.imageparsedata[i].profile)
            }
          }
          this.loanTermList = []; //for payment term
          console.log("After clearing Data of StaffLoan >> " + JSON.stringify(this.global.staffData));

          let confirm = this.alertCtrl.create({
            title: '',
            enableBackdropDismiss: false,
            message: "Submit Successfully.",
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  console.log('OK clicked');
                  // this.navCtrl.setRoot(TabsPage, {});
                  // this.navCtrl.popToRoot();
                  this.navCtrl.setRoot(WalletPage, {});
                }
              }
            ]
          })
          confirm.present();
          //this.global.outletSyskey = '';

          // this.download(result.gua1downloadlink, result.gua1downloadname);
          // this.download(result.gua2downloadlink, result.gua2downloadname);
          // this.loading.dismiss();
          // let confirm = this.alertCtrl.create({
          //   title: '',
          //   enableBackdropDismiss: false,
          //   message: "Save  Successfully!\n Please Check PDF File in Download Folder",
          //   buttons: [
          //     {
          //       text: 'OK',
          //       handler: () => {
          //         console.log('OK clicked');
          //         this.global.appPhotoData = this.getDefaultImgObj();
          //         this.global.appNrcFrontData = this.getDefaultImgObj();
          //         this.global.appNrcBackData = this.getDefaultImgObj();
          //         this.global.deptChRecomData = this.getDefaultImgObj();
          //         this.global.familyRegFrontData = this.getDefaultImgObj();
          //         this.global.familyRegBackData = this.getDefaultImgObj();
          //         this.global.staffIdData = this.getDefaultImgObj();
          //         this.global.gua1NrcFrontData = this.getDefaultImgObj();
          //         this.global.gua1NrcBackData = this.getDefaultImgObj();
          //         // this.global.gua2NrcFrontData = this.getDefaultImgObj();
          //         // this.global.gua2NrcBackData = this.getDefaultImgObj();


          //         this.global.staffData.imagealldata.imageparsedata = [];
          //         this.global.staffData = this.global.getDefaultObj();

          //         this.deleteDataLoan(2);

          //       }
          //     }
          //   ]
          // })
          // confirm.present();

          // this.navCtrl.setRoot(LoanRequireInformationPage,
          //   {
          //   });
          // this.navCtrl.setRoot(WalletPage,
          //   {
          //   });



          this.loading.dismiss();
        } else {
          let toast = this.toastCtrl.create({
            message: result.msgDesc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        }
        this.loading.dismiss();
      }, error => {
        let toast = this.toastCtrl.create({
          message: this.util.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
    } else {
      let toast = this.toastCtrl.create({
        message: this.errormessagetext,
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
  }


  uploadPhoto(imgname, img, name, pro) {
    var url = this.global.ipphoto + '/serviceMFI/mobileupload';
    this.profileImg = img;
    this.photoName = name;
    this.profile = pro;
    console.log("profile image :" + JSON.stringify(this.profileImg));
    console.log("Photo name :" + JSON.stringify(this.photoName));
    console.log("profile :" + JSON.stringify(this.profile));
    if (this.profileImg != undefined && this.profileImg != '' && this.profileImg != null) {

      let options: FileUploadOptions = {
        fileKey: 'file',
        fileName: this.photoName,
        chunkedMode: false,
      }
      const fileTransfer: FileTransferObject = this.transfer.create();
      console.log("Before upload" + url);
      fileTransfer.upload(this.profile, url, options).then((data) => {
        let abc = this.imglnk + this.photoName;
        // this.storage.set('localPhotoAndLink', abc);
        // this.events.publish('localPhotoAndLink', abc);
        this.closeProcessingLoading();
        let toast = this.toastCtrl.create({
          message: this.successMsg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);

        //this.slimLoader.complete();

        console.log("uploadPhoto upload success" + JSON.stringify(data));
      },
        error => {
          this.closeProcessingLoading();

          // let toast = this.toastCtrl.create({
          //   message: "Uploading photo is not successful.",
          //   duration: 3000,
          //   position: 'bottom',
          //   dismissOnPageChange: true,
          // });

          // toast.present(toast);
          // console.log("uploadPhoto upload error" + JSON.stringify(error));
        }
      );
    } else {
      this.closeProcessingLoading();

      let toast = this.toastCtrl.create({
        message: this.successMsg,
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
  }

  // gobackmain() {
  //   if (this.global.startshop == 0) {
  //     this.navCtrl.setRoot(TabsPage,
  //       {
  //       });
  //   } else {

  //   }

  // }


  format(valString) {
    if (!valString) {
      return '';
    }
    let val = valString.toString();
    const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
    return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR)
  };

  unFormat(val) {
    if (!val) {
      return '';
    }
    val = val.replace(/^0+/, '').replace(/\D/g, '');
    if (this.GROUP_SEPARATOR === ',') {
      return val.replace(/,/g, '');
    } else {
      return val.replace(/\./g, '');
    }
  };

  BindPaymentTerm() {
    // this.storage.get('orgsyskey').then((orgsyskey) => {
    //  this.orgsyskey = orgsyskey;
    // console.log('orgsyskey', this.orgsyskey);

    // network test
    if (this.global.netWork == "connected") {
      this.loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
        //   duration: 3000
      });
      this.loading.present();
      // http://129.154.101.63:8080/MFIWebService/module001/service002/getBankChargesByOutletLoanTypeSyskey?Outletloantypesyskey=582059 + this.orgsyskey
      this.http.get(this.ipaddress+ '/serviceSMI/getBankChargesByOutletLoanTypeSyskey?Outletloantypesyskey=582059').map(res => res.json()).subscribe(data => {
        if (data != null) {
          let tempArray = [];
          if (!Array.isArray(data.data)) {
            tempArray.push(data.data);
            data.data = tempArray;
          }
          this.loanTermList = data.data;

          // this.loanTerm = [];
          // for (let i = 0; i < data.data.length; i++) {
          //   this.loanTerm.push(data.data[i]);
          // }
          console.log("Loan Term List >>" + JSON.stringify(this.loanTermList));
          // // test for exiting Payment term choose 
          // if (this.global.flagPT == false) {
          //   if (this.global.PersonData.loandata.paymentTermSyskey != null && this.global.PersonData.loandata.paymentTermSyskey != undefined && this.global.PersonData.loandata.paymentTermSyskey != '') {
          //     console.log("Call payment :" + JSON.stringify(this.global.PersonData));
          //     this.CallPayment(this.global.PersonData.loandata.paymentTermSyskey);
          //     this.flag = false;
          //   }
          // }
        }
        else {
          let toast = this.toastCtrl.create({
            message: "Can't connect right now!",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        }
        this.loading.dismiss();
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            //  showCloseButton: true,
            dismissOnPageChange: true,
            // closeButtonText: 'OK'
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    } else {
      let toast = this.toastCtrl.create({
        message: this.util.getErrorMessage("Check your internet connection!"),
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
      this.loading.dismiss();
    }
    // });
  }

  onSelectChangeGua1(selectStatus: any) {
    if (selectStatus == '0') {

      this.global.gua1NrcBackData = this.getDefaultImgObj();
      this.global.gua1NrcFrontData = this.getDefaultImgObj();
      this.global.staffData.staffLoanGuarantor1 = this.Gdata();


      this.Newguran1 = true;
    }
    else {
      if (this.global.staffData.staffLoanGuarantor1.guarantorSyskey != "" && this.global.staffData.staffLoanGuarantor1.guarantorSyskey == this.global.staffData.staffLoanGuarantor2.guarantorSyskey) {
        let toast = this.toastCtrl.create({
          message: "Please choose different guarantor already chosen",
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.global.gua1NrcBackData = this.getDefaultImgObj();
        this.global.gua1NrcFrontData = this.getDefaultImgObj();
        this.global.staffData.staffLoanGuarantor1 = this.Gdata();


      } else {

        // http://localhost:8080/MFIWebService/module001/service002/getLoanGuarantorData?syskey=582170
        this.http.get(this.ipaddress + '/serviceSMI/getLoanGuarantorData?syskey=' + selectStatus).map(res => res.json()).subscribe(data => {
          if (data != null && data.member_name != '' && data.nrc != '') {
            this.global.staffData.staffLoanGuarantor1 = data;
            this.Newguran1 = false;
            this.global.gua1NrcFrontData.edit = "0";
            this.global.gua1NrcFrontData.profile = this.global.ipphoto + this.global.staffData.staffLoanGuarantor1.frontNRCImagePath;
            this.global.gua1NrcBackData.edit = "0";
            this.global.gua1NrcBackData.profile = this.global.ipphoto + this.global.staffData.staffLoanGuarantor1.backNRCImagePath;


          } else {
            let toast = this.toastCtrl.create({
              message: "Can't connect right now!",
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
          }
          this.loading.dismiss();
        },
          error => {
            let toast = this.toastCtrl.create({
              message: this.util.getErrorMessage(error),
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
            this.loading.dismiss();
          });
      }
    }
  }

  onSelectChangeGua2(selectStatus: any) {
    if (selectStatus == '0') {

      this.global.gua2NrcBackData = this.getDefaultImgObj();
      this.global.gua2NrcFrontData = this.getDefaultImgObj();
      this.global.staffData.staffLoanGuarantor2 = this.Gdata();
      this.Newguran2 = true;


    }
    else {
      //test Gua1 & Gua2 not same 
      if (this.global.staffData.staffLoanGuarantor2.guarantorSyskey != "" && this.global.staffData.staffLoanGuarantor1.guarantorSyskey == this.global.staffData.staffLoanGuarantor2.guarantorSyskey) {
        let toast = this.toastCtrl.create({
          message: "Please choose different guarantor already chosen",
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.global.staffData.staffLoanGuarantor2 = this.Gdata();
        this.global.gua2NrcBackData = this.getDefaultImgObj();
        this.global.gua2NrcFrontData = this.getDefaultImgObj();

      } else {
        // http://localhost:8080/MFIWebService/module001/service002/getLoanGuarantorData?syskey=582170
        this.http.get(this.ipaddress+ '/serviceSMI/getLoanGuarantorData?syskey=' + selectStatus).map(res => res.json()).subscribe(data => {
          if (data != null && data.member_name != '' && data.nrc != '') {
            this.global.staffData.staffLoanGuarantor2 = data;
            this.Newguran2 = false;
            this.global.gua2NrcFrontData.edit = "0";
            this.global.gua2NrcFrontData.profile = this.global.ipphoto + this.global.staffData.staffLoanGuarantor2.frontNRCImagePath;
            this.global.gua2NrcBackData.edit = "0";
            this.global.gua2NrcBackData.profile = this.global.ipphoto + this.global.staffData.staffLoanGuarantor2.backNRCImagePath;
          } else {
            let toast = this.toastCtrl.create({
              message: "Can't connect right now!",
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
          }
          this.loading.dismiss();
        },
          error => {
            let toast = this.toastCtrl.create({
              message: this.util.getErrorMessage(error),
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
            this.loading.dismiss();
          });
      }
    }
  }
  Checked(term) {
    this.loanTermData = this.loanTermList.filter((loanTermList) => {
      return (loanTermList.loanTermInYear.toString().toLowerCase().indexOf(term.toString().toLowerCase()) > -1);

    })
    this.global.staffData.staffLoanData.serviceCharges = this.loanTermData[0].serviceChargesRate + " %";
    this.global.staffData.staffLoanData.bankInterestRate = this.loanTermData[0].bankInterestRate + " %";
    // this.global.staffData.staffLoanData.serviceCharges = this.loanTermList[0].serviceChargesRate + " %";
    // this.global.staffData.staffLoanData.bankInterestRate = this.loanTermList[0].bankInterestRate + " %";

  }

  scrollToTop() {
    this.content.scrollToTop();
  }


  insertDataLoan() {


    this.sqlite.create({
      name: "smidb.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      db.executeSql("INSERT INTO LOAN(allData,appfrontNrc,appbackNrc,appPhoto,staffIDPhoto,familyFront,familyBack,"
        + "deptRecommend,gua1FrontNrc,gua1BackNrc,gua2FrontNrc,gua2BackNrc) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
        [JSON.stringify(this.global.staffData), JSON.stringify(this.global.appNrcFrontData), JSON.stringify(this.global.appNrcBackData)
          , JSON.stringify(this.global.appPhotoData), JSON.stringify(this.global.staffIdData), JSON.stringify(this.global.familyRegFrontData),
        JSON.stringify(this.global.familyRegBackData), JSON.stringify(this.global.deptChRecomData), JSON.stringify(this.global.gua1NrcFrontData)
          , JSON.stringify(this.global.gua1NrcBackData), JSON.stringify(this.global.gua2NrcFrontData), JSON.stringify(this.global.gua2NrcBackData)]).then((data) => {
            console.log("Insert data successfully", data);
          }, (error) => {
            console.error("Unable to insert data", error);
          });

    }).catch(
      e => console.log("Insert error " + e)
    );
  }

  deleteDataLoan(val) {
    this.sqlite.create({
      name: "smidb.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      console.log("success create or open database");

      db.executeSql("DELETE FROM LOAN", []).then((data) => {
        console.log("Delete data successfully", data);
        if (val == 1) {
          this.insertDataLoan();
        }
      }, (error) => {
        console.error("Unable to delete data", error);

      });
    }).catch(
      e => console.log("delete error :" + e)
    );
  }

  selectDataLoan() {
    this.loanlist = [];
    console.log("select data from sqlite");
    this.BindApplicantData(); 

    this.sqlite.create({
      name: "smidb.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      console.log("success create or open database");
      db.executeSql("SELECT * FROM LOAN ", []).then((data) => {
        console.log("length == " + data.rows.length);
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            this.global.staffData = JSON.parse(data.rows.item(i).allData);

            this.global.appPhotoData = JSON.parse(data.rows.item(i).appPhoto);
            this.global.staffIdData = JSON.parse(data.rows.item(i).staffIDPhoto);
            this.global.familyRegFrontData = JSON.parse(data.rows.item(i).familyFront);
            this.global.familyRegBackData = JSON.parse(data.rows.item(i).familyBack);
            this.global.deptChRecomData = JSON.parse(data.rows.item(i).deptRecommend);
            this.global.gua1NrcFrontData = JSON.parse(data.rows.item(i).gua1FrontNrc);
            this.global.gua1NrcBackData = JSON.parse(data.rows.item(i).gua1BackNrc);
            this.global.gua2NrcFrontData = JSON.parse(data.rows.item(i).gua2FrontNrc);
            this.global.gua2NrcBackData = JSON.parse(data.rows.item(i).gua2BackNrc);
            this.global.appNrcFrontData = JSON.parse(data.rows.item(i).appfrontNrc);
            this.global.appNrcBackData = JSON.parse(data.rows.item(i).appbackNrc);
          }
          this.RegionChangeGua1(this.global.staffData.staffLoanGuarantor1.currentAddressRegion);
          this.RegionChangeGua2(this.global.staffData.staffLoanGuarantor2.currentAddressRegion);
          console.log("staff loan = " + JSON.stringify(this.global.staffData));
        }
        else {
          this.BindApplicantData();   // why double put >> font not correct
        }
      }, (error) => {
        console.error("Unable to select data", error);

      });

    }).catch(e => console.log(e));

  }
  gobackmain() {

    // this.navCtrl.setRoot(LoanRequireInformationPage,
    //   {

    //   });


  }

  //test for HW Backbtn
  ionViewDidEnter() {
    this.backButtonExit();
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      this.gobackmain();
    });
  }


}
