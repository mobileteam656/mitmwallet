import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, PopoverController, MenuController } from 'ionic-angular';
import { SQLite } from '@ionic-native/sqlite';
import { Storage } from '@ionic/storage';
import { Changefont } from '../changefont/changeFont';
import { PopOverListPage } from '../pop-over-list/pop-over-list';
import { MyPopOverListPage } from '../my-pop-over-list/my-pop-over-list';

import { Login } from '../login/login';
import { Domain } from '../domain/domain';
import { URL } from '../url/url';
import { GlobalProvider } from '../../providers/global/global';
import { FunctProvider } from '../../providers/funct/funct';

@Component({
  selector: 'page-main-blank-page',
  templateUrl: 'main-blank-page.html',
  providers: [Changefont]
})
export class MainBlankPage {
  
  textMyan: any = []; //uin
  textEng: any = []; //uin
  txtData: any = [];
  font: any = '';
  ip: any;
  settings: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public changefont: Changefont, public storage: Storage, 
    public events: Events, public global: GlobalProvider,
    public sqlite: SQLite, public menuCtrl: MenuController) {
    this.storage.get('settings').then((result) => {
      this.settings = result;

      if (this.settings == undefined || this.settings == "" || this.settings == null) {
        this.settings = "demo";

        this.storage.remove('linkStatus');
        this.storage.set('linkStatus', "demo");

        ////console.log('Your default settings is', this.settings);
      }
    });
  }

  gotoUrl(s) {
    if (s == "domain") {
      this.storage.set('settings', s);
      this.navCtrl.push(Domain, {
        data: s
      });
    }
    else if (s == "demo") {
      //ipaddress links
      this.storage.set('ip', this.global.ipaddress);
      this.events.publish('ip', this.global.ipaddress);

      this.storage.set('ipaddressCMS', this.global.ipaddressCMS);
      this.events.publish('ipaddressCMS', this.global.ipaddressCMS);

      this.storage.set('ipaddressWallet', this.global.ipaddressWallet);
      this.events.publish('ipaddressWallet', this.global.ipaddressWallet);

      this.storage.set('ipaddressChat', this.global.ipaddressChat);
      this.events.publish('ipaddressChat', this.global.ipaddressChat);

      this.storage.set('ipaddressTicket', this.global.ipaddressTicket);
      this.events.publish('ipaddressTicket', this.global.ipaddressTicket);

      this.storage.set('imglink', this.global.digitalMedia);
      this.events.publish('imglink', this.global.digitalMedia);

      this.storage.set('profileImage', this.global.digitalMediaProfile);
      this.events.publish('profileImage', this.global.digitalMediaProfile);

      this.storage.set('ipaddressApp', this.global.ipaddressApp);
      this.events.publish('ipaddressApp', this.global.ipaddressApp);

      //others
      this.storage.set('settings', s);

      this.storage.remove('linkStatus');
      this.storage.set('linkStatus', s);
    }
    else if (s == "url") {
      this.navCtrl.push(URL, {
        data: s
      });
      this.storage.set('settings', s);
    }
  }

}
