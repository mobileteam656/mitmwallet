import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

@Component({
  selector: 'page-qr-value',
  templateUrl: 'qr-value.html'
})

export class QRValuePage {

  qrValue: string = '';

  constructor(public navParams: NavParams) {
    this.qrValue = this.navParams.get("paramQRValue");
  }
}
