import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events, Platform, AlertController } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalProvider } from '../../providers/global/global';
import { VideodetailPage } from '../videodetail/videodetail';
import { OnlycommentPage } from '../onlycomment/onlycomment';
import { ViewPhotoMessagePage } from '../view-photo-message/view-photo-message';
import { FunctProvider } from '../../providers/funct/funct';

@Component({
  selector: 'page-fbdetail',
  templateUrl: 'fbdetail.html',
  providers: [ChangelanguageProvider]
})
export class FbdetailPage {
  detailData: any = [];
  // url:string = 'https://javebratt.com/social-sharing-with-ionic/';
  items: any[];
  exit: any = false;
  showClick: any = {};
  showSave: any = {};
  url: string = 'https://javebratt.com/social-sharing-with-ionic/';
  isLoading: any;
  registerData: any = { syskey: '', t1: '' };
  menuList: any;
  menu: any;
  parameter: any;
  photoLink: any;
  font: any;
  textMyan: any = ["မှတ်ချက်များ", "မှတ်ချက် မရှိပါ", "မှတ်ချက်ရေးရန်", "ကြိုက်တယ်", "မှတ်ချက်", "မျှဝေမည်", "အသေးစိတ်"];
  textEng: any = ["Replies", "No Replies", "Write a comment .....", "Like", "Comment", "Share", "Detail"];
  textData: any = [];
  imgArr: any = [];
  ipaddress: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, public global: GlobalProvider,
    public http: Http, public alert: AlertController,
    public toastCtrl: ToastController, public events: Events,
    public changeLanguage: ChangelanguageProvider, public platform: Platform,
    public funct: FunctProvider) {
    this.storage.get('ipaddressCMS').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressCMS;
      }
    });

    this.detailData = this.navParams.get('detailData');
    this.photoLink = this.global.digitalMedia + "upload/image/";
    console.log("detailData ==" + JSON.stringify(this.detailData));
    console.log("detailData ==" + JSON.stringify(this.photoLink));

    if (this.detailData.uploadedPhoto.length > 0) {
      for (let tt = 0; tt < this.detailData.uploadedPhoto.length; tt++) {
        this.imgArr.push(this.detailData.uploadedPhoto[tt].t7);
      }

      console.log("this.imgArr == " + this.imgArr);
    }
    ////console.log("this is delMsg == " + JSON.stringify(this.detailData.delMsg));
    this.storage.get('appData').then((data) => {
      this.registerData = data;
      ////console.log("registerData = " + JSON.stringify(this.registerData));
    });

    this.storage.get('language').then((font) => {
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then((data) => {
        this.textData = data;
        if (font != "zg")
          this.font = 'uni';
        ////console.log("data language=" + JSON.stringify(data));
      });
    });

  }

  ionViewDidEnter() {
    this.backButtonExit();
  }

  singlePhoto(i) {
    ////console.log("viewImage == " + JSON.stringify(i));
    this.navCtrl.push(ViewPhotoMessagePage, {
      data: i,
      contentImg: "singlePhoto"
    });
  }

  comment(data) {
    this.navCtrl.push(OnlycommentPage, {
      data: data
    });
  }

  clickLike(data) {
    ////console.log("data=" + JSON.stringify(data));
    if (!data.showLike) {
      this.detailData.showLike = true;
      this.detailData.n2 = this.detailData.n2 + 1;
      //this.detailData.likeCount = this.funct.getChangeCount(this.detailData.n2);
      this.detailData.likeCount = this.detailData.n2;
      this.getLike(data);
    }
    else {
      this.detailData.showLike = false;
      this.detailData.n2 = this.detailData.n2 - 1;
      //this.detailData.likeCount = this.funct.getChangeCount(this.detailData.n2);
      this.detailData.likeCount = this.detailData.n2;
      this.getUnlike(data);
    }
  }

  getLike(data) {
    let parameter = {
      key: data.syskey,
      userSK: this.registerData.usersyskey,
      type: data.t3
    }
    ////console.log("request clickLike = ", JSON.stringify(parameter));
    this.http.get(this.ipaddress + '/serviceArticle/clickLikeArticle?key=' + data.syskey + '&userSK=' + this.registerData.usersyskey + '&type=' + data.t3).map(res => res.json()).subscribe(data => {
     // //console.log("response clickLike = ", JSON.stringify(data));
      if (data.state) {
        this.detailData.showLike = true;
      }
      else {
        this.detailData.showLike = false;
        this.detailData.n2 = this.detailData.n2 - 1;
      }
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  getUnlike(data) {
    let parameter = {
      key: data.syskey,
      userSK: this.registerData.usersyskey,
      type: data.t3
    }
    ////console.log("request clickLUnlike = ", JSON.stringify(parameter));
    this.http.get(this.ipaddress + '/serviceArticle/clickUnlikeArticle?key=' + data.syskey + '&userSK=' + this.registerData.usersyskey + '&type=' + data.t3).map(res => res.json()).subscribe(data => {
      ////console.log("response clickLUnlike = ", JSON.stringify(data));
      if (data.state) {
        this.detailData.showLike = false;
      }
      else {
        this.detailData.showLike = true;
        this.detailData.n2 = this.detailData.n2 + 1;
      }
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  clickBookMark(data) {
    ////console.log("data=" + JSON.stringify(data));
    if (!data.showContent) {
      this.detailData.showContent = true;
      this.saveContent(data);
    }
    else {
      this.detailData.showContent = false;
      this.unsaveContent(data);
    }
  }

  saveContent(data) {
    let parameter = {
      t1: data.t3,
      t4: this.registerData.t3,
      n1: this.registerData.usersyskey,
      n2: data.syskey,
      n3: 1
    }
    ////console.log("request saveContent parameter= ", JSON.stringify(parameter));
    this.http.post(this.ipaddress + '/serviceContent/saveContent', parameter).map(res => res.json()).subscribe(data => {
      ////console.log("response saveContent = ", JSON.stringify(data));
      // this.isLoading = false;
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  ionViewCanLeave() {
    // //console.log("Now i am leaving="+ this.detailData.t2)
  }

  unsaveContent(data) {
    ////console.log("request unsaveContent = ", JSON.stringify(data));
    let parameter = {
      t1: data.t3,
      t4: this.registerData.t3,
      n1: this.registerData.usersyskey,
      n2: data.syskey,
      n3: 0
    }
    ////console.log("request unsaveContent parameter = ", JSON.stringify(parameter));
    this.http.post(this.ipaddress + '/serviceContent/unsaveContent', parameter).map(res => res.json()).subscribe(data => {
      ////console.log("response unsaveContent = ", JSON.stringify(data));

      //this.isLoading = false;
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });
    toast.present(toast);
    this.isLoading = false;
    // this.loading.dismiss();
    ////console.log("Oops!");
  }

  /*share(i) {
    //     //console.log("this is share url == "+JSON.stringify(i)+"  //////////////  url === "+JSON.stringify(i.t8));


          let sahareImg = this.global.digitalMediaProfile;
          if(i.uploadedPhoto.length>0){
               sahareImg = i.uploadedPhoto[0].t7;
           }


     const Branch = window['Branch'];
     this.url = "https://b2b101.app.link/R87NzKABHL";

      var propertiesObj = {
        canonicalIdentifier: 'content/123',
        canonicalUrl: 'https://example.com/content/123',
        title:  i.t1,
        contentDescription: '' + Date.now(),
        contentImageUrl: sahareImg,
        price: 12.12,
        currency: 'GBD',
        contentIndexingMode: 'private',
        contentMetadata: {
        custom: 'data',
        testing: i.syskey,
        this_is: true
       }
     }

    // create a branchUniversalObj variable to reference with other Branch methods
     var branchUniversalObj = null
     Branch.createBranchUniversalObject(propertiesObj).then(function (res) {
     branchUniversalObj = res
     //console.log('Response1: ' + JSON.stringify(res))
     //optional field
     var analytics = {
              channel: 'facebook',
              feature: 'onboarding',
              campaign: 'content 123 launch',
              stage: 'new user',
              tags: ['one', 'two', 'three']
             }

    //          // optional fields
    var properties1 = {
                $desktop_url: 'http://www.example.com/desktop',
                $android_url: 'http://www.example.com/android',
                $ios_url: 'http://www.example.com/ios',
                $ipad_url: 'http://www.example.com/ipad',
                $deeplink_path: 'content/123',
                $match_duration: 2000,
                custom_string: i.syskey,
                custom_integer: Date.now(),
                custom_boolean: true
             }

         branchUniversalObj.generateShortUrl(analytics, properties1).then(function (res) {

    //             //console.log('Response2: ' + JSON.stringify(res.url));
                // optional fields
                var analytics = {
                  channel: 'facebook',
                  feature: 'onboarding',
                  campaign: 'content 123 launch',
                  stage: 'new user',
                  tags: ['one', 'two', 'three']
                }

    //            // optional fields
            var properties = {
                  $desktop_url: 'http://www.example.com/desktop',
                  custom_string: i.syskey,
                  custom_integer: Date.now(),
                  custom_boolean: true
                }
                var message = 'Check out this link';
    //            // optional listeners (must be called before showShareSheet)
                branchUniversalObj.onShareSheetLaunched(function (res) {
    
                })
                branchUniversalObj.onShareSheetDismissed(function (res) {
    
                })
                branchUniversalObj.onLinkShareResponse(function (res) {
   
                })
                branchUniversalObj.onChannelSelected(function (res) {
   
                })
    
                branchUniversalObj.showShareSheet(analytics, properties, message)
              }).catch(function (err) {
   
              })
        }).catch(function (err) {
   
      })
  }*/

  share(i) {
    console.log("this is share url == "+JSON.stringify(i)+"  //////////////  url === "+JSON.stringify(i.t8));


     let sahareImg = this.global.digitalMediaProfile;
      if(i.uploadedPhoto.length>0){
          sahareImg = i.uploadedPhoto[0].t7;
      }

      //let title =this.changefont.UnitoZg(i.t1);
const Branch = window['Branch'];
 //  this.url = "https://b2b101.app.link/R87NzKABHL";

   var propertiesObj = {
     canonicalIdentifier: 'content/123',
     canonicalUrl: 'https://example.com/content/123',
     title:  i.t1,
     contentDescription: '' + Date.now(),
     contentImageUrl: sahareImg,
     price: 12.12,
     currency: 'GBD',
     contentIndexingMode: 'private',
     contentMetadata: {
       custom: 'data',
       testing: i.syskey,
       this_is: true
     }
   }

   // create a branchUniversalObj variable to reference with other Branch methods
   var branchUniversalObj = null
   Branch.createBranchUniversalObject(propertiesObj).then(function (res) {
     branchUniversalObj = res
       console.log('Response1: ' + JSON.stringify(res))
      // optional fields
         var analytics = {
           channel: 'facebook',
           feature: 'onboarding',
           campaign: 'content 123 launch',
           stage: 'new user',
           tags: ['one', 'two', 'three']
         }

         // optional fields
         var properties1 = {
           $desktop_url: 'http://www.example.com/desktop',
           $android_url: 'http://www.example.com/android',
           $ios_url: 'http://www.example.com/ios',
           $ipad_url: 'http://www.example.com/ipad',
           $deeplink_path: 'content/123',
           $match_duration: 2000,
           custom_string: i.syskey,
           custom_integer: Date.now(),
           custom_boolean: true
         }

         branchUniversalObj.generateShortUrl(analytics, properties1).then(function (res) {
        
            console.log('Response2: ' + JSON.stringify(res.url));
           // optional fields
           var analytics = {
             channel: 'facebook',
             feature: 'onboarding',
             campaign: 'content 123 launch',
             stage: 'new user',
             tags: ['one', 'two', 'three']
           }

           // optional fields
           var properties = {
             $desktop_url: 'http://www.example.com/desktop',
             custom_string: i.syskey,
             custom_integer: Date.now(),
             custom_boolean: true
           }
           var message = 'Check out this link';
           // optional listeners (must be called before showShareSheet)
           branchUniversalObj.onShareSheetLaunched(function (res) {
             // android only
             console.log(res)
           })
           branchUniversalObj.onShareSheetDismissed(function (res) {
             console.log(res)
           })
           branchUniversalObj.onLinkShareResponse(function (res) {
             console.log(res)
           })
           branchUniversalObj.onChannelSelected(function (res) {
             // android only
             console.log(res)
           })
           // share sheet
           branchUniversalObj.showShareSheet(analytics, properties, message)
         }).catch(function (err) {
           console.error('Error2: ' + JSON.stringify(err))
         })
   }).catch(function (err) {
     console.error('Error1: ' + JSON.stringify(err))
   })
  }

  goDetail(url) {
    this.navCtrl.push(VideodetailPage, {
      url: url
    });
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      ////console.log("Active Page=" + this.navCtrl.getActive().name);
      this.navCtrl.pop();
    });
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad HomeDetailPage');
  }

}