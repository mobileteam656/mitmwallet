import { Component } from '@angular/core';
import { Events, NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import 'rxjs/Rx';
import { Changefont } from '../changefont/changeFont';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'my-pop-over-list',
  templateUrl: 'my-pop-over-list.html',
  providers: [Changefont]
})
export class MyPopOverListPage {

  headeritem = "Settings";
  popoverItemList: any[] = [
    { name: '', key: 0 }, { name: '', key: 1 },
    { name: '', key: 2 }, { name: '', key: 3 },
    { name: '', key: 4 }
  ];
  textMyan: any = [
    { name: 'လျှို့ဝှက်နံပါတ်ပြောင်းရန်', key: 0 }, { name: 'ဘာသာစကား', key: 1 },
    { name: "ထွက်မည်", key: 2 }, { name: 'ဗားရှင်း1.1.32', key: 3 },
    { name: 'ဖောင့်', key: 4 }
  ];
  textEng: any = [
    { name: 'Change Password', key: 0 }, { name: 'Language', key: 1 },
    { name: 'Logout', key: 2 }, { name: 'Version1.1.32', key: 3 },
    { name: 'Font', key: 4 }
  ];
  showFont: any;
  font: string;
  tempArray: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController, 
    public changefont: Changefont,
    public events: Events, 
    public storage: Storage,
    public global: GlobalProvider
  ) {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      ////console.log('Your language is', font);
      this.changelanguage(font);
    });
  }

  changelanguage(lan) {
    if (lan != 'eng') {
      for (let j = 0; j < this.textMyan.length; j++) {
        this.popoverItemList[j].name = this.textMyan[j].name;
      }
    }    
    else {
      this.font = '';
      for (let j = 0; j < this.textEng.length; j++) {
        this.popoverItemList[j].name = this.textEng[j].name;
      }
    }
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad PopoverPage');
  }
  changeLanguage(s) {
    this.viewCtrl.dismiss(s);
  }
  goMarket() {
    if (this.global.regionCode == '13000000') {
      //Yangon App (13000000)
      window.open('https://play.google.com/store/apps/details?id=dc.digitalyangon', '_system');
    }
    if (this.global.regionCode == '10000000') {
      //Mandalay App (10000000)
      window.open('https://play.google.com/store/apps/details?id=dc.digitalmandalay', '_system');
    }
    if (this.global.regionCode == '00000000') {
      //All App (00000000)
      window.open('https://play.google.com/store/apps/details?id=wallet.NSBmawallet', '_system');
    }
  }

}

