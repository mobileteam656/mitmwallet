import 'rxjs/add/operator/map';

import {
  AlertController, Events, LoadingController, NavController, NavParams, ToastController, Select, App, PopoverController, Platform
} from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { Component, ViewChild } from '@angular/core';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { PaymentDetailsPage } from '../payment-details/payment-details';
import { PaymentFailPage } from '../payment-fail/payment-fail';

@Component({
  selector: 'page-mpt-topup-confirm',
  templateUrl: 'mpt-topup-confirm.html',
})
 export class MptTopupConfirmPage {
  @ViewChild('myselect') select: Select;
  passtemp: any;
  textEng: any = ["Mobile Number", "Operator Type", "Topup Mobile Number", "Amount", "Description", "SUBMIT", "Choose Account No.", "Choose Operator Type", "Enter Phone No.", "Choose Amount", "Enter Amount", "Mobile Top Up", "RESET", "Account Balance", "Insufficient Balance", "Cash Back","MPT Topup"];
  textMyan: any = ["အကောင့်နံပါတ်", "အော်ပရေတာ အမျိုးအစား", "ငွေဖြည့်မည့် ဖုန်းနံပါတ်", "ငွေပမာဏ", "အကြောင်းအရာ", "ထည့်သွင်းမည်", "စာရင်းအမှတ်ရွေးချယ်ပါ", "Operator အမျိုးအစားရွေးချယ်ပါ", "ဖုန်းနံပါတ်ရိုက်သွင်းပါ", "ငွေပမာဏရွေးချယ်ပါ", "ငွေပမာဏရိုက်သွင်းပါ", "ဖုန်းငွေဖြည့်သွင်းခြင်း", "ပြန်စမည်", "ဘဏ်စာရင်းလက်ကျန်ငွေ", "ပေးချေမည့် ငွေပမာဏ မလုံလောက်ပါ", "ပြန်ရမည့်ငွေ","MPT Topup"];
  showFont: any = [];
  font: any = '';
  userData: any;
  topUpData: any;
  useraccount: any = [];
  ipaddress: any;
  referenceType: any = [];
  referenceTypeBill: any = [];
  amountType: any;
  msgCnpAcc: string;
  msgCnpAmtOther: string;
  msgCnpPhNo: string;
  msgCnpRef: string;
  msgCnpamt: string;
  referencename: any;
  resData: any;
  amt: any;
  activepage: any;
  moreContact: any = false;
  moreContactData: any = [];
  contactPh: any = true;
  primColor: any = { 'background-color': '#3B5998' };
  accountBal: any; cashBack: any;
  loading: any;
  popover: any;
  merchant: any;
  merchantMPT:any;
  alertPresented: any;
  amountBal: any;
  phonenumber: string = '';
  balance: string = '';
  flag1:any;
  amount: any;
  phone:any;
  errormsg1: any;
  fromPage: any;
  narrative:any;
  _obj: any = {
    "userID": "", "sessionID": "", "payeeID": "", "beneficiaryID": "",
    "fromInstitutionCode": "", "toInstitutionCode": "", "reference": "", "toAcc": "",
    "amount": "", "bankCharges": "", "commissionCharges": "", "remark": "", "transferType": "2",
    "sKey": "", fromName: "", toName: "", "wType": "",
    "field1": "", "field2": "","phone":""
  }
  
  constructor(public global: GlobalProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public http: Http,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public events: Events,
    public network: Network,
    public toastCtrl: ToastController,
    public changeLanguage: ChangelanguageProvider,
    public all: AllserviceProvider,
    public util: UtilProvider,
    public appCtrl: App,
    public changefont: Changefont,
    public popoverCtrl: PopoverController,
    public platform: Platform, ) {
       this._obj = this.navParams.get('data');
       console.log('MPT Data:  '+JSON.stringify(this._obj ));
      // // this.isOtp = this.navParams.get("otp");
      // // this.sKey = this.navParams.get('sKey');
      // this.fromPage = this.navParams.get('fromPage');
      this.amount = this.util.formatAmount(this._obj.amount) + "  " + "MMK";
      this.phone=this._obj.phone;
      this.narrative=this._obj.narrative;
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changelanguage(this.font)
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });
    this.storage.get('phonenumber').then((phonenumber) => {
      this._obj.userID = phonenumber;
      console.log('User ID: '+this._obj.userID);
    });
    
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;

      } else {
        this.ipaddress = this.global.ipaddress;
      }
    });
  }
  
  ionViewDidLoad() {
    this.storage.get('userData').then((data) => {
      this.userData = data;
      this.storage.get('ipaddress').then((result) => {
        if (result == null || result == '') {
        }
        else {
          this.ipaddress = result;
        }
      });
    });
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }
  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }
  cancel(){
    this.navCtrl.pop();
  }
  goConfrim(){
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let amount = this.util.formatToDouble(this._obj.amount); 
    let iv = this.all.getIvs();
    let dm = this.all.getIvs();
    let salt = this.all.getIvs(); 
    let parameter = {
      token: this._obj.sessionID,
      senderCode: this._obj.userID,
      merchantID: this._obj.beneficiaryID,
      fromName:this._obj.fromName,
      toName: this._obj.toName,
      amount: amount, 
      prevBalance: this._obj.balance,
      "password": '',
      "iv": iv, "dm": dm, "salt": salt
      //bankCharges: 0, 
      //refNo: 110,
      //field1: '',
      //merchantID:this.merchant.merchantID,
      //trprevBalance: this._obj.balance
    };
    this.http.post(this.ipaddress + '/payment/goMptTopup', parameter).map(res => res.json()).subscribe(data => {
      let resdata = {
        "bankRefNo": data.bankRefNumber,
        "code": data.code,
        "desc": data.desc,
        "transDate": data.transactionDate
      };
      console.log("response for payment in ad=" + JSON.stringify(data));
      let code = data.code;
      if (code == '0000') {
       
        this.resData = data;
        this.appCtrl.getRootNav().setRoot(PaymentDetailsPage, {
          data: resdata,
          data1: this._obj.amount,
          data2: this._obj.toName,
        });
        this.loading.dismiss();
      }
      else if(code == '0015'){
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      }
      else {
        this.resData = [];
        this.navCtrl.push(PaymentFailPage, {
          desc: data.desc,
          data: this._obj
        })
        this.loading.dismiss();
      }
    },
    error => {
      let toast = this.toastCtrl.create({
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
      this.loading.dismiss();
    });
  
  }

}
