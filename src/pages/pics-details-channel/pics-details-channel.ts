import { Component, ViewChild, ElementRef } from '@angular/core';
import { Events, Content, IonicPage, NavController, NavParams, ActionSheetController, Platform, LoadingController, ToastController, TextInput } from 'ionic-angular';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { File } from '@ionic-native/file';
//import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions, CameraPosition, MarkerOptions, Marker } from '@ionic-native/google-maps';
import { GoogleMaps } from '../../providers/google-maps';
import { Changefont } from '../changefont/changeFont';
import { Storage } from '@ionic/storage';
import { Keyboard } from '@ionic-native/keyboard';
import { Chat } from '../chat/chat';
declare var google;

@Component({
  selector: 'page-pics-details-channel',
  templateUrl: 'pics-details-channel.html',
})
export class PicsDetailsChannelPage {

  textEng: any = ["Details", "Write description..."];
  textMyan: any = ["အသေးစိတ်", "စာရိုက်ပါ"];
  textData: string[] = [];
  showFont: any;
  keyboardfont: any;
  language: any;

  @ViewChild(Content) content: Content;
  @ViewChild('chat_input') messageInput: TextInput;
  @ViewChild('map') mapElement: ElementRef;
  //map: GoogleMap;
  place: any;
  createDate: any = '';
  latLng: any;
  lat: any;
  lng: any;
  markers = [];
  loading: any;
  imageDetail_MydocsDetailChannel: any = '';
  from_MydocsDetailChannel: any = { fromName: '' };
  description_MydocsDetailChannel: any = { description: '' };
  passData: any = {};
  msg: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private nativeGeocoder: NativeGeocoder, public platform: Platform,
    public actionSheetCtrl: ActionSheetController, public loadingCtrl: LoadingController,
    private file: File, public toastCtrl: ToastController,
    public events: Events, public storage: Storage,
    public changefont: Changefont, private keyboard: Keyboard,
    public map: GoogleMaps
  ) {
    this.showFont = "uni";

    //{ PhotoPath: this.docData, name: this.photoName, latitude: this.latitude, longitude: this.longitude, location: this.location, CreateDate: this.createdDate, CreateTime: this.createdTime }
    this.imageDetail_MydocsDetailChannel = this.navParams.get('imageDetail');
    this.from_MydocsDetailChannel = this.navParams.get('from');
    this.description_MydocsDetailChannel = this.navParams.get('description');
    this.passData = this.navParams.get('data');
    this.msg = this.navParams.get('msg');

    this.lat = parseFloat(this.imageDetail_MydocsDetailChannel.latitude);
    this.lng = parseFloat(this.imageDetail_MydocsDetailChannel.longitude);
    this.createDate = this.imageDetail_MydocsDetailChannel.CreateDate + ' ' + this.imageDetail_MydocsDetailChannel.CreateTime;

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('font').then((font) => {
      this.keyboardfont = font;

      if (font == "zg" && this.language != "eng") {
        this.textData[1] = this.changefont.UnitoZg(this.textMyan[1]);
      } else if (font == "uni" && this.language != "eng") {
        this.textData[1] = this.textMyan[1];
      }
    });

    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;

      if (font == "zg" && this.language != "eng") {
        this.textData[1] = this.changefont.UnitoZg(this.textMyan[1]);
      } else if (font == "uni" && this.language != "eng") {
        this.textData[1] = this.textMyan[1];
      }
    });
  }

  changelanguage(lan) {
    this.language = lan;

    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }

      if (this.keyboardfont == 'zg') {
        this.textData[1] = this.changefont.UnitoZg(this.textMyan[1]);
      }
    }
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad PicsDetailsChannelPage');

    this.loadMap();
  }

  loadMap() {
    var point = { lat: this.lat, lng: this.lng };
    let divMap = (<HTMLInputElement>document.getElementById('map'));

    this.map = new google.maps.Map(divMap, {
      center: point,
      zoom: 15,
      disableDefaultUI: true,
      draggable: false,
      zoomControl: true
    });

    this.createMapMarker(point);
  }

  createMapMarker(place: any): void {
    var marker = new google.maps.Marker({
      map: this.map,
      position: place
    });

    this.markers.push(marker);
  }

  sendPicture(imageDetailParam) {
    ////console.log("sendPicture param=" + imageDetailParam);

    this.imageDetail_MydocsDetailChannel = imageDetailParam;

    if (this.from_MydocsDetailChannel.fromName == 'ChatPage') {
      this.from_MydocsDetailChannel.fromName = 'PicsDetailsChannelPage';
      this.navCtrl.pop();
    }

    if (this.from_MydocsDetailChannel.fromName == 'PicsChannelPage') {
      this.from_MydocsDetailChannel.fromName = 'PicsDetailsChannelPage';

      ////console.log("pass data (PicsDetailsChannelPage)=" + JSON.stringify(this.passData));

      this.navCtrl.push(Chat, {
        imageDetail: this.imageDetail_MydocsDetailChannel,
        from: this.from_MydocsDetailChannel,
        description: this.description_MydocsDetailChannel,
        data: this.passData,
        msg: this.msg
      });


      ////console.log('this.navCtrl.getByIndex(this.navCtrl.length())=' + this.navCtrl.length());
      ////console.log('this.navCtrl.getByIndex(this.navCtrl.length())='+JSON.stringify(this.navCtrl.getByIndex(0)));
      //this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length()-2));

      //this.navCtrl.length()-3 is Choose Channel Page
    }
  }



  saveGallery(photo) {
    let actionSheet = this.actionSheetCtrl.create({
      // title: 'Languages',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Your picture will be saved in gallery?',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.saveInGallery(photo);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
           // //console.log('Cancel clicked');
          }
        }
      ]
    });

    actionSheet.present();
  }

  addMarker() {
    window.open('geo://?q=' + this.imageDetail_MydocsDetailChannel.latitude + ',' + this.imageDetail_MydocsDetailChannel.longitude + '(' + this.imageDetail_MydocsDetailChannel.location + ')', '_system');
  }

  saveInGallery(photo) {
    ////console.log("dowload image == " + JSON.stringify(photo));

    this.loading = this.loadingCtrl.create({
      content: 'Saving...',
    });
    this.loading.present();

    ////console.log("this.file.externalRootDirectory == " + this.file.externalRootDirectory);
    this.file.checkDir(this.file.externalRootDirectory + 'DCIM/', 'DC').then((data) => {
      ////console.log('Directory exists');

      this.copyFileInDCUnderDCIM(photo);
    }, (error) => {
      ////console.log('Directory doesnt exist');
      this.file.createDir(this.file.externalRootDirectory + 'DCIM/', 'DC', false).then((res) => {
        ////console.log("file create success");

        this.copyFileInDCUnderDCIM(photo);
      }, (error) => {
       // //console.log("file create error");
      });
    });
  }

  copyFileInDCUnderDCIM(photo) {
    ////console.log("namePaht == " + namePath + "   //// currentNmae == " + currentName + "   ////  newFileName == " + newFileName);
   // //console.log("this.file.datadirectory == " + this.file.externalRootDirectory);
    let correctPath = photo.PhotoPath.substr(0, photo.PhotoPath.lastIndexOf('/') + 1);
    this.file.copyFile(correctPath, photo.name, this.file.externalRootDirectory + 'DCIM/DC', photo.name).then(success => {
      this.presentToast("Your picture has been saved in gallery.");
      this.loading.dismissAll();
    }, error => {
      this.loading.dismissAll();
      alert('Error while storing file.' + JSON.stringify(error));
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 5000,
      dismissOnPageChange: true,
      position: 'top'
    });
    toast.present();
  }

  onFocus() {
    this.content.resize();
  }

}
