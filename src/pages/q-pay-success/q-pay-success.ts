
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { WalletPage } from '../wallet/wallet';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-q-pay-success',
  templateUrl: 'q-pay-success.html',
})
export class QPaySuccessPage {

 
  passTemp: any;
  passTemp1: any;
  passTemp2: any;
  textEng: any = ["Account Number", "To account", "Bill Amount", "Bank Reference No.", "Transaction Date", "Card Expired", "Transaction Approved", "Transfer Type", "CLOSE","Commission Amount","Total Amount","Penalty Amount","Details"];
  textMyan: any = ["အကောင့်နံပါတ် မှ", "အကောင့်နံပါတ် သို့", "ငွေပမာဏ", 'အမှတ်စဉ်', 'လုပ်ဆောင်ခဲ့သည့်ရက်', 'သက်တမ်းကုန်ဆုံးချိန်', 'လုပ်ဆောင်မှုအတည်ပြုခြင်း', 'ငွေလွှဲ အမျိုးအစား', "ပိတ်မည်","ကော်မရှင် ငွေပမာဏ","စုစုပေါင်း ငွေပမာဏ", "ဒဏ်ကြေး","အသေးစိတ်အချက်အလက်"];
  showFont: any = [];
  font: any = '';
  amount: any;
  commAmt: any;
  totalAmt: any;
  userid: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public changeLanguage: ChangelanguageProvider) {
    this.passTemp = this.navParams.get("data");
    this.passTemp1 = this.navParams.get("detail");
    this.passTemp2 = this.navParams.get("mdata");
    //this.amount = this.navParams.get("amount");
    //this.commAmt = this.navParams.get("commAmt");
    this.totalAmt = this.navParams.get("totalAmt");
    this.userid = this.navParams.get("userid");
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {

        this.showFont = data;

      });
    });
  }

  ionViewDidLoad() {

  }

  getOK() {
    let params = { tabIndex: 0, tabTitle: 'Wallet' };
    this.navCtrl.setRoot(TabsPage, params);
  }

}
