import { Component } from '@angular/core';
import { Platform, NavController, ViewController, NavParams, MenuController, ModalController, LoadingController, PopoverController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';
import { GlobalProvider } from '../../providers/global/global';
import { DatePipe } from '@angular/common';
import { PaymentConfirm } from '../payment-confirm/payment-confirm';
import { UtilProvider } from '../../providers/util/util';
import { App } from 'ionic-angular';
import { PaymentDetailsPage } from '../payment-details/payment-details';
import { PaymentFailPage } from '../payment-fail/payment-fail';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { MerchantPaymentConfirmPage } from '../merchant-payment-confirm/merchant-payment-confirm';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { EventLoggerProvider } from '../../providers/eventlogger/eventlogger';
import { PaymentPasswordPage } from '../payment-password/payment-password';
import { TransferPasswordPage } from '../transfer-password/transfer-password';
@Component({
  selector: 'page-merchant-payment',
  templateUrl: 'merchant-payment.html',
  providers: [CreatePopoverPage]
})
export class MerchantPaymentPage {
  textEng: any = ["Payment", "Name", "Balance", "Payee", "Institution", "Amount",
    "Remark", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "Pay",
    "Mobile Wallet", "Type", "Payment To", "Reference", "Date",
    "Invalid Amount", "Merchant Name", "Phone Number", "Name", "Commission"];
  textMyan: any = ["ငွေပေးသွင်းရန်", "အမည်", "လက်ကျန်ငွေ", "ဖုန်းနံပါတ်/ စာရင်းနံပါတ်", "အော်ပရေတာ", "ငွေပမာဏ",
    "မှတ်ချက်", "မှားယွင်းနေပါသည်", "Does not match! Please try again.", "Please insert name field!", "ပေးသွင်းမည်",
    "Mobile Wallet", "အမျိုးအစား", "Payment To", "အကြောင်းအရာ", "နေ့စွဲ",
    "ငွေပမာဏ ရိုက်ထည့်ပါ", "Merchant Name", "Phone Number", "Name", "Commission"];

  textData: string[] = [];
  errormsg2 = "";
  type: any = { name: "mWallet", code: "mWallet" };
  institute: any = { name: "", code: "" };
  address: string = '';
  userID: string = '';
  balance: string = '';
  loading: any;
  beneficiaryID: string = '';
  toInstitutionCode: string = '';
  amount: string = '';
  remark: string = '';
  ipaddress: string = '';
  font: string = '';
  storageToken: any;
  token: any;
  lan: any;
  popover: any;
  btnflag: boolean;
  password: any;
  buttonColor: string = '';
  remark_flag: boolean = false;
  _obj = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
    frominstituteCode: "", toInstitutionCode: "", reference: "", toAcc: "", amount: "",
    bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "",
    fromName: "", toName: "", "wType": "",
    field1: "", field2: "", tosKey: "", datetime: "", accountNo: "", balance: ""
  }
  errormsg1: any;
  temp: any;
  normalizePhone: any;
  readOnly: any;
  merchant: any;
  pass: boolean = true;
  public alertPresented: any;
  resData: any;
  rkey: any;
  flag1: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, public menuCtrl: MenuController,
    public modalCtrl: ModalController, public http: Http,
    public datePipe: DatePipe, public loadingCtrl: LoadingController,
    public toastCtrl: ToastController, public sqlite: SQLite,
    public changefont: Changefont, public createPopover: CreatePopoverPage,
    public events: Events, public alertCtrl: AlertController, private eventlog: EventLoggerProvider,
    public popoverCtrl: PopoverController, public platform: Platform,
    public util: UtilProvider, public global: GlobalProvider,
    private slimLoader: SlimLoadingBarService,
    public viewCtrl: ViewController, public appCtrl: App, public all: AllserviceProvider) {
    //public createPopover: CreatePopoverPage) {
    this.readOnly = 0;
    this.flag1 = 0;
    //this.remark_flag = true;
    this.storage.get('userData').then((userData) => {
      this._obj = userData;
      if (userData != null || userData != '') {
        this.balance = userData.balance;
      }
    });

    let institution = this.util.setInstitution();
    this._obj.frominstituteCode = institution.fromInstitutionCode;
    this._obj.toInstitutionCode = institution.toInstitutionCode;
    this._obj.toInstitutionName = institution.toInstitutionName;
    this.institute.name = this._obj.toInstitutionName;
    this.institute.code = this._obj.toInstitutionCode;
    this.storage.get('phonenumber').then((phonenumber) => {
      this._obj.userID = phonenumber;
    });
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddress;
      }
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    })
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    this.merchant = this.navParams.get('param');
    if (this.merchant != null && this.merchant != undefined && this.merchant != '') {
      this._obj.accountNo = this.merchant.accountNo;
      this._obj.beneficiaryID = this.merchant.merchantID;
      this._obj.toName = this.merchant.merchantName;
    }
    console.log('Merchant PAyment Data is: ' + JSON.stringify(this._obj));
    // this.temp = this.navParams.get('data');
    // console.log("Payment temp: " +this.temp);
    // if (this.temp != null && this.temp != undefined && this.temp != '') {
    //   this._obj.beneficiaryID = this.temp[1];    
    //   this.getName(this._obj.beneficiaryID);
    //   this._obj.toInstitutionCode = this.temp[2];   
    //   this._obj.amount = this.temp[3];
    //   this._obj.tosKey = this.temp[6];
    //   if (this._obj.amount != "" && this._obj.amount != "0" && this._obj.amount != "0.0" && this._obj.amount != "0.00") {
    //     this.readOnly = 1;
    //   }
    //   this._obj.remark = this.temp[4];
    //   if (this._obj.remark != '' && this._obj.remark != undefined) {
    //       this.remark_flag = true;
    //   }
    //   this._obj.datetime = this.temp[5];  
    // }
    this.storage.get('phonenumber').then((phonenumber) => {
      this._obj.userID = phonenumber;
    });
    this.storage.get('username').then((username) => {
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    });
    this.events.subscribe('username', username => {
      if (username !== undefined && username !== "") {
        this._obj.fromName = username;
      }
    });
    this.storage.get('firebaseToken').then((firebaseToken) => {
      this.storageToken = firebaseToken;
      console.log('Storage Login Token: ' + this.storageToken);
    });
  }

  getError(error) {
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = "Can't connect right now. [" + code + "]";
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
  }

  ionViewWillEnter() {
    if (this.merchant.merchantID != undefined || this.merchant != '' || this.merchant != null) {
      this._obj.beneficiaryID = this.merchant.merchantID;
      this._obj.toName = this.merchant.merchantName;
    }
  }
  ionViewDidLoad() {
  }

  // presentPopover(ev) {
  //   this.createPopover.presentPopover(ev);
  // }

  typeChange(s) {
    this.type = s;
    this.type.name = s.name;
    this.type.code = s.code;
  }

  // onChange(s, i) {
  //   if (i == "09") {
  //     this._obj.beneficiaryID = "+959";
  //   }
  //   else if (i == "959") {
  //     this._obj.beneficiaryID = "+959";
  //   }
  // }

  // instituteChange(s) {
  //   this._obj.toInstitutionName = s.toInstitutionName;
  //   this._obj.toInstitutionCode = s.toInstitutionCode;
  // }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  showAlert(titleText, subTitleText) {
    if (this.alertPresented == undefined) {
      let alert = this.alertCtrl.create({
        title: titleText,
        subTitle: subTitleText,
        buttons: [
          {
            text: 'Ok',
            handler: () => {
              this.alertPresented = undefined;
            }
          }
        ],
        enableBackdropDismiss: false
      });

      this.alertPresented = alert.present();
      setTimeout(() => alert.dismiss(), 2000 * 60);
    }
  }
  calculate() {
    let flag3 = false;
    if (this._obj.amount != '' && this._obj.amount != null && this._obj.amount != undefined) {
      flag3 = true;
      this.errormsg2 = '';
    } else {
      flag3 = false;
      this.errormsg2 = this.textData[16];
    }
    if (flag3) {
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
      let param =
      {
        userID: this._obj.userID,
        sessionID: this._obj.sessionID,
        amount: this._obj.amount,
        merchantID: this.merchant.merchantID
      };
      console.log("Calculate Commission param>>" + JSON.stringify(param))
      this.http.post(this.ipaddress + '/payment/calculateComm', param).map(res => res.json()).subscribe(data => {
        this.loading.dismiss();
        let code = data.code;
        if (code == '0000') {

        } else if (data.code == "0016") {

        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 5000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    }
  }

  goConfrim() {
    console.log('Payment Data ndh: ' + JSON.stringify(this._obj));
    if (this.util.checkInputIsEmpty(this._obj.amount) || this.util.checkNumberOrLetter(this._obj.amount) ||
      this.util.checkAmountIsZero(this._obj.amount) || this.util.checkAmountIsLowerThanZero(this._obj.amount) ||
      this.util.checkStartZero(this._obj.amount) || this.util.checkPlusSign(this._obj.amount)) {
      this.errormsg1 = this.textData[16];

    }
    else {
      if (this.merchant != null && this.merchant != undefined && this.merchant != '') {
        this._obj.accountNo = this.merchant.accountNo;
        this._obj.beneficiaryID = this.merchant.merchantID;
        this._obj.toName = this.merchant.merchantName;
      }
      let tempdata: any;
      this.slimLoader.start(() => { });
      let userID1 = this._obj.userID;
      let param = { userID: userID1, sessionID: this._obj.sessionID, type: '1', merchantID: this._obj.beneficiaryID };
      this.http.post(this.ipaddress + '/service002/readMessageSetting', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.btnflag = false;
          tempdata = data;
          this.rkey = tempdata.rKey
          if (this.rkey == "true") {
            this.flag1 = 1;
          }
          console.log("tempdata is chk password : " + JSON.stringify(tempdata));
          this.slimLoader.complete();
          this.eventlog.fbevent('merchant_payment_confrim', { Message: 'Merchant Payment Confrim' });
          console.log('Go to Merchant Payment Confrim: ' + JSON.stringify(this._obj));
          this.navCtrl.push(MerchantPaymentConfirmPage, {
            // data: this._obj,
            data: this._obj,
            psw: this.flag1,
            amount: this._obj.amount
          })

        }
        else {
          this.showAlert('Warning!', data.desc);
          this.slimLoader.complete();
        }
      },
        error => {
          this.showAlert('Warning!', this.getError(error));
          this.slimLoader.complete();
        });

    }

  }
}



