import { Component } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, ToastController, LoadingController, PopoverController, AlertController, Events, App, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { GlobalProvider } from '../../providers/global/global';
import { Login } from '../login/login';
import { AccountTransactionDetail } from '../account-transaction-detail/account-transaction-detail';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Language } from '../language/language';
import { MyPopOverListPage } from '../my-pop-over-list/my-pop-over-list';
import { UtilProvider } from '../../providers/util/util';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { MyPopOverPage } from '../my-pop-over-page/my-pop-over-page';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
declare var window;
@Component({
  selector: 'page-account-transaction',
  templateUrl: 'account-transaction.html',
  providers: [ChangelanguageProvider, CreatePopoverPage]

})
export class AccountTransaction {
  userdata: any = {
    userID: "", sessionID: "", accountNo: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
    frominstituteCode: "", toInstitutionCode: "", toAcc: "", amount: "", bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "", field1: "", field2: ""
  };
  accountlist: any;
  loading: any;
  useraccount: any;
  fromDate: any = '';
  toDate: any = '';
  currentpage: any = 1;
  pageSize: any = 10;
  duration: any = 0;
  durationmsg: any;
  passTemp: any = {};
  todayDate: any = '';
  resultdata: any = [];
  walletansfer: any = [];
  merchantpayment: any = [];
  wallettopup: any = [];
  paydata:any;
  recdata:any;
  range: any = false;
  next: number = 1;
  type:string='All';
  previous: number = 1;
  status: any;
  ipaddress: string = '';
  idleState = 'Not started.';
  timedOut = false;
  lastPing: Date = null;
  min: any;
  sec; any;
  modal: any;
  hardwareBackBtn: any = true;
  font: string;
  tempFont: number = 3;
  showFont: string[] = [];
  popover: any;
  selectedTitle: string;
  currency:string;
  drcrtype: any;
  
  displaydata= [{
    "accRef": "", "codTxnCurr": "", "nbrAccount": "","state":"","subRef":"",
    "txnAmount": "", "txnDate": "", "txnDateTime": "", "txnTypeDesc": "", "txtReferenceNo": "", 
   
  }];
  account_data=[{
    "codTxnCurr":"","nbrAccount":"","txnDate":"","txnAmount":"","txtReferenceNo":"","txnTypeDesc":"","txnCode":"",
    "subRef":"","accRef":"","drcr":"","remark":"","txnDateTime":""

  }];


  displayReceiveArray:any=[];
  displayPaymentArray:any=[];
  displayArray:any=[];
  transactiontype=['All','Payment','Receive'];
  textEng: any = ['Transactions', 'Select Account', 'Filter by Date', 'Last 10 Transaction', 'Last 2 Days', 'Last 5 Days', 'No result found!', 'Next', 'Previous', 'Default', '2 Days', '5 Days', 'Custom', 'Start Date', 'End Date', 'Search', 'Are you sure you want to exit', 'Yes', 'No', 'Ref', 'TransNo.'];
  textMyan: any = ['လုပ်ဆောင်မှု မှတ်တမ်း', 'စာရင်းနံပါတ်ရွေးချယ်ပါ', 'ရက်အလိုက်စီစဉ်သည်', 'နောက်ဆုံး၁၀ကြောင်း', 'လွန်ခဲ့သော၂ရက်', 'လွန်ခဲ့သော၅ရက်', 'အချက်အလက်မရှိပါ', 'ရှေ့သို့', 'နောက်သို့', 'နောက်ဆုံး ၁၀ ချက်', 'လွန်ခဲ့သော၂ရက်', 'လွန်ခဲ့သော၅ရက်', 'စိတ်ကြိုက်ရွေးချယ်မည်', 'စတင်သည့်ရက်', 'ပြီးဆုံးသည့်ရက်', 'ရှာမည်', 'ထွက်ရန်သေချာပါသလား', 'သေချာသည်', 'မသေချာပါ', 'အကြောင်းအရာ', 'အမှတ်စဉ်'];
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public http: Http, public datePipe: DatePipe, public popoverCtrl: PopoverController, public alertCtrl: AlertController,
    public events: Events, public app: App, private slimLoader: SlimLoadingBarService,
    public platform: Platform, public changeLanguage: ChangelanguageProvider,
    public util: UtilProvider,
    public viewCtrl: ViewController,
    public global: GlobalProvider,
    public appCtrl: App,
    public createPopover: CreatePopoverPage,
    private firebase: FirebaseAnalytics) {
    this.events.subscribe('userData', val => {
      this.userdata = val;
      this.accountlist = val.accountNo;
    });
    this.storage.get('userData').then((val) => {
      this.userdata = val;
      this.accountlist = val.accountNo;

      this.storage.get('ipaddress').then((ip) => {
        if (ip !== undefined && ip != null && ip !== "") {
          this.ipaddress = ip;
          this.goSearch();
        } else {
          this.ipaddress = this.global.ipaddress;
          this.goSearch();
        }
      });
    });
    let date = new Date();
    this.todayDate = this.datePipe.transform(date, 'dd MM yyyy');
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
   
  }

  ionViewDidLoad() {
    /* this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    }); */
  }
  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  accountChange(s) {
    this.useraccount = s;
    this.tempFont = 3;
    this.duration = 0;
    this.durationmsg = this.showFont[2] + " ( " + this.showFont[3] + " )";
    this.goSearch();
  }

  goduration(s) {
    this.duration = s;
  }

  nextData() {
    this.next++;
    if (this.next <= this.passTemp.pageCount) {
      this.currentpage = this.currentpage + 1;
      this.previous = this.next;
      this.goSearch();
    }
  }

  previousData() {
    this.previous--;
    if (this.previous >= 1) {
      this.currentpage = this.previous;
      this.next = this.previous;
      this.goSearch();
    }
  }
  getType(data){
    this.type=data;
    console.log('Type'+this.type);

  }
  goSearch() {
    if (this.fromDate != '' && this.toDate != '' && this.duration == '3') {
      this.durationmsg = " ( " + this.datePipe.transform(this.fromDate, 'dd-MM-yyyy') + " to " + this.datePipe.transform(this.toDate, 'dd-MM-yyyy') + " )";
    }
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
      //   duration: 3000datePipe
    });
    this.loading.present();
    let phone = this.userdata.userID; //this.util.removePlus(this.userdata.userID);
    var param = {
      userID: phone,
      sessionID: this.userdata.sessionID,
      customerNo: '',
      durationType: this.duration,
      fromDate: this.datePipe.transform(this.fromDate, 'yyyyMMdd'),
      toDate: this.datePipe.transform(this.toDate, 'yyyyMMdd'),
      totalCount: 0,
      acctNo: this.userdata.accountNo,
      currentPage: this.currentpage,
      pageSize: this.pageSize,
      pageCount: 0,

    };
    this.http.post(this.ipaddress + '/service001/getTransactionActivityList', param).map(res => res.json()).subscribe(data => {
      //console.log("My Transaction History data is : " + JSON.stringify(data));
      if (data.code == '0000') {
        this.range = false;
        this.status = 1;
        if (data.pageCount > 1 && this.currentpage == 1) {
          this.next = 1;
          this.previous = 1;
        }
        let tempArray = [];
        if (!Array.isArray(data.data)) {
          tempArray.push(data.data);
          data.data = tempArray;
        }
        this.passTemp = data;
        this.currency='MMK';
        for(let j=0;j<data.data.length;j++){
          this.displayArray=data.data[j].txnTypeDesc.split(",");   
          data.data[j].a = this.displayArray[0];   
          data.data[j].b = this.displayArray[1];   
          data.data[j].c = this.displayArray[2];   
          data.data[j].d = this.displayArray[3];              
        
        }
        
        let group_to_values = data.data.reduce(function (obj, item) {
          obj[item.txnDate] = obj[item.txnDate] || [];
          obj[item.txnDate].push(item);
          return obj;
        }, {});
        let groups = Object.keys(group_to_values).map(function (key) {
          return {
            txnDate: key, data: group_to_values[key]
          };
        });
       this.resultdata = groups;
       console.log('Transaction Date: '+this.resultdata.txnDate);
       this.resultdata.data=data.data;
        //console.log('Length'+JSON.stringify(this.resultdata.data.length));
        //console.log('Length'+JSON.stringify(this.resultdata.data[0].drcr));

    
        for(let i=0;i<this.resultdata.data.length;i++){
            if(this.resultdata.data[i].drcr=='1'){
              this.walletansfer.push(this.resultdata.data[i]);              
            }else {
              this.merchantpayment.push(this.resultdata.data[i]);
              }
        }
        // console.log('Payment Data: '+JSON.stringify(this.walletansfer));
        // console.log('Receive Data: '+JSON.stringify(this.merchantpayment));
        
        for(let k=0;k<this.walletansfer.length;k++){
          this.displayPaymentArray[k]=this.walletansfer[k].txnTypeDesc.split(',');
        }
      console.log('Split Payment Data '+JSON.stringify(this.displayPaymentArray));
        
      for(let k=0;k<this.merchantpayment.length;k++){
        this.displayReceiveArray[k]=this.merchantpayment[k].txnTypeDesc.split(',');
      }
       
        this.loading.dismiss();
      }
      //  if (data.code == "0012") {
      //   this.util.SessiontimeoutAlert(data.desc);
      // }
      else if (data.code == "0016") {
        this.status = 0;
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: data.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                // this.storage.remove('phonenumber');
                this.storage.remove('username');
                this.storage.remove('userData');
                this.storage.remove('firstlogin');
                this.appCtrl.getRootNav().setRoot(Login);
              }
            }
          ]
        })
        confirm.present();
        this.loading.dismiss();
      }
      else {
        this.status = 0;
        this.range = false;
        this.passTemp = {};
        this.resultdata = [];
        let toast = this.toastCtrl.create({
          message: data.desc,
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    },
      error => {
        this.status = 0;
        /* ionic App error
         ............001) url link worng, not found method (404)
         ........... 002) server not response (500)
         ............003) cross fillter not open (403)
         ............004) server stop (-1)
         ............005) app lost connection (0)
         */
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }

  gotoDetail(a) {
    this.navCtrl.push(AccountTransactionDetail,
      {
        data: a

      });
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  backButtonAction() {
    if (this.modal && this.modal.index === 0) {

      this.modal.dismiss();
    } else {

      /* exits the app, since this is the main/first tab */
      if (this.hardwareBackBtn) {

        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;
            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.createPopover.goLogout();
            }
          }]
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }

  datafilter(ev) {
    

    this.popover = this.popoverCtrl.create(MyPopOverPage, {});

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      //    this.resultdata={};

      this.duration = data;
      this.currentpage = 1;
      if (data != 3 && data != null) {
        if (data == 0) {
          this.durationmsg = this.showFont[2] + " ( " + this.showFont[3] + " )";
          this.tempFont = 3;
        }
        if (data == 1) {
          this.durationmsg = this.showFont[2] + " ( " + this.showFont[4] + " )";
          this.tempFont = 4;
        }
        else if (data == 2) {
          this.durationmsg = this.showFont[2] + " ( " + this.showFont[5] + " )";
          this.tempFont = 5;
        }

        this.range = false;
        /* this.fromDate = '';
        this.toDate = ''; */
        this.currentpage = 1;
        this.pageSize = 10;
        this.goSearch();
      }
      else if (data == 3) {
        this.range = true;
        this.status = '0';
        this.tempFont = 6;
      }
  
    });
  }
}
