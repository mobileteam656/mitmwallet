import { Component } from '@angular/core';
import { Platform, NavController, LoadingController, PopoverController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { Changefont } from '../changefont/changeFont';

import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { App } from 'ionic-angular';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { LawsPage } from '../laws/laws';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FileOpener } from '@ionic-native/file-opener';
import { DocumentViewer } from '@ionic-native/document-viewer';

@Component({
  selector: 'page-documents',
  templateUrl: 'documents.html',
  providers: [Changefont, CreatePopoverPage]
})

export class DocumentsPage {

  textEng: any = ["Documents", "Laws", "Forms", "Search", "No PDF Type Found!", "Search"];
  textMyan: any = ["Documents", "Laws", "Forms", "Search", "No PDF Type Found!", "Search"];
  textData: string[] = [];
  font: string = '';
  ipaddressCMS: string = '';
  popover: any;
  keyboardfont: any;

  nores: string = '';
  typeList: any = [];

  pdfList: any = [];

  loading: any;
  keyword: any;

  constructor(public util: UtilProvider, public platform: Platform,
    public storage: Storage, public toastCtrl: ToastController,
    public loadingCtrl: LoadingController, public http: Http,
    public changefont: Changefont, public events: Events,
    public global: GlobalProvider, public popoverCtrl: PopoverController,
    public appCtrl: App, public createPopover: CreatePopoverPage,
    public navCtrl: NavController, private file: File,
    private transfer: FileTransfer, private fileOpener: FileOpener,
    private document: DocumentViewer) {
    this.storage.get('ipaddressCMS').then((ip) => {
      ////console.log("ip get=" + JSON.stringify(ip));
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddressCMS = ip;
        this.getTypeList();
      } else {
        this.ipaddressCMS = this.global.ipaddressCMS;
        this.getTypeList();
      }
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
      ////console.log("language get=" + JSON.stringify(font));
      ////console.log("language get=" + JSON.stringify(this.textData));
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
      ////console.log("changelanguage subscribe=" + JSON.stringify(lan));
      ////console.log("changelanguage subscribe=" + JSON.stringify(this.textData));
    });

    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;
    });

    this.storage.get('font').then((font) => {
      if (font == "zg") {
        this.keyboardfont = 'zg';
      } else {
        this.keyboardfont = 'uni';
      }
    });

    this.nores = "2";
  }

  /* getAllPDF() {
    let param = {
      "type": "",
      "region": this.global.region
    };
    //console.log("param=" + JSON.stringify(param));

    this.http.post(this.ipaddressCMS + '/service001/getAllPdf', param).map(res => res.json()).subscribe(
      result => {
        //console.log("getPdf=" + JSON.stringify(result));

        if (result.code == "0000") {
          let tempArray = [];

          if (!Array.isArray(result.pdfList)) {
            tempArray.push(result.pdfList);
            result.pdfList = tempArray;
          }

          if (result.pdfList.length > 0) {
            this.pdfList = result.pdfList;
            this.tempPdfList = this.pdfList;
          }
        }
      },
      error => {
        //console.log("getPdf=" + JSON.stringify(error));
        this.getError(error);
      });
  } */

  // searchKeyword(ev: any) {
  //   let val = ev.target.value.toLowerCase();
  searchKeyword() {
    this.nores = '1';

    this.getLawPDF();
  }

  getLawPDF() {
    let param = {
      "keyword": this.keyword,
      "region": this.global.region
    };

    ////console.log("param=" + JSON.stringify(param));

    this.http.post(this.ipaddressCMS + '/service001/getKeyword', param).map(res => res.json()).subscribe(result => {
      ////console.log("getPdf=" + JSON.stringify(result));

      if (result.code == "0000") {
        let tempArray = [];

        if (!Array.isArray(result.pdfList)) {
          tempArray.push(result.pdfList);
          result.pdfList = tempArray;
        }

        if (result.pdfList.length > 0) {
          this.pdfList = result.pdfList;
        } else {
        }
      } else {
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
      }
    },
      error => {
        let code;

        if (error.status == 404) {
          code = '001';
        } else if (error.status == 500) {
          code = '002';
        } else if (error.status == 403) {
          code = '003';
        } else if (error.status == -1) {
          code = '004';
        } else if (error.status == 0) {
          code = '005';
        } else if (error.status == 502) {
          code = '006';
        } else {
          code = '000';
        }

        let msg = "Can't connect right now. [" + code + "]";

        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
      });
  }

  openPDF(p) {
    if (this.platform.is('cordova')) {
      let path = null;

      if (this.platform.is('ios')) {
        path = this.file.documentsDirectory;
      } else if (this.platform.is('android')) {
        path = this.file.dataDirectory;
      }
      const transfer = this.transfer.create();
      //p.link expects https://xxxxxxx/abc.pdf
      this.loadingCCP();
      transfer.download(p.link + p.name, path + p.name).then(entry => {
        this.fileOpener.open(path + p.name, 'application/pdf')
          .then(() => {
            ////console.log('File is opened');
            this.dismiss();
          })
          .catch(e => {
           // //console.log('Error opening file', e);
            this.dismiss();
          });
      });
    } else {
      this.downloadPDF(p);
    }
  }

  downloadPDF(p) {
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else if (this.platform.is('android')) {
      path = this.file.dataDirectory;
    }

    const transfer = this.transfer.create();
    //p.name expects abc.pdf
    //p.link expects https://xxxxxxx/abc.pdf
    transfer.download(p.link + p.name, path + p.name).then(entry => {
      let url = entry.toURL();
      this.document.viewDocument(url, 'application/pdf', {});
    });
  }

  goPage(type) {
    this.navCtrl.push(LawsPage, {
      param: type
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  getError(error) {
    let code;

    if (error.status == 404) {
      code = '001';
    } else if (error.status == 500) {
      code = '002';
    } else if (error.status == 403) {
      code = '003';
    } else if (error.status == -1) {
      code = '004';
    } else if (error.status == 0) {
      code = '005';
    } else if (error.status == 502) {
      code = '006';
    } else {
      code = '000';
    }

    let msg = "Can't connect right now. [" + code + "]";

    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });

    //toast.present(toast);
  }

  loadingCCP() {
    this.loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Processing...',
    });

    this.loading.present();
  }

  dismiss() {
    this.loading.dismissAll();
  }

  getTypeList() {
    this.http.get(this.ipaddressCMS + '/serviceAdmLOV/getPDFType').map(res => res.json()).subscribe(result => {
      ////console.log("My register getFileType:" + JSON.stringify(result));

      if (result != null && result.msgCode != null && result.msgCode == "0000") {
        ////console.log('getFileType:' + JSON.stringify(result));
        let tempArray = [];

        if (!Array.isArray(result.refFileType)) {
          tempArray.push(result.refFileType);
          result.refFileType = tempArray;
        }

        this.typeList = result.refFileType;
        
        this.nores = "2";
      } else {
        this.nores = "0";
      }
    },
      error => {
        this.nores = "0";
        this.getError(error);
      });
  }

}
