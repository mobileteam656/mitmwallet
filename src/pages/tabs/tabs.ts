import { Component } from '@angular/core';
import { Platform, NavController, NavParams, PopoverController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ContactPage } from '../contact/contact';//contact
import { Changefont } from '../changefont/changeFont';
import { HomePage } from '../home/home';
import { MessageUserPage } from '../message-user/message-user';
import { GlobalProvider } from '../../providers/global/global';
import { MorePage } from '../more/more';
import { WalletPage } from '../wallet/wallet';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
  providers: [Changefont]
})
export class TabsPage {

  // set the root pages for each tab
  //tab1Root: any = SchedulePage;
  homeRoot: any = HomePage;
  messageRoot: any = MessageUserPage;
  contactRoot: any = ContactPage;
  walletRoot: any = WalletPage;
  moreRoot: any = MorePage;
  mySelectedIndex: number;
  tabTitle: any;
  buttonColor: string = '#000';

  popover: any;

  font: any = '';
  textData: any = [];

  regionAll: boolean;
  regionOther: boolean;
  textMyan: any = ["ဝေါ(လ်)လက်", "မက်ဆေ့ခ်ျ", "အဆက်အသွယ်", "သတင်း"];
  textEng: any = ["Wallet", "Messages", "Contacts", "News"];
  public tabs;
  constructor(
    public platform: Platform,
    public popoverCtrl: PopoverController,
    public navParams: NavParams,
    public storage: Storage,
    public events: Events,
    public changefont: Changefont,
    public global: GlobalProvider,
    public navCtrl: NavController
  ) {
    this.regionAll = true;
    this.regionOther = false;
    this.tabs = [
      { title: "Wallet", root: WalletPage, icon: "cash",color:"white"},
      { title: "Messages", root: MessageUserPage, icon: "chatbubbles",color:"white"},
      { title: "Contacts", root: ContactPage, icon: "contacts",color:"white" },
      { title: "News", root: HomePage, icon: "book",color:"white" }
    ];
    this.mySelectedIndex = navParams.data.tabIndex || 0;
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    }) //...

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    //console.log("navParams.data.tabIndex" + navParams.data.tabIndex);
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.tabs[i].title = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.tabs[i].title = this.textMyan[i];
      }
    }
  }

  ionViewCanEnter() {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    }) //...

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }
  myMethod(){
    this.tabs.color="blue";
    console.log("My Color"+this.tabs.color);
  }
  
  
}
