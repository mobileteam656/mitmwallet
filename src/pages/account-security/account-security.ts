import { Component } from '@angular/core';
import { NavController, Events, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { ChangePasswordPage } from '../change-password/change-password';
import { ResetPassword } from '../reset-password/reset-password';

@Component({
  selector: 'page-account-security',
  templateUrl: 'account-security.html',
  providers: [CreatePopoverPage]
})

export class AccountSecurityPage {

  textEng: any = ["Account Security", "Change Password", "Reset Password"];
  textMyan: any = ["အကောင့်လုံခြုံရေး", "လျှို့ဝှက်နံပါတ် ပြောင်းမည်", "လျှို့ဝှက်နံပါတ် မေ့သွားခြင်း"];
  textData: string[] = [];
  showFont: any;

  _obj: any;

  constructor(
    public storage: Storage,
    public events: Events,
    public navCtrl: NavController,
    public createPopover: CreatePopoverPage, public menuCtrl: MenuController,
  ) {
    this.showFont = 'uni';
    this.menuCtrl.swipeEnable(false);
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('userData').then((userData) => {
      this._obj = userData;
    });
    this.events.subscribe('userData', userData => {
      this._obj = userData;
    });
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  goPage(param) {
    if (param == '1') {
      this.navCtrl.push(ChangePasswordPage, {
        userData: this._obj
      });
    } else if (param == '2') {
      this.navCtrl.push(ResetPassword, {
        userData: this._obj
      });
    }
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

}
