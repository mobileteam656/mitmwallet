import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, Events, AlertController, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import { DatePipe } from '@angular/common';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';
import { UtilProvider } from '../../providers/util/util';
import { GlobalProvider } from '../../providers/global/global';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { WalletPage } from '../wallet/wallet';
import { Storage } from '@ionic/storage';
import { TopUpWalletPage } from '../top-up-wallet/top-up-wallet';
@Component({
  selector: 'page-mobile-account-summary',
  templateUrl: 'mobile-account-summary.html',
 
})
export class MobileAccountSummaryPage {
  textEng: any = ['Account Summary', 'No result Found!', 'Pull to refresh', 'Refreshing', 'Are you sure you want to exit', 'Yes', 'No'];
  textMyan: any = ['စာရင်းချုပ်', 'အချက်အလက်မရှိပါ', 'အချက်အလက်သစ်များရယူရန်အောက်သို့ဆွဲပါ', 'အချက်အလက်သစ်များရယူနေသည်', 'ထွက်ရန်သေချာပါသလား', 'သေချာသည်', 'မသေချာပါ'];
  textData: string[] = [];
  keyboardfont: any;
  language: any;
  showFont: any;
  loading: any;
  userData: any = {};
  allaccount: any;
  ipaddress: string;
  status: any;
  flag: string;
  useraccount: any;
  currentaccount: any = [];
  savingaccount: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public http: Http,
    public datePipe: DatePipe,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public sqlite: SQLite,
    public changefont: Changefont,
    public events: Events,
    public util: UtilProvider,
    public alertCtrl: AlertController,
    public global: GlobalProvider,
    private device: Device,
    public network: Network,
    public platform: Platform,
    private all: AllserviceProvider,
    private slimLoader: SlimLoadingBarService,

  ) {
    this.ipaddress = this.global.ipaddressApp;
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('font').then((font) => {
      this.keyboardfont = font;
    });

    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;
    });
    this.storage.get('loginData').then((result) => {
      this.userData = result;
      console.log('Mobile User: '+JSON.stringify(this.userData));
      this.getAccountSummary();
    });
  }
  changelanguage(lan) {
    this.language = lan;

    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }
  getAccountSummary() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    let parameter = { userID: this.userData.userID, sessionID: this.userData.sessionID };
    this.http.post(this.ipaddress + '/service002/getTransferAccountList', parameter).map(res => res.json()).subscribe(result => {
      if (result.code == "0000") {
        let tempArray = [];
        
        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;  
        }
        this.allaccount = result.dataList;
        console.log('Account Summary: '+JSON.stringify(this.allaccount));
        //this.slimLoader.complete();
      }
      else if (result.code == "0016") {
        let confirm = this.alertCtrl.create({
          title: 'Warning!',
          enableBackdropDismiss: false,
          message: result.desc,
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.storage.remove('userData');
                this.events.publish('login_success', false);
                this.events.publish('lastTimeLoginSuccess', '');
                this.navCtrl.setRoot(WalletPage, {
                });
                this.navCtrl.popToRoot();
              }
            }
          ],
          cssClass: 'warningAlert',
        })
        confirm.present();
        this.loading.dismiss();
        // this.slimLoader.complete();
      }
      else {
        this.status = 0;
        this.allaccount = [];
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
        // this.slimLoader.complete();
      }
    },
      error => {
        let code;
        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        // this.slimLoader.complete();
        this.loading.dismiss();
      });
  }
  
  goTopup(a) {
    this.navCtrl.push(TopUpWalletPage,
      {
        data: a
      });
  }
 

}
