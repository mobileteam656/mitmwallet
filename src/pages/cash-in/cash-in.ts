import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, Platform, Events, AlertController, ToastController, LoadingController, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Login } from '../login/login';
import { GlobalProvider } from '../../providers/global/global';
import { AgentPage } from '../agent/agent';
import { BankAccountPage } from '../bank-account/bank-account';
import { LocationPage } from '../location/location';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
  selector: 'page-cash-in',
  templateUrl: 'cash-in.html',
})
export class CashInPage {
  textMyan: any = [ "ငွေသွင်း", "ကိုယ်စားလှယ်", "ဘဏ်အကောင့်" ];
  textEng: any = [ "Cash In", "Agent", "Bank Account" ];
  textData: any = [
    { title: 'Agent', icon: 'ios-briefcase', status: 1, otpstatus: 5 },
    { title: 'BankAccount', icon: 'md-contacts', status: 2, otpstatus: 6 },  
  ];

  showFont: string[] = [];
  font: string = '';
  popover: any;
  modal: any;
  userdata: any;
  hardwareBackBtn: any = true;
  loading: any;
  ipaddress: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, public platform: Platform,
    public storage: Storage, public all: AllserviceProvider, public events: Events, public alertCtrl: AlertController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public http: Http,
    public global: GlobalProvider, private appCtrl: App,private firebase: FirebaseAnalytics) {
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      });
      this.storage.get('language').then((lan) => {
        this.changelanguage(lan);
      });
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
   
  }

  getNext(s) {
    if (s == 'a') {
      this.navCtrl.push(AgentPage, {
        data: s
      })
    }
    else if (s == 'b') {
      this.navCtrl.push(BankAccountPage, {
        data: s
      })
    }
  }  

  backButtonAction() {
    if (this.modal && this.modal.index === 0) {
      this.modal.dismiss();
    } else {    
      if (this.hardwareBackBtn) {
        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;
            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();

            }
          }]
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }

  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true    
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        // this.firebase.logEvent('log_out', { userID: this.userdata.userID})
        // .then((res: any) => { console.log(res); })
        // .catch((error: any) => console.error(error));
        if (data.code == "0000") {
          this.loading.dismiss();
          // this.firebase.logEvent('log_out', {log_out:'logout-user'});
          // this.storage.remove('phonenumber');
          this.storage.remove('username');
          this.storage.remove('userData');
          this.storage.remove('firstlogin');
          this.appCtrl.getRootNav().setRoot(Login);
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',           
            dismissOnPageChange: true,           
          });
          toast.present(toast);
          this.loading.dismiss();
        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',           
            dismissOnPageChange: true,         
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });
  }

}
