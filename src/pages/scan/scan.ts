import { Component } from '@angular/core';
import { Platform, NavController, ViewController, NavParams, MenuController, ModalController, LoadingController, PopoverController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';

import { GlobalProvider } from '../../providers/global/global';
import { DatePipe } from '@angular/common';
import { TransferConfirm } from '../transfer-confirm/transfer-confirm';
import { UtilProvider } from '../../providers/util/util';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { App } from 'ionic-angular';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Payment } from '../payment/payment';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { LocationPage } from '../location/location';
import { WalletPage } from '../wallet/wallet';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
@Component({
  selector: 'scan',
  templateUrl: 'scan.html',
  providers: [CreatePopoverPage],
})
export class ScanPage {
  textEng: any = ["Transfer to Agent", "Name", "Balance", "Mobile Number", "Institution", "Amount",
  "Remark", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "Scan QR",
  "Mobile Wallet", "Type", "Transfer To", "Reference", "Account Number",
  "Invalid Amount."];
textMyan: any = ["ငွေလွှဲရန်", "အမည်", "လက်ကျန်ငွေ", "ဖုန်းနံပါတ်", "အော်ပရေတာ", "ငွေပမာဏ",
  "မှတ်ချက်", "မှားယွင်းနေပါသည်", "Does not match! Please try again.", "Please insert name field!", "ငွေပေးချေ",
  "Mobile Wallet", "အမျိုးအစား", "ငွေလွှဲရန်", "အကြောင်းအရာ", "အကောင့်နံပါတ်",
  "ငွေပမာဏ ရိုက်ထည့်ပါ"];
textData: string[] = [];
type: any = { name: "mWallet", code: "mWallet" };
institute: any = { name: "", code: "" };
address: string = '';
userID: string = '';
balance: string = '';
beneficiaryID: string = '';
toInstitutionCode: string = '';
amount: any;
remark: string = '';
ipaddress: string = '';
font: string = '';
lan: any;
qrValue: any = [];
popover: any;
loading: any;
isChecked: boolean = false;
_obj = {
  userID: "", sessionID: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
  frominstituteCode: "", toInstitutionCode: "", reference: "", toAcc: "", amount: "",
  bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "",
  fromName: "", toName: "", "wType": "",
  field1: "", field2: ""
}
errormsg1: any;
errormsg2: any;
temp: any;
normalizePhone: any;
//contact : any;
accountList: any;
accNumber: string = '';
contact: any = { 'phone': '', 'name': '' };
keyboardfont: any;
public alertPresented: any;
isLoading: any;
txnType: any;
btnflag: boolean;
phone: any;
constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
  public menuCtrl: MenuController, public modalCtrl: ModalController, public http: Http,
  public datePipe: DatePipe, public loadingCtrl: LoadingController,
  public toastCtrl: ToastController, public sqlite: SQLite,
  public changefont: Changefont,
  public barcodeScanner: BarcodeScanner,
  private all: AllserviceProvider,
  public events: Events, public alertCtrl: AlertController,
  public popoverCtrl: PopoverController, public platform: Platform,
  public util: UtilProvider, public global: GlobalProvider,
  public viewCtrl: ViewController, private slimLoader: SlimLoadingBarService,private firebase: FirebaseAnalytics,
  public appCtrl: App, public createPopover: CreatePopoverPage,
  ) {
  this.storage.get('userData').then((userData) => {
    this._obj = userData;
    if (userData != null || userData != '') {
      this.balance = userData.balance;
    }
  });
 this.goScan();

  let institution = this.util.setInstitution();
  this._obj.frominstituteCode = institution.fromInstitutionCode;
  this._obj.toInstitutionCode = institution.toInstitutionCode;
  this._obj.toInstitutionName = institution.toInstitutionName;
  this.institute.name = this._obj.toInstitutionName;
  this.institute.code = this._obj.toInstitutionCode;

  this.storage.get('phonenumber').then((phonenumber) => {
    this._obj.userID = phonenumber;
  });

  this.storage.get('ipaddressWallet').then((ip) => {
    if (ip !== undefined && ip != null && ip !== "") {
      this.ipaddress = ip;
      //this.getAccountList();
    } else {
      this.ipaddress = this.global.ipaddressWallet;
      //this.getAccountList();
    }
  });

  
  console.log("phone", JSON.stringify(this.phone));

  this.storage.get('username').then((username) => {
    if (username !== undefined && username !== "") {
      this._obj.fromName = username;
    }
  });
  this.events.subscribe('username', username => {
    if (username !== undefined && username !== "") {
      this._obj.fromName = username;
    }
  })

  

  this.storage.get('font').then((font) => {
    if (font == "zg") {
      this.keyboardfont = 'zg';
    } else {
      this.keyboardfont = 'uni';
    }
  });
}

ionViewDidLoad() {

}

ionViewCanEnter() {

}

// checkWallet() {
//   let num = /^[0-9-\+]*$/;
//   let flag = false;

//   if (num.test(this.phone)) {
//     if (this.phone.indexOf("+") == 0 && (this.phone.length == 12 || this.phone.length == 13 || this.phone.length == 11)) {
//       //this.walletIDMsg = '';
//       flag = true;
//     }
//     else if (this.phone.indexOf("7") == 0 && this.phone.length == 9) {
//       //this.walletIDMsg = '';
//       this.phone = '+959' + this.phone;
//       flag = true;
//     }
//     else if (this.phone.indexOf("9") == 0 && this.phone.length == 9) {
//       //this.walletIDMsg = '';
//       this.phone = '+959' + this.phone;
//       flag = true;
//     }
//     else if (this.phone.indexOf("09") == 0 && (this.phone.length == 10 || this.phone.length == 11 || this.phone.length == 9)) {
//       //this.walletIDMsg = '';
//       this.phone = '+959' + this.phone.substring(2);
//       flag = true;
//     }
//     else if (this.phone.indexOf("+") != 0 && this.phone.indexOf("7") != 0 && this.phone.indexOf("9") != 0 && (this.phone.length == 8 || this.phone.length == 9 || this.phone.length == 7)) {
//       //this.walletIDMsg = '';
//       this.phone = '+959' + this.phone;
//       flag = true;
//     }
//     else if (this.phone.indexOf("959") == 0 && (this.phone.length == 11 || this.phone.length == 12 || this.phone.length == 10)) {
//       //this.walletIDMsg = '';
//       this.phone = '+959' + this.phone.substring(3);
//       flag = true;
//     }
//     else {
//       flag = false;
//       //this.walletIDMsg = this.textError[3];
//     }
//   }
//   console.log("phno==", JSON.stringify(this.phone))
//   if (flag) {
//     this.loading = this.loadingCtrl.create({
//       dismissOnPageChange: true
//     });
//     this.loading.present();
//     //this.walletIDMsg = '';
//     let param = { userID: this._obj.userID, sessionID: this._obj.sessionID, loginID: this.phone };
//     console.log("CheckWallet Data in WalletTopup is: " + JSON.stringify(param));

//     this.http.post(this.ipaddress + '/chatservice/checkPhoneNo', param).map(res => res.json()).subscribe(data => {
//       if (data.code == "0000") {
//         this.isChecked = true;
//         this._obj.toName = data.name;
//         this.loading.dismiss();
//       }
//       else if (data.code == "0016") {
//         //this.logoutAlert(data.desc);
//         this.loading.dismiss();
//       }
//       else {
//         //this.all.showAlert('Warning!', data.desc);
//         this.loading.dismiss();
//       }
//     },
//       error => {
//         // this.all.showAlert('Warning!', this.all.getErrorMessage(error));
//         this.loading.dismiss();
//       });
//   }
// }
goScan(){
  this.barcodeScanner.scan().then((barcodeData) => {
    this.firebase.logEvent('page_click', { userID: this._obj.userID,Message:"Scan"})
          .then((res: any) => { console.log(res); })
          .catch((error: any) => console.error(error));
    if (barcodeData.cancelled) {
      this.navCtrl.setRoot(WalletPage);
        return false;
    } 
    try {
        this.qrValue = this.all.getDecryptText(this.all.iv, this.all.salt, this.all.dm, barcodeData.text);
        this.qrValue = JSON.parse(this.qrValue)   
        this.navCtrl.setRoot(Payment, {
          data: this.qrValue
        });
    } catch (e) {
      this.toastInvalidQR();
      // this.crashlytics.addLog("Error while loading data");
      //   this.crashlytics.sendNonFatalCrash(e.message || e);
      //   this.crashlytics.addLog("This is sendCrash");
      //   this.crashlytics.sendCrash();
    }
  }, (err) => {
  });
}
toastInvalidQR() {
  let toast = this.toastCtrl.create({
    message: "Invalid QR Type.",
    duration: 3000,
    position: 'bottom',
    dismissOnPageChange: true,
  });

  toast.present(toast);
}


presentPopover(ev) {
  this.createPopover.presentPopover(ev);
}



getError(error) {
  /* ionic App error
   ............001) url link worng, not found method (404)
   ........... 002) server not response (500)
   ............003) cross fillter not open (403)
   ............004) server stop (-1)
   ............005) app lost connection (0)
   */
  let code;
  if (error.status == 404) {
    code = '001';
  }
  else if (error.status == 500) {
    code = '002';
  }
  else if (error.status == 403) {
    code = '003';
  }
  else if (error.status == -1) {
    code = '004';
  }
  else if (error.status == 0) {
    code = '005';
  }
  else if (error.status == 502) {
    code = '006';
  }
  else {
    code = '000';
  }
  let msg = "Can't connect right now. [" + code + "]";
  let toast = this.toastCtrl.create({
    message: msg,
    duration: 5000,
    position: 'bottom',
    dismissOnPageChange: true,
  });
  toast.present(toast);
  this.isLoading = false;
  ////console.log("Oops!");
}

}
