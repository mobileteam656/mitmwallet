


import { Component } from '@angular/core';
import { NavController, NavParams, Platform, LoadingController, ToastController, ActionSheetController } from 'ionic-angular';//IonicPage,
import { FunctProvider } from '../../providers/funct/funct';
//import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';


@Component({
  selector: 'page-view-photo-message',
  templateUrl: 'view-photo-message.html',
})

export class ViewPhotoMessagePage {
  passData: any;
  image: any;
  imageLink: any;
  senddate: any;
  sendtime: any;
  loading: any;
  contentImg: any = '';
  constructor(public navCtrl: NavController, public navParams: NavParams, public funct: FunctProvider,
    public platform: Platform, private transfer: FileTransfer, public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private file: File, public actionSheetCtrl: ActionSheetController, ) {
    //https://chatting.azurewebsites.net/upload/image/abc.jpg
    this.passData = this.navParams.get("data");
    this.contentImg = this.navParams.get("contentImg");
    //this.imageLink = this.funct.msgImglink + "upload/image/";

    if (this.contentImg != '' && this.contentImg != null && this.contentImg != undefined) {
      this.image = this.passData;
    }
    else {
      this.image = this.imageLink + this.passData.t3;
      this.senddate = this.funct.getTransformDate(this.passData.t7);
      this.sendtime = this.passData.t8;
      ////console.log("this.passData=" + JSON.stringify(this.passData));
      ////console.log("senddate=" + this.senddate);
      ////console.log("sendtime=" + this.sendtime);
    }
  }
  saveGallery(msg) {
    let actionSheet = this.actionSheetCtrl.create({
      // title: 'Languages',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Save to Gallery',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.DownloadIMG(msg);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            ////console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  DownloadIMG(image) {
    ////console.log("dowload image == " + JSON.stringify(image));
    this.loading = this.loadingCtrl.create({
      content: 'Saving...',
    });
    this.loading.present();
    ////console.log("this.file.externalRootDirectory == " + this.file.externalRootDirectory);
    this.file.checkDir(this.file.externalRootDirectory + 'DCIM/', 'DC').then((data) => {
      ////console.log('Directory exists');
      this.imageDownload(image);

    }, (error) => {
      ////console.log('Directory doesnt exist');
      this.file.createDir(this.file.externalRootDirectory + 'DCIM/', 'DC', false).then((res) => {
        ////console.log("file create success");
        this.imageDownload(image);
      }, (error) => {
        ////console.log("file create error");
      });
    });
  }

  imageDownload(image) {
    let url = image;
    let photo = image.substring(image.lastIndexOf('/') + 1, image.length);
    //let url = image;
    //const fileTransfer: TransferObject = this.transfer.create();
    const fileTransfer: FileTransferObject = this.transfer.create();
    var targetPath = this.file.externalRootDirectory + 'DCIM/DC/' + photo;
    fileTransfer.download(url, targetPath).then((entry) => {
      ////console.log('download complete: ' + entry.toURL());
      this.presentToast("Saving completed");
      this.loading.dismissAll();
      //this.viewFile(image);

    }, (error) => {
      this.presentToast("Download Fail!Check your file extension name.")
      ////console.log('download error: ' + JSON.stringify(error));
      this.loading.dismissAll();
    });
  }

  ionViewDidEnter() {
    this.backButtonExit();
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 5000,
      dismissOnPageChange: true,
      position: 'top'
    });
    toast.present();
  }

  backButtonExit() {
    // this.platform.registerBackButtonAction(() => {
    //   //console.log("Active Page=" + this.navCtrl.getActive().name);
    //   this.navCtrl.pop();
    // });
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ViewPhotoMessagePage');
  }

}
