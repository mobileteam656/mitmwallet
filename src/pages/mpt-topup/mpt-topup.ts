import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Select, ToastController, LoadingController, AlertController, Events, PopoverController, Platform, App } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { Contacts } from '@ionic-native/contacts';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { UtilProvider } from '../../providers/util/util';
import { Changefont } from '../changefont/changeFont';
import { Login } from '../login/login';
import { WalletPage } from '../wallet/wallet';
import { Storage } from '@ionic/storage';
import { MptTopupConfirmPage } from '../mpt-topup-confirm/mpt-topup-confirm';
@Component({
  selector: 'page-mpt-topup',
  templateUrl: 'mpt-topup.html',
})
export class MptTopupPage {

  @ViewChild('myselect') select: Select;
  passtemp: any;
  textEng: any = ["Mobile Number", "Choose Operator Type", "Phone Number", "Amount", "Description", "SUBMIT", "Choose Account No.", "Choose Operator Type", "Enter Phone No.", "Choose Amount", "Enter Amount", "Mobile Top Up", "RESET", "Account Balance", "Insufficient Balance", "Cash Back","MPT Topup"];
  textMyan: any = ["အကောင့်နံပါတ်", "အော်ပရေတာ အမျိုးအစား", "ဖုန်းနံပါတ်", "ငွေပမာဏ", "အကြောင်းအရာ", "ထည့်သွင်းမည်", "စာရင်းအမှတ်ရွေးချယ်ပါ", "Operator အမျိုးအစားရွေးချယ်ပါ", "ဖုန်းနံပါတ်ရိုက်သွင်းပါ", "ငွေပမာဏရွေးချယ်ပါ", "ငွေပမာဏရိုက်သွင်းပါ", "ဖုန်းငွေဖြည့်သွင်းခြင်း", "ပြန်စမည်", "ဘဏ်စာရင်းလက်ကျန်ငွေ", "ပေးချေမည့် ငွေပမာဏ မလုံလောက်ပါ", "ပြန်ရမည့်ငွေ","MPT Topup"];
  showFont: any = [];
  font: any = '';
  userData: any;
  useraccount: any = [];
  ipaddress: any;
  referenceType: any = [];
  referenceTypeBill: any = [];
  amountType: any;
  msgCnpAcc: string;
  msgCnpAmtOther: string;
  msgCnpPhNo: string;
  msgCnpRef: string;
  msgCnpamt: string;
  referencename: any;
  amt: any;
  activepage: any;
  moreContact: any = false;
  moreContactData: any = [];
  contactPh: any = true;
  primColor: any = { 'background-color': '#3B5998' };
  accountBal: any; cashBack: any;
  loading: any;
  popover: any;
  merchant: any;
  merchantID:any;
  merchantName:any;
  merchantMPT:any;
  alertPresented: any;
  amountBal: any;
  phonenumber: string = '';
  balance: string = '';
  flag1:any;
 
  topupData: any = [];
  _obj = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "", toInstitutionName: "",
    frominstituteCode: "", toInstitutionCode: "", reference: "", toAcc: "", amount: "",
    bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "",
    fromName: "", toName: "", "wType": "",accountNo:"",
    field1: "", field2: "",phone:"",name:"",amountType:"",activepage:"",value:"",amountother:"",narrative:""
  }
  
  constructor(public global: GlobalProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public barcodeScanner: BarcodeScanner,
    public storage: Storage,
    public http: Http,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public events: Events,
    public network: Network,
    private slimLoader: SlimLoadingBarService,
    public changeLanguage: ChangelanguageProvider,
    private contacts: Contacts,
    public all: AllserviceProvider,
    public util: UtilProvider,
    public appCtrl: App,
    public changefont: Changefont,
    public popoverCtrl: PopoverController,
    public platform: Platform, ) {
    //this._obj.processingCode = "200700";
    this.alertPresented = false;
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changelanguage(this.font)
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan.data)
    });
    this.storage.get('userData').then((userData) => {
      this._obj = userData;
      if (userData != null || userData != '') {
        this.balance = userData.balance;
      }
    });
    this.merchant = this.navParams.get('param');
    if (this.merchant != null && this.merchant != undefined && this.merchant != '') {
     
      this.merchantID = this.merchant.merchantID;
      this.merchantName = this.merchant.merchantName;
    }
    this.getreferenceData();
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;

      } else {
        this.ipaddress = this.global.ipaddress;
      }
    });
  }
  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }
  
  getreferenceData(){
    this.merchantMPT= 
    //  [{"name":"1000","t1":"20.00","value":"1"}];
    [{"lovList":[{"name":"1000","t1":"20.00","value":"1"}],"merchantID":"M00002","merchantName":"MPT","processingCode":"200800"}]
   console.log('Merchant Name: '+this.merchantMPT.merchantName);
    this.merchant= 
       [{"name":"1000","t1":"20.00","value":"1"},{"name":"3000","t1":"60.00","value":"3"},{"name":"5000","t1":"100.00","value":"5"},{"name":"10000","t1":"200.00","value":"10"},{"name":"30000","t1":"600.00","value":"30"}];
    

  }

  getContact() {
    this.contacts.pickContact().then((data) => {
      if (data.phoneNumbers.length > 1) {
        this.contactPh = false;
        this.moreContact = true;
        this.msgCnpPhNo = '';
        this.moreContactData = data.phoneNumbers;
        for (let i = 0; i < this.moreContactData.length; i++) {
          this.moreContactData[i].value = this.moreContactData[i].value.toString().replace(/ ?-?/g, "");
          if (this.moreContactData[i].value.indexOf("7") == 0 && this.moreContactData[i].value.length == "9") {
            this.moreContactData[i].value = '+959' + this.moreContactData[i].value;
          }
          else if (this.moreContactData[i].value.indexOf("9") == 0 && this.moreContactData[i].value.length == "9") {
            this.moreContactData[i].value = '+959' + this.moreContactData[i].value;
          }
          else if (this.moreContactData[i].value.indexOf("+") != 0 && this.moreContactData[i].value.indexOf("7") != 0 && this.moreContactData[i].value.indexOf("9") != 0 && (this.moreContactData[i].value.length == "8" || this.moreContactData[i].value.length == "9" || this.moreContactData[i].value.length == "7")) {
            this.moreContactData[i].value = '+959' + this.moreContactData[i].value;
          }
          else if (this.moreContactData[i].value.indexOf("09") == 0 && (this.moreContactData[i].value.length == 10 || this.moreContactData[i].value.length == 11 || this.moreContactData[i].value.length == 9)) {
            this.moreContactData[i].value = '+959' + this.moreContactData[i].value.substring(2);
          }
          else if (this.moreContactData[i].value.indexOf("959") == 0 && (this.moreContactData[i].value.length == 11 || this.moreContactData[i].value.length == 12 || this.moreContactData[i].value.length == 10)) {
            this.moreContactData[i].value = '+959' + this.moreContactData[i].value.substring(3);
          }
        }
      } else {
        this.contactPh = true;
        this.moreContact = false;
        this._obj.phone = data.phoneNumbers[0].value;
      }
    });
  }
  referenceChange(data) {
    this.referenceTypeBill = data.lovList;
  }

  operatorChange(operatorData) {
    this.msgCnpRef = '';
    this._obj.name = '';
    this.referenceChange(operatorData);
    this.activepage = operatorData;
    this.cashBack = '';
  }

  checkActive(page) {
    return page == this.activepage;
  }
  getamount(data) {
    this.msgCnpAmtOther = '';
    this.msgCnpamt = '';
    this._obj.amountType = data.value;
    this._obj.amount = data.name;
    this._obj.bankCharges = data.bankCharges;
    this.cashBack = data.t1;
  }
 

 

  onChangeSPh(s, i) {
    if (i == "09") {
      this._obj.phone = "+959";
    }
    else if (i == "959") {
      this._obj.phone = "+959";
    }
    this.msgCnpPhNo = "";
  }

  ionViewCanLeave() {
    this.select.close();
    this.platform.registerBackButtonAction(() => {
      this.select.close();
      this.navCtrl.pop();
    });
  }

  reset() {
    this._obj.phone = '';
    this._obj.activepage = "";
    this._obj.value = "";
    this._obj.amountother = ""
    this._obj.name = "";
    this._obj.narrative = "";
    this._obj.amount = "";
    this._obj.amountType = "";
    this.accountBal = "";
    this.amountBal = "";
    this.amountType = "";
    this.msgCnpAcc = "";
    this.msgCnpAmtOther = "";
    this.msgCnpPhNo = "";
    this.msgCnpRef = "";
    this.msgCnpamt = "";
    this.moreContact = false;
    this.contactPh = true;
    this.operatorChange("");
    this.referenceTypeBill = [];
    this.cashBack = '';
  }

  onClick() {
    if (this.referenceTypeBill.length == 0) {
      this.all.showAlert("Warning!", "Please select operator.");
    }
  }

clicksubmit(){
  this._obj.beneficiaryID=this.merchantID;
  this._obj.toName=this.merchantName
  console.log("MPT Topup is:  "+ this._obj); 
  this.navCtrl.push(MptTopupConfirmPage, {  //tzl
    data: this._obj,
  });
  
   
}
  changeAcc(s, account) {
    if (s == 1) {
      this.msgCnpAcc = '';
    }
    else if (s == 2) {
      this.msgCnpPhNo = "";
    }
    for (let i = 0; i < this.useraccount.length; i++) {
      if (account == this.useraccount[i].depositAcc) {
        this.amountBal = this.useraccount[i].avlBal;
        this.accountBal = this.useraccount[i].avlBal + " " + this.useraccount[i].ccy;
      }
    }
  }

  inputChange(i) {
    if (i == 3) {
      this.msgCnpAmtOther = '';
    }
  }
  
  backButton() {
    this.navCtrl.setRoot(WalletPage);
  }
}
