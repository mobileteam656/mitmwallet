import { Component } from '@angular/core';
import { NavController, MenuController, NavParams, LoadingController, PopoverController, Events, Platform } from 'ionic-angular';
import { LocationDetail } from '../location-detail/location-detail';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
// import { DefaultPopOver } from '../default-pop-over/default-pop-over';
// import { LanguagePopOver } from '../language-pop-over/language-pop-over';
// import { IpchangePage } from '../ipchange/ipchange';
import { GlobalProvider } from '../../providers/global/global';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
 import { AllserviceProvider } from '../../providers/allservice/allservice';
// import { PopoverPage } from '../popover-page/popover-page';
import { CreatePopoverPage } from '../create-popover/create-popover';
import {Login} from '../login/login';
import { NearByPage } from '../near-by/near-by';
import { WalletPage } from '../wallet/wallet';
import { TabsPage } from '../tabs/tabs';
import { AgentTransferPage } from '../agent-transfer/agent-transfer';
  
declare var window;

@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
  providers: [ChangelanguageProvider,CreatePopoverPage]
})

export class MapPage {
  mapLocation: any = [];
  location: any;
  alllocation: any=[];
  atmlocation: any = [];
  branchlocation: any = [];
  hasData: boolean = false;
  allocationMessage: string;
  status: any;
  public loading;
  ipaddress: string;
  popover: any;
  font: string = '';
  showFont: any = [];
  textEng: any = ["Location", "Branch", "No Result Found"];
  textMyan: any = ["တည်နေရာ", "ဘဏ်ခွဲများ", "အချက်အလက်မရှိပါ"];
  ststus:any;
  userdata:any;
  locationType: any;
  constructor(public navCtrl: NavController, public menu: MenuController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public storage: Storage,
    private slimLoader: SlimLoadingBarService, public popoverCtrl: PopoverController, public events: Events, public platform: Platform, public changeLanguage: ChangelanguageProvider,
    public global: GlobalProvider, public all: AllserviceProvider,public createPopover: CreatePopoverPage, public menuCtrl: MenuController) {
    // this.menuCtrl.swipeEnable(false);
    this.locationType = this.navParams.get("location");
    console.log("loc param==" + JSON.stringify(this.location));
    //this.location = 'atm';
    this.ststus=this.navParams.get("ststus");
    console.log("loc status==" + JSON.stringify(this.ststus));
   
    if(this.ststus=='1'){
      this.storage.get('userData').then((val) => {
        this.userdata = val;
        // this.ipaddress = this.global.ipaddress;
        // this.getLocation();   
        this.storage.get('ipaddress').then((ip) => {
          if (ip !== undefined && ip !== "" && ip != null) {
            this.ipaddress = ip;
          } else {
            this.ipaddress = this.global.ipaddress;
          }
          this.getLocation();
        });     
      });
    }
    
  }

  getLocation() {
    this.slimLoader.start(() => {
    });
    let param = { userID: '', sessionID: '' };
    this.http.post(this.ipaddress + '/service002/getLocation', param).map(res => res.json()).subscribe(data => {
      
      
      if (data.code == "0000") {
        let tempArray = [];
        if (!Array.isArray(data.data)) {
          tempArray.push(data.data);
          data.data = tempArray;
          //this.alllocation.data = data.data;
        }
        this.alllocation.data = data.data;
        console.log("location==" , JSON.stringify(this.alllocation.data));
        this.status = 1;
        if (this.alllocation.data.length > 0) {
          for(let i=0;i<this.alllocation.data.length ;i++){
            let tempListArray = [];
            if (!Array.isArray(this.alllocation.data[i].dataList)) {
              tempListArray.push(this.alllocation.data[i].dataList);
              this.alllocation.data[i].dataList = tempListArray;
            }
          }

          for (let i = 0; i < this.alllocation.data.length; i++) {
            for (let j = 0; j < this.alllocation.data[i].dataList.length; j++) {
              this.mapLocation.push({ "title": this.alllocation.data[i].dataList[j].name, "address": this.alllocation.data[i].dataList[j].address, "latitude": this.alllocation.data[i].dataList[j].latitude, "longitude": this.alllocation.data[i].dataList[j].longitude, "type":this.alllocation.data[i].dataList[j].locationType});
            }
          }          
          console.log("location==" , JSON.stringify(this.mapLocation));

          this.slimLoader.complete();
          this.hasData = true;
         this.location=this.alllocation.data[3].locationType;
         console.log("location type==" , JSON.stringify(this.location));
        
        } else {
          this.slimLoader.complete();
          this.status = 0;
        }
      } else {
        this.status = 0;
        this.all.showAlert('Warning!', data.desc);
        this.slimLoader.complete();
      }
    },
      error => {
        this.all.showAlert('Warning!', this.all.getErrorMessage(error));
        this.slimLoader.complete();
      });
  }

  ionViewDidLoad() {
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddress;
      }
      this.getLocation();
    });     
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
    window.location = "tel:" + passedNumber;
  }

  getloaction(data) {
    var parm = {
      lat: data.latitude,
      lng: data.longitude,
      name: data.name,
      phone: data.phone1,
      address: data.address
    }
    this.navCtrl.push(LocationDetail, { data: parm })
  }

  goAgent(data) {    
    this.navCtrl.push(AgentTransferPage, { phone: data.phone1 });
  }

  ionViewDidLeave() {
    this.slimLoader.reset();    
  }
  
  setLocation(type){
    this.location=type;
  }
  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  nearby(){
    this.navCtrl.push(NearByPage);
  } 
 
  // backButton()
  // {
  //   this.storage.get('firstlogin').then((firstTime) => {
  //     if (firstTime) {
  //       this.storage.get('phonenumber').then((phone) => {
  //         if (phone != undefined && phone != null && phone != '') {
  //           //this.navCtrl.setRoot(WalletPage);
  //            let params = { tabIndex: 0, tabTitle: 'Wallet' };
  //            this.navCtrl.setRoot(TabsPage, params);
  //         } else {
  //           this.navCtrl.setRoot(Login);

  //         }
  //       });
  //     } else {
  //       this.navCtrl.setRoot(Login);

  //     }
  //   });
  // }
}
