import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import 'rxjs/Rx';
import { Login } from '../login/login';
import { App } from 'ionic-angular';

@Component({
  selector: 'page-account-transaction-detail',
  templateUrl: 'account-transaction-detail.html',
  providers: [ChangelanguageProvider]
})
export class AccountTransactionDetail {
  idleState = 'Not started.';
  timedOut = false;
  refflag=true;
  lastPing: Date = null;
  min: any;
  sec; any;
  displayArray:any[];
  showFont: string[] = [];
  textEng: any = ['Details', 'Account Number', 'Transaction Number', 'Transaction Date', 'Amount', 'Description','Reference','Tansaction Type','Transfer To',""];
  textMyan: any = ['အသေးစိတ်အချက်အလက်', 'စာရင်းနံပါတ်', 'အမှတ်စဉ်', 'လုပ်ဆောင်ခဲ့သည့်ရက်', 'ငွေပမာဏ', 'မှတ်ချက်', 'အကြောင်းအရာ','လုပ်ဆောင်မှုမှတ်တမ်း','ငွေလွှဲမည်သို.'];
  font: string;
  transdate:Date;
  public loading;
  userdata: any;
  resultdata= {
    "accRef": "", "codTxnCurr": "", "nbrAccount": "","state":"","subRef":"",
    "txnAmount": "", "txnDate": "", "txnDateTime": "", "txnTypeDesc": "", "txtReferenceNo": "", "remark":""
   
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public storage: Storage,
    public loadingCtrl: LoadingController,public appCtrl: App, public toastCtrl: ToastController, public http: Http, public changeLanguage: ChangelanguageProvider) {
    this.resultdata = navParams.get("data");
    console.log('Detail Transaction: '+JSON.stringify(this.resultdata));
    this.displayArray=this.resultdata.txnTypeDesc.split(',');
    if(this.displayArray[1]=='Skynet Payment'){
      this.refflag=false;
      console.log('Description : '+this.refflag);
      console.log('Data is :'+this.displayArray[0]);
    }
    else{
      this.refflag=true;
    }

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('userData').then((val) => {
      this.userdata = val;
    });
  }
  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }
  gologout() {
    // this.storage.remove('phonenumber');
    this.storage.remove('username');
    this.storage.remove('userData');
    this.appCtrl.getRootNav().setRoot(Login);
  }

}
