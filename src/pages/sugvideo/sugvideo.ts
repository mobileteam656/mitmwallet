import { Component, ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';
import { IonicPage, NavController, NavParams, AlertController, PopoverController, ViewController, Platform, ToastController, Events, LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { SQLite } from '@ionic-native/sqlite';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { FunctProvider } from '../../providers/funct/funct';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';

//import {trigger,state,style,animate,transition} from '@angular/animations';
//import { HomeDetailPage } from '../home-detail/home-detail';
import { CommentPage } from '../comment/comment';
import { GlobalProvider } from '../../providers/global/global';
import { Slides } from 'ionic-angular';

@Component({
  selector: 'page-sugvideo',
  templateUrl: 'sugvideo.html',
})
export class SugvideoPage {
  @ViewChild(Content) content: Content;
  @ViewChild(Slides) slides: Slides;

  currentPlayingVideo: HTMLVideoElement;

  rootNavCtrl: NavController;
  pageStatus: any;
  registerData: any;
  popover: any;
  start: any = 0;
  end: any = 0;
  nores: any;
  isLoading: any;
  url: any = '';
  menuList: any = [];
  font: any;
  textMyan: any = ["ဗီဒီယို", "ကြိုက်တယ်", "မှတ်ချက်", "ဝေမျှမည်", "ဒေတာ မရှိပါ။", "ကြည့်ရန်", "ခု"];
  textEng: any = ["Video", "Like", "Comment", "Share", "No result found", "View", ""];
  textData: any = [];
  videoLink: SafeResourceUrl;
  photoLink: any;
  noticount: any;
  db: any;
  videoData: any = [];
  vData: any = [];
  vid: any;
  img: any = '';
  ipaddress: any;
  imglink: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, public platform: Platform,
    public storage: Storage,
    public toastCtrl: ToastController,
    public http: Http, public alert: AlertController,
    public funct: FunctProvider,
    public changeLanguage: ChangelanguageProvider,
    public events: Events,
    public sanitizer: DomSanitizer, public sqlite: SQLite,
    public global: GlobalProvider, ) {

    this.vid = document.getElementById("myPlayer");

    this.storage.get('language').then((font) => {
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then((data) => {
        this.textData = data;
        if (font != "zg")
          this.font = 'uni';
        else
          this.font = font;
        ////console.log("data language=" + JSON.stringify(data));
      });
    });

    this.events.subscribe('changelanguage', language => {
      ////console.log("change language = " + language);
      this.changeLanguage.changelanguage(language, this.textEng, this.textMyan).then((data) => {
        this.textData = data;
        /* if(language != "zg")
          this.font = 'uni';
        else
          this.font = language; */
        this.font = 'uni';
        for (let i = 0; i < this.menuList.length; i++) {
          /* this.changeLanguage.changelanguageText(this.font,this.menuList[i].t2).then((res) => {
            this.menuList[i].t2 = res;
            //console.log("video link" + this.menuList[i].t8);
          }) */
        }
        ////console.log("data language=" + JSON.stringify(data));
      });
    })

    this.storage.get('ipaddressCMS').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressCMS;
      }
    });

    this.storage.get('imglink').then((imglink) => {
      if (imglink != '' || imglink != undefined || imglink != null) {
        this.imglink = imglink;
      } else {
        this.imglink = this.global.digitalMedia;
      }
    });
  }

  ionViewDidLoad() {
    this.storage.get('appData').then((data) => {
      this.registerData = data;
      ////console.log("registerData = " + JSON.stringify(this.registerData));
      this.start = 0;
      this.end = 0;
      this.menuList = [];
      this.getList(this.start, '');
    });
  }

  getList(start, infiniteScroll) {
    this.isLoading = true;
    this.end = this.end + 10;
    let parameter = {
      start: start,
      end: this.end,
      size: 10
    }
   // //console.log("request video = ", JSON.stringify(parameter));
    this.http.post(this.ipaddress + '/serviceVideo/searchVideo?searchVal=&userSK=' + this.registerData.usersyskey + '&mobile=' + this.registerData.t1 + '&firstRefresh=1', parameter).map(res => res.json()).subscribe(data => {
     // //console.log("response video = ", JSON.stringify(data));
      if (data.data.length > 0) {
        this.nores = 1;
        for (let i = 0; i < data.data.length; i++) {

          let temp = data.data[i].t8;     // for video link
          let str1 = temp.search("external/");
          let str2 = temp.search(".sd");
          let res = temp.substring(str1 + 9, str2);
          data.data[i].videoLink = "https://i.vimeocdn.com/video/" + res + "_295x166.jpg";
         // //console.log("data.data[i].videoLink == " + data.data[i].videoLink);

          data.data[i].modifiedDate = this.funct.getTransformDate(data.data[i].modifiedDate);
          if (data.data[i].n6 != 1)
            data.data[i].showLike = false;
          else
            data.data[i].showLike = true;
          if (data.data[i].n7 != 1)
            data.data[i].showContent = false;
          else
            data.data[i].showContent = true;
          if (data.data[i].n2 != 0) {
            //data.data[i].likeCount = this.funct.getChangeCount(data.data[i].n2);
            data.data[i].likeCount = data.data[i].n2;
          }
          if (data.data[i].n3 != 0) {
            //data.data[i].commentCount = this.funct.getChangeCount(data.data[i].n3);
            data.data[i].commentCount = data.data[i].n3;
          }
          if (data.data[i].t2.replace(/<\/?[^>]+(>|$)/g, "").length > 200) {
            data.data[i].showread = true;
          }
          else
            data.data[i].showread = false;
          /* this.changeLanguage.changelanguageText(this.font,data.data[i].t1).then((res) => {
            data.data[i].t1 = res;
          });
          this.changeLanguage.changelanguageText(this.font,data.data[i].t2).then((res) => {
            data.data[i].t2 = res;
          }) */
          this.menuList.push(data.data[i]);

        }
      }
      else {
        if (this.end > 10) {
          this.end = this.start - 1;
          if (infiniteScroll != '') {
            infiniteScroll.complete();
          }
        }
      }
      if (this.menuList.length > 0) {
        this.nores = 1;
      }
      else {
        this.isLoading = false;
        this.nores = 2;
      }
    }, error => {
      ////console.log("signin error=" + error.status);
      this.nores = 0;
      this.getError(error);
    });
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    toast.present(toast);
    this.isLoading = false;
    ////console.log("Oops!");
  }

  clickLike(data, k) {
    ////console.log("data=" + JSON.stringify(data));
    if (!data.showLike) {
      this.menuList[k].showLike = true;
      this.menuList[k].n2 = this.menuList[k].n2 + 1;
      //this.menuList[k].likeCount = this.funct.getChangeCount(this.menuList[k].n2);
      this.menuList[k].likeCount = this.menuList[k].n2;
      this.getLike(data, k);
    }
    else {
      this.menuList[k].showLike = false;
      this.menuList[k].n2 = this.menuList[k].n2 - 1;
      //this.menuList[k].likeCount = this.funct.getChangeCount(this.menuList[k].n2);
      this.menuList[k].likeCount = this.menuList[k].n2;
      this.getUnlike(data, k);
    }
  }

  getLike(data, k) {
    ////console.log("K " + k);
    let parameter = {
      key: data.syskey,
      userSK: this.registerData.usersyskey,
      type: 'interview'
    }
    ////console.log("request clickLike = ", JSON.stringify(parameter));
    this.http.get(this.ipaddress + '/serviceArticle/clickLikeArticle?key=' + data.syskey + '&userSK=' + this.registerData.usersyskey + '&type=' + data.t3).map(res => res.json()).subscribe(data => {
      ////console.log("response clickLike = ", JSON.stringify(data));
      if (data.state) {
        this.menuList[k].showLike = true;
      }
      else {
        this.menuList[k].showLike = false;
        this.menuList[k].n2 = this.menuList[k].n2 - 1;
      }
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  getUnlike(data, k) {
    let parameter = {
      key: data.syskey,
      userSK: this.registerData.usersyskey,
      type: 'interview'
    }
    ////console.log("request clickLUnlike = ", JSON.stringify(parameter));
    this.http.get(this.ipaddress + '/serviceArticle/clickUnlikeArticle?key=' + data.syskey + '&userSK=' + this.registerData.usersyskey + '&type=' + data.t3).map(res => res.json()).subscribe(data => {
      ////console.log("response clickLUnlike = ", JSON.stringify(data));
      if (data.state) {
        this.menuList[k].showLike = false;
      }
      else {
        this.menuList[k].showLike = true;
        this.menuList[k].n2 = this.menuList[k].n2 + 1;
      }
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  continue(data) {
    ////console.log("this is sag video page continue data" + JSON.stringify(data));
    this.navCtrl.push(CommentPage, {
      data: data,
      title: "detail"
    });
  }

  share(i) {
  //   //console.log("this is share url == " + JSON.stringify(i) + "  //////////////  url === " + JSON.stringify(i.t8));


  //   let sahareImg = this.imglink + "upload/image/mit_logo.png";
  //   if (i.uploadedPhoto.length > 0) {
  //     sahareImg = i.uploadedPhoto[0].t7;
  //   }


  //   const Branch = window['Branch'];
  //   //  this.url = "https://b2b101.app.link/R87NzKABHL";

  //   var propertiesObj = {
  //     canonicalIdentifier: 'content/123',
  //     canonicalUrl: 'https://example.com/content/123',
  //     title: i.t1,
  //     contentDescription: '' + Date.now(),
  //     contentImageUrl: sahareImg,
  //     price: 12.12,
  //     currency: 'GBD',
  //     contentIndexingMode: 'private',
  //     contentMetadata: {
  //       custom: 'data',
  //       testing: i.syskey,
  //       this_is: true
  //     }
  //   }

  //   // create a branchUniversalObj variable to reference with other Branch methods
  //   var branchUniversalObj = null
  //   Branch.createBranchUniversalObject(propertiesObj).then(function (res) {
  //     branchUniversalObj = res
  //     //console.log('Response1: ' + JSON.stringify(res))
  //     // optional fields
  //     var analytics = {
  //       channel: 'facebook',
  //       feature: 'onboarding',
  //       campaign: 'content 123 launch',
  //       stage: 'new user',
  //       tags: ['one', 'two', 'three']
  //     }

  //     // optional fields
  //     var properties1 = {
  //       $desktop_url: 'http://www.example.com/desktop',
  //       $android_url: 'http://www.example.com/android',
  //       $ios_url: 'http://www.example.com/ios',
  //       $ipad_url: 'http://www.example.com/ipad',
  //       $deeplink_path: 'content/123',
  //       $match_duration: 2000,
  //       custom_string: i.syskey,
  //       custom_integer: Date.now(),
  //       custom_boolean: true
  //     }

  //     branchUniversalObj.generateShortUrl(analytics, properties1).then(function (res) {

  //       //console.log('Response2: ' + JSON.stringify(res.url));
  //       // optional fields
  //       var analytics = {
  //         channel: 'facebook',
  //         feature: 'onboarding',
  //         campaign: 'content 123 launch',
  //         stage: 'new user',
  //         tags: ['one', 'two', 'three']
  //       }

  //       // optional fields
  //       var properties = {
  //         $desktop_url: 'http://www.example.com/desktop',
  //         custom_string: i.syskey,
  //         custom_integer: Date.now(),
  //         custom_boolean: true
  //       }

  //       var message = 'Check out this link'

  //       // optional listeners (must be called before showShareSheet)
  //       branchUniversalObj.onShareSheetLaunched(function (res) {
  //         // android only
  //         //console.log(res)
  //       })
  //       branchUniversalObj.onShareSheetDismissed(function (res) {
  //         //console.log(res)
  //       })
  //       branchUniversalObj.onLinkShareResponse(function (res) {
  //         //console.log(res)
  //       })
  //       branchUniversalObj.onChannelSelected(function (res) {
  //         // android only
  //         //console.log(res)
  //       })

  //       // share sheet
  //       branchUniversalObj.showShareSheet(analytics, properties, message)
  //     }).catch(function (err) {
  //       console.error('Error2: ' + JSON.stringify(err))
  //     })
  //   }).catch(function (err) {
  //     console.error('Error1: ' + JSON.stringify(err))
  //   })
  }

  clickBookMark(data, k) {
    ////console.log("data=" + JSON.stringify(data));
    if (!data.showContent) {
      this.menuList[k].showContent = true;
      this.saveContent(data, k);
    }
    else {
      this.menuList[k].showContent = false;
      this.unsaveContent(data);
    }
  }

  saveContent(data, k) {
    let parameter = {
      t1: data.t3,
      t4: this.registerData.t3,
      n1: this.registerData.usersyskey,
      n2: data.syskey,
      n3: 1

    }
    ////console.log("request saveContent parameter= ", JSON.stringify(parameter));
    this.http.post(this.ipaddress + '/serviceContent/saveContent', parameter).map(res => res.json()).subscribe(data => {
     // //console.log("response saveContent = ", JSON.stringify(data));
      // this.isLoading = false;
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  unsaveContent(data) {
    ////console.log("request unsaveContent = ", JSON.stringify(data));
    let parameter = {
      t1: data.t3,
      t4: this.registerData.t3,
      n1: this.registerData.usersyskey,
      n2: data.syskey,
      n3: 0

    }
   // //console.log("request unsaveContent parameter = ", JSON.stringify(parameter));
    this.http.post(this.ipaddress + '/serviceContent/unsaveContent', parameter).map(res => res.json()).subscribe(data => {
      ////console.log("response unsaveContent = ", JSON.stringify(data));

      //this.isLoading = false;
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  slideChanged(e) {
    ////console.log("ch slid EVENT ");
    if (e.target !== this.currentPlayingVideo)
      this.currentPlayingVideo.pause();

  }

  onPlayingVideo(event) {
    event.preventDefault();
    if (this.currentPlayingVideo === undefined) {
      ////console.log("L");
      this.currentPlayingVideo = event.target;
      this.currentPlayingVideo.play();
    } else {
      ////console.log("A");
      if (event.target !== this.currentPlayingVideo) {
        this.currentPlayingVideo.pause();
        this.currentPlayingVideo = event.target;
        this.currentPlayingVideo.play();
      }
    }
  }
}
