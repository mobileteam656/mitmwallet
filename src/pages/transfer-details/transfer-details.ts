import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, ModalController, LoadingController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';
import { UtilProvider } from '../../providers/util/util';
import { DatePipe } from '@angular/common';
import { GlobalProvider } from '../../providers/global/global';
import { App } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
  selector: 'transfer-details',
  templateUrl: 'transfer-details.html',
})
export class TransferDetails {

  // textEng: any = ["Transfer Success", "Payee", "Balance", "Beneficiary", "Institution Code", "Amount", "Transaction Date", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "Close", "Transaction No.", "Transferred Successfully","Name","MMK"];
  // textMyan: any = ["ငွေလွှဲခြင်း ရလဒ်", "Payee", "Balance", "Beneficiary", "Institution Code", "ငွေပမာဏ", "လုပ်ဆောင်ခဲ့သည့် ရက်", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "ပိတ်မည်", "အမှတ်စဉ်", "ငွေလွှဲခြင်း အောင်မြင်ပါသည်","အမည်","MMK"];
  textEng: any = ["Transfer Success", "Payee", "Balance", "Beneficiary", "Institution Code", "Amount", "Date", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "Close", "Transaction No.", "Transferred Successfully","Transfer To","MMK","Time","Notes","Success"];
  textMyan: any = ["ငွေလွှဲခြင်း ရလဒ်", "Payee", "Balance", "Beneficiary", "Institution Code", "ငွေပမာဏ", "လုပ်ဆောင်ခဲ့သည့် ရက်", "Invalid. Please try again!", "Does not match! Please try again.", "Please insert name field!", "ပိတ်မည်", "အမှတ်စဉ်", "ငွေလွှဲခြင်း အောင်မြင်ပါသည်","အမည်","ကျပ်","အချိန်","အကြောင်းအရာ","လုပ်ဆောင်မှုအောင်မြင်ပါသည်"];
  textData: string[] = []; 

  address: string = '';
  lat: string = '';
  long: string = '';
  userID: string = '';
  balance: string = '';
  beneficiaryID: string = '';
  toInstitutionCode: string = '';
  amount: any;
  remark: string = '';
  ipaddress: string = '';
  transactionDate:string='';
  submitted = false;
  isenabled: boolean = false;
  ischecked: boolean = false;
  font: string = '';
  public loading;
  lan: any;
  regdata: any;
  _obj: any = {
    "userID": "", "sessionID": "", "payeeID": "", "beneficiaryID": "",
    "fromInstitutionCode": "", "toInstitutionCode": "", "toAcc": "", "amount": "", "bankCharges": "", "commissionCharges": "", "remark": "", "transferType": "2", "sKey": "", "field1": "", "field2": ""
  }
  name: any;
  passData: any;
  resData: any;
  errormsg: any;
  errormsg1: any;
  errormsg3: any;
  messNrc: any;
  ammount:any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public http: Http,
    public datePipe: DatePipe,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public sqlite: SQLite,
    public changefont: Changefont,
    public global: GlobalProvider,
    public events: Events,
    public alertCtrl: AlertController,
    public util: UtilProvider,
    public appCtrl: App,
    private firebase: FirebaseAnalytics) {
    this._obj = this.navParams.get('data');
   this.transactionDate=this._obj.transDate.substring(0, 4) + "-" + this._obj.transDate.substring(4, 6) + "-" + this._obj.transDate.substring(6, 8);
    console.log('Transaction Date'+this.transactionDate);
    this.amount = this.navParams.get('data1');
    this.name = this.navParams.get('data2');    
    this.ammount = this.util.formatAmount(this.amount);
    
    this.storage.get('ipaddressWallet').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressWallet;
      }
    });

    this.storage.get('scanlocation').then((scanlocation) => {
      ////console.log('Scan location is', scanlocation);
      if (scanlocation !== undefined && scanlocation !== "") {
        this.address = scanlocation;
      }
    });
    this.storage.get('lat').then((lat) => {
      ////console.log('lat is', lat);
      if (lat !== undefined && lat !== "") {
        this.lat = lat;
      }
    });
    this.storage.get('long').then((long) => {
      ////console.log('long ', long);
      if (long !== undefined && long !== "") {
        this.long = long;
      }
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    }) //...

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
      ////console.log("state data=" + JSON.stringify(this.textData));
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }

    } 
  }

  goBackTwo() {
    let params = { tabIndex: 0, tabTitle: 'Wallet' };
    this.navCtrl.setRoot(TabsPage, params);
  }

  ionViewDidLoad() {
   
  }

}
