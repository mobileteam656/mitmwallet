import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { File } from '@ionic-native/file';
import { PicsDetailsPage } from '../pics-details/pics-details'
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { AlertController, Events, LoadingController, Platform, PopoverController, ToastController } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Changefont } from '../changefont/changeFont';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { PicsDetailsChannelPage } from '../pics-details-channel/pics-details-channel';
import { Chat } from '../chat/chat';

@Component({
  selector: 'page-pics',
  templateUrl: 'pics.html',
  providers: [Changefont]
})
export class PicsPage {

  textEng: any = ["Pics", 'No Picture Found', "Name", "Date"];
  textMyan: any = ["Pics", 'အချက်အလက်မရှိပါ', "နာမည်", "နေ့စွဲ"];
  textData: string[] = [];
  font: string = '';

  photoList_mydocs: any;
  photo_mydocs: any = { id: '', PhotoPath: '', name: '', latitude: '', longitude: '', location: '', CreateDate: '', CreateTime: '' };
  nores_mydocs: any;
  latitude_mydocs: any;
  longitude_mydocs: any;
  location_mydocs: any;
  docData_mydocs: any;
  photoName_mydocs: any;
  imageDetail_Mydocs: any;
  from_Mydocs: any = { fromName: '' };
  description_Mydocs: any = { description: '' };
  passData: any = {};
  db: any;
  constructor(public navCtrl: NavController, public changefont: Changefont,
    public navParams: NavParams, public file: File,
    public storage: Storage, public events: Events,
    private camera: Camera, private geolocation: Geolocation, public sqlite: SQLite,
    private nativeGeocoder: NativeGeocoder, public global: GlobalProvider,
    public toastCtrl: ToastController) {
    /*this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
     // //console.log("success create or open database");

      db.executeSql('CREATE TABLE IF NOT EXISTS DOCPHOTO(id INTEGER PRIMARY KEY AUTOINCREMENT,PhotoPath TEXT,Name TEXT,Latitude TEXT,Longitude TEXT,Location TEXT,CreateDate TEXT,CreateTime TEXT)', {}).then(() => {
      //  //console.log('Executed SQL');

        this.selectData();
      }).catch(e => //console.log(e));
    }).catch(e => //console.log(e));*/

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  imageInfo(photo) {
    ////console.log("from mydocslist page:" + photo);
    //let image = this.photoPath + "/" + photo;
    this.navCtrl.push(PicsDetailsPage, {
      detailData: photo
    });
  }

  goCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      //allowEdit: true,
      targetWidth: 600,
      targetHeight: 600,
    }

    this.camera.getPicture(options).then((imagePath) => {
      ////console.log('Camera success' + imagePath);
      let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);     
      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    }, (error) => {
      ////console.log(JSON.stringify(error));      
    });
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    ////console.log("namePaht == " + namePath + "   //// currentNmae == " + currentName + "   ////  newFileName == " + newFileName);
    ////console.log("this.file.datadirectory == " + this.file.dataDirectory);

    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.docData_mydocs = this.file.dataDirectory + newFileName;
      this.photoName_mydocs = newFileName;
      ////console.log("photos=" + JSON.stringify(this.docData_mydocs));
      this.createDB();   
    }, error => {
      alert('Error while storing file.' + JSON.stringify(error));
    });
  }

  createDB() {
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
     // //console.log("success create or open database");
      this.db = db;
      this.db.executeSql('CREATE TABLE IF NOT EXISTS DOCPHOTO(id INTEGER PRIMARY KEY AUTOINCREMENT,PhotoPath TEXT,Name TEXT,Latitude TEXT,Longitude TEXT,Location TEXT,CreateDate TEXT,CreateTime TEXT)', {})
        .then((success) => {
          this.getLocation();
          ////console.log('Executed SQL success')
        }, (error) => {
          //console.error("Unable to create database", error);
        })
        .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }

  getLocation() {
    let options = {
      timeout: 30000
    };

    this.geolocation.getCurrentPosition(options).then((resp) => {
      this.latitude_mydocs = resp.coords.latitude;
      this.longitude_mydocs = resp.coords.longitude;
      ////console.log(' getting latitude ', resp.coords.latitude);
      ////console.log(' getting longitude ', resp.coords.longitude);
      this.nativeGeocoder.reverseGeocode(this.latitude_mydocs, this.longitude_mydocs)
        .then((result: NativeGeocoderReverseResult[]) => {
          this.location_mydocs = result[0].locality + ", " + result[0].countryName;
          this.insertPhoto();
          ////console.log(JSON.stringify(result))
        }, (err) => {
          //this.dismiss();
          let toast = this.toastCtrl.create({
            message: "Please take photo again. Please turn on location access.",
            duration: 5000,
            position: 'bottom',
            //showCloseButton: true,
            dismissOnPageChange: true,
            //closeButtonText: 'OK'
          });
          toast.present(toast);
          ////console.log('Error getting location', JSON.stringify(err));
        });
      // )
      //   .catch((error: any) => //console.log(error));
    }, (err) => {
      //this.dismiss();
      let toast = this.toastCtrl.create({
        message: "Please take photo again. Please turn on location access.",
        duration: 5000,
        position: 'bottom',
        //showCloseButton: true,
        dismissOnPageChange: true,
        //closeButtonText: 'OK'
      });
      toast.present(toast);
      ////console.log('Error getting location', JSON.stringify(err));
    });
    /* })
    .catch((error: any) => //console.log(error));
}).catch((error) => {
  //console.log('Error getting location', JSON.stringify(error));
}); */
  }

  /* dismiss() {
    this.loadingIcon.dismissAll();
  } */

  insertPhoto() {
    let date = new Date()
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const weekNames = ["SUN", "MON", "TUE", "WED", "THUR", "FRI", "SAT"];
    //const dayNames = [];
    let month = monthNames[date.getMonth()];
    //let day = dayNames[date.getUTCDate()];
    let createData = month + "-" + date.getDate() + "-" + date.getFullYear();
    let wDay = weekNames[date.getDay()];
    let hoursMinutes = [date.getHours(), date.getMinutes()];
    let creteTime = wDay + " " + this.formatAMPM(hoursMinutes);
    ////console.log("time " + creteTime);

    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
     // //console.log("success create or open database");

      db.executeSql("INSERT INTO DOCPHOTO(PhotoPath,Name,Latitude,Longitude,Location,CreateDate,CreateTime) VALUES (?,?,?,?,?,?,?)", [this.docData_mydocs, this.photoName_mydocs, this.latitude_mydocs, this.longitude_mydocs, this.location_mydocs, createData, creteTime]).then((data) => {
        ////console.log("Insert data successfully", JSON.stringify(data));

        this.selectData();
      }, (error) => {
        //console.error("Unable to insert data", JSON.stringify(error));
      });

    }).catch(e => console.log(e));
  }

  selectData() {
    this.nores_mydocs = 0;
    ////console.log("select data from DOCPHOTO");
    this.photoList_mydocs = [];

    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
     // //console.log("success create or open database");

      db.executeSql("SELECT * FROM DOCPHOTO order by id desc ", []).then((data) => {
        ////console.log("length == " + data.rows.length);
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            this.photo_mydocs = { id: '', PhotoPath: '', name: '', latitude: '', longitude: '', location: '', CreateDate: '', CreateTime: '' };
            this.photo_mydocs.PhotoPath = data.rows.item(i).PhotoPath;
            this.photo_mydocs.name = data.rows.item(i).Name;
            this.photo_mydocs.latitude = data.rows.item(i).Latitude;
            this.photo_mydocs.longitude = data.rows.item(i).Longitude;
            this.photo_mydocs.location = data.rows.item(i).Location;
            this.photo_mydocs.CreateDate = data.rows.item(i).CreateDate;
            this.photo_mydocs.CreateTime = data.rows.item(i).CreateTime;
            this.photoList_mydocs.push(this.photo_mydocs);
            ////console.log("DOCPHOTO list = " + JSON.stringify(this.photoList_mydocs));
            //comment=
            // [
            //   {"id":"",
            //   "PhotoPath":"file:///data/user/0/dc.xxx/files/Doc-1529405686299.jpg",
            //   "name":"Doc-1529405686299.jpg",
            //   "latitude":"16.8817628",
            //   "longitude":"96.121786",
            //   "location":"Sitgaing, Myanmar (Burma)",
            //   "CreateDate":"June-19-2018",
            //   "CreateTime":"TUE 5:24 pm"}
            // ]
          }

          if (this.photoList_mydocs != undefined && this.photoList_mydocs.length != 0) {
            this.nores_mydocs = 1;
          }
        }
      }, (error) => {
        this.nores_mydocs = 0;
        //console.error("Unable to select data", JSON.stringify(error));
      });
    }).catch(e => console.log(e));
  }

  formatAMPM(date) {
    var hours = date[0];
    var minutes = date[1];
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  createFileName() {
    let d = new Date().getTime();
    // let n = d.getTime();
    let newFileName = "Doc-" + d + ".jpg";
    return newFileName;
  }

}
