import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import { Changefont } from '../changefont/changeFont';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'page-account-join',
  templateUrl: 'account-join.html',
})
export class AccountJoinPage {
  textMyan: any = ["Account No","Add"];
  textEng: any =["Account No","Add"];
  textData: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage: Storage,public changefont: Changefont,public events: Events) {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
  }
  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountJoinPage');
  }

}
