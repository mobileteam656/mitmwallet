import { Component, NgZone, ViewChild } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, Platform, Events, Content, PopoverController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { DomSanitizer } from '@angular/platform-browser';
import { Clipboard } from '@ionic-native/clipboard';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { ChangefontProvider } from '../../providers/changefont/changefont';
import { FunctProvider } from '../../providers/funct/funct';
import { FbdetailPage } from '../fbdetail/fbdetail';
import { ViewPhotoMessagePage } from '../view-photo-message/view-photo-message';
import { TabsPage } from '../tabs/tabs';
import { LikepersonPage } from '../likeperson/likeperson';
import { ReplyCommentPage } from '../reply-comment/reply-comment';
import { PopoverCommentPage } from '../popover-comment/popover-comment';
import { RegistrationPage } from '../registration/registration';
import { Login } from '../login/login';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'page-single-view',
  templateUrl: 'single-view.html',
  providers: [FunctProvider, ChangelanguageProvider, ChangefontProvider]
})
export class SingleViewPage {
  @ViewChild(Content) content: Content;
  passData: any;
  loading: any;
  textMyan: any = ["အသေးစိတ်အချက်အလက်", "ဒေတာ မရှိပါ။", "မွတ္ခ်က္ေရးရန္ ...", "ကြိုက်တယ်", "မှတ်ချက်", "မျှဝေမည်", "အသေးစိတ်အချက်အလက်"];
  textEng: any = ["Replies", "No Replies", "Write a comment .....", "like", "comment", "share", "Detail"];
  textData: any = [];
  font: any;
  nores: any;
  nores2: any;
  detailData: any = { n2: 0 };
  status: any;
  videoImgLink: any;
  photoLink: any;
  registerData: any;
  replyData: any;
  popover: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public loadingCtrl: LoadingController, public funct: FunctProvider, public clipboard: Clipboard,
    public platform: Platform, public event: Events, public sanitizer: DomSanitizer, public popoverCtrl: PopoverController,
    public http: Http, public toastCtrl: ToastController, public changeLanguage: ChangelanguageProvider,
    public global: GlobalProvider, public _zone: NgZone, public changefont: ChangefontProvider) {
    this.passData = this.navParams.get("data");
    this.status = this.navParams.get("status");
    this.videoImgLink = this.global.digitalMedia + "upload/image/contentImage/videoImage/";
    this.photoLink = this.global.digitalMediaProfile;
    this.storage.get('language').then((font) => {
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then((data) => {
        this.textData = data;
        if (font != "zg")
          this.font = 'uni';
        else
          this.font = font;
        ////console.log("data language=" + JSON.stringify(data));
      });
    });
    this.storage.get('appData').then((data) => {
      ////console.log("registerData = " + JSON.stringify(data));
      if (data == null || data == "" || data == true) {
        this.nores = 0;
      }
      else {
        this.registerData = data;
        if (this.registerData.sessionKey == undefined)
          this.registerData.sessionKey = '';
        this.getSingleViewData();

      }
    });
  }

  dismiss() {
    this.storage.get('appData').then((data) => {
      ////console.log("registerData = " + JSON.stringify(data));
      if (data == null || data == "" || data == true) {
        this._zone.run(() => {
          this.navCtrl.setRoot(Login);
        });
      }
      else {
        this._zone.run(() => {
          this.navCtrl.setRoot(TabsPage);
        });
      }
    });
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad SingleViewPage');
    this.backButtonExit();
  }
  scrollToBottom() {
    setTimeout(() => {
      if (this.content._scroll) this.content.scrollToBottom(0);
    }, 400)
  }
  getSingleViewData() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present(); 
    this.http.get(this.global.ipaddressCMS + '/serviceArticle/getArticleDataBySyskey?syskey=' + this.passData).map(res => res.json()).subscribe(result => {
      ////console.log("return serviceArticle data = " + JSON.stringify(result));
      if (result.data.length > 0) {
        this.nores = 1;
        //this.getCommentData();
        this.detailData = result.data[0];
        this.storage.remove('deepLinkData');

        this.detailData.modifiedDate = this.funct.getTransformDate(this.detailData.modifiedDate);
        //  this.detailData.modifiedTime = this.funct.getTimeTransformDate(this.detailData.modifiedTime);
        if (this.detailData.n6 != 1)
          this.detailData.showLike = false;
        else
          this.detailData.showLike = true;
        if (this.detailData.n7 != 1)
          this.detailData.showContent = false;
        else
          this.detailData.showContent = true;
        if (this.detailData.n2 != 0) {
          //   this.detailData.likeCount = this.funct.getChangeCount(this.detailData.n2);
        }
        if (this.detailData.n3 != 0) {
          //   this.detailData.commentCount = this.funct.getChangeCount(this.detailData.n3);
        }
        if (this.detailData.n10 == 1) {
          if (this.detailData.t8 != '') {
            if (this.detailData.uploadedPhoto.length > 0) {
              this.detailData.videoLink = this.videoImgLink + this.detailData.uploadedPhoto[0].t7;
            }
            else {
              let temp = this.detailData.t8;     // for video link
              let str1 = temp.search("external/");
              let str2 = temp.search(".sd");
              if (str2 < 0) {
                str2 = temp.search(".hd");
              }
              let res = temp.substring(str1 + 9, str2);
              this.detailData.videoLink = "https://i.vimeocdn.com/video/" + res + "_295x166.jpg";
            }
           // //console.log("data.data[i].videoLink == " + this.detailData.videoLink);
            //this.detailData.t8 = this.sanitizer.bypassSecurityTrustResourceUrl(this.detailData.t8);
          }
        }
        else if (this.detailData.n10 == 2) {
          if (this.detailData.uploadedPhoto.length > 0) {
            this.detailData.videoLink = this.videoImgLink + this.detailData.uploadedPhoto[0].t7;
          }
          //this.detailData.t8 = this.sanitizer.bypassSecurityTrustResourceUrl(this.detailData.t8+"?&autoplay=1");
          this.detailData.t8 = this.corrigirUrlYoutube(this.detailData.t8);
          ////console.log("data.data[i].t8 for youtube == " + this.detailData.t8);
        }
        else {
          if (this.detailData.uploadedPhoto.length > 0) {
            if (this.detailData.uploadedPhoto[0].t2 != '') {
              this.detailData.videoLink = this.videoImgLink + this.detailData.uploadedPhoto[0].t7;
              this.detailData.videoStatus = true;
            }
            else {
              this.detailData.videoStatus = false;
            }
          }
        }
        if (this.detailData.t2.indexOf("<img ") > -1) {
          this.detailData.t2 = this.detailData.t2.replace(/<img /g, "<i ");
          ////console.log("replace img == " + this.detailData.t2);
          this.detailData.t2 = this.detailData.t2.replace(/ \/>/g, "></i>");
          ////console.log("replace <i/> == " + this.detailData.t2);
        }
      }
      else {
        this.nores = 0;
        this.storage.remove('deepLinkData');
      }
      this.loading.dismiss();
    },
      error => {
        this.nores = 0;
        this.loading.dismiss();
        this.storage.remove('deepLinkData');
        this.getError(error);
      });
  }

  corrigirUrlYoutube(video) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(video);
  }

  // getCommentData() { //tmn
  //   this.http.get(this.global.ipaddressCMS + '/serviceQuestion/getCommentmobile?id=' + this.passData + '&userSK=' + this.registerData.person.syskey + '&sessionKey=' + this.registerData.sessionKey).map(res => res.json()).subscribe(result => {
  //     //console.log("return Comment data = " + JSON.stringify(result));
  //     if (result.data.length > 0) {
  //       this.nores2 = 1;
  //       for (let i = 0; i < result.data.length; i++) {
  //         result.data[i].differenceTime = this.funct.getTimeDifference(result.data[i].modifiedDate, result.data[i].modifiedTime);

  //         result.data[i].modifiedDate = this.funct.getTransformDate(result.data[i].modifiedDate);

  //         result.data[i].like = "Like";

  //         if (result.data[i].person.length > 0) {
  //           for (let k = 0; k < result.data[i].person.length; k++) {
  //             if (result.data[i].person[k].syskey == this.registerData.person.syskey) {
  //               result.data[i].like = "UnLike";
  //               break;
  //             }
  //           }
  //         }

  //         result.data[i].likeCount = result.data[i].person.length;
  //         result.data[i].replyCount = result.data[i].n3;
  //         if (result.data[i].n3 == 1) {
  //           result.data[i].textreply = "reply";
  //         }
  //         else if (result.data[i].n3 > 1) {
  //           result.data[i].textreply = "replies";
  //         }
  //       }
  //       this.replyData = result.data;
  //       //this.scrollToBottom();
  //       //console.log("replyData data = " + JSON.stringify(this.replyData));
  //     }
  //     else {

  //       this.nores2 = 0;

  //     }
  //   },
  //     error => {
  //       this.nores2 = 0;
  //       this.getError(error);
  //     });
  // }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      ////console.log("Active Page=" + this.navCtrl.getActive().name);
      if (this.status == 1) {
        this.dismiss();
      }
      else {
        this.navCtrl.pop();
      }

    });
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });
    toast.present(toast);
    this.loading.dismiss();
    ////console.log("Oops!");
  }

  singlePhoto(i) {
    ////console.log("viewImage == " + JSON.stringify(i));
    this.navCtrl.push(ViewPhotoMessagePage, {
      image: i,
      link: ''
    });
  }

  viewImage(i) {
    ////console.log("viewImage == " + JSON.stringify(i));
    this.navCtrl.push(FbdetailPage, {
      detailData: i
    });
  }

  changeLike(skey, index) {
    /*this.like = false;
    this.unlike = true;*/
    /*this.replyData[i].like = false;
    this.replyData[i].unlike = true;
    this.likeCount = this.likeCount + 1;*/
    this.replyData[index].likeCount = this.replyData[index].likeCount - 1;
    let parameter = {
      n1: this.registerData.person.syskey,
      n2: skey
    }
    //   this.postcmt = {t3: this.registerData.person.t2 , t2: this.comment};
    ////console.log("requst saveComment=" + JSON.stringify(parameter));
    //  this.replyData.push(this.postcmt);
    this.scrollToBottom();
    this.http.post(this.global.ipaddressCMS + '/serviceQuestion/saveCommentLike', parameter).map(res => res.json()).subscribe(data => {
      ////console.log("return likecomment data = " + JSON.stringify(data));
      ////console.log("data length == " + data.data.length);
      if (data.state && data.data.length > 0) {
        for (let i = 0; i < data.data.length; i++) {

          /*if(data.data[i].recordStatus == 1){
            this.replyData[index].like = "UnLike";
            //this.replyData[index].unlike = "UnLike";
          }
          else if(data.data[i].recordStatus == 4)
          {
            this.replyData[index].like = "Like";
          }
          this.replyData[index].likeCount = data.data[i].n3;*/
          if (data.data[i].person.length > 0) {
            for (let k = 0; k < data.data[i].person.length; k++) {
              if (data.data[i].person[k].syskey == this.registerData.person.syskey) {
                this.replyData[index].like = "UnLike";
                break;
              }
              else {
                this.replyData[index].like = "Like";
              }
            }

          }
          else {
            this.replyData[index].like = "Like";
          }

          this.replyData[index].likeCount = data.data[i].person.length;
          this.replyData[index].person = data.data[i].person;
        }
      }
    },
      error => {
        this.getError(error);
      });

  }

  likePerson(cmt) {

    this.navCtrl.push(LikepersonPage, {
      data: cmt
    });
  }

  ClickReply(cmt) {
    this.navCtrl.push(ReplyCommentPage, {
      data: cmt,
      detaildata: this.detailData
    });
  }

  clickLike(data) {
    ////console.log("data=" + JSON.stringify(data));
    if (!data.showLike) {
      this.detailData.showLike = true;
      this.detailData.n2 = this.detailData.n2 + 1;
      this.detailData.likeCount = this.funct.getChangeCount(this.detailData.n2);
      this.getLike(data);
    }
    else {
      this.detailData.showLike = false;
      this.detailData.n2 = this.detailData.n2 - 1;
      this.detailData.likeCount = this.funct.getChangeCount(this.detailData.n2);
      this.getUnlike(data);
    }
  }

  getLike(data) {
    let parameter = {
      key: data.syskey,
      userSK: this.registerData.person.syskey,
      type: data.t3
    }
    ////console.log("request clickLike = ", JSON.stringify(parameter));
    this.http.get(this.global.ipaddressCMS + '/serviceArticle/clickLikeArticle?key=' + data.syskey + '&userSK=' + this.registerData.person.syskey + '&type=' + data.t3).map(res => res.json()).subscribe(data => {
      ////console.log("response clickLike = ", JSON.stringify(data));
      if (data.state) {
        this.detailData.showLike = true;
      }
      else {
        this.detailData.showLike = false;
        this.detailData.n2 = this.detailData.n2 - 1;
      }
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  getUnlike(data) {
    let parameter = {
      key: data.syskey,
      userSK: this.registerData.person.syskey,
      type: data.t3
    }
    ////console.log("request clickLUnlike = ", JSON.stringify(parameter));
    this.http.get(this.global.ipaddressCMS + '/serviceArticle/clickUnlikeArticle?key=' + data.syskey + '&userSK=' + this.registerData.person.syskey + '&type=' + data.t3).map(res => res.json()).subscribe(data => {
     // //console.log("response clickLUnlike = ", JSON.stringify(data));
      if (data.state) {
        this.detailData.showLike = false;
      }
      else {
        this.detailData.showLike = true;
        this.detailData.n2 = this.detailData.n2 + 1;
      }
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }
  // presentPopover(ev, d) {
  //   let st;
  //   if (d.n5 != this.registerData.person.syskey) {
  //     st = 0;
  //   } else {
  //     st = 1;
  //   }
  //   this.popover = this.popoverCtrl.create(PopoverCommentPage, {
  //     data: st
  //   });

  //   this.popover.present({
  //     ev: ev
  //   });

  //   let doDismiss = () => this.popover.dismiss();
  //   let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
  //   this.popover.onDidDismiss(unregBackButton);

  //   this.popover.onWillDismiss(data => {
  //     //console.log("popover dismissed");
  //     //console.log("Selected Item is " + data);
  //     if (data == "1") {
  //       let copytext = this.changefont.UnitoZg(d.t2);
  //       this.clipboard.copy(copytext);
  //     }
  //     else if (data == "2") {
  //       this.getDeleteMsg(d);
  //     }
  //   });

  // }
  // getDeleteMsg(d) {

  //   this.loading = this.loadingCtrl.create({
  //     content: "Please wait...",
  //     dismissOnPageChange: true
  //     //   duration: 3000
  //   });
  //   this.loading.present();
  //   //console.log("request d.syskey =" + d.syskey);
  //   this.http.get(this.global.ipaddressCMS + "/serviceQuestion/deleteComment?syskey=" + d.syskey).map(res => res.json()).subscribe(result => {
  //     //console.log("response process msg =" + JSON.stringify(result));
  //     if (result.state) {
  //       //this.getCommentData();
  //       this.detailData.n3 = this.detailData.n3 - 1;
  //       this.detailData.commentCount = this.funct.getChangeCount(this.detailData.n3);
  //       this.loading.dismiss();
  //     }
  //     else {
  //       let toast = this.toastCtrl.create({
  //         message: 'Delete Failed! Please try again.',
  //         duration: 5000,
  //         position: 'bottom',
  //         //  showCloseButton: true,
  //         dismissOnPageChange: true,
  //         // closeButtonText: 'OK'
  //       });
  //       toast.present(toast);
  //       this.loading.dismiss();
  //     }
  //   }, error => {
  //     //console.log("signin error=" + error.status);
  //     this.getError(error);
  //     this.loading.dismiss();
  //   });
  // }
}
