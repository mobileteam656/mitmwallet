import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, Popover } from 'ionic-angular';
import { QRCodeComponent } from 'angular2-qrcode';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { UtilProvider } from '../../providers/util/util';

@Component({
  selector: 'page-popover-contact-add',
  templateUrl: 'popover-contact-add.html',
})

export class PopoverContactAddPage {
  _obj = { phone: '', name: '', type: '1' };
  type: any;
  qrValue: any = [];
  temp: any;
  qrData: any;
  constructor(public util: UtilProvider, public navCtrl: NavController,
    public barcodeScanner: BarcodeScanner, public toastCtrl: ToastController,
    public navParams: NavParams, public viewCtrl: ViewController) {
    this.qrValue = this.navParams.get('data');
    if (this.qrValue[1] != undefined && this.qrValue[1] != '') {
      this._obj.phone = this.qrValue[1];
    }
    //console.log("contact data is : " + JSON.stringify(this.qrValue[1]))
  }

  save() {
    let normalizePhone = this.util.normalizePhone(this._obj.phone);
    if (normalizePhone.flag == true) {
      this._obj.phone = normalizePhone.phone;
      this.viewCtrl.dismiss(this._obj);
    } else {
      this.viewCtrl.dismiss(this._obj);
      //console.log("contact data is : " + JSON.stringify(this._obj))
    }
  }

  scanQR() {
    this.barcodeScanner.scan().then((barcodeData) => {
      if (barcodeData.cancelled) {
        ////console.log("User cancelled the action!");

        return false;
      }

     // //console.log("barcodeData.text=" + barcodeData.text);

      try {
        this.qrValue = JSON.parse(barcodeData.text);

        if (this.qrValue[0] == "contacts") {
          this._obj.phone = this.qrValue[1];
        } else {
          this.toastInvalidQR();
        }
      } catch (e) {
        this.toastInvalidQR();
      }

      ////console.log("phone" + this._obj.phone);
    }, (error) => {
     // //console.log(JSON.stringify(error));
    });
  }

  toastInvalidQR() {
    let toast = this.toastCtrl.create({
      message: "Invalid QR Type.",
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });

    toast.present(toast);
  }

}
