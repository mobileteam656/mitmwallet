import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, ToastController, AlertController, Events, Platform, PopoverController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { GlobalProvider } from '../../providers/global/global';
import { Changefont } from '../changefont/changeFont';
import { Receipt } from '../receipt/receipt';
import { DatePipe } from '@angular/common';
import { App } from 'ionic-angular';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { UtilProvider } from '../../providers/util/util';

@Component({
  selector: 'page-qr-receipt',
  templateUrl: 'qr-receipt.html',
  providers: [CreatePopoverPage],
})
export class QrReceipt {

  textEng: any = ["Receive", "Amount", "Display QR", "Reference", "Invalid Amount.","Please enter English Text.","Cash In"];
  textMyan: any = ["ငွေလက်ခံရန်", "ငွေပမာဏ", "QR ပြမည်", "အမှတ်စဉ်", "ငွေပမာဏ ရိုက်ထည့်ပါ","အင်္ဂလိပ်စာသားရိုက်ထည့်ပေးပါ။","ငွေသွင်း"];
  textData: string[] = [];
  font: string = '';
  amount1:any=[];

  passTemp1: any;
  passTemp2: any;
  passTemp3: any;
  passTemp4: any;
  passTemp5: any;//wcs

  dataValue: any = [];
  userData: any;
  amount: string = '';
  reference: string = '';
  today: any;
  popover: any;
  errormsg1: string = '';
  errormsg2: string = '';
  type:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public events: Events, public barcodeScanner: BarcodeScanner, public alertCtrl: AlertController,
    public global: GlobalProvider, public viewCtrl: ViewController,
    public platform: Platform, public popoverCtrl: PopoverController, public datePipe: DatePipe,
    public changefont: Changefont, public appCtrl: App, public createPopover: CreatePopoverPage,
    public util: UtilProvider) {
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
      ////console.log("state data=" + JSON.stringify(this.textData));
    });

    this.storage.get('userData').then((userData) => {
      if (userData != null) {
        this.userData = userData;
        ////console.log("state data=" + JSON.stringify(this.userData));
        ////console.log("this.userData.frominstituteCode -- QR-pay=" + JSON.stringify(this.userData.frominstituteCode));
      }
    });

    this.events.subscribe('userData', userData => {
      if (userData != null) {
        this.userData = userData;
        ////console.log("event data=" + JSON.stringify(this.userData));
        ////console.log("this.userData.frominstituteCode -- QR-pay=" + JSON.stringify(this.userData.frominstituteCode));
      }
    });
    this.type = this.navParams.get("type");
    console.log('Parameter: '+this.type);
  }

  ionViewDidEnter(){
    this.type = this.navParams.get("type");
    console.log("loc param==" + JSON.stringify(this.type));
  }
  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }
  @ViewChild('myInput') myInput;
  ionViewDidLoad(){
    console.log("ionViewDidLoad");
    setTimeout(() => {
      this.myInput.setFocus();
    }, 500);
  }

  printQR() {
    if (this.util.checkInputIsEmpty(this.amount) || this.util.checkNumberOrLetter(this.amount) ||
      this.util.checkAmountIsZero(this.amount) || this.util.checkAmountIsLowerThanZero(this.amount) ||
      this.util.checkStartZero(this.amount) || this.util.checkPlusSign(this.amount) ||
      (!this.util.checkOnlyNumber(this.amount)) || this.util.checkMinusSign(this.amount)) {
      this.errormsg1 = this.textData[4];     
    }
    /* else if(!this.util.checkOnlyEng(this.reference)) {
      this.errormsg2 = this.textData[5];
    } */
    //if (this.errormsg1 == "") {
    else{
      let date = new Date();
      this.today = this.datePipe.transform(date, 'dd-MM-yyyy hh:mm:ss');
      this.passTemp1 = this.userData.userID;
      this.passTemp2 = this.userData.frominstituteCode;
      this.passTemp3 = this.userData.name;
      this.passTemp4 = this.today;
      this.passTemp5 = this.userData.sKey;//wcs

      let temp = [];
      let temp1=[];
      temp1.push(this.amount);
      temp1.push(this.passTemp3);
      temp1.push(this.passTemp1);
      temp.push("payments");//"payment"
      temp.push(this.passTemp1);//this.userData.userID;
      temp.push(this.passTemp2);//this.userData.frominstituteCode;
      //temp.push(this.passTemp3);//this.userData.name;\
      // temp.push("amount:"+this.amount
      // );
      temp.push(this.amount);
      temp.push(this.reference);
      temp.push(this.passTemp4);//this.today;
      temp.push(this.passTemp5);//wcs//this.userData.sKey;//wcs
      this.dataValue = JSON.stringify(temp);
      this.amount1=temp1;
   console.log('dataValue=' + JSON.stringify(this.dataValue));
   console.log('Amount:'+this.amount1);
      this.navCtrl.push(Receipt, {
        data: this.dataValue,
        param:this.amount1,
        type:this.type
      });
    }
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  keyUp(data){
    let REGEX = /[a-zA-Z0-9]+/;
   if(!REGEX.test(data)){
      this.reference="";
   }
  }
}
