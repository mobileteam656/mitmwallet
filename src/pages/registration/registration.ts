import { Component, ElementRef, ViewChild } from '@angular/core';
import { ActionSheetController, Platform, NavController, LoadingController, PopoverController, ToastController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { Changefont } from '../changefont/changeFont';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { App } from 'ionic-angular';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
//import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { MenuController } from 'ionic-angular/components/app/menu-controller';

@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
  providers: [Changefont, CreatePopoverPage]
})

export class RegistrationPage {

  textEng: any = [
    "Registration", "Region", "Name",
    "NRC", "Date of Birth", "Father Name",
    "Save", "Submit", "Profile",
    "Mobile Number", "Invalid Name.", "Invalid NRC.",
    "Invalid Date of Birth.", "Invalid Father Name.", "Address",
    "Edit", "I.C", "Passbook",
    "Invalid ID.", "Invalid Passbook.", "Mobile Number",
    "Invalid Mobile Number.", "Township", "Upload NRC Front",
    "Upload NRC Back", "Invalid Address", "OCR Scan"
  ];
  textMyan: any = [
    "မှတ်ပုံတင်ခြင်း", "ဒေသ", "နာမည်",
    "မှတ်ပုံတင်နံပါတ်", "မွေးနေ့", "အဖနာမည်",
    "သိမ်းမည်", "မှတ်ပုံတင်မည်", "ကိုယ်ပိုင်အချက်အလက်",
    "ဖုန်းနံပါတ်", "Invalid Name.", "Invalid NRC.",
    "Invalid Date of Birth.", "Invalid Father Name.", "လိပ်စာ",
    "ပြင်မည်", "I.C", "Passbook",
    "Invalid ID.", "Invalid Passbook.", "ဖုန်းနံပါတ်",
    "Invalid Mobile Number.", "မြို့", "မှတ်ပုံတင် အရှေ့ဘက်",
    "မှတ်ပုံတင် အနောက်ဘက်", "Invalid Address", "OCR Scan"
  ];
  textData: string[] = [];
  font: string = '';

  ipaddress: string;
  ipaddressCMS: string;

  //state: string = '';
  stateCaption: string = '';
  stateList: any = [];
  tempData: any;

  registerData = {
    syskey: "", phone: "", username: "", realname: "", stateCaption: "",
    photo: "", fathername: "", nrc: "", id: "", passbook: "",
    address: "", dob: "", city: "", t1: "", t2: "", t3: "",
    t4: "", t5: "", state: "", status: "", n1: "",
    n2: "", n3: "", n4: "", n5: ""
    //, dateOfBirth: ""
  };

  buttonMode: string = '';
  labelMode: string = '';

  eMsgName: string = '';
  eMsgNRC: string = '';
  msgImgAction: string = '';
  //eMsgDateOfBirth: string = '';
  eMsgDOB: string = '';
  eMsgFatherName: string = '';
  eMsgID: string = '';
  eMsgPassbook: string = '';
  eMsgAddress: string = '';
  eMsgTownship: string = '';
  eMsgPhone: string = '';
  eMsgCity: string = '';

  popover: any;

  hautosys: any;

  region: string;

  isLoading: any;
  showData: any;
  public loading;
  cityList: any;
  city: any;
  keyboardfont: any;

  photoName: any;
  profile1: any;
  profileImg1: any;
  profile2: any;
  profileImg2: any;
  localStoragePhoto: string = '';
  imglnk: any;
  base64Image: any;

  profileImgBack: any;
  profileBack: any;

  imageLink: any;
  type: any;

  imageOne: any;
  imageTwo: any;
  side: any;
  imageServerSrc1: any;
  imageServerSrc2: any;
  tempPhoto1: any;
  tempPhoto2: any;

  @ViewChild('myInput') myInput: ElementRef;

  constructor(
    public util: UtilProvider,
    public platform: Platform,
    public storage: Storage,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public http: Http,
    public changefont: Changefont,
    public events: Events,
    public global: GlobalProvider,
    public popoverCtrl: PopoverController,
    public appCtrl: App,
    public createPopover: CreatePopoverPage,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    private file: File,
    //private transfer: Transfer,
    private transfer: FileTransfer,
    public navCtrl: NavController,
    public element: ElementRef, public menuCtrl: MenuController
    //private base64ToGallery: Base64ToGallery
  ) {
    //setTimeout(() => this.adjust(), 0);

    this.imageLink = this.global.digitalMedia + "upload/image/userNRC/";
   // this.menuCtrl.swipeEnable(false);
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });

    this.isLoading = true;
    this.showData = false;
    //this.openProcessingLoading();

    this.ipaddress = this.global.ipaddress;

    this.ipaddressCMS = this.global.ipaddressCMS;

    this.storage.get('hautosys').then((data) => {

      if (data.hautosys != undefined || data.hautosys != null || data.hautosys != '' || data.hautosys.length != 0) {
        this.hautosys = data.hautosys;
      } else {
      }
    });
    
    this.storage.get('uari').then((result) => {
      if (result != null) {
        this.registerData = result;
        this.tempPhoto1 = '';
        this.tempPhoto2 = '';
        this.imageServerSrc1 = this.imageLink + this.registerData.t1;
        this.imageServerSrc2 = this.imageLink + this.registerData.t2;

        if (this.registerData.status == "0") {
          this.buttonMode = 'saveAndSubmitMode';
          this.labelMode = 'enable';
        } else {
          this.buttonMode = 'editMode';
          this.labelMode = 'disable';
        }
        /* }else if(this.registerData.status == "3") {
          this.buttonMode = 'disableMode';
          this.labelMode = 'disable';
        } */
        this.isLoading = false;
        this.showData = true;
      } else {
        this.getRegister();
        this.imageServerSrc1 = this.imageLink + this.registerData.t1;
        this.imageServerSrc2 = this.imageLink + this.registerData.t2;
        if (this.registerData.status == "0") {
          this.buttonMode = 'saveAndSubmitMode';
          this.labelMode = 'enable';
        } else {
          this.buttonMode = 'editMode';
          this.labelMode = 'disable';
        }
        this.isLoading = false;
        this.showData = true;
      }
    });

    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;
    });

    this.storage.get('font').then((font) => {
      if (font == "zg") {
        this.keyboardfont = 'zg';
      }
      else {
        this.keyboardfont = 'uni';
      }
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  openProcessingLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true,
      duration: 3000
    });

    this.loading.present();
  }

  closeProcessingLoading() {
    this.loading.dismiss();
  }

  checkEmpty() {
    if (this.util.checkInputIsEmpty(this.registerData.realname)) {
      this.eMsgName = this.textData[10];
    } else {
      this.eMsgName = "";
    }
    if (this.util.checkInputIsEmpty(this.registerData.nrc)) {
      this.eMsgNRC = this.textData[11];
    } else {
      this.eMsgNRC = "";
    }
    if (this.util.checkInputIsEmpty(this.registerData.dob)) {
      this.eMsgDOB = this.textData[12];
    } else {
      this.eMsgDOB = "";
    }
    if (this.util.checkInputIsEmpty(this.registerData.fathername)) {
      this.eMsgFatherName = this.textData[13];
    } else {
      this.eMsgFatherName = "";
    }
    if (this.util.checkInputIsEmpty(this.registerData.address)) {
      this.eMsgAddress = this.textData[25];
    } else {
      this.eMsgAddress = "";
    }
  }

  zgToUni() {
    this.registerData.realname = this.registerData.realname.trim();
    this.registerData.nrc = this.registerData.nrc.trim();
    this.registerData.dob = this.registerData.dob.trim();
    this.registerData.fathername = this.registerData.fathername.trim();
    this.registerData.address = this.registerData.address.trim();

    if (this.keyboardfont == 'zg') {
      this.registerData.realname = this.changefont.ZgtoUni(this.registerData.realname);
    }
    if (this.keyboardfont == 'zg') {
      this.registerData.nrc = this.changefont.ZgtoUni(this.registerData.nrc);
    }
    if (this.keyboardfont == 'zg') {
      this.registerData.dob = this.changefont.ZgtoUni(this.registerData.dob);
    }
    if (this.keyboardfont == 'zg') {
      this.registerData.fathername = this.changefont.ZgtoUni(this.registerData.fathername);
    }
    if (this.keyboardfont == 'zg') {
      this.registerData.address = this.changefont.ZgtoUni(this.registerData.address);
    }
  }

  goSaveV1(statusFromUser) {
    this.uploadPhoto1('1', this.registerData.t1, statusFromUser);
  }

  goSave(statusFromUser) {
    this.checkEmpty();

    if (this.eMsgName == "" && this.eMsgNRC == "" && this.eMsgDOB == "" && this.eMsgFatherName == "" ) {//&& this.eMsgAddress == ""
      this.zgToUni();
      //this.slimLoader.start(() => { });
      //this.isLoading = true;
      //this.showData = false;
      this.openProcessingLoading();

      this.registerData.status = statusFromUser;

      let parameter = {
        hautosys: this.hautosys,
        t3: this.registerData.realname,
        t20: this.registerData.fathername,
        t21: this.registerData.nrc,
        t22: this.registerData.id,
        t23: this.registerData.passbook,
        t24: this.registerData.address,
        t25: this.registerData.dob,
        t7: this.stateCaption,
        t8: this.registerData.city,
        //t36: this.state,
        // t37: this.registerData.city,
        t38: this.registerData.status,
        t26: this.registerData.t1,
        t27: this.registerData.t2,
        t28: this.registerData.t3,
        t29: this.registerData.t4,
        t30: this.registerData.t5,
        t76: this.registerData.n1,
        t77: this.registerData.n2,
        t78: this.registerData.n3,
        t79: this.registerData.n4,
        t80: this.registerData.n5
      };

      this.http.post(this.ipaddress + '/service001/saveRegister', parameter).map(res => res.json()).subscribe(result => {
        if (result != null && result.messageCode != null && result.messageCode == "0000") {
          this.registerData.stateCaption = result.t7;
          //this.registerData.state = result.t36;
         // //console.log("this.registerData" + JSON.stringify(this.registerData));
          if (statusFromUser == "1") {
            this.buttonMode = 'saveAndSubmitMode';
            this.labelMode = 'enable';
          }

          if (statusFromUser == "2") {
            this.buttonMode = 'editMode';
            this.labelMode = 'disable';
          }

          if (result.t38 == "3") {
            this.buttonMode = 'disableMode';
            this.labelMode = 'disable';
          }
          this.closeProcessingLoading();

          let toast = this.toastCtrl.create({
            message: result.messageDesc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.getRegister();
        } else {
          this.closeProcessingLoading();

          let toast = this.toastCtrl.create({
            message: result.messageDesc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        }
      },
        error => {
          this.closeProcessingLoading();
          this.getError(error);
        });
    }
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  getError(error) {
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }

    let msg = '';

    if (code == '005') {
      msg = "Please check internet connection!";
    } else {
      msg = "Can't connect right now. [" + code + "]";
    }

    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });

    toast.present(toast);
  }

  goEdit() {
    this.buttonMode = 'saveAndSubmitMode';
    this.labelMode = 'enable';
    //this.getCityList();
  }

  getRegister() {
    let parameter = {
      hautosys: this.hautosys,
      t7: this.global.region
    };

    this.http.post(this.ipaddress + '/service001/getRegister', parameter).map(res => res.json()).subscribe(result => {
      if (result != null && result.messageCode != null && result.messageCode == "0000") {
        this.registerData.syskey = result.syskey;
        this.registerData.phone = result.userID;
        this.registerData.username = result.userName;
        this.registerData.realname = result.t3;
        this.registerData.stateCaption = result.t7;
        this.registerData.photo = result.t16;
        this.registerData.fathername = result.t20;
        this.registerData.nrc = result.t21;
        this.registerData.id = result.t22;
        this.registerData.passbook = result.t23;
        this.registerData.address = result.t24;
        this.registerData.dob = result.t25;
        this.registerData.city = result.t8;
        this.registerData.t1 = result.t26;
        this.registerData.t2 = result.t27;
        this.registerData.t3 = result.t28;
        this.registerData.t4 = result.t29;
        this.registerData.t5 = result.t30;
        //this.registerData.state = result.t36;
        this.registerData.status = result.t38;
        this.registerData.n1 = result.t76;
        this.registerData.n2 = result.t77;
        this.registerData.n3 = result.t78;
        this.registerData.n4 = result.t79;
        this.registerData.n5 = result.t80;

        this.storage.set("uari", this.registerData);
        this.events.publish("uari", this.registerData);
      } else {
        this.storage.set("uari", null);
        this.events.publish("uari", null);
      }
    },
      error => {
        this.storage.set("uari", null);
        this.events.publish("uari", null);

        this.getError(error);
      });
  } 

  nrcAction() {
    let actionSheet = this.actionSheetCtrl.create({
      // title: 'Languages',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Scanning NRC front',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.imgAction('front');
          }
        },
        {
          text: 'Scanning NRC back',
          icon: !this.platform.is('ios') ? 'images' : null,
          handler: () => {
            this.imgAction('back');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {           
          }
        }
      ]
    });

    actionSheet.present();
  }

  imgAction(paramSide) {
    let actionSheet = this.actionSheetCtrl.create({
      // title: 'Languages',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Take new picture',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.goCamera(paramSide);
          }
        },
        {
          text: 'Select new from gallery',
          icon: !this.platform.is('ios') ? 'images' : null,
          handler: () => {
            this.goGallery(paramSide);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
          }
        }
      ]
    });

    actionSheet.present();
  }

  convertToBase64(url, outputFormat) {
    return new Promise((resolve, reject) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = function () {
        let canvas = <HTMLCanvasElement>document.createElement('CANVAS'),
          ctx = canvas.getContext('2d'),
          dataURL;
        canvas.height = img.height;
        canvas.width = img.width;
        ctx.drawImage(img, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        canvas = null;
        resolve(dataURL);
      };
      img.src = url;
    });
  }

  goCamera(paramSide) {
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 600,
      targetHeight: 600,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      this.convertToBase64(imageData, 'image/jpg').then(
        data1 => {
          let reallyImageData = imageData;
          let imageBaseOnly = data1.toString();
          imageBaseOnly = imageBaseOnly.substring(imageBaseOnly.indexOf(',') + 1);
          this.base64Image = imageBaseOnly;
          this.sendImage(paramSide, 'camera', reallyImageData);
        }
      );
    }, (error) => {
      let toast = this.toastCtrl.create({
        message: "Taking photo is not successful.",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
    });
  }

  goGallery(paramSide) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 600,
      targetHeight: 600,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      this.convertToBase64(imageData, 'image/jpg').then(
        data1 => {
          let reallyImageData = imageData;
          let imageBaseOnly = data1.toString();
          imageBaseOnly = imageBaseOnly.substring(imageBaseOnly.indexOf(',') + 1);
          this.base64Image = imageBaseOnly;
          this.sendImage(paramSide, 'gallery', reallyImageData);
        }
      );
    }, (error) => {
      let toast = this.toastCtrl.create({
        message: "Choosing photo is not successful.",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
    });
  }

  copyFileToLocalDir(namePath, currentName, newFileName, type) {
    if (type == 'front') {
      this.tempPhoto1 = this.profile1;
      this.registerData.t1 = this.photoName;
    } else if (type == 'back') {
      this.tempPhoto2 = this.profile2;
      this.registerData.t2 = this.photoName;
    }

    this.file.copyFile(namePath, currentName, this.file.cacheDirectory, newFileName).then(success => {
      this.localStoragePhoto = success.nativeURL;
    }, error => {
      this.localStoragePhoto = "";
    });
  }

  createPhotoName() {
    let photoName = this.util.getImageName() + this.hautosys + ".jpg";
    this.photoName = this.util.getImageName() + this.hautosys + ".jpg";
    return photoName;
  }

  sendImage(paramSide, media, reallyImageData) {
    let parameter =
    {
      "image": this.base64Image
    }
    this.http.post(this.ipaddress + '/ocr/sendImageOCR', parameter).map(res => res.json()).subscribe(response => {
      if (response.code != undefined && response.code != null && response.code != '' && response.code == '0000') {
        if (response.side == paramSide && response.side == 'front') {
          this.side = "front";
          this.registerData.realname = response.name;
          this.registerData.nrc = response.nrcID;
          this.registerData.dob = response.birth;
          this.registerData.fathername = response.fatherName;
        } else if (response.side == paramSide && response.side == 'back') {
          this.side = "back";
          this.registerData.address = response.address;
        } else {
          this.showToast("Please try again!");
        }

        if ((response.side == paramSide && response.side == 'front') || (response.side == paramSide && response.side == 'back')) {
          this.savePhoto(media, response.side, reallyImageData);
        }
      } else if (response.code != "0000") {
        this.showToast(response.desc);
      } else {
        this.showToast("Please try again.");
      }
    },
      error => {
        this.showToast("Please try again.");
      }
    );
  }

  savePhoto(media, type, imageData) {
    if (type == 'front') {
      this.profileImg1 = imageData;
      this.profile1 = imageData;
    } else if (type == 'back') {
      this.profileImg2 = imageData;
      this.profile2 = imageData;
    }

    let currentName, correctPath = '';

    if (media == 'camera') {
      currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
      correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
    } else if (media == 'gallery') {
      currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      correctPath = imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
    }
    this.copyFileToLocalDir(correctPath, currentName, this.createPhotoName(), type);
  }

  showToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });

    toast.present(toast);
  }

  uploadPhoto1(side, image, statusFromUser) {
    let profileImg = '';
    let profile = '';

    if (side == '1') {
      profileImg = this.profileImg1;
      profile = this.profile1;
    } else if (side == '2') {
      profileImg = this.profileImg2;
      profile = this.profile2;
    }
    var url = this.ipaddress + '/service001/uploadNRC';
    if (profileImg != undefined && profileImg != '' && profileImg != null) {
      let options: FileUploadOptions = {
        fileKey: 'file',
        fileName: image,
        chunkedMode: false,
      }
      //const fileTransfer: TransferObject = this.transfer.create();
      const fileTransfer: FileTransferObject = this.transfer.create();
      fileTransfer.upload(profile, url, options).then((data) => {
        this.uploadPhoto2('2', this.registerData.t2, statusFromUser);

        this.closeProcessingLoading();

        let toast = this.toastCtrl.create({
          message: "Uploaded successfully.",
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
      },
        error => {
          this.closeProcessingLoading();

          let toast = this.toastCtrl.create({
            message: "Uploading photo is not successful.",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });

          toast.present(toast);
        }
      );
    } else {
      this.closeProcessingLoading();

      let toast = this.toastCtrl.create({
        message: "Uploading photo is not successful.",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
    }
  }

  uploadPhoto2(side, image, statusFromUser) {
    let profileImg = '';
    let profile = '';

    if (side == '1') {
      profileImg = this.profileImg1;
      profile = this.profile1;
    } else if (side == '2') {
      profileImg = this.profileImg2;
      profile = this.profile2;
    }

    var url = this.ipaddress + '/service001/uploadNRC';

    if (profileImg != undefined && profileImg != '' && profileImg != null) {
      let options: FileUploadOptions = {
        fileKey: 'file',
        fileName: image,
        chunkedMode: false,
      }

      //const fileTransfer: TransferObject = this.transfer.create();
      const fileTransfer: FileTransferObject = this.transfer.create();
      fileTransfer.upload(profile, url, options).then((data) => {
        this.goSave(statusFromUser);

        this.closeProcessingLoading();

        let toast = this.toastCtrl.create({
          message: "Uploaded successfully.",
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
      },
        error => {
          this.closeProcessingLoading();

          let toast = this.toastCtrl.create({
            message: "Uploading photo is not successful.",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });

          toast.present(toast);
        }
      );
    } else {
      this.closeProcessingLoading();

      let toast = this.toastCtrl.create({
        message: "Uploading photo is not successful.",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
    }
  }

  /* viewImage(type) {
    let imageLinkOne = '';
    if (type == '1') {
      imageLinkOne = this.imageLink + this.registerData.t1;
    } else if (type == '2') {
      imageLinkOne = this.imageLink + this.registerData.t2;
    }
    this.navCtrl.push(ViewPhotoMessagePage, {
      data: imageLinkOne,
      contentImg: "singlePhoto"
    });
  } */

}
