import { Component, ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';
import { IonicPage, NavController, NavParams, AlertController, PopoverController, ViewController, Platform, ToastController, Events, LoadingController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { SQLite } from '@ionic-native/sqlite';
import { GlobalProvider } from '../../providers/global/global';
/* import { PopoverPage } from '../popover/popover'; */
import { CommentPage } from '../comment/comment';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import { FunctProvider } from '../../providers/funct/funct';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
//import {trigger,state,style,animate,transition} from '@angular/animations';
//import { SettingsProvider } from './../../providers/settings/settings';
import { SugvideoPage } from '../sugvideo/sugvideo';

@Component({
  selector: 'page-videodetail',
  templateUrl: 'videodetail.html',
  providers: [ChangelanguageProvider]
})
export class VideodetailPage {
  @ViewChild(Content) content: Content;
  @ViewChild('videoPlayer') videoplayer: any;

  rootNavCtrl: NavController;
  pageStatus: any;
  registerData: any;
  popover: any;
  start: any = 0;
  end: any = 0;
  nores: any;
  isLoading: any;
  url: any = '';
  menuList: any = [];
  font: any;
  textMyan: any = ["ဗီဒီယို", "ကြိုက်တယ်", "မှတ်ချက်", "ဝေမျှမည်", "ဒေတာ မရှိပါ။", "ကြည့်ရန်", "ခု"];
  textEng: any = ["Video", "Like", "Comment", "Share", "No result found", "View", "Count"];
  textData: any = [];
  videoLink: SafeResourceUrl;
  photoLink: any;
  videoImgLink: any;
  noticount: any;
  db: any;
  videoData: any = [];
  vData: any = [];
  img: any = '';

  youtubeurl: any = [];
  index: any;
  sContent: any;
  mDate: any;
  mTime: any;
  ulink: any;
  sLike: any;
  sCmt: any;
  lCount: any;
  onscroll: boolean = false;
  ind: any = 0;
  sread: any;
  para: any;

  scrTop: number = 0;
  moreVdo: any = '';
  disabled: boolean = false;
  selectedTheme: String;
  passData: any;
  vLink: any;
  backcolor: string = '#000000';
  buttonColor: string = '#000000';
  ipaddress: string = '';
  currentPlayingVideo: HTMLVideoElement;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public popoverCtrl: PopoverController, public platform: Platform,
    public storage: Storage, public toastCtrl: ToastController,
    public http: Http, public events: Events,
    public funct: FunctProvider, public alert: AlertController,
    public changeLanguage: ChangelanguageProvider, public global: GlobalProvider,
    public sanitizer: DomSanitizer, public sqlite: SQLite) {
    // this.settings.getActiveTheme().subscribe(val => this.selectedTheme = val);
    
    this.storage.get('ipaddressCMS').then((ip) => {
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressCMS;
      }
    });

    this.passData = this.navParams.get("url");

    ////console.log("video detail == " + JSON.stringify(this.passData));
    this.vLink = this.global.digitalMedia + "upload/video/";    
	
    // this.ulink = this.passData.t8.changingThisBreaksApplicationSecurity;
    if (this.passData != '' && this.passData != 'undefined' && this.passData != null) {
      if (this.passData.n10 == 1) {
        this.passData.playvideoLink= this.passData.t8.changingThisBreaksApplicationSecurity;
      }
      else if (this.passData.n10 == 2) {
        this.passData.playvideoLink = this.passData.t8.changingThisBreaksApplicationSecurity;
        ////console.log("this is u link == " + this.passData.playvideoLink);
      }
      else {
        //if (this.passData.uploadedPhoto[0].t2 != '') {
          this.passData.playvideoLink = this.vLink + this.passData.uploadedPhoto[0].t2;
        //}
        if(this.passData !='' && this.passData !=undefined){
          if (this.passData.t2.replace(/<\/?[^>]+(>|$)/g, "").length > 200) {
            this.passData.seeMore = true;
          }
          else
          {
            this.passData.seeMore = false;
          } 
          if(this.passData.t2.indexOf("<img ") >-1){
            this.passData.t2 = this.passData.t2.replace(/<img /g,"<i ");
            console.log("replace img == "+this.passData.t2);
            this.passData.t2 = this.passData.t2.replace(/ \/>/g,"></i>");
            console.log("replace <i/> == "+this.passData.t2);
          }
        }
      }
    }

    this.storage.get('language').then((font) => {
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then((data) => {
        this.textData = data;
        if (font != "zg")
          this.font = 'uni';
        else
          this.font = font;
        ////console.log("data language=" + JSON.stringify(data));
      });
    });

    this.storage.get('appData').then((data) => {
      this.registerData = data;
      ////console.log("registerData = " + JSON.stringify(this.registerData));
    });

    this.events.subscribe('changelanguage', language => {
     // //console.log("change language = " + language);
      this.changeLanguage.changelanguage(language, this.textEng, this.textMyan).then((data) => {
        this.textData = data;
        if (language != "zg")
          this.font = 'uni';
        else
          this.font = language;
        ////console.log("data language=" + JSON.stringify(data));
      });
    })

  }
  ionViewDidEnter() {
    this.platform.registerBackButtonAction(() => {
      ////console.log("Active Page=" + this.navCtrl.getActive().name);
      this.navCtrl.pop();
    });

  }
  getError(error) {
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
    });
    toast.present(toast);
    this.isLoading = false;
    ////console.log("Oops!");
  }


  ionViewDidLoad() {
    ////console.log('ionViewDidLoad VideoPage');
  }

  clickLike(data) {
    ////console.log("data=" + JSON.stringify(data));
    if (!data.showLike) {
      this.passData.showLike = true;
      this.passData.n2 = this.passData.n2 + 1;
      //this.passData.likeCount = this.funct.getChangeCount(this.passData.n2);
      this.passData.likeCount = this.passData.n2;
      this.getLike(data);
    }
    else {
      this.passData.showLike = false;
      this.passData.n2 = this.passData.n2 - 1;
      //this.passData.likeCount = this.funct.getChangeCount(this.passData.n2);
      this.passData.likeCount = this.passData.n2;
      this.getUnlike(data);
    }
  }

  getLike(data) {
    let parameter = {
      key: data.syskey,
      userSK: this.registerData.usersyskey,
      type: data.t3
    }
    ////console.log("request clickLike = ", JSON.stringify(parameter));
    this.http.get(this.ipaddress + '/serviceArticle/clickLikeArticle?key=' + data.syskey + '&userSK=' + this.registerData.usersyskey + '&type=' + data.t3).map(res => res.json()).subscribe(data => {
      ////console.log("response clickLike = ", JSON.stringify(data));
      if (data.state) {
        this.passData.showLike = true;
      }
      else {
        this.passData.showLike = false;
        this.passData.n2 = this.passData.n2 - 1;
      }
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  getUnlike(data) {
    let parameter = {
      key: data.syskey,
      userSK: this.registerData.usersyskey,
      type: data.t3
    }
    ////console.log("request clickLUnlike = ", JSON.stringify(parameter));
    this.http.get(this.ipaddress + '/serviceArticle/clickUnlikeArticle?key=' + data.syskey + '&userSK=' + this.registerData.usersyskey + '&type=' + data.t3).map(res => res.json()).subscribe(data => {
     // //console.log("response clickLUnlike = ", JSON.stringify(data));
      if (data.state) {
        this.passData.showLike = false;
      }
      else {
        this.passData.showLike = true;
        this.passData.n2 = this.passData.n2 + 1;
      }
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  continue(i) {
    //  this.videoplayer.nativeElement.pause();
    if (this.passData.n10 != 2 && this.currentPlayingVideo != undefined) {
      //this.videoplayer.nativeElement.pause();
      this.currentPlayingVideo.pause();
    }
    this.navCtrl.push(CommentPage, {
      data: i,
      title: "detail"
    });
  }

  share(i) {
    //     //console.log("this is share url == "+JSON.stringify(i)+"  //////////////  url === "+JSON.stringify(i.t8));


    //     let sahareImg = this.funct.imglink+"upload/image/mit_logo.png";
    //             if(i.uploadedPhoto.length>0){
    //                 sahareImg = i.uploadedPhoto[0].t7;
    //             }


    // const Branch = window['Branch'];
    //  //  this.url = "https://b2b101.app.link/R87NzKABHL";

    //    var propertiesObj = {
    //      canonicalIdentifier: 'content/123',
    //      canonicalUrl: 'https://example.com/content/123',
    //      title:  i.t1,
    //      contentDescription: '' + Date.now(),
    //      contentImageUrl: sahareImg,
    //      price: 12.12,
    //      currency: 'GBD',
    //      contentIndexingMode: 'private',
    //      contentMetadata: {
    //        custom: 'data',
    //        testing: i.syskey,
    //        this_is: true
    //      }
    //    }

    //    // create a branchUniversalObj variable to reference with other Branch methods
    //    var branchUniversalObj = null
    //    Branch.createBranchUniversalObject(propertiesObj).then(function (res) {
    //      branchUniversalObj = res
    //        //console.log('Response1: ' + JSON.stringify(res))
    //       // optional fields
    //          var analytics = {
    //            channel: 'facebook',
    //            feature: 'onboarding',
    //            campaign: 'content 123 launch',
    //            stage: 'new user',
    //            tags: ['one', 'two', 'three']
    //          }

    //          // optional fields
    //          var properties1 = {
    //            $desktop_url: 'http://www.example.com/desktop',
    //            $android_url: 'http://www.example.com/android',
    //            $ios_url: 'http://www.example.com/ios',
    //            $ipad_url: 'http://www.example.com/ipad',
    //            $deeplink_path: 'content/123',
    //            $match_duration: 2000,
    //            custom_string: i.syskey,
    //            custom_integer: Date.now(),
    //            custom_boolean: true
    //          }

    //          branchUniversalObj.generateShortUrl(analytics, properties1).then(function (res) {

    //             //console.log('Response2: ' + JSON.stringify(res.url));
    //            // optional fields
    //            var analytics = {
    //              channel: 'facebook',
    //              feature: 'onboarding',
    //              campaign: 'content 123 launch',
    //              stage: 'new user',
    //              tags: ['one', 'two', 'three']
    //            }

    //            // optional fields
    //            var properties = {
    //              $desktop_url: 'http://www.example.com/desktop',
    //              custom_string: i.syskey,
    //              custom_integer: Date.now(),
    //              custom_boolean: true
    //            }

    //            var message = 'Check out this link'

    //            // optional listeners (must be called before showShareSheet)
    //            branchUniversalObj.onShareSheetLaunched(function (res) {
    //              // android only
    //              //console.log(res)
    //            })
    //            branchUniversalObj.onShareSheetDismissed(function (res) {
    //              //console.log(res)
    //            })
    //            branchUniversalObj.onLinkShareResponse(function (res) {
    //              //console.log(res)
    //            })
    //            branchUniversalObj.onChannelSelected(function (res) {
    //              // android only
    //              //console.log(res)
    //            })

    //            // share sheet
    //            branchUniversalObj.showShareSheet(analytics, properties, message)
    //          }).catch(function (err) {
    //            console.error('Error2: ' + JSON.stringify(err))
    //          })
    //    }).catch(function (err) {
    //      console.error('Error1: ' + JSON.stringify(err))
    //    })
  }

 onPlayingVideo(event,index) {
    console.log("index="+index)
    event.preventDefault();
    if (this.currentPlayingVideo === undefined) {
      console.log("L");
      this.currentPlayingVideo = event.target;
      this.currentPlayingVideo.play();
    } else {
      console.log("A");
      if (event.target !== this.currentPlayingVideo) {
        this.currentPlayingVideo.pause();
        this.currentPlayingVideo = event.target;
      }
    }
  }
  clickBookMark(data) {
    ////console.log("data=" + JSON.stringify(data));
    if (!data.showContent) {
      this.passData.showContent = true;
      this.saveContent(data);
    }
    else {
      this.passData.showContent = false;
      this.unsaveContent(data);
    }
  }

  saveContent(data) {
    let parameter = {
      t1: data.t3,
      t4: this.registerData.t3,
      n1: this.registerData.usersyskey,
      n2: data.syskey,
      n3: 1

    }
    ////console.log("request saveContent parameter= ", JSON.stringify(parameter));
    this.http.post(this.ipaddress + '/serviceContent/saveContent', parameter).map(res => res.json()).subscribe(data => {
      ////console.log("response saveContent = ", JSON.stringify(data));
      // this.isLoading = false;
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  unsaveContent(data) {
    ////console.log("request unsaveContent = ", JSON.stringify(data));
    let parameter = {
      t1: data.t3,
      t4: this.registerData.t3,
      n1: this.registerData.usersyskey,
      n2: data.syskey,
      n3: 0

    }
    ////console.log("request unsaveContent parameter = ", JSON.stringify(parameter));
    this.http.post(this.ipaddress + '/serviceContent/unsaveContent', parameter).map(res => res.json()).subscribe(data => {
      ////console.log("response unsaveContent = ", JSON.stringify(data));

      //this.isLoading = false;
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  onScroll(e) {
    this.content.ionScrollEnd.subscribe((data) => {
      let dimensions = this.content.getContentDimensions();
      let scrollTop = this.content.scrollTop;
      ////console.log("Scroll Top " + scrollTop);
      let contentHeight = dimensions.contentHeight;
      ////console.log("contentHeight " + scrollTop);
      let scrollHeight = dimensions.scrollHeight;
     // //console.log("scrollHeight " + scrollTop);
    });
    if (e.scrollTop <= 123) {
      this.onscroll = false;
      this.moreVdo = "";
      this.videoplayer.nativeElement.play();
      this.buttonColor = '#000000';
    }
    else if (e.scrollTop > 123) {
      this.onscroll = true;
      this.moreVdo = "More Videos";
      this.videoplayer.nativeElement.pause();
     // //console.log("BUTTON COLOR" + this.buttonColor);
      this.buttonColor = '#345465';
      ////console.log("BUTTON COLOR" + this.buttonColor);
    }
    else
      this.onscroll = false;
  }

  addEvent() {
    this.buttonColor = '#345465';
  }

  goDetail() {
    // this.videoplayer.nativeElement.pause();
    if (this.passData.n10 != 2) {
      this.videoplayer.nativeElement.pause();
    }
    this.navCtrl.push(SugvideoPage, {
      //data: this.menuList
    });
  }


}
