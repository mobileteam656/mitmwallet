import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, Platform, Events, AlertController, ToastController, LoadingController, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Login } from '../login/login';
import { GlobalProvider } from '../../providers/global/global';
import { LocationPage } from '../location/location';
import { NearByPage } from '../near-by/near-by';
import { MapPage } from '../map/map'
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
  selector: 'page-cash-out',
  templateUrl: 'cash-out.html',
})
export class CashOutPage {

  textMyan: any = [ "ငွေထုတ်", "YCB Payကို အသုံးပြုပြီးငွေဘယ်လိုထုတ်မလဲ?", "၁။ နီးစပ်ရာကိုယ်စားလှယ်ရှာမည် ဤစာမျက်နှာပေါ်တွင်ရှိသော 'နီးစပ်ရာကိုယ်စားလှယ်ကိုရှာပါ' ဆိုသောခလုပ်သို့မဟုတ် ပင်မစာမျက်နှာပေါ်တွင်ရှိသော 'နီးစပ်ရာ' ခလုပ်အားနှိပ်ပြီး ကိုယ်စားလှယ်၏တည်နေရာရှာဖွေပါ။",
  "၂။ ကိုယ်စားလှယ်အားပိုက်ဆံပေးပါ ကိုယ်စားလှယ်၏ကုဒ်အတိုကိုရိုက်ထည့်ပါ သို့မဟုတ် QR ကို scan လုပ်ပြီးကိုယ်စားလှယ်၏ အမည်ကိုစစ်ဆေးပါ။ထို့နောက်ငွေလွှမည့်ပမာဏအားရွေးချယ်(သို့)ရိုက်ထည့်ပြီးပေးချေရန်အတည်ပြုပါ။",
   "၃။ ငွေထုတ်ခြင်းပြီးမြောက်ပါပြီ။","နီးစပ်ရာကိုယ်စားလှယ်ကိုရှာပါ" ];
  textEng: any = [ "Cash Out", "How to cash out at agent?", 
  "1.Find Nearby Agent Click on Search button for nearby agentson this page or Near By button on homepage to get the location of agent.",
  "2.Give money to Agent Enter agent's short code or scan QR and review the Agent name. Then select or enter amount of cash out and confirm to pay.",
  "3.Finish Cash Out Agent will receive SMS of cash out. The agent will then delivery cash to the user. User will receive cash from the agent.",
  "Search for nearby Agent"];
  textData: any = [
    { title: 'Agent', icon: 'ios-briefcase', status: 1, otpstatus: 5 },
    { title: 'BankAccount', icon: 'md-contacts', status: 2, otpstatus: 6 },    
  ];

  showFont: string[] = [];
  font: string = '';
  popover: any;
  modal: any;
  userdata: any;
  hardwareBackBtn: any = true;
  loading: any;
  ipaddress: any;
  location: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController, public platform: Platform,
    public storage: Storage, public all: AllserviceProvider, public events: Events, public alertCtrl: AlertController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public http: Http,
    public global: GlobalProvider, private appCtrl: App,private firebase: FirebaseAnalytics) {
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      });
      this.storage.get('language').then((lan) => {
        this.changelanguage(lan);
      });
      this.location = "Agent";
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }

  ionViewDidLoad() {
   
  }  

  backButtonAction() {
    if (this.modal && this.modal.index === 0) {
      this.modal.dismiss();
    } else {      
      if (this.hardwareBackBtn) {
        let alert = this.alertCtrl.create({
          title: 'Are you sure you want to exit',
          enableBackdropDismiss: false,
          message: '',
          buttons: [{
            text: 'No',
            handler: () => {
              this.hardwareBackBtn = true;
            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.gologout();
            }
          }]
        });
        alert.present();
        this.hardwareBackBtn = false;
      }
    }
  }
  gologout() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true      
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = ipaddress;
      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        // this.firebase.logEvent('log_out', { userID: this.userdata.userID})
        // .then((res: any) => { console.log(res); })
        // .catch((error: any) => console.error(error));
        if (data.code == "0000") {
          this.loading.dismiss();
          // this.firebase.logEvent('log_out', {log_out:'logout-user'});
          // this.storage.remove('phonenumber');
          this.storage.remove('username');
          this.storage.remove('userData');
          this.storage.remove('firstlogin');
          this.appCtrl.getRootNav().setRoot(Login);
        }
        else {
          let toast = this.toastCtrl.create({
            message: data.desc,
            duration: 3000,
            position: 'bottom',           
            dismissOnPageChange: true,            
          });
          toast.present(toast);
          this.loading.dismiss();
        }
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.all.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',            
            dismissOnPageChange: true,    
          });
          toast.present(toast);
          this.loading.dismiss();
        });
    });
  }

  nearby(){
    this.navCtrl.push(NearByPage);
  }

  Start(){
    this.navCtrl.push(LocationPage,{
      location: this.location
    });
  }

}
 