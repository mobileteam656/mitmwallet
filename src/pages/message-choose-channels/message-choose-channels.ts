import { Component } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Chat } from '../chat/chat';
import { FunctProvider } from '../../providers/funct/funct';
import { GlobalProvider } from '../../providers/global/global';
import { Platform, NavController, NavParams, PopoverController, Events, ToastController } from 'ionic-angular';
import { App } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
//import { Transfer } from '@ionic-native/transfer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';

@Component({
  selector: 'page-message-choose-channels',
  templateUrl: 'message-choose-channels.html',
  providers: [CreatePopoverPage],
})
export class MessageChooseChannelsPage {

  chatList: any = [];
  isLoading: any;
  userData: any;
  nores: any;
  start: any = 0;
  end: any = 0;
  ipaddress: string;
  userDataChat: any;
  channelData: any = [];
  docData: any;
  from: any;
  description: string = '';
  db: any;
  constructor(public platform: Platform, private transfer: FileTransfer,
    public popoverCtrl: PopoverController, public navCtrl: NavController,
    public navParams: NavParams, public http: Http,
    public toastCtrl: ToastController, public global: GlobalProvider,
    private file: File, public sqlite: SQLite,
    public funct: FunctProvider, public events: Events,
    public storage: Storage, public appCtrl: App,
    public createPopover: CreatePopoverPage, private camera: Camera,
    private geolocation: Geolocation) {
    //from: 'PicsDetailPage'
    //this.photo = { id: '', PhotoPath: '', name: '', latitude: '', longitude: '', location: '', CreateDate: '', CreateTime: '' };
    this.docData = this.navParams.get('data');
    this.from = this.navParams.get('from');
    this.description = this.navParams.get('description');

    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success create or open database");
      this.db = db;
      this.db.executeSql("CREATE TABLE IF NOT EXISTS channel (id INTEGER PRIMARY KEY AUTOINCREMENT,username TEXT,messagecontent TEXT)", {}).then((data) => {
       // //console.log("TABLE CREATED: ", data);
        this.selectData();
      }, (error) => {
        //console.error("Unable to execute sql", error);
      });
    }).catch(e => console.log(e));

    this.storage.get('userData').then((userData) => {
      ////console.log('Your userData is message.ts', userData);
      if (userData !== undefined && userData !== "") {
        this.userDataChat = userData;
        this.start = this.start + 11;
        this.getchatUserList(this.start);
      }
    });
  }

  ionViewCanEnter() {
    this.storage.get("phonenumber").then((data) => {
      ////console.log("ionViewCanEnter this.storage.get(phonenumber) in message.ts = " + JSON.stringify(data));
      this.userData = data;
      this.start = 0;
      this.end = 0;

      this.ipaddress = this.global.ipaddressChat;
    });
  }

  insertData() {
    ////console.log("channel data length = ", this.chatList.length);

    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success create or open database");
      for (let i = 0; i < this.chatList.length; i++) {

        db.executeSql("INSERT INTO channel(messagecontent) VALUES (?)", [JSON.stringify(this.chatList[i])]).then((data) => {
          ////console.log("Insert data successfully", data);
        }, (error) => {
          //console.error("Unable to insert data", error);
        });
      }
    }).catch(e => console.log(e));
  }

  deleteData() {
    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
     // //console.log("success create or open database");

      db.executeSql("DELETE FROM channel", []).then((data) => {
        ////console.log("Delete data successfully", data);
        this.insertData();
      }, (error) => {
       // console.error("Unable to delete data", error);
      });
    }).catch(e => console.log(e));
  }

  selectData() {
    ////console.log("select data from sqlite");
    this.chatList = [];

    this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success create or open database");
      db.executeSql("SELECT * FROM channel ", []).then((data) => {
        ////console.log("length == " + data.rows.length);
        if (data.rows.length > 0) {
          for (var i = 0; i < data.rows.length; i++) {
            ////console.log("data.rows.item(i).allData == " + data.rows.item(i).messagecontent);
            this.channelData.push(JSON.parse(data.rows.item(i).messagecontent));
            ////console.log("Channel Data " + JSON.stringify(this.channelData));
            this.chatList = this.channelData;
          }
          this.nores = 1;
        }
      });
    }).catch(e => console.log(e));
  }

  getchatUserList(start) {
    this.storage.get('userData').then((userData) => {
      ////console.log('Your userData is message.ts in getchatUserList', userData);
      if (userData !== undefined && userData !== "") {
        this.userDataChat = userData;
      }
    });
    // this.isLoading = true;
    this.end = this.end + 10;
    let msgparams = {
      start: start,
      end: this.end
    }
    ////console.log("request con=" + this.userDataChat.sKey);
    //searchChatList
    this.http.post(this.global.ipaddress3 + "serviceChat/searchChatList1?syskey=" + this.userDataChat.sKey + "&role=" + "&domain=" + this.global.domainChat + "&appId=" + this.global.appIDChat, msgparams).map(res => res.json()).subscribe(result => {
      ////console.log("response chatList =>" + JSON.stringify(result));
      if (result.data == undefined || result.data == null || result.data == '') {
        this.deleteData();
        this.nores = 0;
      } else {
        let tempArray = [];
        if (!Array.isArray(result.data)) {
          tempArray.push(result.data);
          result.data = tempArray;
        }

        for (let i = 0; i < result.data.length; i++) {
          if (this.global.regionCode == '13000000') {
            //Yangon App (13000000)
            if (!(result.data[i].t1.includes("YCDC"))) {
              result.data.splice(i, 1);
              i--;
            }
          }
          if (this.global.regionCode == '10000000') {
            //Mandalay App (10000000)
            if (!(result.data[i].t1.includes("MCDC"))) {
              result.data.splice(i, 1);
              i--;
            }
          }
          if (this.global.regionCode == '00000000') {
            //All App (00000000)
            if (result.data[i].t1 != "general@dc.com") {
              result.data.splice(i, 1);
              i--;
            }
          }
        }

        if (result.data.length > 0) {
          this.chatList = result.data;
          ////console.log("My Chat List is = " + JSON.stringify(this.chatList));
          this.getGroupData();
          this.deleteData();
          this.nores = 1;
          //   //this.deleteTable();
        }
      }
      this.isLoading = false;
    }, error => {
      ////console.log("signin error=" + error.status);
      //this.nores = 0;
      this.getError(error);
    });
  }
  from_Mydocs: any = { fromName: '' };
  description_Mydocs: any = { description: '' };
  viewChat(d) {
    this.from_Mydocs.fromName = 'MessageChooseChannelsPage';
    this.description_Mydocs.description = this.description;
    ////console.log("pass data (MessageChooseChannelsPage)=" + JSON.stringify(d));
    this.navCtrl.push(Chat, {
      data: d,
      docData: this.docData,
      from: this.from_Mydocs,
      description: this.description_Mydocs
    });
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      showCloseButton: true,
      dismissOnPageChange: true,
      closeButtonText: 'OK'
    });
    // toast.present(toast);
    this.isLoading = false;
    // this.loading.dismiss();
    ////console.log("Oops!");
  }

  doRefresh(refresher) {
    ////console.log('Begin async operation', refresher);
    this.start = this.start + 11;
    this.getchatUserList(this.start);
    setTimeout(() => {
      ////console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  getGroupData() {
    let monthNames = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    for (let i = 0; i < this.chatList.length; i++) {
      let modifiedDateStr = this.chatList[i].modifiedDate;

      // if (this.isToday(modifiedDateStr)) {
      //   this.chatList[i].modifiedDate = time;
      // } else {
      let monthStr = modifiedDateStr.substring(4, 6);

      let monthInt = parseInt(monthStr);

      let month = monthNames[monthInt];

      let dateStr = modifiedDateStr.substring(6);

      let date = parseInt(dateStr);

      this.chatList[i].modifiedDate = date + " " + month;
      // }
    }

    ////console.log("chatList=" + JSON.stringify(this.chatList));

    let sortedContacts = this.chatList.sort(function (a, b) {
      if (a.t16 < b.t16) return 1;
      if (a.t16 > b.t16) return -1;
      return 0;
    });

    let group_to_values = sortedContacts.reduce(function (obj, item) {
      obj[item.t16] = obj[item.t16] || [];
      obj[item.t16].push(item);
      return obj;
    }, {});

    let groups = Object.keys(group_to_values).map(function (key) {
      ////console.log("group_to_values" + JSON.stringify(group_to_values));

      return { t16: key, data: group_to_values[key] };
    });

    this.chatList = groups;

    ////console.log("allcontacts List=" + JSON.stringify(this.chatList));
  }

  ionViewWillEnter() {
    this.storage.get('userData').then((userData) => {
      ////console.log('Your userData is message.ts in getchatUserList', userData);
      if (userData !== undefined && userData !== "") {
        this.userDataChat = userData;
        this.start = this.start + 11;
        this.getchatUserList(this.start);
      }
    });
  }

  isToday(yyyymmdd) {
    /* let yyyyParam = yyyymmdd.substring(0, 4);
    let mmParam = yyyymmdd.substring(4, 6);
    let ddParam = yyyymmdd.substring(6); */

    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let yyyy = today.getFullYear();

    let todayString = "" + yyyy + mm + dd;

    if (todayString == yyyymmdd) {
      return true;
    } else {
      return false;
    }
  }

}