import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, Events, Badge} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { UtilProvider } from '../../providers/util/util';
import 'rxjs/add/operator/map';
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html'
})
export class NotificationPage {
  textEng: any = ["Notification"];
  textMyan: any = ["အသိပေးချက်"];
  showFont: string[] = [];
  fcmtoken:any;
  btnMenu: boolean;
  carryData: any = [];
  errormsg: string = '';
  userData: any = {};
  isLoading: any;
  ipaddress: any;
  shownGroup: any = null;
  userID: string;
  loading: any;
  _obj: any;
  currentpage: any = 1;
  pageSize: any = 10;
 
  items: any = [{ 'name': '', 'icon': 'ios-timer-outline', 'data': [] },
  { 'name': '', 'icon': 'ios-create-outline', 'data': [] },
  { 'name': '', 'icon': 'ios-list-box-outline', 'data': [] },
  { 'name': '', 'icon': 'ios-clipboard-outline', 'data': [] },
  { 'name': '', 'icon': 'ios-contact-outline', 'data': [] }];
  notiData: any = { "empSK": 0, "empname": "", "count": 0, "noti": [] };
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public storage: Storage, public http: Http, public global: GlobalProvider, 
    public toastCtrl: ToastController, public util: UtilProvider, public loadingCtrl: LoadingController,public events: Events) {
    this.storage.get('phonenumber').then((phonenumber) => {
      this.userID = phonenumber;
      ////console.log("Transfer userData ID=" + JSON.stringify(this.userID));
    });
    this.storage.get('userData').then((userData) => {
      this._obj = userData;
      this.storage.get('ipaddress').then((ip) => {
        if (ip !== undefined && ip != null && ip !== "") {
          this.ipaddress = ip;
          console.log('IP is: '+this.ipaddress);
          this.getNotiData();
        } else {
          this.ipaddress = this.global.ipaddress;
          this.getNotiData();
        }
      });
       
    });   
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

  }
  itemTapped(event, page) {
    this.navCtrl.push(page.component);
  }

  toggleGroup(i) {
    if (this.isGroupShown(i)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = i;
    }
  }
  isGroupShown(group) {
    return this.shownGroup === group;
  }

  parse(str) {
    let y, m, d;
    if (!/^(\d){8}$/.test(str))
      return "invalid date";
    y = str.substr(0, 4);
    m = parseInt(str.substr(4, 2)) - 1;
    d = str.substr(6, 2);
    if (parseInt(str.substr(4, 2)) < 10)
      m = '0' + m;
    return d + "/" + m + "/" + y;
  }
  ymd(str) {
    if (!/^(\d){8}$/.test(str))
      return "invalid date";
    let y = str.substr(0, 4);
    let m = parseInt(str.substr(4, 2)) - 1;
    let d = str.substr(6, 2);
    return y + '' + m + '' + d;

  }
 
  getNotiData(){
    var param = {
      userID: this._obj.userID,
      sessionID: this._obj.sessionID,
      customerNo: '',
      totalCount: 0,
      currentPage: this.currentpage,
      pageSize: this.pageSize,
      pageCount: 0,
    };
    //msgCode
    this.http.post(this.ipaddress + '/service007/getAllTokenNotification', param).map(res => res.json()).subscribe(data => {
      if(data.msgCode == '0000'){
        if (data.data != null) {
          if (!(data.data instanceof Array)) {
            let m = [];
            m[0] = data.data;
            for(let i=0;i<m.length;i++){
              if(m[i].notiMessage!=" "){
                this.carryData[i]=m[i];
              }
            }
            //this.carryData = m;
            console.log('Notification Data'+JSON.stringify(this.carryData));
          }
          else {
            for(let i=0;i<data.data.length;i++){
              if(data.data[i].notiMessage!=""){
                this.carryData[i]=data.data[i];

              }
            }
            //this.carryData = data.data;
            console.log('Carry Notification'+JSON.stringify(this.carryData));
          }
        }
      }
      // this.loading.dismiss();
    },
      error => {
       // //console.log("notification error=" + error.status);
        let code;

        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });

  }
  
  getNotification() {
    let phone = this.userID;//this.util.removePlus(this.userID);
    let param = {
      "userID": phone,
      "sessionID": this._obj.sessionID
    };
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    ////console.log("request notification  data>>" + JSON.stringify(param));
    this.http.post(this.ipaddress + '/service001/getNotiData', param).map(res => res.json()).subscribe(data => {
    console.log("response notification  data>>" + JSON.stringify(data));
      if(data.code == '0000'){
        if (data.data != null) {
          if (!(data.data instanceof Array)) {
            let m = [];
            m[0] = data.data;
            for(let i=0;i<m.length;i++){
              if(m[i].notiMessage!=" "){
                this.carryData[i]=m[i];
              }

            }
            //this.carryData = m;
            console.log('Notification Data'+JSON.stringify(this.carryData));
          }
          else {
            for(let i=0;i<data.data.length;i++){
              if(data.data[i].notiMessage!=""){
                this.carryData[i]=data.data[i];

              }
            }
            //this.carryData = data.data;
            // console.log('Carry Notification'+JSON.stringify(this.carryData));
          }
        }
      }
      this.loading.dismiss();
    },
      error => {
       // //console.log("notification error=" + error.status);
        let code;

        if (error.status == 404) {
          code = '001';
        }
        else if (error.status == 500) {
          code = '002';
        }
        else if (error.status == 403) {
          code = '003';
        }
        else if (error.status == -1) {
          code = '004';
        }
        else if (error.status == 0) {
          code = '005';
        }
        else if (error.status == 502) {
          code = '006';
        }
        else {
          code = '000';
        }
        let msg = "Can't connect right now. [" + code + "]";
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
  }

  ionViewDidLoad() { 
    
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.showFont[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.showFont[i] = this.textMyan[i];
      }
    }
  }
}
