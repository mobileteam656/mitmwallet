import { Component } from '@angular/core';
import { ActionSheetController, Platform, LoadingController, ToastController, Events, NavController,normalizeURL } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { Camera, CameraOptions } from '@ionic-native/camera';
//import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { Changefont } from '../changefont/changeFont';
import { TabsPage } from '../tabs/tabs';


@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [CreatePopoverPage]
})

export class Profile {

  textEng: any = ["Profile", "Mobile Number", "Name", "Save", "Invalid Name."];
  textMyan: any = ["ကိုယ်ပိုင်အချက်အလက်", "ဖုန်းနံပါတ်", "နာမည်", "သိမ်းမည်", "Invalid Name."];
  textData: string[] = [];
  keyboardfont: any;
  language: any;
  showFont: any;

  ipaddress: string;
  ipaddressChat: string;
  hautosys: any;

  imglnk: any;
  photoExist: any;
  profile: any;
  photoName: any;
  profileImg: any;
  photo: any;
  localStoragePhoto: string = '';

  registerData = {
    syskey: 0, phone: "", username: "", t16: ""
  };

  successMsg: string = '';
  eMsgName: string = '';
  popover: any;
  public loading;

  registerDataLS = {
    syskey: "", phone: "", username: "", realname: "", stateCaption: "",
    photo: "", fathername: "", nrc: "", id: "", passbook: "",
    address: "", dob: "", t1: "", t2: "", t3: "",
    t4: "", t5: "", state: "", status: "", n1: "",
    n2: "", n3: "", n4: "", n5: "", city: "", phonenumber: ""
  };
  
  

  profileImage: any;
  _obj: any;
  phoneno: any;
  private postId: string;

  

  constructor(
    public loadingCtrl: LoadingController,
    //private transfer: Transfer,
    private transfer: FileTransfer,
    public changefont: Changefont,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    public util: UtilProvider,
    public platform: Platform,
    public storage: Storage,
    public toastCtrl: ToastController,
    public http: Http,
    public events: Events,
    public global: GlobalProvider,
    private file: File,
    public createPopover: CreatePopoverPage,
    public navCtrl: NavController,
   // private imageSrv: ImageProvider,
    
     
  ) {
    this.storage.get('ipaddress3').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddressChat = ip;
      } else {
        this.ipaddressChat = this.global.ipaddress3;
      }
    });
    this.storage.get('ipaddress').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress= this.global.ipaddress;
      }
    });
    this.showFont = "uni";

    this.imglnk = this.file.cacheDirectory;

    this.storage.get('hautosys').then((data) => {
      if (data.hautosys != undefined && data.hautosys != null && data.hautosys != '' && data.hautosys.length != 0) {
        this.hautosys = data.hautosys;
      }
    });
    this.events.subscribe('hautosys', data => {
      if (data.hautosys != undefined && data.hautosys != null && data.hautosys != '' && data.hautosys.length != 0) {
        this.hautosys = data.hautosys;
      }
    });

    this.storage.get('localPhotoAndLink').then((photo) => {
     // //console.log('localPhotoAndLink in profile constructor get:', photo);
      this.photo = photo;

      if (this.photo != '' && this.photo != undefined && this.photo != null && this.photo.includes("/")) {
        this.profile = this.photo;
       // //console.log('file cache directory and photo(e.g.xxx.jpg):', this.profile);
      } else if (this.photo == 'Unknown.png' || this.photo == 'user-icon.png' || this.photo == '') {
        this.profile = "assets/images/user-icon.png";
       // //console.log('Unknown.png or user-icon.png or blank string:', this.profile);
      } else {
        this.storage.get('profileImage').then((profileImage) => {
          if (profileImage != undefined && profileImage != null && profileImage != '') {
            this.profileImage = profileImage;
          } else {
            this.profileImage = this.global.digitalMediaProfile;
          }
          this.profile = this.profileImage + this.photo;
          this.download(this.profile, this.photo);
        });
       // //console.log('server link and photo(e.g.xxx.jpg):', this.profile);
      }
    });
    this.events.subscribe('localPhotoAndLink', photo => {
      ////console.log('localPhotoAndLink in profile constructor subscribe:', photo);
      this.photo = photo;

      if (this.photo != undefined && this.photo != null && this.photo != '' && this.photo.includes("/")) {
        this.profile = this.photo;
       // //console.log('file cache directory and photo(e.g.xxx.jpg):', this.profile);
      } else if (this.photo == 'Unknown.png' || this.photo == 'user-icon.png' || this.photo == '') {
        this.profile = "assets/images/user-icon.png";
      } else {
        this.storage.get('profileImage').then((profileImage) => {
          if (profileImage != undefined && profileImage != null && profileImage != '') {
            this.profileImage = profileImage;
          } else {
            this.profileImage = this.global.digitalMediaProfile;
          }
          this.profile = this.profileImage + this.photo;
          this.download(this.profile, this.photo);
        });
        //console.log('server link and photo(e.g.xxx.jpg):', this.profile);
      }//
    });
    this.storage.get('userData').then((userData) => {
      this._obj = userData;
    });
    this.storage.get('phonenumber').then((phonenumber) => {
      if (phonenumber !== undefined && phonenumber !== "" && phonenumber != null && phonenumber.length != 0) {
        this.registerData.phone = phonenumber;
        this.phoneno=this.registerData.phone;
        this.phoneno=this.phoneno;
         console.log(this.phoneno);
        //  console.log(this.registerData.phone);
        
      }
    });
    this.events.subscribe('phonenumber', phonenumber => {
      if (phonenumber !== undefined && phonenumber !== "" && phonenumber != null && phonenumber.length != 0) {
        this.registerData.phone = phonenumber;
        // console.log(this.registerData.phone);
      }
    });

    this.storage.get('username').then((username) => {
      if (username !== undefined && username !== "" && username != null && username.length != 0) {
        this.registerData.username = username;
      }
    });
    this.events.subscribe('username', username => {
      if (username !== undefined && username !== "" && username != null && username.length != 0) {
        this.registerData.username = username;
      }
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('font').then((font) => {
      this.keyboardfont = font;
    });

    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;
    });
  }

 


  changelanguage(lan) {
    this.language = lan;

    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  openProcessingLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true,
      duration: 3000
    });

    this.loading.present();
  }

  closeProcessingLoading() {
    this.loading.dismiss();
  }

  download(photoAndServerLink, photo) {
    //const fileTransfer: TransferObject = this.transfer.create();
    const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(photoAndServerLink, this.file.cacheDirectory + photo).then((entry) => {
      ////console.log('photo download success: ' + entry.toURL());
      let abc = entry.toURL();
      this.storage.set('localPhotoAndLink', abc);
      this.events.publish('localPhotoAndLink', abc);
      //this.closeProcessingLoading();
    }, (error) => {
      ////console.log('photo download error: ' + JSON.stringify(error));
      //this.closeProcessingLoading();
    });
  }

  saveUserProfile() {
    if (this.util.checkInputIsEmpty(this.registerData.username)) {
      this.eMsgName = this.textData[4];
    } else {
      this.eMsgName = "";
    }

    if (this.eMsgName == "") {
      this.saveProfile();
    }
  }

  saveProfile() {
    this.registerData.username = this.registerData.username.trim();
    if (this.keyboardfont == 'zg') {
      this.registerData.username = this.changefont.ZgtoUni(this.registerData.username);
    }

    //this.slimLoader.start(() => { });
    let chatapinameupdateparam = {
      syskey: this.hautosys,
      t1: this.registerData.phone,
      //t1: this.registerData.phone,
      t2: this.registerData.username,
      t3: this.global.domainChat,
      t4: "",
      t9: this.global.appIDChat,
      t16: "",
      t18: this.photoName
    }
////console.log("request for profile update : " + JSON.stringify(chatapinameupdateparam));
    this.openProcessingLoading();
    this.http.post(this.ipaddressChat + 'serviceChat/updateProfileforupdate', chatapinameupdateparam).map(res => res.json()).subscribe(result => {
////console.log("response for profile update >> " + JSON.stringify(result));
      if (result.sysKey != 0) {
        let phone = result.t1;//this.util.removePlus(result.t1);
        let parameter = {
          //userID: phone,
          userID:this.registerData.phone,
          syskey: this.hautosys,
          userName: this.registerData.username,
          t16: this.photoName,
          sessionID: this._obj.sessionID
        };
        ////console.log("request for saveProfile: " + JSON.stringify(parameter));
        this.http.post(this.ipaddress + '/service001/saveProfile', parameter).map(res => res.json()).subscribe(result => {
          //this.phoneno=phone.indexOf(6,4);
         // //console.log("response for saveProfile:" + JSON.stringify(result));

          if (result != null && result.messageCode != null && result.messageCode == "0000") {
            this.storage.set('username', this.registerData.username);
            this.events.publish('username', this.registerData.username);

            this.storage.get('appData').then((dataappData) => {
              if (dataappData != undefined && dataappData != null && dataappData != '') {
                let appData = { usersyskey: dataappData.usersyskey, t1: dataappData.t1, t3: this.registerData.username, syskey: dataappData.syskey, t16: dataappData.t16 };
                this.storage.set('appData', appData);
                this.events.publish('appData', appData);
              }
            });

            this.storage.get('userData').then((datauserData) => {
              if (datauserData != undefined && datauserData != null && datauserData != '') {
                let resData = {
                  "sKey": datauserData.sKey, "userID": datauserData.userID,
                  "name": this.registerData.username, "sessionID": datauserData.sessionID,
                  "accountNo": datauserData.accountNo, "balance": datauserData.balance,
                  "frominstituteCode": datauserData.frominstituteCode, "institutionName": "",
                  "field1": datauserData.field1, "field2": datauserData.field2,
                  "region": this.global.region, "t16": datauserData.t16
                };

                this.storage.set("userData", resData);
                this.events.publish('userData', resData);
              }
            });

            // this.getRegister();

            this.successMsg = result.messageDesc;
            

            this.uploadPhoto();
           // this.uploadImage(this.profile);
            let params = { tabIndex: 0, tabTitle: "Home" };
            this.navCtrl.setRoot(TabsPage, params).catch((err: any) => {
            });
          } else {
            this.closeProcessingLoading();

            let toast = this.toastCtrl.create({
              message: result.messageDesc,
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });

            toast.present(toast);

            //this.slimLoader.complete();
          }
        },
          error => {
            this.closeProcessingLoading();

            this.getError(error);

            //this.slimLoader.complete();
          });
      }
    });
  }

  uploadPhoto() {
    var url = this.ipaddress + '/service001/mobileupload';

    if (this.profileImg != undefined && this.profileImg != '' && this.profileImg != null) {
      let options: FileUploadOptions = {
        fileKey: 'file',
        fileName: this.photoName,
        chunkedMode: false,
      }

      //const fileTransfer: TransferObject = this.transfer.create();
      const fileTransfer: FileTransferObject = this.transfer.create();
      console.log("Upload Photo:" + fileTransfer + "\nprofile:" + this.profile + "\nurl:" + url + "\noptions:" + options);
      fileTransfer.upload(this.profile, url, options).then((data) => {
        let abc = this.imglnk + this.photoName;
        this.storage.set('localPhotoAndLink', abc);
        this.events.publish('localPhotoAndLink', abc);

        this.closeProcessingLoading();

        let toast = this.toastCtrl.create({
          message: this.successMsg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
        console.log("img src",abc);

        //this.slimLoader.complete();

        ////console.log("uploadPhoto upload success" + JSON.stringify(data));

        //===========================================================================
        
      },
        error => {
          this.closeProcessingLoading();

          let toast = this.toastCtrl.create({
            message: "Uploading photo is not successful.",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });

          toast.present(toast);

          //this.slimLoader.complete();

         // //console.log("uploadPhoto upload error" + JSON.stringify(error));
        }
      );
    } else {
      this.closeProcessingLoading();

      let toast = this.toastCtrl.create({
        message: this.successMsg,
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);

      //this.slimLoader.complete();
    }
  }

  imageEdit() {
    let actionSheet = this.actionSheetCtrl.create({
      // title: 'Languages',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Take new picture',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.goCamera();
          }
        },
        {
          text: 'Select new from gallery',
          icon: !this.platform.is('ios') ? 'images' : null,
          handler: () => {
            this.goGallery();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
          //  //console.log('Cancel clicked');
          }
        }
      ]
    });

    actionSheet.present();
  }

  goCamera() {
    /*const options: CameraOptions = {
      quality: 80,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      correctOrientation: false,
      targetWidth: 100,
      targetHeight: 100
    }*/
    const options: CameraOptions = {
      quality: 100,
      allowEdit : true,
      targetWidth: 300,
      targetHeight: 300,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }

    
    this.camera.getPicture(options).then((imageData) => {
      ////console.log("goCamera getPicture success:" + JSON.stringify(imageData));

      this.profileImg = imageData;
      this.profile = imageData;

      let currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
      let correctPath = this.profileImg.substr(0, imageData.lastIndexOf('/') + 1);

      this.copyFileToLocalDir(correctPath, currentName, this.createPhotoName());
      

    
  },(error) => {
     // //console.log("goCamera getPicture error:", JSON.stringify(error));

      let toast = this.toastCtrl.create({
        message: "Taking photo is not successful.",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
    });

  }
 

  goGallery() {
    /*const options: CameraOptions = {
      quality: 80,
      sourceType: 2,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      correctOrientation: false,
      targetWidth: 100,
      targetHeight: 100
    }*/

    const options:CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      targetHeight: 300,
      targetWidth: 300,
      allowEdit:true,
    }

    this.camera.getPicture(options).then((imageData) => {
     // //console.log("goGallery getPicture success:", JSON.stringify(imageData));

      this.profileImg = imageData;
      this.profile = imageData;

      /* ios */
      /* let currentName = '';
      let correctPath = '';
      if (imageData.indexOf('?') > -1) {
        currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
        correctPath = 'file:///' + imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      }
      else {
        currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
        correctPath = 'file:///' + imageData.substr(0, imageData.lastIndexOf('/') + 1);
      } */

      /* acmy */
      let currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      let correctPath = imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      ////console.log("goGallery getPicture error:", currentName + "/////////" + correctPath);
      this.copyFileToLocalDir(correctPath, currentName, this.createPhotoName());
    }, (error) => {
      ////console.log("goGallery getPicture error:", JSON.stringify(error));

      let toast = this.toastCtrl.create({
        message: "Choosing photo is not successful.",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
    });
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.cacheDirectory, newFileName).then(success => {
     // //console.log("copyFileToLocalDir in profile success:" + JSON.stringify(success));
      this.localStoragePhoto = success.nativeURL;
    }, error => {
     // //console.log('copyFileToLocalDir in profile error:' + JSON.stringify(error));
      this.localStoragePhoto = "";
    });
  }

  createPhotoName() {
    let photoName = this.util.getImageName() + this.hautosys + ".jpg";
    this.photoName = this.util.getImageName() + this.hautosys + ".jpg";
    return photoName;
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }

    let msg = '';

    if (code == '005') {
      msg = "Please check internet connection!";
    } else {
      msg = "Can't connect right now. [" + code + "]";
    }

    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      dismissOnPageChange: true,
    });

    toast.present(toast);
  }

  getRegister() {
    let parameter = {
      hautosys: this.hautosys,
      t7: this.global.region
    };

    ////console.log('getRegister param in register:' + JSON.stringify(parameter));

    this.http.post(this.ipaddress + '/service001/getRegister', parameter).map(res => res.json()).subscribe(result => {
      //.log("getRegister param in register success or error:" + JSON.stringify(result));

      if (result != null && result.messageCode != null && result.messageCode == "0000") {
        this.registerDataLS.syskey = result.syskey;
        this.registerDataLS.phone = result.userID;
        this.registerDataLS.username = result.userName;
        this.registerDataLS.realname = result.t3;
        this.registerDataLS.stateCaption = result.t7;
        this.registerDataLS.photo = result.t16;
        this.registerDataLS.fathername = result.t20;
        this.registerDataLS.nrc = result.t21;
        this.registerDataLS.id = result.t22;
        this.registerDataLS.passbook = result.t23;
        this.registerDataLS.address = result.t24;
        this.registerDataLS.dob = result.t25;
        this.registerDataLS.t1 = result.t26;
        this.registerDataLS.t2 = result.t27;
        this.registerDataLS.t3 = result.t28;
        this.registerDataLS.t4 = result.t29;
        this.registerDataLS.t5 = result.t30;
        this.registerDataLS.state = result.t36;
        this.registerDataLS.status = result.t38;
        this.registerDataLS.n1 = result.t76;
        this.registerDataLS.n2 = result.t77;
        this.registerDataLS.n3 = result.t78;
        this.registerDataLS.n4 = result.t79;
        this.registerDataLS.n5 = result.t80;
        this.registerDataLS.city = result.t8;

        this.storage.set("uari", this.registerDataLS);
        this.events.publish("uari", this.registerDataLS);
      } else {
        this.storage.set("uari", null);
        this.events.publish("uari", null);
      }
    },
      error => {
        this.storage.set("uari", null);
        this.events.publish("uari", null);

        this.getError(error);
      });
  }

  // uploadImageToFirebase(image){
  //   image = normalizeURL(image);

  //   //uploads img to firebase storage
  //   this.uploadImage(image);
    // .then(photoURL => {
      
    //   let toast = this.toastCtrl.create({
    //     message: 'Image was updated successfully',
    //     duration: 3000
    //   });
    //   toast.present();
    //   })
  //}
//   uploadImage(imageURI){
//     //var file=imageURI;
//     var file = imageURI;
//     // let correctPath = file.substring(0, file.lastIndexOf('/') + 1, file.lastIndexOf('?'));
//     // console.log("currentName",correctPath);

   
//     var storagefire = firebase.app().storage("gs://nsb-wallet.appspot.com/");
//     var metadata = {
//       contentType: 'image/jpg'
//     };
//     console.log("file",file);
   
//       let storageRef = storagefire.ref();
//       let uploadTask = storageRef.child("image/").putString(file);
//       console.log("===",uploadTask);
      
//       uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
//   function(snapshot) {
//     // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
//     var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
//     console.log('Upload is ' + progress + '% done');
//     switch (snapshot.state) {
//       case firebase.storage.TaskState.PAUSED: // or 'paused'
//         console.log('Upload is paused');
//         break;
//       case firebase.storage.TaskState.RUNNING: // or 'running'
//         console.log('Upload is running');
//         break;
//     }
//   }, function(error) {

//   // A full list of error codes is available at
//   // https://firebase.google.com/docs/storage/web/handle-errors
//  console.log(error);
// }, function() {
//   // Upload completed successfully, now we can get the download URL
//   uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
//     console.log('File available at', downloadURL);
//   });
// });
    
//   }
  

}
