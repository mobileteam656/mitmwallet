import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, MenuController, NavParams, LoadingController, PopoverController, Events, Platform,AlertController, App } from 'ionic-angular';
import { LocationDetail } from '../location-detail/location-detail';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { GlobalProvider } from '../../providers/global/global';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Login } from '../login/login';
import { Geolocation } from '@ionic-native/geolocation';
//import { GoogleMap, LatLng } from '@ionic-native/google-maps';
import { Locations } from '../../providers/locations';
import { GoogleMaps } from '../../providers/google-maps';
import { MyPopOverLocationPage } from '../my-pop-over-location/my-pop-over-location';
import { CallNumber } from '@ionic-native/call-number';
declare var google;

import 'rxjs/add/operator/map';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';


@Component({
  selector: 'page-near-by',
  templateUrl: 'near-by.html',
})
export class NearByPage {
  @ViewChild('map') mapElement: ElementRef;  
    latLng: any;
    contentString:string;
    currentpage: any = 1;
    pageSize: any = 10;
    duration: any = 0;
    durationmsg: any;
    tempFont: number = 3;
    range: any = false;  
    map: any;  
    createdMap: any;
    markers: any = [];
    latlang: any = [];  
    alllocation: any = [];
    atmlocation: any = [];
    branchlocation: any = [];
    hasData: boolean = false;
    allocationMessage: string;
    status: any;
    public loading;
    ipaddress: string;
    popover: any;
    font: string = '';
    showFont: any = [];
    textEng: any = ["Near by"];
    textMyan: any = ["အနီးအနား"];
    ststus: any;
    isChecked: any;
    userdata: any;
    mapLocation: any = [];
    data: any;
    callnumber:string;
    phone:string;
    constructor(private appCtrl: App, public navCtrl: NavController,private firebase: FirebaseAnalytics, public maps: GoogleMaps, public menu: MenuController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public storage: Storage,
      private slimLoader: SlimLoadingBarService, public popoverCtrl: PopoverController, public events: Events, public platform: Platform, public changeLanguage: ChangelanguageProvider,
      public global: GlobalProvider,public alertController: AlertController,private callNumber: CallNumber, public location: Locations, public all: AllserviceProvider, public menuCtrl: MenuController, public geolocation: Geolocation) {//public createPopover: CreatePopoverPage,
      // this.menuCtrl.swipeEnable(false);
     
      this.ststus = this.navParams.get("ststus");
  
      if (this.ststus == '1') {
        this.storage.get('userData').then((val) => {
          this.userdata = val;
          this.ipaddress = this.global.ipaddress;
          this.getLocation();          
        });
      }
  
    }
  
    getLocation() {
      this.slimLoader.start(() => {
      });
      let param = { userID: '', sessionID: '' };
      this.http.post(this.ipaddress + '/service002/getLocation', param).map(res => res.json()).subscribe(data => {
  
  
        if (data.code == "0000") {
          let tempArray = [];
          if (!Array.isArray(data.data)) {
            tempArray.push(data.data);
            data.data = tempArray;            
          }
          this.alllocation.data = data.data;
          this.status = 1;
          if (this.alllocation.data.length > 0) {
            for (let i = 0; i < this.alllocation.data.length; i++) {
              let tempListArray = [];
              if (!Array.isArray(this.alllocation.data[i].dataList)) {
                tempListArray.push(this.alllocation.data[i].dataList);
                this.alllocation.data[i].dataList = tempListArray;               
              }
            }
            for (let i = 0; i < this.alllocation.data.length; i++) {
              for (let j = 0; j < this.alllocation.data[i].dataList.length; j++) {
                this.mapLocation.push({ "title": this.alllocation.data[i].dataList[j].name, "address": this.alllocation.data[i].dataList[j].address, "latitude": this.alllocation.data[i].dataList[j].latitude, "longitude": this.alllocation.data[i].dataList[j].longitude, "type":this.alllocation.data[i].dataList[j].locationType,"phone":this.alllocation.data[i].dataList[j].phone1});
                console.log("TITLE",this.alllocation.data[i].dataList[j].name);
                console.log("address",this.alllocation.data[i].dataList[j].address);
                console.log("Phone",this.alllocation.data[i].dataList[j].phone1);
                
              }
            }     
                    
            this.loadall();
            
            this.slimLoader.complete();
            this.hasData = true;
            this.location=this.alllocation.data.locationType;
  
          } else {
            this.slimLoader.complete();
            this.status = 0;
          }
        } else {
          this.status = 0;
          this.all.showAlert('Warning!', data.desc);
          this.slimLoader.complete();
        }
      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.slimLoader.complete();
        });
    }
  
    ionViewDidLoad() {
      this.ipaddress = this.global.ipaddress;     
      this.getLocation();
      this.storage.get('language').then((font) => {
        this.font = font;
        this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
          this.showFont = data;
        });
      });
      this.events.subscribe('changelanguage', lan => {
        this.font = lan.data;
        this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
          this.showFont = data;
        });
      });
    }   
  
   
    loadMap(){
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      this.loading.present();
    
      this.geolocation.getCurrentPosition().then((position) => {
        var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);   
       
        let mapOptions = {
          center: latLng,
          zoom: 18,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          //animation: google.maps.Animation.DROP,                    
        }
    
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);     
        
        let marker = new google.maps.Marker({  
          map: this.map,
          animation: google.maps.Animation.DROP,    
          position:latLng, 
          icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
          //icon: 'assets/location.gif'
        });      
  
        let content = "current location";
    
          let infoWindow = new google.maps.InfoWindow({
            content: content
          });
    
          google.maps.event.addListener(marker, 'click', () => {
            infoWindow.open(this.map, marker);
          });
          
          this.showLocation();
  
      }, (err) =>{   
      });
    }
       
    showLocation() {       
  
      for (let i = 0; i < this.alllocation.data.length; i++) {
          for (let j = 0; j < this.alllocation.data[i].dataList.length; j++) {
          let type= this.alllocation.data[i].dataList[j].locationType;        
          if(type=="ATM"){
          this.markers = new google.maps.Marker({         
            position: new google.maps.LatLng(this.alllocation.data[i].dataList[j].latitude, this.alllocation.data[i].dataList[j].longitude),          
            map: this.map,        
            title: this.alllocation.data[i].dataList[j].address,
            address: this.alllocation.data[i].dataList[j].address,
            type: this.alllocation.data[i].dataList[j].locationType,   
            
            icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'         
                 
            
          });
         // this.addInfoWindow(this.markers,this.alllocation.data[i].dataList[j].address);    
          
        }else if(type=="Branch"){
         
        this.markers = new google.maps.Marker({
          position: new google.maps.LatLng(this.alllocation.data[i].dataList[j].latitude, this.alllocation.data[i].dataList[j].longitude),
          map: this.map,        
          title: this.alllocation.data[i].dataList[j].address,        
          type: this.alllocation.data[i].dataList[j].locationType,     
          
          icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'                  
          
        }); 
       // this.addInfoWindow(this.markers,this.alllocation.data[i].dataList[j].address);                
         
        }else if(type=="Agent"){
          this.markers = new google.maps.Marker({
            position: new google.maps.LatLng(this.alllocation.data[i].dataList[j].latitude, this.alllocation.data[i].dataList[j].longitude),
            map: this.map,        
            title: this.alllocation.data[i].dataList[j].address,
            address: this.alllocation.data[i].dataList[j].address,
            type: this.alllocation.data[i].dataList[j].locationType,     
            
            icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'                  
            
          }) ; 
         // this.addInfoWindow(this.markers,this.alllocation.data[i].dataList[j].address);              
       
        }else if(type=="Merchant"){
          this.markers = new google.maps.Marker({
            position: new google.maps.LatLng(this.alllocation.data[i].dataList[j].latitude, this.alllocation.data[i].dataList[j].longitude),
            map: this.map,        
            title: this.alllocation.data[i].dataList[j].address,
            address: this.alllocation.data[i].dataList[j].address,
            type: this.alllocation.data[i].dataList[j].locationType,     
            
            icon: 'http://maps.google.com/mapfiles/ms/icons/orange-dot.png'                  
            
          }) ; 
          //this.addInfoWindow(this.markers,this.alllocation.data[i].dataList[j].address);              
       
        }
      }
    }   
      this.loading.dismiss();   
 }
  
    getloaction(data) {
      var parm = {
        lat: data.latitude,
        lng: data.longitude,
        name: data.name,
        address: data.address
      }
      this.navCtrl.push(LocationDetail, { data: parm })
    }
  
    ionViewDidLeave() {
      this.slimLoader.reset();
    }
  
    gologout() {
  
      this.loading = this.loadingCtrl.create({
        content: "Please wait...",
        dismissOnPageChange: true
      });
      this.loading.present();
      this.storage.get('ipaddress').then((ipaddress) => {
        this.ipaddress = this.global.ipaddress;
        this.ipaddress = ipaddress;
  
        let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };
  
        this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
          if (data.code == "0000") {
            this.loading.dismiss();
            // this.firebase.logEvent('log_out', {log_out:'logout-user'});
            // this.storage.remove('phonenumber');
            this.storage.remove('username');
            this.storage.remove('userData');
            this.storage.remove('firstlogin');
            this.appCtrl.getRootNav().setRoot(Login);
          }
          else {
            this.all.showAlert('Warning!', data.desc);
            this.loading.dismiss();
          }
  
        },
          error => {
            this.all.showAlert('Warning!', this.all.getErrorMessage(error));
            this.loading.dismiss();
          });
      });
  
    }
  
  
    setLocation(type) {
      this.location = type;
    }
   
    datafilter(ev) {
      this.popover = this.popoverCtrl.create(MyPopOverLocationPage, {});  
      this.popover.present({
        ev: ev
      });
  
      let doDismiss = () => this.popover.dismiss();
      let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
      this.popover.onDidDismiss(unregBackButton);  
      this.popover.onWillDismiss(data => {      
        this.duration = data;
        this.currentpage = 1;
        if (data != 3 && data != null) {
          if (data == 0) {                                          
            this.loadMap();            
          }
          if (data == 1) {
            this.loadatm();
          }
          else if (data == 2) {
            this.loadbranch();
          }else if (data == 4){
            this.loadmerchant();
          }
         
          this.range = false;
         
          this.currentpage = 1;
          this.pageSize = 10;
          
        }
        else if (data == 3) {
          this.loadagent();
          
        }
  
      });           
    }

    addInfoWindow(marker, content){

      let infoWindow = new google.maps.InfoWindow({
        content: content
              
      });
  
      google.maps.event.addListener(marker, 'click', () => {
        infoWindow.open(this.map, marker);
      });
  
    }
  
   loadatm(){ 
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
       
    this.geolocation.getCurrentPosition().then((position) => {
      var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);   
     
      let mapOptions = {
        center: latLng,
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        //animation: google.maps.Animation.DROP,      
            
      }
  
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);     
      
      let marker = new google.maps.Marker({  
        map: this.map,
        //animation: google.maps.Animation.DROP,    
        position:latLng, 
        icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'        
     
      });      

      let content = "current location";
  
        let infoWindow = new google.maps.InfoWindow({
          content: content
        });
  
        google.maps.event.addListener(marker, 'click', () => {
          infoWindow.open(this.map, marker);
        });
        

        for (let i = 0; i < this.alllocation.data.length; i++) {
          for (let j = 0; j < this.alllocation.data[i].dataList.length; j++) {
          let type= this.alllocation.data[i].dataList[j].locationType;
          if(type=="ATM"){
          this.markers = new google.maps.Marker({
            position: new google.maps.LatLng(this.alllocation.data[i].dataList[j].latitude, this.alllocation.data[i].dataList[j].longitude),
            map: this.map,        
            title: this.alllocation.data[i].dataList[j].address,
            address: this.alllocation.data[i].dataList[j].address,
            type: this.alllocation.data[i].dataList[j].locationType,     
            
            icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'                 
                    
          })
          this.contentString="<font color='blue'><bold>"+this.alllocation.data[i].dataList[j].name+"</bold></font><br><br>Address : "+this.alllocation.data[i].dataList[j].address+"<br>Tel. "+this.alllocation.data[i].dataList[j].phone1+","+this.alllocation.data[i].dataList[j].phone2+"<br><div><button onclick='loadContent()'>Call</button><div>";
              this.addInfoWindow(this.markers,this.contentString);
        }
      }
      
    }      
      this.loading.dismiss();
    }, (err) =>{   
    });         
      
   }
  
   loadbranch(){
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    
    this.geolocation.getCurrentPosition().then((position) => {
      var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);   
     
      let mapOptions = {
        center: latLng,
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        //animation: google.maps.Animation.DROP,      
            
      }
  
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);     
      
      let marker = new google.maps.Marker({  
        map: this.map,
        //animation: google.maps.Animation.DROP,    
        position:latLng, 
        icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'        
     
      });      

      let content = "current location";
  
        let infoWindow = new google.maps.InfoWindow({
          content: content
        });
  
        google.maps.event.addListener(marker, 'click', () => {
          infoWindow.open(this.map, marker);
        });
        

        for (let i = 0; i < this.alllocation.data.length; i++) {
          for (let j = 0; j < this.alllocation.data[i].dataList.length; j++) {
          let type= this.alllocation.data[i].dataList[j].locationType;
          if(type=="Branch"){
         
            this.markers = new google.maps.Marker({
              position: new google.maps.LatLng(this.alllocation.data[i].dataList[j].latitude, this.alllocation.data[i].dataList[j].longitude),
              map: this.map,        
              title: this.alllocation.data[i].dataList[j].address,        
              type: this.alllocation.data[i].dataList[j].locationType,     
              
              icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'                  
              
            }) ; 
            this.contentString="<font color='blue'><bold>"+this.alllocation.data[i].dataList[j].name+"</bold></font><br><br>Address : "+this.alllocation.data[i].dataList[j].address+"<br>Tel. "+this.alllocation.data[i].dataList[j].phone1+","+this.alllocation.data[i].dataList[j].phone2;
              this.addInfoWindow(this.markers,this.contentString);
        }
      }
      
    }
      
    this.loading.dismiss();
    }, (err) =>{   
    });    
   }
  
   loadagent(){
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    this.geolocation.getCurrentPosition().then((position) => {
      var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);  
     
      let mapOptions = {
        center: latLng,
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        //animation: google.maps.Animation.DROP,      
            
      }
  
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);     
      
      let marker = new google.maps.Marker({  
        map: this.map,
        //animation: google.maps.Animation.DROP,    
        position:latLng, 
        icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'        
     
      });      

      let content = "current location";
  
        let infoWindow = new google.maps.InfoWindow({
          content: content
        });
  
        google.maps.event.addListener(marker, 'click', () => {
          infoWindow.open(this.map, marker)
        });
        

        for (let i = 0; i < this.alllocation.data.length; i++) {
          for (let j = 0; j < this.alllocation.data[i].dataList.length; j++) {
          let type= this.alllocation.data[i].dataList[j].locationType;
          console.log("jjjj",this.alllocation.data[i].dataList[j].phone1);
          if(type=="Agent"){
            this.markers = new google.maps.Marker({
              position: new google.maps.LatLng(this.alllocation.data[i].dataList[j].latitude, this.alllocation.data[i].dataList[j].longitude),
              map: this.map,        
              title: this.alllocation.data[i].dataList[j].address,
              address: this.alllocation.data[i].dataList[j].address,
              type: this.alllocation.data[i].dataList[j].locationType,     
             
              
              icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'                  
              
              
            }) ; 
            this.contentString="<font color='blue'><bold>"+this.alllocation.data[i].dataList[j].name+"</bold></font><br><br>Address : "+this.alllocation.data[i].dataList[j].address+"<br>Tel. "+this.alllocation.data[i].dataList[j].phone1+","+this.alllocation.data[i].dataList[j].phone2;
              this.addInfoWindow(this.markers,this.contentString);
        }
      }      
    }     
    this.loading.dismiss();
    }, (err) =>{   
    });   
   }

   loadmerchant(){
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    this.geolocation.getCurrentPosition().then((position) => {
      var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);  
     
      let mapOptions = {
        center: latLng,
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        //animation: google.maps.Animation.DROP,      
            
      }
  
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);     
      
      let marker = new google.maps.Marker({  
        map: this.map,
        //animation: google.maps.Animation.DROP,    
        position:latLng, 
        icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'        
     
      });      

      let content = "current location";
  
        let infoWindow = new google.maps.InfoWindow({
          content: content
        });
  
        google.maps.event.addListener(marker, 'click', () => {
          infoWindow.open(this.map, marker);
        });
        

        for (let i = 0; i < this.alllocation.data.length; i++) {
          for (let j = 0; j < this.alllocation.data[i].dataList.length; j++) {
          let type= this.alllocation.data[i].dataList[j].locationType;
          if(type=="Merchant"){
            this.markers = new google.maps.Marker({
              position: new google.maps.LatLng(this.alllocation.data[i].dataList[j].latitude, this.alllocation.data[i].dataList[j].longitude),
              map: this.map,        
              title: this.alllocation.data[i].dataList[j].address,
              address: this.alllocation.data[i].dataList[j].address,
              type: this.alllocation.data[i].dataList[j].locationType,     
              
              icon: 'http://maps.google.com/mapfiles/ms/icons/orange-dot.png'                  
              
              
            }) ; 
            this.contentString="<font color='blue'><bold>"+this.alllocation.data[i].dataList[j].name+"</bold></font><br><br>Address : "+this.alllocation.data[i].dataList[j].address+"<br>Tel. "+this.alllocation.data[i].dataList[j].phone1+","+this.alllocation.data[i].dataList[j].phone2;
              this.addInfoWindow(this.markers,this.contentString);
        }
      }      
    }     
    this.loading.dismiss();
    }, (err) =>{   
    });   
   }


/////////////////////ALL//////////////////////////////////////////////////////////////////////////////
   loadall(){
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    this.loading.present();
    this.geolocation.getCurrentPosition().then((position) => {
      var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);  
     
      let mapOptions = {
        center: latLng,
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        //animation: google.maps.Animation.DROP,      
            
      }
  
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);     
      
      let marker = new google.maps.Marker({  
        map: this.map,
        //animation: google.maps.Animation.DROP,    
        position:latLng, 
        icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'        
     
      });      

      let content = "current location";
  
        let infoWindow = new google.maps.InfoWindow({
          content: content
        });
  
        google.maps.event.addListener(marker, 'click', () => {
          infoWindow.open(this.map, marker)
        });
        

        for (let i = 0; i < this.alllocation.data.length; i++) {
          for (let j = 0; j < this.alllocation.data[i].dataList.length; j++) {
          let type= this.alllocation.data[i].dataList[j].locationType;
          console.log("jjjj",this.alllocation.data[i].dataList[j].phone1);
          if(type=="Agent"){
            this.markers = new google.maps.Marker({
              position: new google.maps.LatLng(this.alllocation.data[i].dataList[j].latitude, this.alllocation.data[i].dataList[j].longitude),
              map: this.map,        
              title: this.alllocation.data[i].dataList[j].address,
              address: this.alllocation.data[i].dataList[j].address,
              type: this.alllocation.data[i].dataList[j].locationType,     
             
              
              icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'                  
              
              
            }) ; 
            this.contentString="<font color='blue'><bold>"+this.alllocation.data[i].dataList[j].name+"</bold></font><br><br>Address : "+this.alllocation.data[i].dataList[j].address+"<br>Tel. "+this.alllocation.data[i].dataList[j].phone1+","+this.alllocation.data[i].dataList[j].phone2;
              this.addInfoWindow(this.markers,this.contentString);
        }
        if(type=="Merchant"){
          this.markers = new google.maps.Marker({
            position: new google.maps.LatLng(this.alllocation.data[i].dataList[j].latitude, this.alllocation.data[i].dataList[j].longitude),
            map: this.map,        
            title: this.alllocation.data[i].dataList[j].address,
            address: this.alllocation.data[i].dataList[j].address,
            type: this.alllocation.data[i].dataList[j].locationType,     
            
            icon: 'http://maps.google.com/mapfiles/ms/icons/orange-dot.png'                  
            
            
          }) ; 
          this.contentString="<font color='blue'><bold>"+this.alllocation.data[i].dataList[j].name+"</bold></font><br><br>Address : "+this.alllocation.data[i].dataList[j].address+"<br>Tel. "+this.alllocation.data[i].dataList[j].phone1+","+this.alllocation.data[i].dataList[j].phone2;
            this.addInfoWindow(this.markers,this.contentString);
      }
      if(type=="Branch"){
         
        this.markers = new google.maps.Marker({
          position: new google.maps.LatLng(this.alllocation.data[i].dataList[j].latitude, this.alllocation.data[i].dataList[j].longitude),
          map: this.map,        
          title: this.alllocation.data[i].dataList[j].address,        
          type: this.alllocation.data[i].dataList[j].locationType,     
          
          icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'                  
          
        }) ; 
        this.contentString="<font color='blue'><bold>"+this.alllocation.data[i].dataList[j].name+"</bold></font><br><br>Address : "+this.alllocation.data[i].dataList[j].address+"<br>Tel. "+this.alllocation.data[i].dataList[j].phone1+","+this.alllocation.data[i].dataList[j].phone2;
          this.addInfoWindow(this.markers,this.contentString);
    }
    if(type=="ATM"){
      this.markers = new google.maps.Marker({
        position: new google.maps.LatLng(this.alllocation.data[i].dataList[j].latitude, this.alllocation.data[i].dataList[j].longitude),
        map: this.map,        
        title: this.alllocation.data[i].dataList[j].address,
        address: this.alllocation.data[i].dataList[j].address,
        type: this.alllocation.data[i].dataList[j].locationType,     
        
        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'                 
                
      })
      this.phone=this.alllocation.data[i].dataList[j].phone1;
      this.contentString="<font color='blue'><bold>"+this.alllocation.data[i].dataList[j].name+"</bold></font><br><br>Address : "+this.alllocation.data[i].dataList[j].address+"<br>Tel. "+this.alllocation.data[i].dataList[j].phone1+","+this.alllocation.data[i].dataList[j].phone2;
          this.addInfoWindow(this.markers,this.contentString);
         
    }
      }      
    }     
    this.loading.dismiss();
    }, (err) =>{   
    });   
   }
   
  
}
