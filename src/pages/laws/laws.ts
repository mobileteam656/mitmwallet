import { Component } from '@angular/core';
import 'rxjs/add/operator/map';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Platform, NavController, NavParams, PopoverController, Events, ToastController, LoadingController } from 'ionic-angular';
import { Changefont } from '../changefont/changeFont';
import { App } from 'ionic-angular';
import { UtilProvider } from '../../providers/util/util';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { File } from '@ionic-native/file';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FileOpener } from '@ionic-native/file-opener';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'page-laws',
  templateUrl: 'laws.html',
  providers: [CreatePopoverPage],
})
export class LawsPage {

  textEng: any = ["", "No PDF Found!"];
  textMyan: any = ["", "No PDF Found!",];
  textData: string[] = [];
  font: string = '';

  pdfLink: string = '';
  pdfName: string = '';
  pdfList: any = [];
  tempPdfList: any = [];

  nores: string = '';

  userData: any;
  ipaddress: string;
  ipaddressCMS: string;
  popover: any;
  loading: any;
  title: any;
  keyword: any;

  constructor(public util: UtilProvider, public http: Http, public navCtrl: NavController,
    public storage: Storage, public events: Events,
    public global: GlobalProvider, public platform: Platform,
    public popoverCtrl: PopoverController, public changefont: Changefont,
    public toastCtrl: ToastController, public appCtrl: App,
    public createPopover: CreatePopoverPage, private document: DocumentViewer,
    private file: File, private transfer: FileTransfer,
    private fileOpener: FileOpener, public loadingCtrl: LoadingController,
    public navParams: NavParams) {
    this.title = this.navParams.get('param');

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });

    this.storage.get('userData').then((userData) => {
      if (userData != null) {
        this.userData = userData;
        this.storage.get('ipaddressCMS').then((ip) => {
          if (ip !== undefined && ip !== "" && ip != null) {
            this.ipaddress = ip;
            ////console.log("this.ipaddress1=" + this.ipaddress);
            this.getLawPDF();
          } else {
            this.ipaddress = this.global.ipaddressCMS;
            ////console.log("this.ipaddress2=" + this.ipaddress);
            this.getLawPDF();
          }
        });
      }
    });

    this.events.subscribe('userData', userData => {
      if (userData != null) {
        this.userData = userData;
        ////console.log("this.userData=" + JSON.stringify(this.userData));
      }
    });

    this.storage.get('ipaddressCMS').then((ip) => {
      ////console.log("ip get=" + JSON.stringify(ip));
      if (ip !== undefined && ip != null && ip !== "") {
        this.ipaddressCMS = ip;
      } else {
        this.ipaddressCMS = this.global.ipaddressCMS;
      }
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    } else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  loadingCCP() {
    this.loading = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Processing...',
    });

    this.loading.present();
  }

  dismiss() {
    this.loading.dismissAll();
  }

  openPDF(p) {
    if (this.platform.is('cordova')) {
      let path = null;

      if (this.platform.is('ios')) {
        path = this.file.documentsDirectory;
      } else if (this.platform.is('android')) {
        path = this.file.dataDirectory;
      }
      const transfer = this.transfer.create();
      //p.link expects https://xxxxxxx/abc.pdf
      this.loadingCCP();
      transfer.download(p.link + p.name, path + p.name).then(entry => {
        this.fileOpener.open(path + p.name, 'application/pdf')
          .then(() => { console.log('File is opened'); this.dismiss(); })
          .catch(e => { console.log('Error opening file', e); this.dismiss(); });
      });
    } else {
      this.downloadPDF(p);
    }
  }

  downloadPDF(p) {
    let path = null;

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    } else if (this.platform.is('android')) {
      path = this.file.dataDirectory;
    }

    const transfer = this.transfer.create();
    //p.name expects abc.pdf
    //p.link expects https://xxxxxxx/abc.pdf
    transfer.download(p.link + p.name, path + p.name).then(entry => {
      let url = entry.toURL();
      this.document.viewDocument(url, 'application/pdf', {});
    });
  }

  /* searchKeyword(ev: any) {
    let val = ev.target.value.toLowerCase();

    this.pdfList = this.tempPdfList;

    if (val && val.trim() != '') {
      this.pdfList = this.pdfList.filter((item) => {
        return (item.name.toLowerCase().indexOf(val) > -1) || (item.keyword.indexOf(val) > -1);
      });
    }
  } */

  getLawPDF() {
    let param = {
      "type": this.title,
      "region": this.global.region
    };

    ////console.log("param=" + JSON.stringify(param));

    this.nores = "1";

    ////console.log("pdfList=" + JSON.stringify(this.pdfList));

    this.http.post(this.ipaddress + '/service001/getPdf', param).map(res => res.json()).subscribe(result => {
      ////console.log("getPdf=" + JSON.stringify(result));

      if (result.code == "0000") {
        let tempArray = [];

        if (!Array.isArray(result.pdfList)) {
          tempArray.push(result.pdfList);
          result.pdfList = tempArray;
        }

        if (result.pdfList.length > 0) {
          this.nores = "1";
          this.pdfList = result.pdfList;
          this.tempPdfList = this.pdfList;
        } else {
          this.nores = "0";
        }
      } else {
        let toast = this.toastCtrl.create({
          message: result.desc,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
      }
    },
      error => {
        let code;

        if (error.status == 404) {
          code = '001';
        } else if (error.status == 500) {
          code = '002';
        } else if (error.status == 403) {
          code = '003';
        } else if (error.status == -1) {
          code = '004';
        } else if (error.status == 0) {
          code = '005';
        } else if (error.status == 502) {
          code = '006';
        } else {
          code = '000';
        }

        let msg = "Can't connect right now. [" + code + "]";

        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);
      });
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  searchKeyword() {
    this.nores = '1';

    this.getLawPDF();
  }

  getKeyword() {
    let param = {
      "keyword": this.keyword,
      "region": this.global.region,
      "type": this.title
    };

    ////console.log("param=" + JSON.stringify(param));

    this.http.post(this.ipaddressCMS + '/service001/getKeyword', param).map(res => res.json()).subscribe(result => {
      ////console.log("getPdf=" + JSON.stringify(result));

      if (result.code == "0000") {
        if (result.pdfList.length > 0) {
          let tempArray = [];

          if (!Array.isArray(result.pdfList)) {
            tempArray.push(result.pdfList);
            result.pdfList = tempArray;
          }
  
          if (result.pdfList.length > 0) {
            this.pdfList = result.pdfList;
          }

          this.nores = "1";
          this.pdfList = result.pdfList;
          this.tempPdfList = this.pdfList;
        } else {
          this.nores = "0";
        }
      } else {
        this.nores = "0";
        
        // let toast = this.toastCtrl.create({
        //   message: result.desc,
        //   duration: 3000,
        //   position: 'bottom',
        //   dismissOnPageChange: true,
        // });

        // toast.present(toast);
      }
    },
      error => {
        this.nores = "0";
        let code;

        if (error.status == 404) {
          code = '001';
        } else if (error.status == 500) {
          code = '002';
        } else if (error.status == 403) {
          code = '003';
        } else if (error.status == -1) {
          code = '004';
        } else if (error.status == 0) {
          code = '005';
        } else if (error.status == 502) {
          code = '006';
        } else {
          code = '000';
        }

        let msg = "Can't connect right now. [" + code + "]";

        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        //toast.present(toast);
      });
  }

}
