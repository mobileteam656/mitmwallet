import { Component } from '@angular/core';
import { NavController, MenuController, NavParams, LoadingController, PopoverController, Events, Platform, App } from 'ionic-angular';
import { LocationDetail } from '../location-detail/location-detail';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
// import { DefaultPopOver } from '../default-pop-over/default-pop-over';
// import { LanguagePopOver } from '../language-pop-over/language-pop-over';
// import { IpchangePage } from '../ipchange/ipchange';
import { GlobalProvider } from '../../providers/global/global';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
 import { AllserviceProvider } from '../../providers/allservice/allservice';
// import { PopoverPage } from '../popover-page/popover-page';
import { CreatePopoverPage } from '../create-popover/create-popover';
import {Login} from '../login/login';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';


@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
  providers: [ChangelanguageProvider,CreatePopoverPage]
})
export class ContactUsPage {
  branchlocation: any = [];
  hasData: boolean = false;
  allocationMessage: string;
  status: any;
  public loading;
  ipaddress: string;
  popover: any;
  font: string = '';
  showFont: any = [];
  textEng: any = ["656 Maha Thukhita Road,Insein Township, Yangon, Myanmar", "Welcome to mWallet", "We would love to hear from you. Please use the address below", "Contact Us"];
  textMyan: any = ["၆၅၆ မဟာသုခိတာလမ်း၊ အင်းစိန်မြို့နယ်၊ ရန်ကုန်၊ မြန်မာ", "mWallet မှကြိုဆိုပါသည်။", "မိတ်ဆွေထံမှ စုံစမ်းမှုများပြုလုပ်လိုပါလျှင် အောက်ပါလိပ်စာများသို့ ဆက်သွယ်မေးမြန်းနိုင်ပါသည်။", "ဆက်သွယ်ရန်"];
  ststus:any;
  userdata:any;
  constructor(private appCtrl: App, public navCtrl: NavController,private firebase: FirebaseAnalytics,public menu: MenuController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public storage: Storage,
    private slimLoader: SlimLoadingBarService, public popoverCtrl: PopoverController, public events: Events, public platform: Platform, public changeLanguage: ChangelanguageProvider,
    public global: GlobalProvider, public all: AllserviceProvider,public createPopover: CreatePopoverPage, public menuCtrl: MenuController) {
    // this.menuCtrl.swipeEnable(false);
    
    this.ststus=this.navParams.get("ststus");
    if(this.ststus=='1'){
      this.storage.get('userData').then((val) => {
        this.userdata = val;
        this.ipaddress = this.global.ipaddress;
              
      });
    }
    
  }
 // callIT(passedNumber) {
  //  passedNumber = encodeURIComponent(passedNumber);
  //  window.location = "tel:" + passedNumber;
  //}
  gotoshwewebsite() {
    if (this.platform.is('android')) {
      window.open('http://www.mit.com.mm', '_system');
    }
    else if (this.platform.is('ios')) {
      window.open('http://www.mit.com.mm'); // or itms://
    }
    if (this.platform.is('mobileweb')) {
      // //console.log("running in a browser on mobile!");
    }
  }

  
  ionViewDidLoad() {
    this.ipaddress = this.global.ipaddress;
   // this.getLocation();
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguage.changelanguage(lan.data, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

  callIT(passedNumber) {
    passedNumber = encodeURIComponent(passedNumber);
   // window.location = "tel:" + passedNumber;
  }

  getloaction(data) {
    var parm = {
      lat: data.latitude,
      lng: data.longitude,
      name: data.name,
      address: data.address
    }
    this.navCtrl.push(LocationDetail, { data: parm })
  }

  ionViewDidLeave() {
    this.slimLoader.reset();
  }

  gologout() {

    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
    });
    this.loading.present();
    this.storage.get('ipaddress').then((ipaddress) => {
      this.ipaddress = this.global.ipaddress;
      this.ipaddress = ipaddress;

      let param = { userID: this.userdata.userID, sessionID: this.userdata.sessionID };

      this.http.post(this.ipaddress + '/service001/mobileSignout', param).map(res => res.json()).subscribe(data => {
        if (data.code == "0000") {
          this.loading.dismiss();
          // this.firebase.logEvent('log_out', {log_out:'logout-user'});
          // this.storage.remove('phonenumber');
          this.storage.remove('username');
          this.storage.remove('userData');
          this.storage.remove('firstlogin');
          this.appCtrl.getRootNav().setRoot(Login);
        }
        else {
          this.all.showAlert('Warning!', data.desc);
          this.loading.dismiss();
        }

      },
        error => {
          this.all.showAlert('Warning!', this.all.getErrorMessage(error));
          this.loading.dismiss();
        });
    });

  }
  setLocation(type){
   // this.location=type;
  }
  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

}
