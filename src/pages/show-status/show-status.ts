import { Component, ViewChild } from '@angular/core';
import { IonicPage, ActionSheetController, Platform, NavController, Events, NavParams, ToastController, MenuController, AlertController, LoadingController, Keyboard } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Changefont } from '../changefont/changeFont';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { UtilProvider } from '../../providers/util/util';
import { LoanListInfoPage } from '../loan-list-info/loan-list-info';
import { Slides } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'page-show-status',
  templateUrl: 'show-status.html',
  providers: [Changefont]
})
export class ShowStatusPage {
  @ViewChild(Slides) slides: Slides;
  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  font: string = '';
  ipaddress: string;

  ExitLoan: any;

  items = [];
  usersyskey: any;
  loandata: any = [];

  checkScroll: boolean = true;
  loanApplyDate: any = '';
  FromPage: any;


  titleMyan: any = ["ချေးငွေတင်ထားသောစာရင်းများ", "ပြန်ဆပ်ရန်အချိန်ဇယား", "နံပါတ်စဉ်", "အာမခံသူ", "လျှောက်သည်.ရက်စွဲ", "ဒေတာမရှိပါ", "အခြေအနေ"];
  titleEng: any = ["Loan History", "Loan Schedule", "Loan ID", "Guarantor Name", "Apply Date", "No Result Found !!!", "Status"];
  titledata: any = [];

  popoverItemList: any[] = [{ name: '', key: 0 }, { name: '', key: 1 }, { name: '', key: 2 }, { name: '', key: 3 }];

  textMyan: any = [
    { status: '', descript: 'အားလုံး', type: 0 },
    { status: '3', descript: 'စောင့်ဆိုင်းစာရင်း', type: 1 },
    { status: '7', descript: 'အတည်ပြုစာရင်း', type: 2 },
    { status: '0', descript: 'ပယ်စာရင်း', type: 3 }];

  textEng: any = [
    { status: '', descript: 'All', type: 0 },
    { status: '3', descript: 'Pending', type: 1 },
    { status: '7', descript: 'Approved', type: 2 },
    { status: '0', descript: 'Reject', type: 3 }];

  textdata: any = [{ status: '', descript: '', type: 0 }];
  defaultStatus = this.getDefaultObj();
  getDefaultObj() {
    return { Type: 0 };
  }

  ST: any;
  //infinte scroll loop 
  tempData: any;
  loanDataLength: any;
  tempDataLength: any;
  loopLength: any;
  public loading;
  flagNet: any;
  loantype: any;

  constructor(public events: Events, public navCtrl: NavController, public global: GlobalProvider, public http: Http,
    public navParams: NavParams, public storage: Storage, public changefont: Changefont, public toastCtrl: ToastController,
    public util: UtilProvider, public platform: Platform, public loadingCtrl: LoadingController, public network: Network,
    public datePipe: DatePipe) {
    console.log("Test ps ExitLoan data >> " + this.ExitLoan);
  
    this.storage.get('applicantsyskey').then((applicantsyskey) => {
      this.global.applicantsyskey=applicantsyskey;
      console.log('Global syskey: '+this.global.applicantsyskey);

     });
    this.storage.get('ip').then((ip) => {
      console.log('Your ip is', ip);
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
        console.log("Your ip is", this.ipaddress);
      }
     
      else {
        console.log(" undefined  conditon ");
        this.ipaddress = global.ipaddress;
        console.log("Your ip main is", this.ipaddress);
      }
      //Font change
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      })
      this.storage.get('language').then((font) => {
        this.changelanguage(font);
      });
    
      //test for default Status Binding 
      this.onSelectChange(0);// ** ps rechange
    });
    let date = new Date();
    this.loanApplyDate = date.toISOString();
    console.log("HELLO ExitLoan data >>" + this.ExitLoan);
    console.log("TEST data of Show Status Page >>" + this.loandata.length);

  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
      for (let i = 0; i < this.titleEng.length; i++) {
        this.titledata[i] = this.titleEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
      for (let i = 0; i < this.titleMyan.length; i++) {
        this.titledata[i] = this.changefont.UnitoZg(this.titleMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
      for (let i = 0; i < this.titleMyan.length; i++) {
        this.titledata[i] = this.titleMyan[i];
      }
    }
  }

  ionViewDidLoad() {
  
      if (this.ipaddress!= undefined)
        this.onSelectChange(0);


    // this.FromPage = this.navParams.get("FromPage");
    // if (this.FromPage == 'LoanList') {
    //   console.log(" Test PSST called  Loan List Info ");
    // } else {
    //   if (this.ipaddress != undefined)
    //     // this.onSelectChange(this.defaultStatus.Type);
    // }

  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    setTimeout(() => {
      //infiniteScroll got loop length
      this.loopLength = 0;
      this.loanDataLength = this.loandata.length;

      if (this.loanDataLength == this.tempData.length) {
        this.checkScroll = false;
      } else {
        if (this.tempDataLength > 10) {
          this.loopLength = 10;
          this.tempDataLength = this.tempDataLength - 10;
        } else
          this.loopLength = this.tempDataLength;

        for (let i = 0; i < this.loopLength; i++) {
          this.loandata.push(this.tempData[this.loanDataLength + i]);
        }
        console.log("Testing scroll Loan data >> " + JSON.stringify(this.loandata));
        console.log("Testing scroll Temp data >> " + JSON.stringify(this.tempData));
      }
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  FindandSelect(array, property, value) {
    var sta;
    array.forEach(function (result, index) {
      if (result[property] === value) {
        sta = array[index].status;
        console.log(sta);
      }
    });
    return sta;
  }

  onSelectChange(selectStatus: any) {
    console.log('Selected', selectStatus);
    this.checkScroll = true;
    this.ST = this.FindandSelect(this.textdata, 'type', selectStatus);
    this.usersyskey = this.global.applicantsyskey;
    if (this.global.netWork== "connected") {
      this.loading = this.loadingCtrl.create({
        content: "", spinner: 'circles',
        dismissOnPageChange: true,
        duration: 3000
      });

      this.loading.present();

      this.http.get(this.global.ipAdressMFI + '/serviceMFI/getLoanListByApplicantSyskeyandLoanStatus?syskey=' + this.usersyskey + '&loanstatus=' +
        this.ST).map(res => res.json()).subscribe(data => {
          // this.http.get(this.ipaddress + '/serviceMFI/getLoanListByApplicantSyskeyandLoanStatus?syskey=1195&loanstatus=' +
          //       this.ST).map(res => res.json()).subscribe(data => {
          console.log("TEST data of Show Status Page >>" + data.data.length);

          if (data.data != null) {
            //psst test for show UI (No data ava)

            if (data.data.length > 0)
              this.ExitLoan = 1;
            else
              this.ExitLoan = 0;

            let tempArray = [];
            if (!Array.isArray(data.data)) {
              tempArray.push(data.data);
              data.data = tempArray;
            }

            this.loandata = [];
            this.tempData = [];
            for (let i = 0; i < data.data.length; i++) {
              // if (data.data[i].loandata.monthlyAmount != "" &&
              //   data.data[i].loandata.monthlyAmount != undefined && data.data[i].loandata.monthlyAmount != null) {
              //   data.data[i].loandata.monthlyAmount = data.data[i].loandata.monthlyAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              // }

              // if (data.data[i].loandata.totalAmount != "" &&
              //   data.data[i].loandata.totalAmount != undefined && data.data[i].loandata.totalAmount != null) {
              //   data.data[i].loandata.totalAmount = data.data[i].loandata.totalAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              // }
              this.tempData.push(data.data[i]);
              this.loanApplyDate = this.datePipe.transform(data.data[i].loandata.loanAppliedDate, 'dd-MM-yyyy');
              console.log("Apply date  Is >> " + JSON.stringify(this.loanApplyDate));
              data.data[i].loandata.loanAppliedDate = this.datePipe.transform(data.data[i].loandata.loanAppliedDate, 'dd-MM-yyyy');
            }
          }

          //infiniteScroll got loop length
          this.tempDataLength = this.tempData.length;
          if (this.tempDataLength > 10) {
            this.loopLength = 10;
            this.tempDataLength = this.tempDataLength - 10;
          } else
            this.loopLength = this.tempDataLength;

          for (let i = 0; i < this.loopLength; i++) {
            //this.loandata[i] = this.tempData[i];
            this.loandata.push(this.tempData[i]);
          }

          this.loading.dismiss();

        },
          error => {
            let toast = this.toastCtrl.create({
              message: this.util.getErrorMessage(error),
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
            this.loading.dismiss();
          });
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }
  }

  nextPage(obj) {
    this.navCtrl.push(LoanListInfoPage,
      {
        PersonDataDetail: obj,
        loanListflag: true,
        toPage: 'LoanListInfo'
      });
  }

  gobackmain() {
    this.navCtrl.setRoot(TabsPage,
      {
      });
  }

  ionViewDidEnter() {
    this.backButtonExit();

  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      this.platform.exitApp();
    });
  }

}
