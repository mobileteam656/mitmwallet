import { Component, ViewChild } from '@angular/core';
import { IonicPage, ActionSheetController, Platform, NavController, Events, NavParams, ToastController, MenuController, AlertController, LoadingController, Keyboard } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Changefont } from '../changefont/changeFont';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { UtilProvider } from '../../providers/util/util';
import { ScheduledetailPage } from '../scheduledetail/scheduledetail';
import { TabsPage } from '../tabs/tabs';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
  providers: [Changefont]
})
export class SchedulePage {

  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  font: string = '';
  ipaddress: string;
  ExitLoan: any;

  items = [];
  usersyskey: any;
  loandata: any = [];
  PersonData = this.global.PersonData;
  checkScroll: boolean = true;
  textMyan: any = ["ပြန်ဆပ်ရန်အချိန်ဇယား", "နံပါတ်စဉ်", "အာမခံသူ", "လျှောက်သည်.ရက်စွဲ", "ဒေတာမရှိပါ"];
  textEng: any = ["Loan Schedule", "Loan ID", "Guarantor Name", "Apply Date", "No Result Found !!!"];
  textdata: any = [];
  defaultStatus = this.getDefaultObj();
  getDefaultObj() {
    return { Type: 0 };
  }

  ST: any;
  //infinte scroll loop 
  tempData: any;
  loanDataLength: any;
  tempDataLength: any;
  loopLength: any;
  public loading;
  loanApplyDate: any = '';

  constructor(public events: Events, public navCtrl: NavController, public global: GlobalProvider, public http: Http,
    public navParams: NavParams, public storage: Storage, public changefont: Changefont, public toastCtrl: ToastController,
    public util: UtilProvider, public platform: Platform, public loadingCtrl: LoadingController,
    public datePipe: DatePipe) {
      
    this.storage.get('ip').then((ip) => {
      console.log('Your ip is', ip);
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
        console.log("Your ip is", this.ipaddress);
      }
      else {
        console.log(" undefined  conditon ");
        this.ipaddress = global.ipaddress;
        console.log("Your ip main is", this.ipaddress);
      }
      //Font change
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      })
      this.storage.get('language').then((font) => {
        this.changelanguage(font);
      });
      this.storage.get('applicantsyskey').then((applicantsyskey) => {
        this.global.applicantsyskey=applicantsyskey;
        console.log('Global syskey: '+this.global.applicantsyskey);
       });

       this.onSelectChange('7');  //*** reopen
      let date = new Date();
      this.loanApplyDate = date.toISOString();
      console.log("HELLO ExitLoan data >>" + this.ExitLoan);

    });
  
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
    }
  }

  // ionViewDidLoad() {
  

  //   if (this.ipaddress!=undefined)
  //   this.onSelectChange('7');
  // }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    setTimeout(() => {
      //infiniteScroll got loop length
      this.loopLength = 0;
      this.loanDataLength = this.loandata.length;

      if (this.loanDataLength == this.tempData.length) {
        this.checkScroll = false;
      } else {

        if (this.tempDataLength > 10) {
          this.loopLength = 10;
          this.tempDataLength = this.tempDataLength - 10;
        } else
          this.loopLength = this.tempDataLength;

        for (let i = 0; i < this.loopLength; i++) {
          this.loandata.push(this.tempData[this.loanDataLength + i]);
        }
        console.log("Testing scroll Loan data >> " + JSON.stringify(this.loandata));
        console.log("Testing scroll Temp data >> " + JSON.stringify(this.tempData));
      }
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  FindandSelect(array, property, value) {
    var sta;
    array.forEach(function (result, index) {
      if (result[property] === value) {
        console.log("Result of Testingggggg>>  " + JSON.stringify(array[index].status));
        sta = array[index].status;
        console.log(sta);
      }
    });

    return sta;
  }

  onSelectChange(selectStatus: any) {
    console.log('Selected', selectStatus);
    this.checkScroll = true;
    // tested for default status
   // this.ST = this.FindandSelect(this.textdata, 'type', selectStatus);
   this.ST = selectStatus;
    console.log("Choosing Status is >> " + this.ST);
    // tested for real 
    this.usersyskey = this.global.applicantsyskey;
    console.log('Applicant Syskey for Schedule: '+this.usersyskey);

    // added network test
    if (this.global.netWork == "connected") {
      this.loading = this.loadingCtrl.create({
        content: "", spinner: 'circles',
        dismissOnPageChange: true
        //   duration: 3000
      });
      this.loading.present();
      this.http.get(this.global.ipAdressMFI + '/serviceMFI/loadLoanRepaymentInstallmentlist?syskey=' + this.global.applicantsyskey + '&loanstatus=' +
        this.ST).map(res => res.json()).subscribe(data => {

          if (data.data != null) {
            // test for show UI (No data ava)
            if (data.data.length > 0)
              this.ExitLoan = 1;
            else
              this.ExitLoan = 0;

            let tempArray = [];
            if (!Array.isArray(data.data)) {
              tempArray.push(data.data);
              data.data = tempArray;
            }

            this.loandata = [];
            this.tempData = [];
            for (let i = 0; i < data.data.length; i++) {
              console.log("monthly Amount ps>>: " + JSON.stringify(data.data[i].loandata.monthlyAmount));
              if (data.data[i].loandata.monthlyAmount != "" &&
                data.data[i].loandata.monthlyAmount != undefined && data.data[i].loandata.monthlyAmount != null) {
                data.data[i].loandata.monthlyAmount = data.data[i].loandata.monthlyAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              }

              if (data.data[i].loandata.totalAmount != "" &&
                data.data[i].loandata.totalAmount != undefined && data.data[i].loandata.totalAmount != null) {
                data.data[i].loandata.totalAmount = data.data[i].loandata.totalAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              }
              this.tempData.push(data.data[i]);
              this.loanApplyDate = this.datePipe.transform(data.data[i].loandata.loanAppliedDate, 'dd-MM-yyyy');
              console.log("Apply date  Is >> " + JSON.stringify(this.loanApplyDate));
              data.data[i].loandata.loanAppliedDate = this.datePipe.transform(data.data[i].loandata.loanAppliedDate, 'dd-MM-yyyy');
            }
            ;
          }

          //infiniteScroll got loop length
          this.tempDataLength = this.tempData.length;
          if (this.tempDataLength > 10) {
            this.loopLength = 10;
            this.tempDataLength = this.tempDataLength - 10;
          } else
            this.loopLength = this.tempDataLength;

          for (let i = 0; i < this.loopLength; i++) {
            this.loandata.push(this.tempData[i]);
          }
          console.log("Testing scroll Loan data >> " + JSON.stringify(this.loandata));
          console.log("Testing scroll Temp data >> " + JSON.stringify(this.tempData));
          this.loading.dismiss();
        },
          error => {
            let toast = this.toastCtrl.create({
              message: this.util.getErrorMessage(error),
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
            this.loading.dismiss();
          });
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }
  }

  // nextPage(obj) {
  //   this.navCtrl.push(ScheduledetailPage,
  //     {
  //       loanListflag: true
  //     });
  // }
  nextPage(obj) {
    this.navCtrl.push(ScheduledetailPage,
      {
        PersonDataDetail: obj,
        loanListflag: true
      });
  }

  gobackmain() {
    this.navCtrl.setRoot(TabsPage,
      {
      });
  }

  ionViewDidEnter() {
    this.backButtonExit();
  }



  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      this.platform.exitApp();
    });
  }
}
