import { Component,ViewChild,ElementRef } from '@angular/core';
import { NavController, NavParams,ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { AgentTransferPage } from '../agent-transfer/agent-transfer';
/**
 * Generated class for the LocationDetail page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare var google;


@Component({
  selector: 'page-location-detail',
  templateUrl: 'location-detail.html',
  providers : [ChangelanguageProvider]
})
export class LocationDetail {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  data={lat:'',lng:'',name:'',phone:'',address:''};
  lat:any;
  lng:any;
  flag=false;

  latLng:any;
  font:string;
  showFont:any=[];
  textEng: any=["mWallet","Transfer"];
  textMyan : any = ["mWallet","လုပ်ဆောင်မည်"];
  constructor(public navCtrl: NavController, public navParams: NavParams, public geolocation: Geolocation,public toastCtrl:ToastController,public changeLanguage:ChangelanguageProvider,public storage:Storage) {
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguage.changelanguage(font, this.textEng,this.textMyan).then(data =>
      {
        this.showFont = data;
      });
    });
  }

  ionViewDidLoad() {
    this.data = this.navParams.get('data');
    console.log("Agent Location====="+JSON.stringify(this.data));
    this.flag=this.navParams.get('show');
    this.lat=this.data.lat;
    this.lng=this.data.lng;
    this.loadMap();
  }
  loadMap(){
    try{
      this.latLng = new google.maps.LatLng(this.lat, this.lng);
      let mapOptions = {
        center: this.latLng,
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position:this.latLng
      });

      let content = "<h6>"+this.data.name+"</h6>";

      let infoWindow = new google.maps.InfoWindow({
        content: content
      });

      google.maps.event.addListener(marker, 'click', () => {
        infoWindow.open(this.map, marker);
      });
    }catch(e){
      ////console.log("error>>"+JSON.stringify(e));
      let toast = this.toastCtrl.create({
        message:"Fail to show map.",
         duration: 3000,
         position: 'bottom',
      //  showCloseButton: true,
      //  closeButtonText: 'OK',
        dismissOnPageChange: true,
      });
      
      toast.present(toast);
    }

  }
  goAgent() { 
    //console.log('Post Data'+JSON.stringify(data));   
    //this.navCtrl.push(CashoutTransferPage, { phone: data.phone1,name:data.name});    
    this.navCtrl.push(AgentTransferPage, {
       phone: this.data.phone,
       data: ''
     });
  }
}
