import { Component } from '@angular/core';
import { Platform, NavController, ToastController, ViewController, AlertController, NavParams, Events, PopoverController, MenuController, ActionSheetController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Changefont } from '../changefont/changeFont';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { ChooseContactPage } from '../choose-contact/choose-contact';
import { UtilProvider } from '../../providers/util/util';
import { NotificationPage } from '../notification/notification';
import { App, LoadingController } from 'ionic-angular';
import { TopUp } from '../top-up/top-up';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { File } from '@ionic-native/file';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Geolocation } from '@ionic-native/geolocation';
import { PicsPage } from '../pics/pics';
import { BarcodeScanner, BarcodeScannerOptions } from "@ionic-native/barcode-scanner";
import { Payment } from '../payment/payment';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { ContactPage } from '../contact/contact';
import { Device } from '@ionic-native/device';
import { QRValuePage } from '../qr-value/qr-value';
import { DocumentsPage } from '../documents/documents';
import { MessagePage } from '../message/message';
@Component({
  selector: 'page-more',
  templateUrl: 'more.html',
  providers: [Changefont, CreatePopoverPage]
})
export class MorePage {

  letterObj = {
    to: '',
    from: '',
    text: ''
  }

  textEng: any = [
    "More", "Channels", "Camera",
    "QR", "Map", "Pics",
    "Docs"
  ];
  textMyan: any = [
    "More", "Channels", "ကင်မရာ",
    "QR", "​​မြေပုံ", "Pics",
    "Docs"
  ];
  textData: any = [];
  showFont: any = '';

  confirm: any;
  level: any;
  ipaddress: string;
  popover: any;
  selectedTitle: string;
  showText: string;
  carryData: any = {};
  username: string = '';
  _obj = {
    userID: "", sessionID: "", payeeID: "", beneficiaryID: "",
    frominstituteCode: "", toInstitutionCode: "", toAcc: "", balance: "", bankCharges: "", commissionCharges: "", remark: "", transferType: "2", sKey: "", field1: "", field2: ""
  };
  type: any = { name: "mWallet", code: "mWallet" };
  dType: any;

  options: BarcodeScannerOptions;
  photoName: any;
  loadingIcon: any;
  latitude: any;
  longitude: any;
  location: any;
  buttonText: any;
  scannedText: any;
  docData: any;
  isLoading: any;
  public loading;

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    public changefont: Changefont,
    public storage: Storage,
    public events: Events,
    public sqlite: SQLite,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public popoverCtrl: PopoverController,
    public global: GlobalProvider,
    public appCtrl: App,
    public http: Http,
    public toastCtrl: ToastController,
    public util: UtilProvider,
    public createPopover: CreatePopoverPage,
    private file: File,
    private geolocation: Geolocation,
    private _barcodeScanner: BarcodeScanner,
    private nativeGeocoder: NativeGeocoder,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    private device: Device,
    public actionSheetCtrl: ActionSheetController,
  ) {
    this.showFont = "uni";

    this.dType = 'digital';
    //this.menuCtrl.swipeEnable(false);
    this.storage.get('userData').then((userData) => {
      this._obj = userData;
      ////console.log("Transfer userData data=" + JSON.stringify(this._obj.balance));
    });
    this.events.subscribe('userData', userData => {
      this._obj = userData;
      ////console.log("userData-event-subscribe>>" + JSON.stringify(this._obj.balance));
    });
    this.storage.get('phonenumber').then((phonenumber) => {
      this._obj.userID = phonenumber;
      ////console.log("Transfer userData ID=" + JSON.stringify(this._obj.userID));
    });
    this.storage.get('username').then((username) => {
      ////console.log('Your name is', username);
      this.username = username;
    });
    this.events.subscribe('username', username => {
      ////console.log('Your user name is', username);
      this.username = username;
    })
    this.events.subscribe('userData', userData => {
      ////console.log('Your userData is', userData);
      if (userData !== undefined && userData !== "") {
        this._obj = userData;
      }
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  noticount() {
    this.showText = '';
    this.navCtrl.push(NotificationPage);
  }
  itemTapped(event, page) {
    this.showText = "";
    this.navCtrl.push(NotificationPage);
  }

  getNotification() {
    let phone = this._obj.userID; //this.util.removePlus(this._obj.userID)
    let param = {
      "userID": phone
    };
    ////console.log("request notification  data>>" + JSON.stringify(param));
    if (this.global.netWork == 'connected') {
      this.http.post(this.ipaddress + '/service001/getNotiCount', param).map(res => res.json()).subscribe(data => {
        ////console.log("response notification  data>>" + JSON.stringify(data));
        if (data.code == '0000') {
          if (data.notiCount > 0) {
            this.showText = data.notiCount;
          }
        }
      }, error => {
        this.getError(error);
      });
    }
  }

  gomenu(s) {
    if (s == 1) {
      this.navCtrl.push(MessagePage);
    } else if (s == 2) {
      this.goCamera();
    } else if (s == 3) {
      this.chooseQR();
    } else if (s == 4) {
      this.showMap();
    } else if (s == 5) {
      this.navCtrl.push(PicsPage);
    } else if (s == 6) {
      this.navCtrl.push(DocumentsPage);
    }
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  typeChange(s) {
    this.type = s;
    this.type.name = s.name;
    this.type.code = s.code;
    ////console.log(JSON.stringify(s));
  }

  goTopUp() {
    this.navCtrl.push(TopUp);
  }

  goCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      //allowEdit: true,
      targetWidth: 600,
      targetHeight: 600,
    }

    this.camera.getPicture(options).then((imagePath) => {
      this.loadingCCP();
      ////console.log('Camera success' + imagePath);
      let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());

    }, (error) => {
      this.getError(error);
    });
  }

  loadingCCP() {
    this.loadingIcon = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Processing...',
    });

    this.loadingIcon.present();
  }

  dismiss() {
    this.loadingIcon.dismissAll();
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    ////console.log("namePaht == " + namePath + "   //// currentNmae == " + currentName + "   ////  newFileName == " + newFileName);
    ////console.log("this.file.datadirectory == " + this.file.dataDirectory);
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.docData = this.file.dataDirectory + newFileName;
      this.photoName = newFileName;
      ////console.log("photos=" + JSON.stringify(this.docData));
      this.createDB();
    }, error => {
      alert('Error while storing file.' + JSON.stringify(error));
    });
  }

  createDB() {
    /*this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success create or open database");
      db.executeSql('CREATE TABLE IF NOT EXISTS DOCPHOTO(id INTEGER PRIMARY KEY AUTOINCREMENT,PhotoPath TEXT,Name TEXT,Latitude TEXT,Longitude TEXT,Location TEXT,CreateDate TEXT,CreateTime TEXT)', {})
        .then((success) => {
          this.getLocation();
         // //console.log('Executed SQL success')
        }, (error) => {
          //console.error("Unable to create database", error);
        })
        .catch(e => console.log(e));
    }).catch(e => console.log(e));*/
  }

  getLocation() {
    let options = {
      timeout: 30000
    };

    this.geolocation.getCurrentPosition(options).then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      ////console.log(' getting latitude ', resp.coords.latitude);
      ////console.log(' getting longitude ', resp.coords.longitude);
      this.nativeGeocoder.reverseGeocode(this.latitude, this.longitude)
        .then((result: NativeGeocoderReverseResult[]) => {
          this.location = result[0].locality + ", " + result[0].countryName;
          this.insertPhoto();
         ////console.log(JSON.stringify(result))
        }, (err) => {
          this.dismiss();
          let toast = this.toastCtrl.create({
            message: "Please take photo again. Please turn on location access.",
            duration: 5000,
            position: 'bottom',
            //showCloseButton: true,
            dismissOnPageChange: true,
            //closeButtonText: 'OK'
          });
          toast.present(toast);
          ////console.log('Error getting location', JSON.stringify(err));
        });
      // )
      //   .catch((error: any) => //console.log(error));
    }, (err) => {
      this.dismiss();
      let toast = this.toastCtrl.create({
        message: "Please take photo again. Please turn on location access.",
        duration: 5000,
        position: 'bottom',
        //showCloseButton: true,
        dismissOnPageChange: true,
        //closeButtonText: 'OK'
      });
      toast.present(toast);
      ////console.log('Error getting location', JSON.stringify(err));
    });
    /* })
    .catch((error: any) => //console.log(error));
}).catch((error) => {
  //console.log('Error getting location', error);
}); */
  }

  insertPhoto() {
    let date = new Date()
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    const weekNames = ["SUN", "MON", "TUE", "WED", "THUR", "FRI", "SAT"];
    const dayNames = [];
    let month = monthNames[date.getMonth()];
    let day = dayNames[date.getUTCDate()];
    let createData = month + "-" + date.getDate() + "-" + date.getFullYear();
    let wDay = weekNames[date.getDay()];
    let hoursMinutes = [date.getHours(), date.getMinutes()];
    let creteTime = wDay + " " + this.formatAMPM(hoursMinutes);
    ////console.log("time " + creteTime);

    /*this.sqlite.create({
      name: "mw.db",
      location: "default"
    }).then((db: SQLiteObject) => {
      ////console.log("success create or open database");

      db.executeSql("INSERT INTO DOCPHOTO(PhotoPath,Name,Latitude ,Longitude ,Location,CreateDate, CreateTime) VALUES (?,?,?,?,?,?,?)", [this.docData, this.photoName, this.latitude, this.longitude, this.location, createData, creteTime]).then((data) => {
        ////console.log("Insert data successfully", data);
        this.dismiss();
        this.navCtrl.push(PicsPage);
      }, (error) => {
        //console.error("Unable to insert data", error);
      });
    }).catch(e => //console.log(e));*/
  }

  formatAMPM(date) {
    var hours = date[0];
    var minutes = date[1];
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  createFileName() {
    let d = new Date().getTime();
    // let n = d.getTime();
    let newFileName = "Doc-" + d + ".jpg";
    return newFileName;
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //showCloseButton: true,
      dismissOnPageChange: true,
      //closeButtonText: 'OK'
    });
    toast.present(toast);
    this.isLoading = false;
    ////console.log("Oops!");
  }

  scanQR(param) {
    this.buttonText = "Loading..";
    this.loading = true;
    this.options = {
      formats: "QR_CODE",
      "prompt": " ",
    }

    this._barcodeScanner.scan(this.options).then((barcodeData) => {
      if (barcodeData.cancelled) {
       // //console.log("User cancelled the action!");
        this.buttonText = "Scan";
        this.loading = false;

        return false;
      }

      this.scannedText = barcodeData.text;
      ////console.log("scan result=" + this.scannedText);

      if (param == "contact") {
        try {
          let datacheck_scan = JSON.parse(this.scannedText);

          if (datacheck_scan[0] == "contacts") {
            this.navCtrl.push(ContactPage, {
              data: datacheck_scan,
              fromPage: "QR"
            });
          } else {
            this.toastInvalidQR();
          }
        } catch (e) {
          this.toastInvalidQR();
          
        }
      } else if (param == "payment") {
        try {
          let datacheck_scan = JSON.parse(this.scannedText);

          if (datacheck_scan[0] == "payments") {
            this.navCtrl.push(Payment, {
              data: datacheck_scan
            });
          } else {
            this.toastInvalidQR();
          }
        } catch (e) {
          this.toastInvalidQR();
          
        }
      } else if (param == "other") {
        if (this.isURL(this.scannedText)) {
          window.open(this.scannedText, '_system');
        } else {
          this.navCtrl.push(QRValuePage, {
            paramQRValue: this.scannedText
          });
        }
      }
    }, (err) => {
      ////console.log(err);
    });
  }

  chooseQR() {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Add Contact',
          icon: !this.platform.is('ios') ? 'contact' : null,
          handler: () => {
            this.scanQR("contact");
          }
        },
        {
          text: 'Payment',
          icon: !this.platform.is('ios') ? 'cash' : null,
          handler: () => {
            this.scanQR("payment");
          }
        },
        {
          text: 'Other',
          icon: !this.platform.is('ios') ? 'link' : null,
          handler: () => {
            this.scanQR("other");
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            ////console.log('Cancel clicked');
          }
        }
      ]
    });

    actionSheet.present();
  }

  toastInvalidQR() {
    let toast = this.toastCtrl.create({
      message: "Invalid QR Type.",
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true,
    });

    toast.present(toast);
  }

  isURL(str) {
    let pattern = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    ////console.log("pattern test = " + pattern.test(str));

    if (!pattern.test(str)) {
      return false;
    } else {
      return true;
    }
  }

  showMap() {
    window.open('geo://?q=', '_system');  // android
    // window.open('http://maps.apple.com/?q=','_system') //ios
  }

  ionViewWillEnter() {
    this.menuCtrl.swipeEnable(true);
  } 

  ionViewCanEnter() {
    this.menuCtrl.swipeEnable(true);
  }
}
