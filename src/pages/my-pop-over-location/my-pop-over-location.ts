import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'page-my-pop-over-location',
  templateUrl: 'my-pop-over-location.html',
  providers: [ChangelanguageProvider]
})
export class MyPopOverLocationPage {
  popoverItemList: any = [{ name: '', key: 0, icon: 'assets/red-dot.png' }, { name: '', key: 1, icon: 'assets/blue-dot.png' }, { name: '', key: 2, icon: 'assets/green-dot.png' }, { name: '', key: 3, icon: 'assets/yellow-dot.png' },{ name: '', key: 4, icon: 'assets/orange-dot.png' }];
  font: string;
  textMyan: any = [{ name: 'အားလုံး', key: 0, icon: 'assets/blue-dot.png' }, { name: 'အေတီအမ်', key: 1, icon: 'assets/blue-dot.png' }, { name: 'ဘဏ်ခွဲ', key: 2, icon: 'ios-home-outline' }, { name: 'ကိုယ်စားလှယ်', key: 3, icon: 'ios-home-outline' }, { name: 'Merchant', key: 1, icon: 'assets/orange-dot.png' }];
  textEng: any = [{ name: 'All', key: 0, icon: 'assets/blue-dot.png' }, { name: 'ATM', key: 1, icon: 'assets/blue-dot.png' }, { name: 'Branch', key: 2, icon: 'ios-home-outline' }, { name: 'Agent', key: 3, icon: 'ios-home-outline' }, { name: 'Merchant', key: 1, icon: 'assets/orange-dot.png' }];
  showFont: any;
  isChecked: any;
  flag_lan: boolean;
  flag_lan0: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public changelanguage: ChangelanguageProvider, public events: Events, public storage: Storage, public global: GlobalProvider) {
    this.events.subscribe('changelanguage', lan => {
      this.font = lan.data;
      this.changeLanguageForPopup(lan, this.textEng, this.textMyan).then(data => {
        this.popoverItemList = data;
      });
    });
    this.storage.get('language').then((font) => {
      this.font = font;
      this.changeLanguageForPopup(font, this.textEng, this.textMyan).then(data => {
        this.popoverItemList = data;
      });
    });
  }

  changeLanguageForPopup(lan, txtEng, txtMyan) {
    //console.log("hh="+JSON.stringify(txtMyan))
    if (lan == 'uni') {
      for (let j = 0; j < txtMyan.length; j++) {
        this.popoverItemList[j].name = txtMyan[j].name;
      }
    }
    else if (lan == 'zg') {
      for (let j = 0; j < txtMyan.length; j++) {
        //this.popoverItemList[j].name = this.changefont.UnitoZg(txtMyan[j].name);
      }
    } else if (lan == 'myan') {
      for (let j = 0; j < txtMyan.length; j++) {
        this.popoverItemList[j].name = txtMyan[j].name;
      }
    }
    else {
      for (let j = 0; j < txtEng.length; j++) {
        this.popoverItemList[j].name = txtEng[j].name;
      }
    }
    return Promise.resolve(this.popoverItemList);
  }

  ionViewDidLoad() {

  }

  changeLanguage(s, a) {
    this.viewCtrl.dismiss(s);
    this.global.nearbyIndex = a;
  }

}
