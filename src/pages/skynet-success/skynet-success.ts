
import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AlertController, LoadingController, NavController, NavParams } from 'ionic-angular';
import { ChangelanguageProvider } from '../../providers/changelanguage/changelanguage';
import { UtilProvider } from '../../providers/util/util';
import { Http } from '@angular/http';
import { GlobalProvider } from '../../providers/global/global';
import { WalletPage } from '../wallet/wallet';
@Component({
  selector: 'page-skynet-success',
  templateUrl: 'skynet-success.html'
})
export class SkynetSuccessPage {

  textEng: any = [
    "Skynet Payment Success", "Bank Reference No.",
    "Card No.", "Package Name",
    "Voucher Type", "Amount",
    "Bank Charges", "Total Amount",
    "Bank Reference No.", "Transaction Date",
    "CLOSE", "Movie Name"
  ];
  textMyan: any = [
    "အသေးစိတ်အချက်အလက်", "အမှတ်စဉ်",
    "ကဒ်နံပါတ်​", "ပက်​ကေ့နာမည်",
    "​ဘောက်ချာ အမျိုးအစား", "ငွေပမာဏ",
    "ဝန်​ဆောင်ခ", "စုစုပေါင်း ငွေပမာဏ",
    'အမှတ်စဉ်', 'လုပ်ဆောင်ခဲ့သည့်ရက်',
    "ပိတ်မည်", "​ဇာတ်ကားနာမည်"
  ];
  showFont: any = [];
  font: any = '';

  merchantSuccessObj: any;
  amount: any;
  commissionAmount: any;
  totalAmount: any;
  loading: any;
  ipaddress: any;
  constructor(
    public navCtrl: NavController, public navParams: NavParams,public util: UtilProvider,
    public storage: Storage, public changeLanguage: ChangelanguageProvider,public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,public http: Http, public global: GlobalProvider
  ) {
    this.merchantSuccessObj = this.navParams.get("data");
    
    this.amount = this.util.formatAmount(this.merchantSuccessObj.amount);
    this.commissionAmount = this.util.formatAmount(this.merchantSuccessObj.commissionAmount);
    this.totalAmount = this.util.formatAmount(this.merchantSuccessObj.totalAmount);
    this.storage.get('ipaddress').then((result) => {
      if (result == null || result == '') {
          this.ipaddress = this.global.ipaddress;
      }
      else {
          this.ipaddress = result;
      }
  })
    this.storage.get('language').then((font) => {
      this.font = font;

      this.changeLanguage.changelanguage(font, this.textEng, this.textMyan).then(data => {
        this.showFont = data;
      });
    });
  }

 // closeSkynetSuccess() {
  //  this.navCtrl.setRoot(WalletPage);
    /* let paramMerchantSuccessObj = {
      merchantID: this.merchantSuccessObj.merchantID,
      //processingCode: this.merchantSuccessObj.merchantCode
    }; */
    /* this.navCtrl.setRoot(SkynetTopupPage, {
      param: paramMerchantSuccessObj
    }); */
  //}
  closeSkynetSuccess(){ 
    this.checkContactCardNo();   
  } 

  checkContactCardNo() {    
    let parameter = {
      sessionID: this.merchantSuccessObj.token,
      userID: this.merchantSuccessObj.senderCode,
      t1: this.merchantSuccessObj.cardNo,
      //contact_Name: this.merchantSuccessObj.contact_Name
    }
    this.http.post(this.ipaddress + '/payment/checkContactCardNo', parameter).map(res => res.json()).subscribe(
      result => {
        if(result.code == "0000"){
          this.navCtrl.setRoot(WalletPage);
        }
        else if(result.code == "0001"){
          let confirm = this.alertCtrl.create({
            title: "Confirm",
            message: "Do you want to add Card No.",
            enableBackdropDismiss: false,
            buttons: [
              {
                text: 'Yes',
                handler: () => {
                  this.addContactCardNo();
                }
              },
              {
                text: 'No',
                handler: () => {
                  this.navCtrl.setRoot(WalletPage);
                }
              }
            ]
          });
          confirm.present();
        }else{
          this.navCtrl.setRoot(WalletPage);
        }
      },
      error => {        
        this.loading.dismiss();
      }
    );
  }
  addContactCardNo() {    
    let parameter = {
      sessionID: this.merchantSuccessObj.token,
      userID: this.merchantSuccessObj.senderCode,
      t1: this.merchantSuccessObj.cardNo,
      t2: this.merchantSuccessObj.contactName
    }
    this.http.post(this.ipaddress + '/payment/addContactCardNo', parameter).map(res => res.json()).subscribe(
      result => {
        if(result.code == "0000"){
          this.navCtrl.setRoot(WalletPage);
        }else if(result.code == "0016"){
          this.navCtrl.setRoot(WalletPage);
        }
        
      },
      error => {        
        
      }
    );
  }
}


