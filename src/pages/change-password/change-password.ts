import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, ModalController, LoadingController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { DatePipe } from '@angular/common';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
  providers: [CreatePopoverPage]
})
export class ChangePasswordPage {

  textEng: any = ["Change Password", "Old Password", "New Password", "Confirm Password", "Change"];
  textMyan: any = ["လျှို့ဝှက်နံပါတ် ပြောင်းမည်", "လျှို့ဝှက်နံပါတ်အဟောင်း", "လျှို့ဝှက်နံပါတ်အသစ်", "လျှို့ဝှက်နံပါတ်အသစ်အတည်ပြု", "အတည်ပြုပါ"];
  textData: string[] = [];
  language: any;
  showFont: any;

  ipaddress: string;

  phone: any;
  userdata: any;
  public loading;

  oldPassword: string = '';
  newPassword: string = '';
  passwordConfirm: string = '';

  errormsg: any;

  msgForUser: any;
  pdata:any={};
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public http: Http,
    public datePipe: DatePipe,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public sqlite: SQLite,
    public events: Events,
    public util: UtilProvider,
    public alertCtrl: AlertController,
    public global: GlobalProvider,
    public createPopover: CreatePopoverPage, private all: AllserviceProvider
  ) {
    this.showFont = "uni";
    // this.menuCtrl.swipeEnable(false);

    this.userdata = navParams.get("userData");
    ////console.log("sign up=", JSON.stringify(this.userdata));

    this.phone = this.userdata.userID;

   // this.ipaddress = this.global.ipaddress;
   this.storage.get('ipaddress').then((ip) => {
    if (ip !== undefined && ip !== "" && ip != null) {
      this.ipaddress = ip;
    } else {
      this.ipaddress = this.global.ipaddress;
    }
    this.readPasswordPolicy();
  });
   
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
  }

  changelanguage(lan) {
    this.language = lan;

    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  checkUndefinedOrNullOrBlank(param) {
    if (param == undefined || param == null || param == "") {
      return true;
    } else {
      return false;
    }
  }

  checkBlankAfterTrim(param) {
    if (param == "") {
      return true;
    } else {
      return false;
    }
  }

  updatePassword() {
    let isValid = true;

    if (this.checkUndefinedOrNullOrBlank(this.oldPassword)) {
      isValid = false;
      this.errormsg = "Please Enter Old Password.";
      //this.showAlert();
    }

    else if (this.checkUndefinedOrNullOrBlank(this.newPassword)) {
      isValid = false;
      this.errormsg = "Please Enter New Password.";
      //this.showAlert();
    }

    else if (this.checkUndefinedOrNullOrBlank(this.passwordConfirm)) {
      isValid = false;
      this.errormsg = "Please Enter Confirm Password.";
      //this.showAlert();
    }

    if (isValid) {
      this.oldPassword = this.oldPassword.trim();
      this.newPassword = this.newPassword.trim();
      this.passwordConfirm = this.passwordConfirm.trim();

      if (this.checkBlankAfterTrim(this.oldPassword)) {
        this.msgForUser = "Invalid Old Password.";
        this.showAlert();
      } else if (this.checkBlankAfterTrim(this.newPassword)) {
        this.errormsg = "Invalid New Password.";
        //this.showAlert();
      } else if (this.checkBlankAfterTrim(this.passwordConfirm)) {
        this.errormsg = "Invalid Confirm Password.";
        //this.showAlert();
      } else if (this.newPassword != this.passwordConfirm) {
        this.msgForUser = "New Password and Confirm Password should be same.";
        this.showAlert();
      } else if (this.oldPassword == this.newPassword) {
        this.msgForUser = "Old Password and New Password should not be same.";
        this.showAlert();
      } else {
        this.doUpdatePassword();
      }
    } 
  }

  doUpdatePassword() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true
    });
    this.loading.present();
    let iv = this.all.getIvs();
    let dm = this.all.getIvs();
    let salt = this.all.getIvs(); 
    let changePasswordParams = {
      password: this.all.getEncryptText(iv, salt, dm, this.oldPassword),
      newpassword: this.all.getEncryptText(iv, salt, dm, this.newPassword),
      userid: this.phone,
      sessionid: this.userdata.sessionID,
      "iv": iv, "dm": dm, "salt": salt
    };
  /*   let text = this.all.getEncryptText(iv, salt, dm, 'လျှို့ဝှက်နံပါတ် ပြောင်းမည်')
    console.log("encrypt password>>" + text)
    console.log("decrypt password>>" + this.all.getDecryptText(iv, salt, dm, text)) */
    this.http.post(this.ipaddress + '/service001/changePassword', changePasswordParams).map(res => res.json()).subscribe(
      data => {
        this.loading.dismiss();
        ////console.log("response >> " + JSON.stringify(data));

        if (data.msgCode == '0000') {
          this.msgForUser = "Your password updated successfully.";
          let toast = this.toastCtrl.create({
            message: this.msgForUser,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.oldPassword = '';
          this.newPassword = '';
          this.passwordConfirm = '';
          let params = { tabIndex: 0, tabTitle: "Home" };
          this.navCtrl.setRoot(TabsPage, params).catch((err: any) => {
          });
        } else {
          this.msgForUser = data.msgDesc;
          this.showAlert();
        }
      }, error => {
        this.loading.dismiss();

        this.msgForUser = "Your password updated fails.";
        this.showAlert();

        this.getError(error);
      }
    );
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: '',
      enableBackdropDismiss: false,
      message: this.msgForUser,
      buttons: [{
        text: 'OK',
        handler: () => {
          ////console.log('OK clicked');
        }
      }]
    });

    alert.present();
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }

    let msg = '';

    if (code == '005') {
      msg = "Please check internet connection!";
    } else {
      msg = "Can't connect right now. [" + code + "]";
    }

    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });

    //toast.present(toast);
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }
  
  readPasswordPolicy(){
    let param = {
      "userID": this.userdata.userID,
      "sessionID": this.userdata.sessionID,
      "pswminlength":"",
      "pswmaxlength":"",
      "spchar":"",
      "upchar":"",
      "lowerchar":"",
      "pswno":"",
      "msgCode" : "",
      "msgDesc" : ""
    };
    
    this.http.post(this.ipaddress + '/service001/readPswPolicy', param).map(res => res.json()).subscribe(response => {
      
      if (response.msgCode == "0000") {
        this.pdata=response;
      }
    },
      error => {
        this.getError(error);
      });
  }
}