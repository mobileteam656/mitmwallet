import { Component,ViewChild,HostListener } from '@angular/core';
import 'rxjs/add/operator/map';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Login } from '../login/login';
import { Platform, NavController,ViewController, NavParams, PopoverController, Events, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { MyPopOverListPage } from '../my-pop-over-list/my-pop-over-list';
import { App } from 'ionic-angular';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { UtilProvider } from '../../providers/util/util';
import { AccountTransfer } from '../transfer/transfer';
import { Language } from '../language/language';
import { Contacts,ContactFieldType, ContactFindOptions } from '@ionic-native/contacts';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { EventLoggerProvider } from '../../providers/eventlogger/eventlogger';

@Component({
  selector: 'page-choose-contact',
  templateUrl: 'choose-contact.html'
})
export class ChooseContactPage {
  // @ViewChild('ph') ph;
  rootNavCtrl: NavController;
  cont: any = [];
  groupedContacts: any = [];
  userData: any;
  userDataChat: any;
  isLoading: any;
  nores: any;
  UserList: any = [];
  UserList1:any=[];
  ipaddress: string = '';
  regdata: any;
  totalCount: any;
  state: boolean;
  popover: any;
  errormsg: string = '';
  contactData: any = [];
  contact: boolean = false;
  public loading;
  profileImageLink: any;
  phoneflag:boolean=true;
  textEng: any = [
    "Transfer To","Phone number","Please enter your transfer phone no"
  ];
  phno:any=[];
  textMyan: any = [
    "ငွေလွှဲမည့်သူ ရွေးရန်","ဖုန်းနံပါတ်","ကျေးဇူးပြု၍ ငွေလွှဲမည့်သူ ဖုန်းနံပါတ်ထည့်ပါ"
  ];
  textData: any = [];
  arr:string="";
  code: any;
  
  constructor(public popoverCtrl: PopoverController, public events: Events,private firebase: FirebaseAnalytics,
    public navCtrl: NavController, public loadingCtrl: LoadingController,
    public platform: Platform, public global: GlobalProvider,
    public storage: Storage, public navParams: NavParams,
    public http: Http, public toastCtrl: ToastController,
    public util: UtilProvider, public appCtrl: App,
    private slimLoader: SlimLoadingBarService,private eventlog:EventLoggerProvider,
    private contacts: Contacts,public viewCtrl: ViewController,
    public alertCtrl: AlertController) {
    this.storage.get('profileImage').then((profileImage) => {
      if (profileImage != undefined && profileImage != null && profileImage != '') {
        this.profileImageLink = profileImage;
      } else {
        this.profileImageLink = this.global.digitalMediaProfile;
      }
    });
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((font) => {
      this.changelanguage(font);
    });
    this.storage.get('userData').then((userData) => {
      if (userData !== undefined && userData !== "") {
        this.userDataChat = userData;
       // this.ipaddress = this.global.ipaddressWallet;
       this.storage.get('ipaddress').then((ip) => {
        if (ip !== undefined && ip != null && ip !== "") {
          this.ipaddress = ip;
        } else {
          this.ipaddress = this.global.ipaddress;
        }
      });
       
    
      }
    });

    this.rootNavCtrl = navParams.get('rootNavCtrl');
    this.isLoading = true;

  }
  @ViewChild('myInput') myInput;
  ionViewDidLoad(){
    console.log("ionViewDidLoad");
    setTimeout(() => {
      this.myInput.setFocus();
    }, 500);
  }
 

  getChatUser() {
    let param = {
      "userID": this.userDataChat.userID,
      "sessionID": this.userDataChat.sessionID
    }
    console.log("request list =" + JSON.stringify(param));
    this.http.post(this.ipaddress + "/chatservice/getContact", param).map(res => res.json()).subscribe(result => {
      //console.log("mine contact response list =" + JSON.stringify(result));
      // if (result.dataList == undefined || result.dataList == null || result.dataList == '') {
      //   this.nores = 0;
        
      // } else 
      if (result.code == "0012") {
        this.util.SessiontimeoutAlert(result.desc);
      }
      else {
        this.code=result.code;
        ////console.log("result.regdata is else");
        let tempArray = [];

        if (!Array.isArray(result.dataList)) {
          tempArray.push(result.dataList);
          result.dataList = tempArray;
        }
        if (result.dataList.length > 0) {
          //  this.groupedContacts = result.data;
          this.UserList = result.dataList;
          //  this.getGroupData();
          this.nores = 1;
        }
      }
      console.log("response Contact list =" + JSON.stringify(this.UserList));
      this.isLoading = false;
    }, error => {
      ////console.log("signin error=" + error.status);
      this.nores = 0;
      this.isLoading = false;
      this.getError(error);
    });
   
  }

  getGroupData() {
    let group_to_values = this.groupedContacts.reduce(function (obj, item) {
      obj[item.t16] = obj[item.t16] || [];
      obj[item.t16].push(item);
      return obj;
    }, {});
    let groups = Object.keys(group_to_values).map(function (key) {
      let name;
      if (key == '')
        name = "User";
      else
        name = "Administrator";
      return { t16: name, data: group_to_values[key] };
    });

    this.UserList = groups;
    if (this.userData.t16 == '') {
      for (let i = 0; i < this.UserList.length; i++) {
        if (this.UserList[i].t16 == "Administrator") {
          let array = [];
          if (this.UserList[i].data.length > 0) {
            this.UserList[i].data[0].t2 = "Admin";
            array.push(this.UserList[i].data[0]);
          }
          this.UserList[i].data = array;
        }
      }
    }
   
  }

  chooseContact(a) {
    this.navCtrl.push(AccountTransfer, {
      param: a
    });
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = "Can't connect right now. [" + code + "]";
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });
    toast.present(toast);
    this.isLoading = false;
    // this.loading.dismiss();
    ////console.log("Oops!");
  }


  doRefresh(refresher) {
    ////console.log('Begin async operation', refresher);
    this.getChatUser();
    setTimeout(() => {
      ////console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(MyPopOverListPage, {});
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data == "1") {
        this.presentPopoverLanguage(ev);
      }
      if (data == "2") {
        this.goLogout();
      }
    });
  }

  presentPopoverLanguage(ev) {
    this.popover = this.popoverCtrl.create(Language, {
    });

    this.popover.present({
      ev: ev
    });

    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);

    this.popover.onWillDismiss(data => {
      ////console.log("Selected Item is " + data);
    });
  }

  /* presentPopoverContact(ev) {
    this.popover = this.popoverCtrl.create(PopoverContactAddPage, {}, { cssClass: 'contact-popover' });
    this.popover.present({
      ev: ev
    });
    let doDismiss = () => this.popover.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.popover.onDidDismiss(unregBackButton);
    this.popover.onWillDismiss(data => {
      if (data != undefined && data !=null && data !='') {
        this.contactData = data;
        this.goContactSave();
      }
    });
  } */


  goLogout() {
    // this.storage.remove('phonenumber');
    // this.firebase.logEvent('log_out', {log_out:'logout-user'});
    this.storage.remove('username');
    this.storage.remove('userData');
    this.storage.remove('firstlogin');
    this.appCtrl.getRootNav().setRoot(Login);
  }

  changelanguage(lan) {
    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }

  ionViewWillEnter() {
    this.storage.get('userData').then((userData) => {
      if (userData != undefined && userData != null && userData != "") {
        this.userDataChat = userData;
        this.storage.get('ipaddress').then((ip) => {
          if (ip !== undefined && ip != null && ip !== "") {
            this.ipaddress = ip;
            this.getChatUser();
          } else {
            this.ipaddress = this.global.ipaddress;
            this.getChatUser();
          }
        });
      }
    });

    this.events.subscribe('userData', userData => {
      if (userData != undefined && userData != null && userData != "") {
        this.userDataChat = userData;
        //this.getChatUser();
      }
    });
  }

  enterContact(){
    this.contacts.pickContact().then((data) => {
      this.contactData=data.phoneNumbers;
      for (let i = 0; i < this.contactData.length; i++) {
        // this.ph =this.contactData[i].value;
        
        this.contactData[i].value = this.contactData[i].value.toString().replace(/ +/g, "");
        this.arr=this.contactData[i].value;
      }
      console.log("contactdata",this.contactData.value);
    },error=>{
      console.log(error);
    });
  }
  
  
  next(){
    // if(this.arr != undefined && this.arr != null && this.arr != "" && this.arr.trim() != "" && this.arr.length !=2){
      if(this.arr.length ==10 || this.arr.length ==11 ||this.arr.length ==12 || this.arr.length==9){
      this.errormsg='';
    let normalizePhone = this.util.normalizePhone(this.arr);
    if (normalizePhone.flag == true) {
      this.arr= normalizePhone.phone;
      this.phno=normalizePhone.phone;
    console.log("arrrrrrrrrrr",this.arr);      
    } else {
      // this.viewCtrl.dismiss(this.arr);
      
    }
    this.isLoading = true;   
    let removePlusPhone = this.arr;
    let parameter = {
      userID: this.userDataChat.userID,
      sessionID: this.userDataChat.sessionID,
      phone: removePlusPhone,
      name: '',
      type: '1',
      t3: '0'
    };
    if (this.global.netWork == 'connected') {
      this.http.post(this.ipaddress + '/chatservice/addContact', parameter).map(res => res.json()).subscribe(data => {
       
        if (data.code == "0000") {  
          let param = {
            "userID": this.userDataChat.userID,
            "sessionID": this.userDataChat.sessionID
          }
          console.log("request list =" + JSON.stringify(param));
          this.http.post(this.ipaddress + "/chatservice/getContact", param).map(res => res.json()).subscribe(data1 => { 
          //this.getChatUser();
          //console.log("CODE",this.code);
          if(data1.code=="0000"){
            if (data1.dataList.length > 0) {
              //  this.groupedContacts = result.data;
              this.UserList = data1.dataList;}
            for (let i = 0; i < this.UserList.length; i++) {
    
              if(this.UserList[i].phone==this.arr){
                this.navCtrl.push(AccountTransfer,{
                  param : this.UserList[i]})
                 //this.phoneflag=false;
              }   
            
            } 
             }
                  // else if (data1.code == "0012") {
                  //   this.util.SessiontimeoutAlert(data.desc);
                  // }
                  else{
                    console.log("ARRRRRRRRRRRRRR")
                  }
                });
                    this.isLoading = false;
                    
        } 
        else {
            let alert = this.alertCtrl.create({
              title: '',
              enableBackdropDismiss: false,
              message: 'Invalid Session',
              buttons: [{
                text: 'OK',
                handler: () => {
                 
                }
              }]
            });
            alert.present();
          this.isLoading = false;
        }
      }, error => {
        let code;
        this.getError(error);
        this.isLoading = false;     
      }
      );
    }
  } 
  
  else {
    this.arr = "";
    this.errormsg = this.textData[2];
  }
  

  }
  showAlert() {
    let alert = this.alertCtrl.create({
      title: '',
      enableBackdropDismiss: false,
      message: "Please enter correct wallet account number",
      buttons: [{
        text: 'OK',
        handler: () => {
          
        }
      }]
    });

    alert.present();
  }
  showAlertConn() {
    let alert = this.alertCtrl.create({
      title: '',
      enableBackdropDismiss: false,
      message: "Please check your connecton!",
      buttons: [{
        text: 'OK',
        handler: () => {
          
        }
      }]
    });

    alert.present();
  }
  
}
