import { Component } from '@angular/core';
 import {
  AlertController,
  IonicPage,
  LoadingController,
  NavController,
  NavParams,
  Platform,
  ToastController
} from 'ionic-angular';
import { SelectParticipantPage } from '../select-participant/select-participant';
import {TabsPage} from "../tabs/tabs";
import {Storage} from "@ionic/storage";
import {FunctProvider} from "../../providers/funct/funct";
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
//import {ChatHomeTabsPage} from "../chat-home-tabs/chat-home-tabs";
 /**
 * Generated class for the ViewGroupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
import { GlobalProvider } from '../../providers/global/global';

@Component({
  selector: 'page-view-group',
  templateUrl: 'view-group.html',
})
export class ViewGroupPage {
  admin:any = [];
  member:any = [];
  flag:boolean = false;
 // yourData : any = [];
  yourSys: any;
  imgLink: any;
  alertPopup : any;
  loading : any;
  userData: any ;
  updateGroupName : any = "";
  constructor(public alertCtrl: AlertController, public loadingCtrl:LoadingController, public http: Http,  public toastCtrl: ToastController, public platform:Platform, public alert: AlertController, public funct:FunctProvider,public storage:Storage,public navCtrl: NavController, public navParams: NavParams,public global: GlobalProvider) {
    this.member = this.navParams.get("data");
    ////console.log('this.member='+JSON.stringify(this.member));

    this.storage.get('userData').then((result) => {
      ////console.log("your data is in view group = "+ JSON.stringify(result));
      if(result != null || result != ''){
       // this.yourData = result;
        this.userData = result;
        this.yourSys = result.sKey;
        ////console.log('this.yourName='+this.yourSys);
      }
    });

    this.imgLink = this.global.digitalMediaProfile;

  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad ViewGroupPage');
  }

  showAll(){
    this.flag = true;
  }

  createGroup(){
    this.navCtrl.push(SelectParticipantPage,{
      data:  this.member
    });
  }

  leaveAGroup(){
    this.alertPopup = this.alert.create({
      cssClass:'uni',
      message:'Are you sure want to leave group?',
      enableBackdropDismiss : false,
      buttons: [{
        text: "Cancel",
        role: 'cancel',
        handler: () => {
        }
      },{
        text: "OK",
        handler: () => {
          //leave group
          this.leaveGroup();
        }
      }]
    })
    this.alertPopup.present();
    let doDismiss = () => this.alertPopup.dismiss();
    let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
    this.alertPopup.onDidDismiss(unregBackButton);
  }

  leaveGroup(){
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
  
    let url = this.global.ipaddress3 + 'serviceChat/deleteGroup?sysKey=' + this.member.channelkey
    + '&registerKey=' + this.userData.sKey 
    + '&Group=1';

    ////console.log("url=" + url);

    this.http.get(url).map(res => res.json()).subscribe(data => {
      ////console.log("reponse leave group in VG== "+JSON.stringify(data));

      if(data.state){

        this.presentToast(data.msgDesc);
        this.navCtrl.pop();

      }
      else{
       this.presentToast(data.msgDesc);
      }
      this.loading.dismiss();

    }, error => {

      this.presentToast('Delete Fail!');
     // //console.log("leave group error in VG==" + error.status);
      this.loading.dismiss();
       this.getError(error);
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 5000,
      dismissOnPageChange: true,
      position: 'top'
    });
    toast.present();
  }


  changeGruopName() {
    let prompt = this.alertCtrl.create({
      title: this.member.t2,
      //  message: "Enter a name for this new album you're so keen on adding",
      inputs: [
        {
          name: 'groupName',
          placeholder: 'Change Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
           // //console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            ////console.log('Saved clicked');
            this.updateGroupName = data.groupName;
            this.changeName();
          }
        }
      ]
    });
    prompt.present();
  }

  changeName(){
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    let params = {
      autokey: this.member.channelkey,
      groupName: this.updateGroupName
    }
    ////console.log("request changeChannelName =" + JSON.stringify(params));
    this.http.post(this.global.ipaddress3 + "serviceChat/changeChannelName", params).map(res => res.json()).subscribe(result => {
      ////console.log("response changeChannelName =" + JSON.stringify(result));
      if (result.state) {
        this.member.t2 = this.updateGroupName
      }
      else {
        let toast = this.toastCtrl.create({
          message: 'Updated Failed! Please try again.',
          duration: 5000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
      }
      this.loading.dismiss();
    }, error => {
     // //console.log("signin error=" + error.status);
      this.loading.dismiss();
      this.getError(error);
    });
  }

  getError(error){
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     ............006) bad gateway http error (502)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg ='';
    if(code == '005'){
      msg = "Please check internet connection!";
    }
    else{
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });
    toast.present(toast);
    // this.isLoading = false;
    // this.loading.dismiss();
    ////console.log("Oops!");
  }
}
