import { Component } from '@angular/core';
import { IonicPage, NavController, Events, NavParams, ToastController, MenuController, AlertController, LoadingController, Platform } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Http, JSONPBackend } from '@angular/http';
import { UtilProvider } from '../../providers/util/util';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { Changefont } from '../changefont/changeFont';
import { PopoverController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { ShowStatusPage } from '../show-status/show-status';


@Component({
  selector: 'page-loan-list-info',
  templateUrl: 'loan-list-info.html',
  providers: [Changefont]

})
export class LoanListInfoPage {

  ipaddress: string;
  public loading;
  loanHistory: any = [];

  textMyan: any = ["အသေးစိတ်အရစ်ကျချေးငွေစာရင်း", "လျှောက်ထားသူအမည်", "လျှောက်ထားသူဖုန်းနံပါတ်", "အာမခံသူအမည်", "အာမခံသူဖုန်းနံပါတ်",
    "ဆိုင်အမည်", "ဆိုင်လိပ်စာ", "ပစ္စည်းအမည်", "စျေးနှုန်း", "စုစုပေါင်းပမာဏ", "ကြိုတင်ငွေ",
    "ပေးရန်ကာလ", "လစဉ်ဆပ်ရန်ပမာဏ"];
  textEng: any = ["Details", "Applicant Name", "Applicant Phone No", "Gurantor Name", "Gurantor Phone No",
    "Shop Name", "Shop Address", "Product Name", "Product Price", "Total Amount", "Deposit Amount",
    "Terms", "Monthly Installment Amount"];
  textdata: string[] = [];
  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  font: string = '';
  PersonData = this.global.PersonData;
  flagNet: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public events: Events,
    public global: GlobalProvider, public changefont: Changefont, public http: Http, public toastCtrl: ToastController,
    public util: UtilProvider, public platform: Platform, public loadingCtrl: LoadingController, private network: Network) {

    this.storage.get('ip').then((ip) => {
      console.log('Your ip is', ip);
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
        console.log("Your ip is", this.ipaddress);
      }
      else {
        console.log(" undefined  conditon ");
        this.ipaddress = global.ipaddress;
        console.log("Your ip main is", this.ipaddress);
      }

      //Font change
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      })
      this.storage.get('language').then((font) => {

        this.changelanguage(font);
      });
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
    }
  }

  LoanHistory() {
    this.PersonData = this.navParams.get("PersonDataDetail");
    console.log("data :" + this.PersonData.applicantdata.syskey + " : " + this.PersonData.loandata.loanSyskey);
    //added network test
    if (this.global.netWork == "connected") {
      this.loading = this.loadingCtrl.create({
        content: "", spinner: 'circles',
        dismissOnPageChange: true
        //   duration: 3000
      });
      console.log("Applicant syskey :" + this.PersonData.applicantdata.syskey);
      console.log("Loan Syskey :" + this.PersonData.loandata.loanSyskey);
      this.loading.present();
      this.http.get(this.global.ipAdressMFI + '/serviceMFI/getLoanhistoryByAppandLoanSyskey?syskey=' + this.PersonData.applicantdata.syskey + '&loansyskey=' + this.PersonData.loandata.loanSyskey)
        .map(res => res.json()).subscribe(data => {
          console.log("data :" + data);
          if (data.data != null) {
            let tempArray = [];
            if (!Array.isArray(data.data)) {
              tempArray.push(data.data);
              data.data = tempArray;
            }
            this.loanHistory = [];
            if (data.data[0].loandata.monthlyAmount != "" && data.data[0].loandata.monthlyAmount != undefined && data.data[0].loandata.monthlyAmount != null) {
              data.data[0].loandata.monthlyAmount = data.data[0].loandata.monthlyAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
            if (data.data[0].loandata.totalAmount != "" && data.data[0].loandata.totalAmount != undefined && data.data[0].loandata.totalAmount != null) {
              data.data[0].loandata.totalAmount = data.data[0].loandata.totalAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
            for (let i = 0; i < data.data[0].productListData.data.length; i++) {
              if (data.data[0].productListData.data[i].price != "" && data.data[0].productListData.data[i].price != undefined && data.data[0].productListData.data[i].price != null) {
                data.data[0].productListData.data[i].price = data.data[0].productListData.data[i].price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
              }
            }
            this.loanHistory = data.data;
            console.log("Loan List History Info:  is  >>" + JSON.stringify(this.loanHistory));
          }
          this.loading.dismiss();
        },
          error => {
            let toast = this.toastCtrl.create({
              message: this.util.getErrorMessage(error),
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
            this.loading.dismiss();
          });
    } else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
    }
  }

  ionViewDidEnter() {
    this.backButtonExit();
  }

  ionViewDidLoad() {
    this.storage.get('ip').then((ip) => {
      console.log('Your ip is', ip);
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
        console.log("Your ip is", this.ipaddress);
      }
      else {
        console.log(" undefined  conditon ");
        this.ipaddress = this.global.ipaddress;
        console.log("Your ip main is", this.ipaddress);
      }
      if (this.loanHistory.length == 0)
        this.LoanHistory();
    });
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      //this.navCtrl.pop();
      //ps test 
      this.navCtrl.setRoot(ShowStatusPage,
        {
          FromPage: 'LoanList'
        });
    });
  }
}
