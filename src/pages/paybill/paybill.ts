import { Component, NgModule, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Navbar, Events, ToastController, LoadingController, Platform, AlertController } from 'ionic-angular';
import { Changefont } from '../changefont/changeFont';
import { Storage } from '@ionic/storage';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { UtilProvider } from '../../providers/util/util';
//import { MainhomepagePage } from '../mainhomepage/mainhomepage';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { InfoPage } from '../info/info';
import { Login } from '../login/login';
import { TabsPage } from '../tabs/tabs';
import { Network } from '@ionic-native/network';
import { ShoppingCartInfoPage } from '../shopping-cart-info/shopping-cart-info';
//import { ShoppingCartInfoPage } from '../shopping-cart-info/shopping-cart-info';

/**
 * Generated class for the PaybillPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-paybill',
  templateUrl: 'paybill.html',
  providers: [Changefont]

})
export class PaybillPage {

  textMyan: any = ["ငွေပေးချေရမည်.ကာလကိုရွေးချယ်ပါ", "ပရိုမိုးရှင်းအမျိုးအစား", "မရှိပါ", "တွက်ချက်ပါ", "စုစုပေါင်းကုန်ကျစရိတ်",
    "တစ်လလျှင်အရစ်ကျပေးချေရမည်.ငွေ", "ဝန်ဆောင်မှုစရိတ်", "လျှောက်မည်", "အရစ်ကျလျှောက်ခြင်း", "နောက်သို."];

  textEng: any = ['Choose Payment Terms', 'Promotion Type',
    "None", "PROCEED", "Total", "Installment Per Month", "Service Charges", "APPLY NOW", "Loan Apply", "Next"];

  textdata: string[] = [];
  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  font: string = '';
  ipaddress: string;
  testRadioResult;
  amtLimit: any;
  itemLimit: any;
  flag = true;
  public loading;
  photoName: any;
  profileImg: any;
  profile: any;
  imglnk: any;
  successMsg: any;

  PayTermList = [{ 'paymentTerm': '', 'paymentTermSyskey': 0, 'amountLimit': 0, 'itemLimit': '' }];
  PeroidList = [];
  orgsyskey: any;

  defaultimgdata() { return { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" } };

  defaultregtype: any = "";
  //end default
  @ViewChild(Navbar) navBar: Navbar;
  flagNet: any;

  constructor(public navCtrl: NavController, public storage: Storage, public navParams: NavParams,
    public events: Events, public changefont: Changefont, public toastCtrl: ToastController, public alertCtrl: AlertController,
    public http: Http, public util: UtilProvider, private transfer: FileTransfer,
    public global: GlobalProvider, public alerCtrl: AlertController, private file: File,
    public platform: Platform, public loadingCtrl: LoadingController, public network: Network) {
    this.imglnk = this.file.cacheDirectory;

    this.storage.get('ip').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
        console.log("not undefined  conditon ");
        console.log("Your ip is", this.ipaddress);
      }
      else {
        console.log(" undefined  conditon ");
        this.ipaddress = this.global.ipaddress;
        console.log("Your ip main is", this.ipaddress);
      }


      console.log("pay bill :" + JSON.stringify(this.global.PersonData));

      this.BindPaymentTerm();
      console.log("start shop data  paybill:" + this.global.startshop);
      //Font change
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      })
      this.storage.get('language').then((font) => {
        console.log('Your language is', font);
        this.changelanguage(font);
      });
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
    }
  }

  BindPaymentTerm() {
    this.storage.get('orgsyskey').then((orgsyskey) => {
      this.orgsyskey = orgsyskey;
      console.log('orgsyskey', this.orgsyskey);
      // added network test
      if (this.global.netWork == "connected") {
        this.loading = this.loadingCtrl.create({
          content: "", spinner: 'circles',
          dismissOnPageChange: true
          //   duration: 3000
        });
        this.loading.present();
        this.http.get(this.global.ipAdressMFI + '/serviceMFI/getPaymentTermsByOrgSyskey?orgsyskey=' + this.orgsyskey).map(res => res.json()).subscribe(data => {
          if (data != null) {
            let tempArray = [];
            if (!Array.isArray(data.data)) {
              tempArray.push(data.data);
              data.data = tempArray;
            }
            this.PeroidList = [];
            for (let i = 0; i < data.data.length; i++) {
              this.PeroidList.push(data.data[i]);
            }
            console.log("Paybill Period Month List >>" + JSON.stringify(this.PeroidList));
            //psst test for exiting Payment term choose 

            if (this.global.PersonData.loandata.paymentTermSyskey != null && this.global.PersonData.loandata.paymentTermSyskey != undefined && this.global.PersonData.loandata.paymentTermSyskey != '') {
              console.log("call payment :" + this.global.PersonData);
              this.CallPayment(this.global.PersonData.loandata.paymentTermSyskey);
              this.flag = false;
            }

          }
          else {
            let toast = this.toastCtrl.create({
              message: "Can't connect right now!",
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
          }
          this.loading.dismiss();
        },
          error => {
            let toast = this.toastCtrl.create({
              message: this.util.getErrorMessage(error),
              duration: 3000,
              position: 'bottom',
              //  showCloseButton: true,
              dismissOnPageChange: true,
              // closeButtonText: 'OK'
            });
            toast.present(toast);
            this.loading.dismiss();
          });
      } else {
        let toast = this.toastCtrl.create({
          message: "Check your internet connection!",
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
      }
    });
  }

  Checked(paymentTermSyskey) {
    this.CallPayment(paymentTermSyskey);
    console.log("data: " + JSON.stringify(paymentTermSyskey));
    if (paymentTermSyskey != "0" && paymentTermSyskey != "" && paymentTermSyskey != null) {
      this.flag = false;
    }
  }

  CallPayment(paymentTermSyskey) {
    for (var i = 0; i < this.global.PersonData.applyProductList.data.length; i++) {
      this.global.PersonData.applyProductList.data[i].productprice = parseInt(this.global.PersonData.applyProductList.data[i].productprice.toString().replace(/,/g, ""));
      this.global.PersonData.applyProductList.data[i].productprice = parseInt(this.global.PersonData.applyProductList.data[i].productprice.toString().replace(/MMK/g, ""));
    }
    // added for amt 
    this.global.PersonData.loandata.totalAmount = this.global.PersonData.loandata.totalAmount.toString().replace(/MMK/g, "");
    this.global.PersonData.loandata.totalAmount = this.global.PersonData.loandata.totalAmount.toString().replace(/,/g, "");
    this.global.PersonData.loandata.monthlyAmount = this.global.PersonData.loandata.monthlyAmount.toString().replace(/MMK/g, "");
    this.global.PersonData.loandata.monthlyAmount = this.global.PersonData.loandata.monthlyAmount.toString().replace(/,/g, "");
    this.global.PersonData.loandata.paymentTermSyskey = paymentTermSyskey;
    this.global.PersonData.applicantdata.monthly_basic_income = this.global.PersonData.applicantdata.monthly_basic_income.toString().replace(/,/g, "");
    this.global.PersonData.applicantdata.creditAmount = this.global.PersonData.applicantdata.creditAmount.toString().replace(/,/g, "");
    this.global.PersonData.guarantordata.monthly_basic_income = this.global.PersonData.guarantordata.monthly_basic_income.toString().replace(/,/g, "");
    this.global.PersonData.guarantordata.creditAmount = this.global.PersonData.guarantordata.creditAmount.toString().replace(/,/g, "");

    this.http.post(this.global.ipAdressMFI + '/serviceMFI/getMonthlyAmountByTerm', this.global.PersonData).map(res => res.json()).subscribe(data => {
      if (data != null) {
        if (data.loandata.totalAmount != "" && data.loandata.totalAmount != undefined && data.loandata.totalAmount != null) {
          data.loandata.totalAmount = data.loandata.totalAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " MMK";
        }
        if (data.loandata.monthlyAmount != "" && data.loandata.monthlyAmount != undefined && data.loandata.monthlyAmount != null) {
          data.loandata.monthlyAmount = data.loandata.monthlyAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " MMK";
        }
        this.global.PersonData.loandata = data.loandata;
      } else {
        let toast = this.toastCtrl.create({
          message: "PersonData Required!",
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      }
      //this.loading.dismiss();
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.util.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        // this.loading.dismiss();
      });
  }

  LoanApply() {

    this.global.PersonData.loandata.totalAmount = this.global.PersonData.loandata.totalAmount.toString().replace(/,/g, "");
    this.global.PersonData.loandata.totalAmount = this.global.PersonData.loandata.totalAmount.toString().replace(/MMK/g, "");
    this.global.PersonData.loandata.monthlyAmount = this.global.PersonData.loandata.monthlyAmount.toString().replace(/,/g, "");
    this.global.PersonData.loandata.monthlyAmount = this.global.PersonData.loandata.monthlyAmount.toString().replace(/MMK/g, "");
    for (let i = 0; i < this.global.PersonData.applyProductList.data.length; i++) {
      if (this.global.PersonData.applyProductList.data[i].productPrice != "" && this.global.PersonData.applyProductList.data[i].productPrice != undefined && this.global.PersonData.applyProductList.data[i].productPrice != null) {
        this.global.PersonData.applyProductList.data[i].productPrice = this.global.PersonData.applyProductList.data[i].productPrice.toString().replace(",", "");
      }
    }
    //psst added network test
    if (this.global.netWork== "connected") {
      this.loading = this.loadingCtrl.create({
        content: "", spinner: 'circles',
        dismissOnPageChange: true
        //   duration: 3000
      });
      console.log("PersonData from Paybill Page ps>>" + JSON.stringify(this.global.PersonData));
      this.loading.present();
      this.http.post(this.global.ipAdressMFI + '/serviceMFI/applyLoan', this.global.PersonData).map(res => res.json()).subscribe(result => {
        if (result.msgCode == "0000") {
          for (var i = 0; i < this.global.PersonData.imagealldata.imageparsedata.length; i++) {
            console.log("image parse data" + JSON.stringify(this.global.PersonData.imagealldata.imageparsedata[i]));
            this.uploadPhoto(this.global.PersonData.imagealldata.imageparsedata[i].imgname, this.global.PersonData.imagealldata.imageparsedata[i].profileImg, this.global.PersonData.imagealldata.imageparsedata[i].photoName, this.global.PersonData.imagealldata.imageparsedata[i].profile)
          }
          this.global.PersonData = this.global.defaultData();
          this.global.frontnrcdata = this.defaultimgdata();
          this.global.backnrcdata = this.defaultimgdata();
          this.global.guafrontnrcdata = this.defaultimgdata();
          this.global.guabacknrcdata = this.defaultimgdata();
          this.global.householdfrontdata = this.defaultimgdata();
          this.global.householdbackdata = this.defaultimgdata();
          this.global.policecertificatedata = this.defaultimgdata();
          this.global.employmentletterdata = this.defaultimgdata();

          this.global.PersonData.imagealldata.imageparsedata = [];
          this.global.regtype = this.defaultregtype;
          this.global.outletSyskey = '';
          let confirm = this.alertCtrl.create({
            title: '',
            enableBackdropDismiss: false,
            message: "Submit Successfully",
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  console.log('OK clicked');
                  this.navCtrl.setRoot(Login, {}); //*** navigate tab page 
                  //this.navCtrl.popToRoot();
                }
              }
            ]
          })
          confirm.present();
        } else {
          let toast = this.toastCtrl.create({
            message: result.msgDesc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        }
        this.loading.dismiss();
      }, error => {
        let toast = this.toastCtrl.create({
          message: this.util.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
    } else {
      let toast = this.toastCtrl.create({
        message: this.util.getErrorMessage("Check your internet connection!"),
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
      //this.loading.dismiss();
    }
  }

  uploadPhoto(imgname, img, name, pro) {
    var url = this.ipaddress + '/serviceMFI/mobileupload';
    this.profileImg = img;
    this.photoName = name;
    this.profile = pro;
    console.log("profile image :" + JSON.stringify(this.profileImg));
    console.log("Photo name :" + JSON.stringify(this.photoName));
    console.log("profile :" + JSON.stringify(this.profile));
    if (this.profileImg != undefined && this.profileImg != '' && this.profileImg != null) {

      let options: FileUploadOptions = {
        fileKey: 'file',
        fileName: this.photoName,
        chunkedMode: false,
      }

      const fileTransfer: FileTransferObject = this.transfer.create();
      console.log("Before upload" + url);
      fileTransfer.upload(this.profile, url, options).then((data) => {
        let abc = this.imglnk + this.photoName;
        // this.storage.set('localPhotoAndLink', abc);
        // this.events.publish('localPhotoAndLink', abc);
        this.closeProcessingLoading();
        let toast = this.toastCtrl.create({
          message: this.successMsg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        //this.slimLoader.complete();
        console.log("uploadPhoto upload success" + JSON.stringify(data));
      },
        error => {
          this.closeProcessingLoading();

          let toast = this.toastCtrl.create({
            message: "Uploading photo is not successful.",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });

          toast.present(toast);
          //this.slimLoader.complete();
          console.log("uploadPhoto upload error" + JSON.stringify(error));
        }
      );
    } else {
      this.closeProcessingLoading();

      let toast = this.toastCtrl.create({
        message: this.successMsg,
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
      //this.slimLoader.complete();
    }
  }

  openProcessingLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true,
      duration: 3000
    });
    this.loading.present();
  }

  closeProcessingLoading() {
    this.loading.dismiss();
  }

  NextApplicant(obj) {
    this.global.comeHome = true;
    console.log("start page paybill next:" + this.global.startshop);
    console.log("start page paybill next:" + this.global.PersonData);
    this.storage.get('userData').then((result) => {
      if (result == null || result == '') {
        this.navCtrl.setRoot(Login,
          {
            obj: this.global.PersonData
          });
      }
      else {
        this.navCtrl.push(InfoPage,
          {
            obj: this.global.PersonData
          });
      }
    });
  }

  gobackmain() {
    this.navCtrl.setRoot(ShoppingCartInfoPage,
      {

      });

  }
  ionViewDidEnter() {
    this.backButtonExit();
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      console.log("Active Page=" + this.navCtrl.getActive().name);
      this.gobackmain();

    });
  }

  touchPay() {
    if (this.PeroidList.length <= 0 || this.PeroidList == undefined || this.PeroidList == null) {
      this.BindPaymentTerm();
    }
  }
}
