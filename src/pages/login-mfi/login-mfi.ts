import { Component } from '@angular/core';
import { IonicPage, Platform, MenuController, NavController, NavParams, LoadingController, PopoverController, ToastController, AlertController, Events, ActionSheetController } from 'ionic-angular';
import { SQLite } from '@ionic-native/sqlite';
import { Storage } from '@ionic/storage';
// import { SMS } from '@ionic-native/sms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Global } from '../global/global';
import { Changefont } from '../changefont/changeFont';
import 'rxjs/add/operator/map';
import { Language } from '../language/language';
// import { Register } from '../register/register';
// import { MainhomepagePage } from '../mainhomepage/mainhomepage';


import { PopOverListPage } from '../pop-over-list/pop-over-list';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';

// import { OtpConfirm } from '../otp-confirm/otp-confirm';
import { MainBlankPage } from '../main-blank-page/main-blank-page';
import { Device } from '@ionic-native/device';
import { InfoPage } from '../info/info';
import { TabsPage } from '../tabs/tabs';
import { Network } from '@ionic-native/network';
// import { LoanListInfoPage } from '../loan-list-info/loan-list-info';
// import { SchedulePage } from '../schedule/schedule';
// import { ShowStatusPage } from '../show-status/show-status';
import { PaybillPage } from '../paybill/paybill';
import { ShowStatusPage } from '../show-status/show-status';
import { SchedulePage } from '../schedule/schedule';
/**
* Generated class for the Login page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
declare var require: any

@Component({
  selector: 'page-login-mfi',
  templateUrl: 'login-mfi.html',
  providers: [Global, Changefont]
})
export class LoginMfi {
  regtypeMsg: string = "Register by Email Address";
  textMyan: any = ["ဖုန်းနံပါတ်ရိုက်ထည့်ပါ", 'လျှို့ဝှက်နံပါတ်', 'အိုင်ဒီနံပါတ်ရိုက်ထည့်ပါ', "လျှို့ဝှက်နံပါတ်ရိုက်ထည့်ပါ", "အကောင့်ထဲဝင်ရန်",
    "ဘာသာစကားများ", "အင်္ဂလိပ်", "ယူနီကုဒ်", "ဇော်ဂျီ", "ပယ်ဖျက်မည်", "ဖုန်းနံပါတ်", "စည်းကမ်းနှင့်သတ်မှတ်ချက်များ ကိုသဘောတူခြင်း",
    "အကောင့်သစ်လုပ်ရန်", "မှတ်ပုံတင်ခြင်း", "အကောင့်ဖွင်.မည်", "စည်းကမ်းနှင့်သတ်မှတ်ချက်များ", "ဖုန်းနံပါတ်စစ်ပါ", "အကောင့်ထဲဝင်ရန်"];

  textEng: any = ['Enter Phone No.', 'Password', 'Enter User ID', 'Enter Password', 'LOGIN',
    "Languages", "English", "Unicode", "Zawgyi", "Cancel", "Phone", "By Signing up you agree to the Terms and Conditions",
    "Apply a new Account", "Register", "Create Account", "Terms & Conditions", "Check Phone No.", 'Login'];
  textdata: string[] = [];
  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  errormsg: string = '';
  font: string = '';
  register: any = {};
  apptype: string = "001";
  appName: string;
  brand: string = "0002"; // Yum Yum
  enable: any = true;
  img: any = "assets/yumyum3.png";
  region: string[] = [];
  public loading;
  phonenum: any;
  popover: any;
  selectedTitle: string;
  ipaddress: string;
  loginmessage: string;
  normalizePhone: any;
  deviceID: string = '';
  orgsyskey: any;
  loginData = {
    'userph': '',
    //'userpsw': ''
  };
  appdata: any;
  userph: any;
  flagNet: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController, public popoverCtrl: PopoverController,
    public toastCtrl: ToastController, public storage: Storage, public changefont: Changefont, public globalPage: Global, public alertCtrl: AlertController,
    public events: Events, public platform: Platform, public actionsheetCtrl: ActionSheetController, public menuCtrl: MenuController
    ,public global: GlobalProvider, public util: UtilProvider, public device: Device, private network: Network) {
    this.register.regtype = "phone";
    this.appName = this.global.appName;
    this.menuCtrl.swipeEnable(true);  //psst
    this.storage.get('ip').then((ip) => {


      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      }
      else {
        this.ipaddress = global.ipaddress;
      }

      if (this.device.uuid != '' && this.device.uuid != undefined) {
        this.deviceID = this.device.uuid;
      }

      this.getOrganizationSyskey();
      console.log("start shop data  login:" + this.global.startshop);
      console.log("login presondata:" + JSON.stringify(this.global.PersonData));
    });

    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    }) //...
    this.storage.get('language').then((font) => {

      this.changelanguage(font);
    });
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
    }
  }

  // ionViewDidLoad() {
  //   this.global.checkNetwork();
  //   console.log("Hello Network from Login : " + this.global.flagNet);
  // }

  onLogin() {
    //let num = /^[0-9-\+]*$/;
    // let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    console.log("start shop data  info1:" + this.global.startshop);
    let re = /^\+?[0-9]*$/;
    let flag = true;
    if (this.loginData.userph == '' || this.loginData.userph == null || this.loginData.userph == undefined) {
      this.loginmessage = this.textdata[2];
    } else {
      this.loginmessage = "";
      if (re.test(this.loginData.userph)) {
        this.normalizePhone = this.util.normalizePhone(this.loginData.userph);
        //psst add plus sign 
        if (this.normalizePhone.flag == true) {
          this.loginData.userph = this.normalizePhone.phone;
          this.userph = this.util.removePlus(this.normalizePhone.phone);
        }


      }
      else {
        let phoneData = { phone: "", flag: false }
        this.normalizePhone = phoneData;
      }
      if (this.normalizePhone.flag == true) {
        if (flag) {
          this.loading = this.loadingCtrl.create({
            content: "", spinner: 'circles',
            dismissOnPageChange: true
            //   duration: 3000
          });
          this.loading.present();
          let param = {
            registered_mobileNo: this.userph,   //psst changed add +959
            deviceID: this.deviceID,
            userType: 2,
          };
          console.log("start shop data  info2:" + this.global.startshop);
          this.http.post(this.global.ipAdressMFI + '/service001/mbsignin', param).map(data => data.json()).subscribe(data => {
            console.log("start shop data  info3:" + this.global.startshop);
            if (data.msgCode == '0000') {
              if (data.otpcode) {
                let paramOTP = { "userPhno": data.registered_mobileNo, "membersyskey": data.syskey, "type": "" };

                this.http.post(this.global.ipAdressMFI + '/service001/getOTP', paramOTP).map(res => res.json()).subscribe(res => {
                  let dataOTP = { phoneNo: '+' + data.registered_mobileNo, otpcode: res.otpcode, rKey: res.rKey };

                  if (res.code == '0000') {

                    this.loading = this.loadingCtrl.create({
                      content: "", spinner: 'circles',
                      dismissOnPageChange: true,
                      duration: 3000
                    });
                    this.loading.present();
                    this.appdata = data;
                    this.http.get(this.global.ipAdressMFI + '/service001/sendSms?phoneno=' + dataOTP.phoneNo + '&message=' + res.msg + '').map(res => res.json().data).subscribe(data => {
                      if (data != null) {
                        if (data.RESULT == "Send Successfully") {

                          // this.navCtrl.setRoot(OtpConfirm, {
                          //   dataOTP: dataOTP,
                          //   data: this.appdata,
                          //   frompage: "loginpage"
                          // });
                        }
                        else {
                          let toast = this.toastCtrl.create({
                            message: data.ERRORDESC,
                            duration: 3000,
                            position: 'bottom',
                            dismissOnPageChange: true,
                          });
                          toast.present(toast);

                        }


                      } else {
                        // this.navCtrl.setRoot(OtpConfirm, {
                        //   dataOTP: dataOTP,
                        //   data: this.appdata,
                        //   frompage: "loginpage"
                        // });
                      }

                    }, error => {
                      let toast = this.toastCtrl.create({
                        message: this.util.getErrorMessage(error),
                        duration: 3000,
                        position: 'bottom',
                        //  showCloseButton: true,
                        dismissOnPageChange: true,
                        // closeButtonText: 'OK'
                      });
                      toast.present(toast);
                      //this.loading.dismiss();
                    });


                    this.loading.dismiss();



                    this.storage.get('userData').then((result) => {
                      if (result == null || result == '') {
                        this.storage.set('userData', data);
                        this.events.publish('userData', data);
                      }
                      else {
                        this.storage.remove('userData');
                        this.storage.set('userData', data);
                        this.events.publish('userData', data);
                      }
                      this.global.applicantsyskey = data.syskey;
                      this.loading.dismiss();

                    });

                    this.loading.dismiss();

                  } else if (res.code == '0014') {
                    let toast = this.toastCtrl.create({
                      message: res.desc,
                      position: 'bottom',
                      showCloseButton: true,
                      dismissOnPageChange: true,
                      closeButtonText: 'OK'
                    });
                    toast.present(toast);
                    this.loading.dismiss();
                  } else if (res.code == '0016') {
                    let confirm = this.alertCtrl.create({
                      title: 'Warning!',
                      enableBackdropDismiss: false,
                      message: res.desc,
                      buttons: [
                        {
                          text: 'OK',
                          handler: () => {

                            this.storage.remove('userData');
                            //this.storage.set('userData', '');
                            // this.events.publish('userData', '');
                            this.global.PersonData = this.global.defaultData();
                            this.global.applicantsyskey = "";
                            this.navCtrl.setRoot(LoginMfi, {});
                            this.navCtrl.popToRoot();
                          }
                        }
                      ]
                    })
                    confirm.present();
                    this.loading.dismiss();

                  }
                }, error => {
                  let toast = this.toastCtrl.create({
                    message: this.util.getErrorMessage(error),
                    duration: 3000,
                    position: 'bottom',
                    //  showCloseButton: true,
                    dismissOnPageChange: true,
                    // closeButtonText: 'OK'
                  });
                  toast.present(toast);
                  this.loading.dismiss();
                }
                );
              } else {
                console.log("start shop data  info1:" + this.global.startshop);


                this.storage.get('userData').then((result) => {
                  if (result == null || result == '') {
                    this.storage.set('userData', data);
                    this.events.publish('userData', data);
                  }
                  else {
                    this.storage.remove('userData');
                    this.storage.set('userData', data);
                    this.events.publish('userData', data);
                  }
                  this.global.applicantsyskey = data.syskey;

                  this.loading.dismiss();

                });
                if (this.global.startloanstaus == 1) {  
                 
                  this.storage.set('loan', ShowStatusPage);
                  this.events.publish('loan', ShowStatusPage);            
                  this.navCtrl.setRoot(ShowStatusPage, {   //psst updated 

                  });

                } else if (this.global.startschedulestatus == 1) {                 
                  this.storage.set('schedule', SchedulePage);
                  this.events.publish('schedule', SchedulePage);                 
                  this.navCtrl.setRoot(SchedulePage, {
                  });
                } else {
                  this.storage.set('applyloan', InfoPage);
                  this.events.publish('applyloan', InfoPage);                                
                  this.navCtrl.setRoot(InfoPage, {

                  });
                }
              }
            }
            else if (data.msgCode == '0016') {
              // this.navCtrl.setRoot(Register, {
              //   data: data,
              // });
            }
            else {
              let toast = this.toastCtrl.create({
                message: data.desc,
                position: 'bottom',
                showCloseButton: true,
                dismissOnPageChange: true,
                closeButtonText: 'OK'
              });
              toast.present(toast);
              this.loading.dismiss();

            }
          },
            error => {
              let toast = this.toastCtrl.create({
                message: this.util.getErrorMessage(error),
                duration: 3000,
                position: 'bottom',
                //  showCloseButton: true,
                dismissOnPageChange: true,
                // closeButtonText: 'OK'
              });
              toast.present(toast);
              this.loading.dismiss();
            });
        }
      } else {
        let toast = this.toastCtrl.create({
          message: this.textdata[16],
          duration: 3000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    }
  }



  inputChange(data) {
    this.loginmessage = "";
  }

  presentPopover(ev) {
    this.popover = this.popoverCtrl.create(PopOverListPage, {}, { cssClass: 'contact-popover' });
    this.popover.present({
      ev: ev
    });
    this.popover.onWillDismiss(data => {

      if (data.key == 0) {
        this.navCtrl.push(MainBlankPage, {
          data: ''
        });
      }
      else if (data.key == 1) {
        this.navCtrl.push(Language, {
          data: ''
        });
      }
    });
  }

  openRegister() {
    // this.navCtrl.setRoot(Register, {
    //   data: ''
    // });
  }

  getOrganizationSyskey() {
    //psst added network test
    if (this.global.netWork== "connected") {
      // this.loading = this.loadingCtrl.create({
      //   content: "", spinner: 'circles',
      //  // dismissOnPageChange: true
      //      duration: 300
      // });
      // this.loading.present();

      this.http.get(this.global.ipAdressMFI + '/service001/getOrganizationSyskey').map(res => res.json()).subscribe(data => {
        if (data != null) {
          this.orgsyskey = data.syskey;

          this.storage.get('orgsyskey').then((result) => {
            if (result == null || result == '') {
              this.storage.set('orgsyskey', this.orgsyskey);
              this.events.publish('orgsyskey', this.orgsyskey);
            }
            else {
              this.storage.remove('orgsyskey');
              this.storage.set('orgsyskey', this.orgsyskey);
            }
          });
        } else {
          let toast = this.toastCtrl.create({
            message: data.msgCode,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);   //temporary ps 
        }
        // this.loading.dismiss();
      },
        error => {
          console.log("log " + error);
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          //this.loading.dismiss();
        });

    }
    else {
      let toast = this.toastCtrl.create({
        message: "Check your internet connection!",
        duration: 3000,
        position: 'bottom',
        //  showCloseButton: true,
        dismissOnPageChange: true,
        // closeButtonText: 'OK'
      });
      toast.present(toast);
      this.loading.dismiss();
    }
  }

  onChange(s, i) {
    if (i == "09") {
      this.loginData.userph = "+959";
    }
    else if (i == "959") {
      this.loginData.userph = "+959";
    }
    // if(i=="2" || i=="4" || i=="5" || i=="7" || i=="96" ||  i=="97" ||  i=="98" ||  i=="969")
    // {
    //   this.loginData.userph = "+959"+this.loginData.userph;
    // }
  }

  checkNetwork() {
    this.network.onDisconnect().subscribe(() => {
      this.flagNet = 'none';
      console.log('network was disconnected :-(');
    });

    this.network.onConnect().subscribe(() => {
      this.flagNet = 'success';
      console.log('network connected!');
    });
  }
  gobackmain() {
    this.navCtrl.setRoot(PaybillPage,
      {
      });

  }
  ionViewDidEnter() {
    this.backButtonExit();
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      if (this.global.startshop == 1) { this.gobackmain(); }
      else {
        this.platform.exitApp();
      }

    });
  }

 
}
