import { Component, NgModule, ViewChild } from '@angular/core';
import { IonicPage, ActionSheetController, Platform, NavController, NavParams, Navbar, ToastController, MenuController, AlertController, LoadingController, Events, Content, TextInput } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { Http } from '@angular/http';
import { UtilProvider } from '../../providers/util/util';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { Changefont } from '../changefont/changeFont';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FunctProvider } from '../../providers/funct/funct';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { ShoplistPage } from '../shoplist/shoplist';
import { Slides } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
//mport { MainhomepagePage } from '../mainhomepage/mainhomepage';
import { TabsPage } from '../tabs/tabs';
import { Network } from '@ionic-native/network';
import { PaybillPage } from '../paybill/paybill';
import { AutoResume } from '@ng-idle/core';
import { NewShopPage } from '../new-shop/new-shop';
import { WalletPage } from '../wallet/wallet';

@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
  providers: [Changefont]
})

export class InfoPage {
  @ViewChild(Slides) slides: Slides;
  testRadioOpen: boolean;
  testRadioResult;
  title: any;
  callGuran: boolean = true;

  Gdata() {
    return {
      'member_name': '', 'nrc': '', 'dob': '',
      'current_address_region': '', 'current_address_township': '',
      'current_address_street': '', 'current_address_houseno': '',
      'mobile': '', 'email': '', 'company_name': '', 'department': '',
      'current_position': '', 'years_of_service': '', 'monthly_basic_income': '',
      'company_ph_no': '', 'creditForOhters': '0', 'creditAmount': '', 'syskey': '0', "front_nrc_image_path": "", "back_nrc_image_path": ""
    };
  }

  GuranInfo = [{ 'syskey': '', 'member_name': '' }];

  textMyan: any = ["အမည်", "မှတ်ပုံတင်", "မွေးသက္ကရာဇ်", "တိုင်းဒေသ", "မြို့နယ်", "လိပ်စာ", "ဖုန်းနံပါတ်", "အီးမေးလ်", "ကုမ္ပဏီအမည်",
    "ဌာန", "လက်ရှိရာထူး", "အလုပ်သက်တမ်း", "လစဉ်ဝင်ငွေ(ကျပ်)", "ချေးငွေယူဖူးပါသလား", "ငွေ(ကျပ်)ပမာဏကိုရိုက်ထည့်ပါ", "ယူဖူးပါတယ်", "မယူဖူးပါဘူး", "ကုမ္ပဏီဖုန်းနံပါတ်",
    "အမည်ကိုရိုက်ထည့်ပါ", "မှတ်ပုံတင်ကိုရိုက်ထည့်ပါ", "မွေးသက္ကရာဇ်ကိုရိုက်ထည့်ပါ", "ဒေသကိုရွေးချယ်ပါ", "မြို့နယ်ကိုရွေးချယ်ပါ", "လိပ်စာကိုရိုက်ထည့်ပါ",
    "ဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ", "အီးမေးလ်ကိုရိုက်ထည့်ပါ", "ကုမ္ပဏီအမည်ကိုရိုက်ထည့်ပါ", "ဌာနကိုရိုက်ထည့်ပါ", "လက်ရှိရာထူးကိုရိုက်ထည့်ပါ", "လုပ်သက်ကိုရိုက်ထည့်ပါ",
    "လစဉ်ဝင်ငွေကိုရိုက်ထည့်ပါ", "ချေးငွေယူဖူးပါသလားကိုရွေးချယ်ပါ", "ငွေပမာဏကိုရိုက်ထည့်ပါ", "ကုမ္ပဏီဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ",
    "မှန်ကန်သောမှတ်ပုံတင်ကိုရိုက်ထည့်ပါ", "မှန်ကန်သောဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ", "မှန်ကန်သောအီးမေးလ်ကိုရိုက်ထည့်ပါ", "18 နှစ်ပြည့်သူများသာချေးငွေလျှောက်ထားနိုင်ပါသည်",
    "မှန်ကန်သောကုမ္ပဏီဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ", "နောက်သို.", "မှတ်ပုံတင်အရှေ.ဘက်", "မှတ်ပုံတင်အနောက်ဘက်", "​လျှောက်ထားသူမှတ်ပုံတင်အရှေ.ဘက်",
    "လျှောက်ထားသူမှတ်ပုံတင်အနောက်ဘက်", "အာမခံသူ​မှတ်ပုံတင်အရှေ.ဘက်", "အာမခံသူမှတ်ပုံတင်အနောက်ဘက်", "အိမ်အရှေ.ဘက်ပုံ", "အိမ်အနောက်ဘက်ပုံ",
    "ရဲစခန်းထောက်ခံစာ", "အလုပ်သမားထောက်ခံစာ", "လျှောက်ထားသူလျှောက်မည်.အမျိုးအစားကိုရွေးချယ်ပါ", "လျှောက်ထားသူမှတ်ပုံတင်အရှေ.ဘက်ရွေးပါ", "လျှောက်ထားသူမှတ်ပုံတင်အနောက်ဘက်ရွေးပါ",
    "အာမခံသူ​မှတ်ပုံတင်အရှေ.ဘက်ရွေးပါ", "အာမခံသူမှတ်ပုံတင်အနောက်ဘက်ရွေးပါ", "လျှောက်ထားသူအလုပ်သမားထောက်ခံစာရွေးပါ", "အိမ်အရှေ.ဘက်ပုံရွေးပါ", "အိမ်အနောက်ဘက်ပုံရွေးပါ", "ရဲစခန်းထောက်ခံစာရွေးပါ",
    "မှတ်ပုံတင်မည်.အမျိုးအစားရွေးပါ", "ရွေးရန်", "လျှောက်ထားသူအချက်အလက်", "အာမခံသူအချက်အလက်", "ပုံတင်ပေးရန်", "လျှောက်ထားသူသာပုံတင်ပေးရန်", "အိမ်ပိုင်ဆိုင်မှုအထောက်အထား", "ရဲစခန်းအထောက်အထား",
    "အာမခံသူ​အမည်ကိုရိုက်ထည့်ပါ", "အာမခံသူ​မှတ်ပုံတင်ကိုရိုက်ထည့်ပါ", "အာမခံသူ​မွေးသက္ကရာဇ်ကိုရိုက်ထည့်ပါ", "အာမခံသူ​ဒေသကိုရွေးချယ်ပါ", "အာမခံသူ​မြို့နယ်ကိုရွေးချယ်ပါ", "အာမခံသူ​လိပ်စာကိုရိုက်ထည့်ပါ",
    "အာမခံသူ​ဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ", "အာမခံသူ​အီးမေးလ်ကိုရိုက်ထည့်ပါ", "အာမခံသူ​ကုမ္ပဏီအမည်ကိုရိုက်ထည့်ပါ", "အာမခံသူ​ဌာနကိုရိုက်ထည့်ပါ", "အာမခံသူ​လက်ရှိရာထူးကိုရိုက်ထည့်ပါ", "အာမခံသူ​လုပ်သက်ကိုရိုက်ထည့်ပါ",
    "အာမခံသူ​လစဉ်ဝင်ငွေကိုရိုက်ထည့်ပါ", "အာမခံသူ​ချေးငွေယူဖူးပါသလားကိုရွေးချယ်ပါ", "အာမခံသူ​ငွေပမာဏကိုရိုက်ထည့်ပါ", "အာမခံသူ​ကုမ္ပဏီဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ",
    "အာမခံသူ​မှန်ကန်သောမှတ်ပုံတင်ကိုရိုက်ထည့်ပါ", "အာမခံသူ​မှန်ကန်သောဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ", "အာမခံသူ​မှန်ကန်သောအီးမေးလ်ကိုရိုက်ထည့်ပါ", "18 နှစ်ပြည့်အာမခံသူ​များသာချေးငွေလျှောက်ထားနိုင်ပါသည်",
    "အာမခံသူ​မှန်ကန်သောကုမ္ပဏီဖုန်းနံပါတ်ကိုရိုက်ထည့်ပါ", "အာမခံသူကိုရွေးချယ်ပါ", "​လျှောက်မည်"];

  textEng: any = ['Name', 'NRC', 'Date of Birth', 'Region', 'Township', "Current Address", "Phone No.", "Email", "Company Name",
    "Department", "Current Position", "Years of Service", "Monthly Basic Income (MMK)", "Credit from Others?", "Enter Credit Amount (MMK)", "Yes", "No", "Company Phone No.",
    "Enter Applicant Name", "Enter Applicant NRC", "Enter Applicant Date of Birth", "Choose Applicant Address Region", "Choose Applicant Address Township",
    "Enter Applicant Current Address", "Enter Applicant Phone No.", "Enter Applicant Email", "Enter Applicant Company Name", "Enter Applicant Department",
    "Enter Applicant Current Position", "Enter Applicant Years of service", "Enter Applicant Monthly Basic Income", "Choose Applicant CreditorForOthers",
    "Enter Applicant Credit Amount", "Enter Applicant Company Phone No.", "Invalid Applicant NRC No!", "Invalid Applicant Phone No.!", "Invalid Applicant Email!",
    "Applicant only apply loan over 18 years!", "Invalid Applicant Company Phone No.!",
    "Next", "Front NRC", "Back NRC", "Applicant's Front NRC",
    "Applicant's Back NRC", "Guarantor’s Front NRC", "Guarantor’s Back NRC ", "Household Recommendataion (Front)", "Household Recommendataion (Back)",
    "Police Recommendataion", "Applicant Employment Letter", "Choose Applicant Recommendataion", "Choose Applicant's Front NRC ", "Choose Applicant's Back NRC ",
    "Choose Guarantor’s Front NRC ", "Choose Guarantor’s Back NRC ", "Choose Applicant Employment Letter",
    "Choose Household Recommendataion (Front)", "Choose Household Recommendataion (Back)", "Choose Police Recommendataion",
    "Choose Recommendataion Type", "Browse...", "Applicant Information", "Gurantor Information", "Upload Photo", "Upload only for Applicant",
    "House Recommendataion", "Police Recommendataion", "Enter Guarantor Name", "Enter Guarantor NRC", "Enter Guarantor Date of Birth", "Choose Guarantor Address Region",
    "Choose Guarantor Address Township", "Enter Guarantor Current Address", "Enter Guarantor Phone No.", "Enter Guarantor Email", "Enter Guarantor Company Name",
    "Enter Guarantor Department", "Enter Guarantor Current Position", "Enter Guarantor Years of service", "Enter Guarantor Monthly Basic Income", "Choose Guarantor CreditorForOthers",
    "Enter Guarantor Credit Amount", "Enter Guarantor Company Phone No.", "Invalid Guarantor NRC No!", "Invalid Guarantor Phone No.!", "Invalid Guarantor Email!",
    "Guarantor only apply loan over 18 years!", "Invalid Guarantor Company Phone No.!", "Choose your Guarantor", "APPLY NOW"];

  frontnrcdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  backnrcdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  guafrontnrcdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  guabacknrcdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  householdfrontdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  householdbackdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  policecertificatedata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  employmentletterdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  applicantsyskey: any;

  textdata: string[] = [];
  myanlanguage: string = "မြန်မာ";
  englanguage: string = "English";
  language: string;
  font: string = '';
  flag: string;
  errormessagetext: string;
  shop: any[];
  regionList: string[] = [];
  regionListGua: string[] = [];
  townshipList: string[] = [];
  townshipListGua: string[] = [];
  regionData: any = '';
  townData: any = '';
  ipaddress: string;
  dob: any;
  usersyskey: any;
  public loading;
  isToggled: boolean;
  isGuaToggled: boolean;

  //photo upload
  profileImage: any;
  imglnk: any;
  photoExist: any;
  photo: any;
  localStoragePhoto: string = '';
  syskey: any;
  photoName: any;
  profileImg: any;
  profile: any;
  successMsg: any;
  imname: any;
  photos: any = [];
  chatphoto: any;
  DECIMAL_SEPARATOR = ".";
  GROUP_SEPARATOR = ",";

  Newguran: any;

  recommendTypeEng: any = [{ value: 1, descript: 'House Recommendataion' },
  { value: 2, descript: 'Police Recommendataion' }];

  recommendTypeMyan: any = [{ value: 1, descript: 'အိမ်ပိုင်ဆိုင်မှုအထောက်အထား' },
  { value: 2, descript: 'ရဲစခန်းအထောက်အထား' }];

  recommendTypeData: any = [{ value: '', descript: '' }];



  newgran = { 'syskey': '0', 'member_name': 'New Guarantor' };
  @ViewChild(Content) content: Content;
  getDefaultImgObj() {
    return { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  }

  defaultregtype: any = "";
  //end default

  flagNet: any;
  changeFont: any;

  @ViewChild(Navbar) navBar: Navbar;
  constructor(public navCtrl: NavController, public navParams: NavParams, public global: GlobalProvider
    , public http: Http, public util: UtilProvider, public loadingCtrl: LoadingController, public storage: Storage, public alertCtrl: AlertController,
    public events: Events, public toastCtrl: ToastController, public changefont: Changefont, private file: File,
    public actionSheetCtrl: ActionSheetController, private transfer: FileTransfer, private camera: Camera, public platform: Platform,
    public alerCtrl: AlertController, public menuCtrl: MenuController, public datepipe: DatePipe, private keyboard: Keyboard
    , public network: Network) {

    this.menuCtrl.swipeEnable(true);
    
    if (this.global.PersonData.applicantdata.creditForOhters == "0") {
      this.isToggled = false;
    } else {
      this.isToggled = true;
    }
    if (this.global.PersonData.guarantordata.creditForOhters == "0") {
      this.isGuaToggled = false;
    } else {
      this.isGuaToggled = true;
    }
    this.storage.get('phonenumber').then((phonenumber) => {
      this.global.PersonData.applicantdata.mobile = phonenumber;
      console.log('My Phone no is:'+this.global.PersonData.applicantdata.mobile);
    });
    this.storage.get('username').then((username) => {
      this.global.PersonData.applicantdata.member_name = username;
      console.log(this.global.PersonData.applicantdata.member_name);
    });
    this.storage.get('applicantsyskey').then((applicantsyskey) => {
      this.global.applicantsyskey=applicantsyskey;
      console.log('Global syskey: '+this.global.applicantsyskey);
     });
    this.global.PersonData.applicantdata.syskey = this.global.applicantsyskey; //new memeber
    this.BindApplicantData();
    this.storage.get('ip').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      }
      else {
        this.ipaddress = global.ipAdressMFI;
      }
     
      //Font change
      this.events.subscribe('changelanguage', lan => {
        this.changelanguage(lan);
      })
      this.storage.get('language').then((font) => {
        this.changelanguage(font);
        this.title = this.textdata[61]; //for nav title
      });

      this.events.subscribe('changeFont', lan => {
        this.changeFont = lan;
        console.log("font data is " + this.changeFont);
        // if (this.global.PersonData.applicantdata.member_name == "" && this.global.PersonData.applicantdata.nrc == "") {
          // this.BindApplicantData();
        //}
      })
      this.storage.get('font').then((font) => {
        this.changeFont = font;
        console.log("font data get is " + this.changeFont);
        // if (this.global.PersonData.applicantdata.member_name == "" && this.global.PersonData.applicantdata.nrc == "") {
         // this.BindApplicantData();
        //}
      });

      //photo upload 
      this.imglnk = this.file.cacheDirectory;
      this.getRegion();
      this.global.regtype = global.regtype;
      console.log("Test Length >> " + this.GuranInfo.length);

    });
  }

  notify() {
    console.log("before is toggle " + this.global.PersonData.applicantdata.creditForOhters);
    if (this.isToggled == true) {
      this.global.PersonData.applicantdata.creditForOhters = "1";
      console.log("afer notify " + this.global.PersonData.applicantdata.creditForOhters);
    } else {
      this.global.PersonData.applicantdata.creditForOhters = "0";
      this.global.PersonData.applicantdata.creditAmount = "";
    }
    console.log("after notity " + this.global.PersonData.applicantdata.creditForOhters);
  }

  notifyGua() {
    console.log("before toggle gua " + this.global.PersonData.guarantordata.creditForOhters);
    if (this.isGuaToggled == true) {
      this.global.PersonData.guarantordata.creditForOhters = "1";
      console.log("afer notify gua" + this.global.PersonData.guarantordata.creditForOhters);
    } else {
      this.global.PersonData.guarantordata.creditForOhters = "0";
      this.global.PersonData.guarantordata.creditAmount = "";
    }
    console.log("after notify gus" + this.global.PersonData.guarantordata.creditForOhters);
  }

  // toggle control affect on slide
  touch() {
    this.slides.lockSwipes(true);
  }
  release() {
    this.slides.lockSwipes(false);
  }

  changelanguage(font) {
    if (font == "eng") {
      this.font = "";
      for (let i = 0; i < this.textEng.length; i++) {
        this.textdata[i] = this.textEng[i];
      }
      //test
      for (let i = 0; i < this.recommendTypeEng.length; i++) {
        this.recommendTypeData[i] = this.recommendTypeEng[i];
      }
    }
    else if (font == "zg") {
      this.font = "zg";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.changefont.UnitoZg(this.textMyan[i]);
      }
      for (let i = 0; i < this.recommendTypeMyan.length; i++) {
        this.recommendTypeData[i] = this.changefont.UnitoZg(this.recommendTypeMyan[i]);
      }
    }
    else {
      this.font = "uni";
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textdata[i] = this.textMyan[i];
      }
      for (let i = 0; i < this.recommendTypeMyan.length; i++) {
        this.recommendTypeData[i] = this.recommendTypeMyan[i];
      }
    }
  }

  validateEmail(mail) {
    if (mail == "") {
      return true;
    }
    if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(mail)) {
      return (true);
    }
    return (false);
  }

  validateInput() {
    let flag = true;
    if (this.global.PersonData.applicantdata.member_name == undefined || this.global.PersonData.applicantdata.member_name == null || this.global.PersonData.applicantdata.member_name == '') {
      flag = false;
      this.errormessagetext = this.textdata[18];
    } else if (this.global.PersonData.applicantdata.nrc == undefined || this.global.PersonData.applicantdata.nrc == null || this.global.PersonData.applicantdata.nrc == '') {
      flag = false;
      this.errormessagetext = this.textdata[19];
    }
    else if (this.global.PersonData.applicantdata.dob == undefined || this.global.PersonData.applicantdata.dob == null || this.global.PersonData.applicantdata.dob == '') {
      flag = false;
      this.errormessagetext = this.textdata[20];
    } else if (this.global.PersonData.applicantdata.current_address_region == undefined || this.global.PersonData.applicantdata.current_address_region == null || this.global.PersonData.applicantdata.current_address_region == '') {
      flag = false;
      this.errormessagetext = this.textdata[21];
    } else if (this.global.PersonData.applicantdata.current_address_township == undefined || this.global.PersonData.applicantdata.current_address_township == null || this.global.PersonData.applicantdata.current_address_township == '') {
      flag = false;
      this.errormessagetext = this.textdata[22];
    } else if (this.global.PersonData.applicantdata.current_address_street == undefined || this.global.PersonData.applicantdata.current_address_street == null || this.global.PersonData.applicantdata.current_address_street == '') {
      flag = false;
      this.errormessagetext = this.textdata[23];
    } else if (this.global.PersonData.applicantdata.mobile == undefined || this.global.PersonData.applicantdata.mobile == null || this.global.PersonData.applicantdata.mobile == '') {
      flag = false;
      this.errormessagetext = this.textdata[24];
    } else if (this.global.PersonData.applicantdata.company_name == undefined || this.global.PersonData.applicantdata.company_name == null || this.global.PersonData.applicantdata.company_name == '') {
      flag = false;
      this.errormessagetext = this.textdata[26];
    } else if (this.global.PersonData.applicantdata.department == undefined || this.global.PersonData.applicantdata.department == null || this.global.PersonData.applicantdata.department == '') {
      flag = false;
      this.errormessagetext = this.textdata[27];
    } else if (this.global.PersonData.applicantdata.current_position == undefined || this.global.PersonData.applicantdata.current_position == null || this.global.PersonData.applicantdata.current_position == '') {
      flag = false;
      this.errormessagetext = this.textdata[28];
    } else if (this.global.PersonData.applicantdata.years_of_service == undefined || this.global.PersonData.applicantdata.years_of_service == null || this.global.PersonData.applicantdata.years_of_service == '') {
      flag = false;
      this.errormessagetext = this.textdata[29];
    } else if (this.global.PersonData.applicantdata.monthly_basic_income == undefined || this.global.PersonData.applicantdata.monthly_basic_income == null || this.global.PersonData.applicantdata.monthly_basic_income == '') {
      flag = false;
      this.errormessagetext = this.textdata[30];
    }
    // else if (this.global.PersonData.applicantdata.creditForOhters == undefined || this.global.PersonData.applicantdata.creditForOhters == null || this.global.PersonData.applicantdata.creditForOhters == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[31];
    // }
    else if (this.isToggled == true && (this.global.PersonData.applicantdata.creditAmount == undefined || this.global.PersonData.applicantdata.creditAmount == null || this.global.PersonData.applicantdata.creditAmount == '')) {
      flag = false;
      this.errormessagetext = this.textdata[32];
    } else if (this.global.PersonData.applicantdata.company_ph_no == undefined || this.global.PersonData.applicantdata.company_ph_no == null || this.global.PersonData.applicantdata.company_ph_no == '') {
      flag = false;
      this.errormessagetext = this.textdata[33];
    } else if (this.global.PersonData.guarantordata.syskey == undefined || this.global.PersonData.guarantordata.syskey == null || this.global.PersonData.guarantordata.syskey == '') {
      flag = false;
      this.errormessagetext = this.textdata[88];  //added new
    } else if (this.global.PersonData.guarantordata.member_name == undefined || this.global.PersonData.guarantordata.member_name == null || this.global.PersonData.guarantordata.member_name == '') {
      flag = false;
      this.errormessagetext = this.textdata[67];
    } else if (this.global.PersonData.guarantordata.nrc == undefined || this.global.PersonData.guarantordata.nrc == null || this.global.PersonData.guarantordata.nrc == '') {
      flag = false;
      this.errormessagetext = this.textdata[68];
    }
    else if (this.global.PersonData.guarantordata.dob == undefined || this.global.PersonData.guarantordata.dob == null || this.global.PersonData.guarantordata.dob == '') {
      flag = false;
      this.errormessagetext = this.textdata[69];
    } else if (this.global.PersonData.guarantordata.current_address_region == undefined || this.global.PersonData.guarantordata.current_address_region == null || this.global.PersonData.guarantordata.current_address_region == '') {
      flag = false;
      this.errormessagetext = this.textdata[70];
    } else if (this.global.PersonData.guarantordata.current_address_township == undefined || this.global.PersonData.guarantordata.current_address_township == null || this.global.PersonData.guarantordata.current_address_township == '') {
      flag = false;
      this.errormessagetext = this.textdata[71];
    } else if (this.global.PersonData.guarantordata.current_address_street == undefined || this.global.PersonData.guarantordata.current_address_street == null || this.global.PersonData.guarantordata.current_address_street == '') {
      flag = false;
      this.errormessagetext = this.textdata[72];
    } else if (this.global.PersonData.guarantordata.mobile == undefined || this.global.PersonData.guarantordata.mobile == null || this.global.PersonData.guarantordata.mobile == '') {
      flag = false;
      this.errormessagetext = this.textdata[73];
    } else if (this.global.PersonData.guarantordata.company_name == undefined || this.global.PersonData.guarantordata.company_name == null || this.global.PersonData.guarantordata.company_name == '') {
      flag = false;
      this.errormessagetext = this.textdata[75];
    } else if (this.global.PersonData.guarantordata.department == undefined || this.global.PersonData.guarantordata.department == null || this.global.PersonData.guarantordata.department == '') {
      flag = false;
      this.errormessagetext = this.textdata[76];
    } else if (this.global.PersonData.guarantordata.current_position == undefined || this.global.PersonData.guarantordata.current_position == null || this.global.PersonData.guarantordata.current_position == '') {
      flag = false;
      this.errormessagetext = this.textdata[77];
    } else if (this.global.PersonData.guarantordata.years_of_service == undefined || this.global.PersonData.guarantordata.years_of_service == null || this.global.PersonData.guarantordata.years_of_service == '') {
      flag = false;
      this.errormessagetext = this.textdata[78];
    } else if (this.global.PersonData.guarantordata.monthly_basic_income == undefined || this.global.PersonData.guarantordata.monthly_basic_income == null || this.global.PersonData.guarantordata.monthly_basic_income == '') {
      flag = false;
      this.errormessagetext = this.textdata[79];
    }
    // else if (this.global.PersonData.guarantordata.creditForOhters == undefined || this.global.PersonData.guarantordata.creditForOhters == null || this.global.PersonData.guarantordata.creditForOhters == '') {
    //   flag = false;
    //   this.errormessagetext = this.textdata[80];
    // }
    else if (this.isGuaToggled == true && (this.global.PersonData.guarantordata.creditAmount == undefined || this.global.PersonData.guarantordata.creditAmount == null || this.global.PersonData.guarantordata.creditAmount == '')) {
      flag = false;
      this.errormessagetext = this.textdata[81];
    } else if (this.global.PersonData.guarantordata.company_ph_no == undefined || this.global.PersonData.guarantordata.company_ph_no == null || this.global.PersonData.guarantordata.company_ph_no == '') {
      flag = false;
      this.errormessagetext = this.textdata[82];
    }
    //  for Photo upload 
    else if (this.global.frontnrcdata.profile == undefined || this.global.frontnrcdata.profile == null || this.global.frontnrcdata.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[51];
    } else if (this.global.backnrcdata.profile == undefined || this.global.backnrcdata.profile == null || this.global.backnrcdata.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[52];
    }
    else if (this.global.guafrontnrcdata.profile == undefined || this.global.guafrontnrcdata.profile == null || this.global.guafrontnrcdata.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[53];
    } else if (this.global.guabacknrcdata.profile == undefined || this.global.guabacknrcdata.profile == null || this.global.guabacknrcdata.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[54];
    } else if (this.global.regtype == undefined || this.global.regtype == null || this.global.regtype == '') {
      flag = false;
      this.errormessagetext = this.textdata[50];
    } else if (this.global.regtype == "1" && (this.global.householdfrontdata.profile == undefined || this.global.householdfrontdata.profile == null || this.global.householdfrontdata.profile == '')) {
      flag = false;
      this.errormessagetext = this.textdata[56];
    } else if (this.global.regtype == "1" && (this.global.householdbackdata.profile == undefined || this.global.householdbackdata.profile == null || this.global.householdbackdata.profile == '')) {
      flag = false;
      this.errormessagetext = this.textdata[57];
    } else if (this.global.regtype == "2" && (this.global.policecertificatedata.profile == undefined || this.global.policecertificatedata.profile == null || this.global.policecertificatedata.profile == '')) {
      flag = false;
      this.errormessagetext = this.textdata[58];
    } else if (this.global.employmentletterdata.profile == undefined || this.global.employmentletterdata.profile == null || this.global.employmentletterdata.profile == '') {
      flag = false;
      this.errormessagetext = this.textdata[55];
    }


    if (this.isToggled == false) {
      this.global.PersonData.applicantdata.creditAmount = "";
    }
    if (this.isGuaToggled == false) {
      this.global.PersonData.guarantordata.creditAmount = "";
    }
    return flag;
  }

  //psst tested for hide keyboard text 
  ionViewCanEnter() {
    // this.keyboard.onKeyboardShow()
    //   .subscribe(data => {

    //     let conte = this.content;
    //     let abc = conte.getNativeElement().querySelector('.swiper-pagination');
    //     if (abc !== undefined && abc !== null) {
    //       abc.setAttribute("style", 'display: none');
    //     }

    //     //psst tested for No result found 
    //     let Nodata = conte.getNativeElement().querySelector('.swiper-pagination');
    //     if (abc !== undefined && abc !== null) {
    //       abc.setAttribute("style", 'display: none');
    //     }

    //   });

    // this.keyboard.onKeyboardHide()
    //   .subscribe(data => {

    //     let conte = this.content;
    //     let abc = conte.getNativeElement().querySelector('.swiper-pagination');
    //     if (abc !== undefined && abc !== null) {
    //       abc.removeAttribute("style");
    //     }
    //   });
      this.storage.get('applicantsyskey').then((applicantsyskey) => {
        this.global.applicantsyskey=applicantsyskey;
        console.log('Global syskey: '+this.global.applicantsyskey);
       });


  }

  validateFormat() {
    let flag = true;
    // if (this.PersonData.applicantdata.nrc.length > 0) {
    // if (!/^(([1-9]|1[0-9])\/[A-Za-z]+\([N|n]\)[0-9]{6})$/.test(this.PersonData.applicantdata.nrc)) {
    //   this.errormessagetext = this.textdata[34];
    //   flag = false;
    // }
    // }
    if (this.global.PersonData.applicantdata.mobile.length < 9) {
      this.errormessagetext = this.textdata[35];
      flag = false;
    } if (!this.validateEmail(this.global.PersonData.applicantdata.email)) {
      this.errormessagetext = this.textdata[36];
      flag = false;
    } if (this.global.PersonData.applicantdata.company_ph_no.length < 8) {
      this.errormessagetext = this.textdata[38];
      flag = false;
    }
    //psst updated for Guarantor
    if (this.global.PersonData.guarantordata.mobile.length < 9) {
      this.errormessagetext = this.textdata[84];
      flag = false;
    } if (!this.validateEmail(this.global.PersonData.guarantordata.email)) {
      this.errormessagetext = this.textdata[85];
      flag = false;
    } if (this.global.PersonData.guarantordata.company_ph_no.length < 8) {
      this.errormessagetext = this.textdata[87];
      flag = false;
    }
    return flag;
  }

  BindApplicantData() {
    if (this.global.netWork == "connected") {
      this.loading = this.loadingCtrl.create({
        content: "", spinner: 'circles',
        dismissOnPageChange: true,
        //duration: 300
      });
      this.loading.present();
      this.usersyskey = this.global.applicantsyskey;
      this.http.get(this.global.ipAdressMFI + '/serviceMFI/getMemberData?syskey=' + this.usersyskey).map(res => res.json()).subscribe(data => {
        if (data != null && data.member_name != "" && data.nrc != '') {
          this.global.PersonData.applicantdata = data;
          console.log('My Test Data: '+this.global.PersonData.applicantdata.mobile);

          //change font 
          if (this.changeFont == "zg") {
            this.global.PersonData.applicantdata.member_name = this.changefont.UnitoZg(this.global.PersonData.applicantdata.member_name);
            this.global.PersonData.applicantdata.nrc = this.changefont.UnitoZg(this.global.PersonData.applicantdata.nrc);
            this.global.PersonData.applicantdata.current_address_street = this.changefont.UnitoZg(this.global.PersonData.applicantdata.current_address_street);
            this.global.PersonData.applicantdata.company_name = this.changefont.UnitoZg(this.global.PersonData.applicantdata.company_name);
            this.global.PersonData.applicantdata.department = this.changefont.UnitoZg(this.global.PersonData.applicantdata.department);
            this.global.PersonData.applicantdata.current_position = this.changefont.UnitoZg(this.global.PersonData.applicantdata.current_position);

            this.global.PersonData.guarantordata.member_name = this.changefont.UnitoZg(this.global.PersonData.guarantordata.member_name);
            this.global.PersonData.guarantordata.nrc = this.changefont.UnitoZg(this.global.PersonData.guarantordata.nrc);
            this.global.PersonData.guarantordata.current_address_street = this.changefont.UnitoZg(this.global.PersonData.guarantordata.current_address_street);
            this.global.PersonData.guarantordata.company_name = this.changefont.UnitoZg(this.global.PersonData.guarantordata.company_name);
            this.global.PersonData.guarantordata.department = this.changefont.UnitoZg(this.global.PersonData.guarantordata.department);
            this.global.PersonData.guarantordata.current_position = this.changefont.UnitoZg(this.global.PersonData.guarantordata.current_position);
          }
          this.global.frontnrcdata.edit = "0";
          this.global.frontnrcdata.profile = this.global.ipphoto + this.global.PersonData.applicantdata.front_nrc_image_path;
          this.global.backnrcdata.edit = "0";
          this.global.backnrcdata.profile = this.global.ipphoto + this.global.PersonData.applicantdata.back_nrc_image_path;

          if (this.global.PersonData.applicantdata.creditForOhters == "1") {
            this.isToggled = true;
          }
          else {
            this.isToggled = false;
          }
          this.global.PersonData.applicantdata.monthly_basic_income = this.format(this.global.PersonData.applicantdata.monthly_basic_income);

          this.global.PersonData.applicantdata.creditAmount = this.format(this.global.PersonData.applicantdata.creditAmount);


        } else {

        }
        this.loading.dismiss();

      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          this.loading.dismiss();
        });
   
    // }
    // else {
    //   let toast = this.toastCtrl.create({
    //     message: "Check your internet connection!",
    //     duration: 3000,
    //     position: 'bottom',
    //     //  showCloseButton: true,
    //     dismissOnPageChange: true,
    //     // closeButtonText: 'OK'
    //   });
    //   toast.present(toast);
    // }
  }
}

  getRegion() {

    // added network test
    // if (this.global.flagNet == "success") {
    //   this.loading = this.loadingCtrl.create({
    //     content: "", spinner: 'circles',
    //     dismissOnPageChange: true
    //     //   duration: 3000
    //   });
    //   this.loading.present();
    this.http.get(this.global.ipAdressMFI + '/serviceMFI/getRegionList').map(res => res.json()).subscribe(data => {

      if (data.data != null) {
        this.regionList = data.data;
        if (this.global.PersonData.applicantdata.current_address_region != '' && this.global.PersonData.applicantdata.current_address_region != undefined && this.global.PersonData.applicantdata.current_address_region != null) {
          this.getTownship(this.global.PersonData.applicantdata.current_address_region);
        }
        this.regionListGua = data.data;
        if (this.global.PersonData.guarantordata.current_address_region != '' && this.global.PersonData.guarantordata.current_address_region != undefined && this.global.PersonData.guarantordata.current_address_region != null) {
          this.getTownshipGua(this.global.PersonData

            .guarantordata.current_address_region);
        }
      } else {
        let toast = this.toastCtrl.create({
          message: "Can't connect right now!",
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      }
      this.loading.dismiss();
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.util.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();
      });
    // }
    // else {
    //   let toast = this.toastCtrl.create({
    //     message: this.textdata[16],
    //     duration: 3000,
    //     position: 'bottom',
    //     //  showCloseButton: true,
    //     dismissOnPageChange: true,
    //     // closeButtonText: 'OK'
    //   });
    //   toast.present(toast);
    // }
  }

  RegionChange(r) {
    if (r != null && r != undefined && r != '')
      this.getTownship(r);
  }

  RegionChangeGua(r) {
    if (r != null && r != undefined && r != '')
      this.getTownshipGua(r);
  }

  RegChange(r: any) {
    this.global.regtype = r;
    //psst updated close below & up open
    //this.global.PersonData.loandata.registrationType = r;    
  }

  getTownship(r) {

    this.http.get(this.global.ipAdressMFI + '/serviceMFI/getTownshipList?regionRefSyskey=' + r).map(res => res.json()).subscribe(result => {
      if (result.data != null) {
        this.townshipList = result.data;
      } else {
        let toast = this.toastCtrl.create({
          message: "Can't connect right now!",
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
      }
      // this.loading.dismiss();
    },
      error => {
        let toast = this.toastCtrl.create({
          message: this.util.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        //this.loading.dismiss();
      });
  }

  getTownshipGua(r) {
    if (r != undefined && r != null && r != '' && r != "") {
      this.http.get(this.global.ipAdressMFI + '/serviceMFI/getTownshipList?regionRefSyskey=' + r).map(res => res.json()).subscribe(result => {
        if (result.data != null) {
          this.townshipListGua = result.data;
        } else {
          let toast = this.toastCtrl.create({
            message: "Can't connect right now!",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);  //temporary ps 
        }
        // this.loading.dismiss();
      },
        error => {
          let toast = this.toastCtrl.create({
            message: this.util.getErrorMessage(error),
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
          // this.loading.dismiss();
        });
    }
  }

  inputChange(data) {
    this.errormessagetext = "";
  }

  //gurantor InfoPage
  ExitGurantor() {
    console.log('Call Gkdgjkdfgjk');
    // if (this.global.applicantsyskey != null) {

    // if (this.global.applicantsyskey != null) {

      this.usersyskey = this.global.applicantsyskey;
      //psst test for new gauran combo
      // if (this.usersyskey == 0) {
      //   this.GuranInfo = [];
      // }
      // else {
        console.log("guatantor info length " + this.GuranInfo.length);
        if (this.GuranInfo.length == 1 && (this.GuranInfo[0].member_name == "" || this.GuranInfo[0].member_name == "New Guarantor")) {   //exiting data while change slide 
          this.http.get(this.global.ipAdressMFI + '/serviceMFI/checkGuarantor?syskey=' + this.usersyskey).map(res => res.json()).subscribe(data => {
            if (data != null) {
              if (data.member_name != '' || data.nrc != '') {
                let tempArray = [];
                if (!Array.isArray(data.data)) {
                  tempArray.push(data.data);
                  data.data = tempArray;
                }
                this.GuranInfo = [];
                this.GuranInfo.push(this.newgran);
                for (let i = 0; i < data.data.length; i++) {
                  this.GuranInfo.push(data.data[i]);
                }

              }
            } else {
              this.GuranInfo = [];
              this.GuranInfo.push(this.newgran);
              let toast = this.toastCtrl.create({
                message: data.msgDesc,
                duration: 3000,
                position: 'bottom',
                dismissOnPageChange: true,
              });
              toast.present(toast);
            }
            //this.loading.dismiss();
          },
            error => {
              this.GuranInfo = [];
              this.GuranInfo.push(this.newgran);
              let toast = this.toastCtrl.create({
                message: this.util.getErrorMessage(error),
                duration: 3000,
                position: 'bottom',
                dismissOnPageChange: true,
              });
              toast.present(toast);
              //this.loading.dismiss();
            });
        }
      //}
    //}
  }





  doPrompt() {
    this.global.PersonData.guarantordata = this.Gdata();
  }

  //photo Upload 
  imageEdit(name) {
    let actionSheet = this.actionSheetCtrl.create({
      // title: 'Languages',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Take new picture',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            this.goCamera(name);
          }
        },
        {
          text: 'Select new from gallery',
          icon: !this.platform.is('ios') ? 'images' : null,
          handler: () => {
            this.goGallery(name);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }

  goCamera(name) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: true,
      allowEdit: false,
    }

    this.camera.getPicture(options).then((imageData) => {
      if (name == "AppNRCFront") {
        this.global.frontnrcdata.profileImg = imageData;
        this.global.frontnrcdata.profile = imageData;
      }
      else if (name == "AppNRCBack") {
        this.global.backnrcdata.profileImg = imageData;
        this.global.backnrcdata.profile = imageData;

      } else if (name == "GuaNRCFront") {
        this.global.guafrontnrcdata.profileImg = imageData;
        this.global.guafrontnrcdata.profile = imageData;

      } else if (name == "GuaNRCBack") {
        this.global.guabacknrcdata.profileImg = imageData;
        this.global.guabacknrcdata.profile = imageData;

      } else if (name == "HouseHoldFront") {
        this.global.householdfrontdata.profileImg = imageData;
        this.global.householdfrontdata.profile = imageData;

      } else if (name == "HouseHoldBack") {
        this.global.householdbackdata.profileImg = imageData;
        this.global.householdbackdata.profile = imageData;

      } else if (name == "PoliceCertificate") {
        this.global.policecertificatedata.profileImg = imageData;
        this.global.policecertificatedata.profile = imageData;

      } else if (name == "EmploymentLetter") {
        this.global.employmentletterdata.profileImg = imageData;
        this.global.employmentletterdata.profile = imageData;
      }
      // let currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
      // let correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
      let currentName = '';
      let correctPath = '';
      if (imageData.indexOf('?') > -1) {
        currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
        correctPath = imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      }
      else {
        currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
        correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
      }
      this.copyFileToLocalDir(name, correctPath, currentName, this.createPhotoName(name));
    }, (error) => {

      let toast = this.toastCtrl.create({
        message: "Taking photo is not successful.",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
    });
  }

  goGallery(name) {
    ''
    const options: CameraOptions = {

      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      correctOrientation: true
    }
    this.camera.getPicture(options).then((imageData) => {
      /* ios */
      /* let currentName = '';
      let correctPath = '';
      if (imageData.indexOf('?') > -1) {
        currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
        correctPath = 'file:///' + imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      }
      else {
        currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
        correctPath = 'file:///' + imageData.substr(0, imageData.lastIndexOf('/') + 1);
      } */

      /* acmy */

      if (name == "AppNRCFront") {
        this.global.frontnrcdata.profileImg = imageData;
        this.global.frontnrcdata.profile = imageData;
      }
      else if (name == "AppNRCBack") {
        this.global.backnrcdata.profileImg = imageData;
        this.global.backnrcdata.profile = imageData;

      } else if (name == "GuaNRCFront") {
        this.global.guafrontnrcdata.profileImg = imageData;
        this.global.guafrontnrcdata.profile = imageData;

      } else if (name == "GuaNRCBack") {
        this.global.guabacknrcdata.profileImg = imageData;
        this.global.guabacknrcdata.profile = imageData;

      } else if (name == "HouseHoldFront") {
        this.global.householdfrontdata.profileImg = imageData;
        this.global.householdfrontdata.profile = imageData;

      } else if (name == "HouseHoldBack") {
        this.global.householdbackdata.profileImg = imageData;
        this.global.householdbackdata.profile = imageData;

      } else if (name == "PoliceCertificate") {
        this.global.policecertificatedata.profileImg = imageData;
        this.global.policecertificatedata.profile = imageData;

      } else if (name == "EmploymentLetter") {
        this.global.employmentletterdata.profileImg = imageData;
        this.global.employmentletterdata.profile = imageData;
      }




      // let currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      // let correctPath = imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      let correctPath = '';
      let currentName = '';
      if (imageData.indexOf('?') > -1) {
        currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
        correctPath = imageData.substring(0, imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
      }
      else {
        currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
        correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
      }
      this.copyFileToLocalDir(name, correctPath, currentName, this.createPhotoName(name));
    }, (error) => {
      let toast = this.toastCtrl.create({
        message: "Choosing photo is not successful.",
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });

      toast.present(toast);
    });
  }


  createPhotoName(name) {
    let photoName = name + this.util.getImageName() + this.usersyskey + ".jpg";
    this.photoName = name + this.util.getImageName() + this.usersyskey + ".jpg";
    if (name == 'AppNRCFront') {
      this.global.frontnrcdata.photoName = this.photoName;
      this.global.frontnrcdata.edit = "1";
    }
    else if (name == 'AppNRCBack') {
      this.global.backnrcdata.photoName = this.photoName;
      this.global.backnrcdata.edit = "1";
    } else if (name == "GuaNRCFront") {
      this.global.guafrontnrcdata.photoName = this.photoName;
      this.global.guafrontnrcdata.edit = "1";
    } else if (name == "GuaNRCBack") {
      this.global.guabacknrcdata.photoName = this.photoName;
      this.global.guabacknrcdata.edit = "1";
    } else if (name == "HouseHoldFront") {
      this.global.householdfrontdata.photoName = this.photoName;
      this.global.householdfrontdata.edit = "1";
    } else if (name == "HouseHoldBack") {
      this.global.householdbackdata.photoName = this.photoName;
      this.global.householdbackdata.edit = "1";
    } else if (name == "PoliceCertificate") {
      this.global.policecertificatedata.photoName = this.photoName;
      this.global.policecertificatedata.edit = "1";
    } else if (name == "EmploymentLetter") {
      this.global.employmentletterdata.photoName = this.photoName;
      this.global.employmentletterdata.edit = "1";
    }
    return photoName;
  }

  openProcessingLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Processing",
      dismissOnPageChange: true,
      duration: 3000
    });
    this.loading.present();
  }

  closeProcessingLoading() {
    this.loading.dismiss();
  }

  findAndRemove(array, property, value) {
    array.forEach(function (result, index) {
      if (result[property] === value) {
        //Remove from array
        array.splice(index, 1);
      }
    });
  }

  copyFileToLocalDir(name, namePath, currentName, newFileName) {
    let targetPath1;
    if (this.platform.is('ios')) {
      targetPath1 = this.file.documentsDirectory;
    } else {
      targetPath1 = this.file.externalApplicationStorageDirectory;
    }
    this.file.checkDir(targetPath1, 'MFIPhoto').then(() => {
    }, (error) => {
      this.file.createDir(targetPath1, "MFIPhoto", true).then(() => {

      }, (error) => {

      });
    });

    this.file.copyFile(namePath, currentName, targetPath1 + 'MFIPhoto/', newFileName).then(file => {
      this.chatphoto = targetPath1 + 'MFIPhoto/' + newFileName;
      // this.photos.push(this.chatphoto);
      if (name == "AppNRCFront") {
        this.global.frontnrcdata.profileImg = this.chatphoto;
        this.global.frontnrcdata.profile = this.chatphoto;
      }
      else if (name == "AppNRCBack") {
        this.global.backnrcdata.profileImg = this.chatphoto;
        this.global.backnrcdata.profile = this.chatphoto;
      } else if (name == "GuaNRCFront") {
        this.global.guafrontnrcdata.profileImg = this.chatphoto;
        this.global.guafrontnrcdata.profile = this.chatphoto;
      } else if (name == "GuaNRCBack") {
        this.global.guabacknrcdata.profileImg = this.chatphoto;
        this.global.guabacknrcdata.profile = this.chatphoto;
      } else if (name == "HouseHoldFront") {
        this.global.householdfrontdata.profileImg = this.chatphoto;
        this.global.householdfrontdata.profile = this.chatphoto;
      } else if (name == "HouseHoldBack") {
        this.global.householdbackdata.profileImg = this.chatphoto;
        this.global.householdbackdata.profile = this.chatphoto;
      } else if (name == "PoliceCertificate") {
        this.global.policecertificatedata.profileImg = this.chatphoto;
        this.global.policecertificatedata.profile = this.chatphoto;
      } else if (name == "EmploymentLetter") {
        this.global.employmentletterdata.profileImg = this.chatphoto;
        this.global.employmentletterdata.profile = this.chatphoto;
      }
      //this.savephoto();
    }, (err) => {

    });
  }

  BindPhotoData() {
    // this.imglink=this.global.ipphoto+"upload/image/userProfile/";
    // this.frontnrc=this.imglink+this.PersonData.applicantdata.front_nrc_image_path;
    // this.backnrc=this.imglink+this.PersonData.applicantdata.back_nrc_image_path;
    // this.guafrontnrc=this.imglink+this.PersonData.guarantordata.front_nrc_image_path;
    // this.guabacknrc=this.imglink+this.PersonData.guarantordata.front_nrc_image_path;
  }

  download(photoAndServerLink, photo) {
    const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(photoAndServerLink, this.file.cacheDirectory + photo).then((entry) => {
      let abc = entry.toURL();
      this.storage.set('localPhotoAndLink', abc);
      this.events.publish('localPhotoAndLink', abc);
      //this.closeProcessingLoading();
    }, (error) => {

      //this.closeProcessingLoading();
    });
  }


  NextApplicant(obj) {
    this.global.PersonData.applicantdata.syskey = this.global.applicantsyskey;
    console.log("font type in next :" + this.changeFont);
  console.log("next applicant " + this.global.PersonData.applicantdata.creditForOhters);
    if (this.global.frontnrcdata.edit == "1") {
      this.imname = 'AppNRCFront';
      this.global.frontnrcdata.imgname = this.imname;

      this.global.PersonData.imagealldata.imageparsedata.push(this.global.frontnrcdata);
      this.global.PersonData.applicantdata.front_nrc_image_path = this.global.frontnrcdata.photoName;
    } else {
      this.global.PersonData.applicantdata.front_nrc_image_path = this.global.frontnrcdata.profile.substr(this.global.frontnrcdata.profile.lastIndexOf('/') + 1);
    }
    if (this.global.backnrcdata.edit == "1") {
      this.imname = 'AppNRCBack';
      this.global.backnrcdata.imgname = this.imname;

      this.global.PersonData.imagealldata.imageparsedata.push(this.global.backnrcdata);
      this.global.PersonData.applicantdata.back_nrc_image_path = this.global.backnrcdata.photoName;
    }
    else {
      this.global.PersonData.applicantdata.back_nrc_image_path = this.global.backnrcdata.profile.substr(this.global.backnrcdata.profile.lastIndexOf('/') + 1);
    }
    if (this.global.guafrontnrcdata.edit == "1") {
      this.imname = 'GuaNRCFront';
      this.global.guafrontnrcdata.imgname = this.imname;

      this.global.PersonData.imagealldata.imageparsedata.push(this.global.guafrontnrcdata);
      this.global.PersonData.guarantordata.front_nrc_image_path = this.global.guafrontnrcdata.photoName;
    }
    else {
      this.global.PersonData.guarantordata.front_nrc_image_path = this.global.guafrontnrcdata.profile.substr(this.global.guafrontnrcdata.profile.lastIndexOf('/') + 1);
    }
    if (this.global.guabacknrcdata.edit == "1") {
      this.imname = 'GuaNRCBack';
      this.global.guabacknrcdata.imgname = this.imname;

      this.global.PersonData.imagealldata.imageparsedata.push(this.global.guabacknrcdata);
      this.global.PersonData.guarantordata.back_nrc_image_path = this.global.guabacknrcdata.photoName;
    }
    else {
      this.global.PersonData.guarantordata.back_nrc_image_path = this.global.guabacknrcdata.profile.substr(this.global.guabacknrcdata.profile.lastIndexOf('/') + 1);
    }
    if (this.global.regtype == "1") {
      if (this.global.householdfrontdata.edit == "1") {
        this.imname = 'HouseHoldFront';
        this.global.householdfrontdata.imgname = this.imname;

        this.global.PersonData.imagealldata.imageparsedata.push(this.global.householdfrontdata);
        this.global.PersonData.loandata.householdRegistraionFileLocation = this.global.householdfrontdata.photoName;
      }
      if (this.global.householdbackdata.edit == "1") {
        this.imname = 'HouseHoldBack';
        this.global.householdbackdata.imgname = this.imname;

        this.global.PersonData.imagealldata.imageparsedata.push(this.global.householdbackdata);
        this.global.PersonData.loandata.householdRegistrationImageFile = this.global.householdbackdata.photoName;
      }
      this.global.policecertificatedata.imgname = "";
      this.global.policecertificatedata.profileImg = "";
      this.global.policecertificatedata.profile = "";
      this.global.policecertificatedata.photoName = "";
      this.global.PersonData.loandata.policeCertificateFIleLocation = "";

      this.findAndRemove(this.global.PersonData.imagealldata.imageparsedata, 'imgname', this.global.policecertificatedata.imgname);
    }
    else if (this.global.regtype == "2") {
      if (this.global.policecertificatedata.edit == "1") {
        this.imname = 'PoliceCertificate';
        this.global.policecertificatedata.imgname = this.imname;

        this.global.PersonData.imagealldata.imageparsedata.push(this.global.policecertificatedata);
        this.global.PersonData.loandata.policeCertificateFIleLocation = this.global.policecertificatedata.photoName;
      }
      this.global.householdfrontdata.imgname = "";
      this.global.householdfrontdata.profileImg = "";
      this.global.householdfrontdata.profile = "";
      this.global.householdfrontdata.photoName = "";

      this.global.PersonData.loandata.householdRegistraionFileLocation = "";
      this.findAndRemove(this.global.PersonData.imagealldata.imageparsedata, 'imgname', this.global.householdfrontdata.imgname);

      this.global.householdbackdata.imgname = "";
      this.global.householdbackdata.profileImg = "";
      this.global.householdbackdata.profile = "";
      this.global.householdbackdata.photoName = "";
      this.global.PersonData.loandata.householdRegistrationImageFile = "";
      this.findAndRemove(this.global.PersonData.imagealldata.imageparsedata, 'imgname', this.global.householdbackdata.imgname);
    }
    if (this.global.employmentletterdata.edit == "1") {
      this.imname = 'EmploymentLetter';
      this.global.employmentletterdata.imgname = this.imname;
      this.global.PersonData.imagealldata.imageparsedata.push(this.global.employmentletterdata);
      this.global.PersonData.loandata.employmentLetterFileLocation = this.global.employmentletterdata.photoName;
    }
    console.log("before validate " + this.global.PersonData.applicantdata.creditForOhters);
    if (this.validateInput() && this.validateFormat()) {
      console.log("after validate " + this.global.PersonData.applicantdata.creditForOhters);
      this.global.comeHome = true;
      console.log("person data :" + JSON.stringify(this.global.PersonData));
      this.dob = this.global.PersonData.applicantdata.dob.replace(/-/g, "");
      this.global.PersonData.applicantdata.dob = this.dob;
      this.dob = this.global.PersonData.guarantordata.dob.replace(/-/g, "");
      this.global.PersonData.guarantordata.dob = this.dob;
      this.global.PersonData.applicantdata.monthly_basic_income = this.global.PersonData.applicantdata.monthly_basic_income.toString().replace(/,/g, "");
      this.global.PersonData.applicantdata.creditAmount = this.global.PersonData.applicantdata.creditAmount.toString().replace(/,/g, "");
      this.global.PersonData.guarantordata.monthly_basic_income = this.global.PersonData.guarantordata.monthly_basic_income.toString().replace(/,/g, "");
      this.global.PersonData.guarantordata.creditAmount = this.global.PersonData.guarantordata.creditAmount.toString().replace(/,/g, "");

      //change font 

      if (this.changeFont == 'zg') {
        this.global.PersonData.applicantdata.member_name = this.changefont.ZgtoUni(this.global.PersonData.applicantdata.member_name);
        this.global.PersonData.applicantdata.nrc = this.changefont.ZgtoUni(this.global.PersonData.applicantdata.nrc);
        this.global.PersonData.applicantdata.current_address_street = this.changefont.ZgtoUni(this.global.PersonData.applicantdata.current_address_street);
        this.global.PersonData.applicantdata.company_name = this.changefont.ZgtoUni(this.global.PersonData.applicantdata.company_name);
        this.global.PersonData.applicantdata.department = this.changefont.ZgtoUni(this.global.PersonData.applicantdata.department);
        this.global.PersonData.applicantdata.current_position = this.changefont.ZgtoUni(this.global.PersonData.applicantdata.current_position);

        this.global.PersonData.guarantordata.member_name = this.changefont.ZgtoUni(this.global.PersonData.guarantordata.member_name);
        this.global.PersonData.guarantordata.nrc = this.changefont.ZgtoUni(this.global.PersonData.guarantordata.nrc);
        this.global.PersonData.guarantordata.current_address_street = this.changefont.ZgtoUni(this.global.PersonData.guarantordata.current_address_street);
        this.global.PersonData.guarantordata.company_name = this.changefont.ZgtoUni(this.global.PersonData.guarantordata.company_name);
        this.global.PersonData.guarantordata.department = this.changefont.ZgtoUni(this.global.PersonData.guarantordata.department);
        this.global.PersonData.guarantordata.current_position = this.changefont.ZgtoUni(this.global.PersonData.guarantordata.current_position);
      }
      this.navCtrl.setRoot(ShoplistPage,
        {

        });
    } else {
      let toast = this.toastCtrl.create({
        message: this.errormessagetext,
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
  }

  slideChanged(slide) {
    
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current Index: '+currentIndex)

    if (currentIndex == 3 || currentIndex == 4 || currentIndex == 5) {
      this.title = this.textdata[62];
    }
    if (currentIndex == 3) {
      console.log('Current Index: '+currentIndex);
      if (this.callGuran == true) {
        this.ExitGurantor();
        this.callGuran = false;
      }
    }

    if (currentIndex == 0 || currentIndex == 1 || currentIndex == 2) {
      this.title = this.textdata[61];
    }
    if (currentIndex == 6 || currentIndex == 7 || currentIndex == 8) {
      this.title = this.textdata[63];
    }
  }

  onSelectChange(selectStatus: any) {
    if (this.regionList == undefined || this.regionList == null || this.regionList.length <= 0 || this.regionListGua == undefined || this.regionListGua == null || this.regionListGua.length <= 0) {
      this.getRegion();
    }

    if (selectStatus != "") {
      if (selectStatus == '0') {
        this.global.guafrontnrcdata = this.getDefaultImgObj();
        this.global.guabacknrcdata = this.getDefaultImgObj();
        this.global.PersonData.guarantordata = this.Gdata();
        this.Newguran = 1;
      }
      else {
        this.http.get(this.global.ipAdressMFI + '/serviceMFI/getGuarantorData?syskey=' + selectStatus).map(res => res.json()).subscribe(data => {
          if (data != null && data.member_name != '' && data.nrc != '') {
            this.global.PersonData.guarantordata.syskey = selectStatus;
            this.global.PersonData.guarantordata = data;

            this.Newguran = 0;

            this.global.guafrontnrcdata.edit = "0";
            this.global.guafrontnrcdata.profile = this.global.ipphoto + this.global.PersonData.guarantordata.front_nrc_image_path;
            this.global.guabacknrcdata.edit = "0";
            this.global.guabacknrcdata.profile = this.global.ipphoto + this.global.PersonData.guarantordata.back_nrc_image_path;
            this.global.PersonData.guarantordata.monthly_basic_income = this.format(this.global.PersonData.guarantordata.monthly_basic_income);
            this.global.PersonData.guarantordata.creditAmount = this.format(this.global.PersonData.guarantordata.creditAmount);

            if (this.global.PersonData.guarantordata.creditForOhters == "1") {
              this.isGuaToggled = true;
            }
            else {
              this.isGuaToggled = false;
            }


          } else {
            let toast = this.toastCtrl.create({
              message: "Can't connect right now!",
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
          }
          // this.loading.dismiss();
        },
          error => {
            let toast = this.toastCtrl.create({
              message: this.util.getErrorMessage(error),
              duration: 3000,
              position: 'bottom',
              dismissOnPageChange: true,
            });
            toast.present(toast);
            //this.loading.dismiss();
          });
      }
    }
  }

  LoanApply() {
    console.log('Global Skys Key: '+this.global.applicantsyskey);
    this.global.PersonData.applicantdata.syskey = this.global.applicantsyskey;
    console.log("font type in applyloan :" + this.changeFont);
    if (this.global.frontnrcdata.edit == "1") {
      this.imname = 'AppNRCFront';
      this.global.frontnrcdata.imgname = this.imname;

      this.global.PersonData.imagealldata.imageparsedata.push(this.global.frontnrcdata);
      this.global.PersonData.applicantdata.front_nrc_image_path = this.global.frontnrcdata.photoName;
    } else {
      this.global.PersonData.applicantdata.front_nrc_image_path = this.global.frontnrcdata.profile.substr(this.global.frontnrcdata.profile.lastIndexOf('/') + 1);
    }
    if (this.global.backnrcdata.edit == "1") {
      this.imname = 'AppNRCBack';
      this.global.backnrcdata.imgname = this.imname;

      this.global.PersonData.imagealldata.imageparsedata.push(this.global.backnrcdata);
      this.global.PersonData.applicantdata.back_nrc_image_path = this.global.backnrcdata.photoName;
    }
    else {
      this.global.PersonData.applicantdata.back_nrc_image_path = this.global.backnrcdata.profile.substr(this.global.backnrcdata.profile.lastIndexOf('/') + 1);
    }
    if (this.global.guafrontnrcdata.edit == "1") {
      this.imname = 'GuaNRCFront';
      this.global.guafrontnrcdata.imgname = this.imname;

      this.global.PersonData.imagealldata.imageparsedata.push(this.global.guafrontnrcdata);
      this.global.PersonData.guarantordata.front_nrc_image_path = this.global.guafrontnrcdata.photoName;
    }
    else {
      this.global.PersonData.guarantordata.front_nrc_image_path = this.global.guafrontnrcdata.profile.substr(this.global.guafrontnrcdata.profile.lastIndexOf('/') + 1);
    }
    if (this.global.guabacknrcdata.edit == "1") {
      this.imname = 'GuaNRCBack';
      this.global.guabacknrcdata.imgname = this.imname;

      this.global.PersonData.imagealldata.imageparsedata.push(this.global.guabacknrcdata);
      this.global.PersonData.guarantordata.back_nrc_image_path = this.global.guabacknrcdata.photoName;
    }
    else {
      this.global.PersonData.guarantordata.back_nrc_image_path = this.global.guabacknrcdata.profile.substr(this.global.guabacknrcdata.profile.lastIndexOf('/') + 1);
    }
    if (this.global.regtype == "1") {
      if (this.global.householdfrontdata.edit == "1") {
        this.imname = 'HouseHoldFront';
        this.global.householdfrontdata.imgname = this.imname;

        this.global.PersonData.imagealldata.imageparsedata.push(this.global.householdfrontdata);
        this.global.PersonData.loandata.householdRegistraionFileLocation = this.global.householdfrontdata.photoName;
      }
      if (this.global.householdbackdata.edit == "1") {
        this.imname = 'HouseHoldBack';
        this.global.householdbackdata.imgname = this.imname;

        this.global.PersonData.imagealldata.imageparsedata.push(this.global.householdbackdata);
        this.global.PersonData.loandata.householdRegistrationImageFile = this.global.householdbackdata.photoName;
      }
      this.global.policecertificatedata.imgname = "";
      this.global.policecertificatedata.profileImg = "";
      this.global.policecertificatedata.profile = "";
      this.global.policecertificatedata.photoName = "";
      this.global.PersonData.loandata.policeCertificateFIleLocation = "";

      this.findAndRemove(this.global.PersonData.imagealldata.imageparsedata, 'imgname', this.global.policecertificatedata.imgname);
    }
    else if (this.global.regtype == "2") {
      if (this.global.policecertificatedata.edit == "1") {
        this.imname = 'PoliceCertificate';
        this.global.policecertificatedata.imgname = this.imname;

        this.global.PersonData.imagealldata.imageparsedata.push(this.global.policecertificatedata);
        this.global.PersonData.loandata.policeCertificateFIleLocation = this.global.policecertificatedata.photoName;
      }
      this.global.householdfrontdata.imgname = "";
      this.global.householdfrontdata.profileImg = "";
      this.global.householdfrontdata.profile = "";
      this.global.householdfrontdata.photoName = "";
      this.global.PersonData.loandata.householdRegistraionFileLocation = "";
      this.findAndRemove(this.global.PersonData.imagealldata.imageparsedata, 'imgname', this.global.householdfrontdata.imgname);
      this.global.householdbackdata.imgname = "";
      this.global.householdbackdata.profileImg = "";
      this.global.householdbackdata.profile = "";
      this.global.householdbackdata.photoName = "";
      this.global.PersonData.loandata.householdRegistrationImageFile = "";
      this.findAndRemove(this.global.PersonData.imagealldata.imageparsedata, 'imgname', this.global.householdbackdata.imgname);
    }
    if (this.global.employmentletterdata.edit == "1") {
      this.imname = 'EmploymentLetter';
      this.global.employmentletterdata.imgname = this.imname;
      this.global.PersonData.imagealldata.imageparsedata.push(this.global.employmentletterdata);
      this.global.PersonData.loandata.employmentLetterFileLocation = this.global.employmentletterdata.photoName;
    }
    if (this.validateInput() && this.validateFormat()) {
      this.global.comeHome = true;
      this.global.PersonData.loandata.totalAmount = this.global.PersonData.loandata.totalAmount.toString().replace(/,/g, "");
      this.global.PersonData.loandata.totalAmount = this.global.PersonData.loandata.totalAmount.toString().replace(/MMK/g, "");
      this.global.PersonData.loandata.monthlyAmount = this.global.PersonData.loandata.monthlyAmount.toString().replace(/,/g, "");
      this.global.PersonData.loandata.monthlyAmount = this.global.PersonData.loandata.monthlyAmount.toString().replace(/MMK/g, "");

      this.global.PersonData.applicantdata.monthly_basic_income = this.global.PersonData.applicantdata.monthly_basic_income.toString().replace(/,/g, "");
      this.global.PersonData.applicantdata.creditAmount = this.global.PersonData.applicantdata.creditAmount.toString().replace(/,/g, "");
      this.global.PersonData.guarantordata.monthly_basic_income = this.global.PersonData.guarantordata.monthly_basic_income.toString().replace(/,/g, "");
      this.global.PersonData.guarantordata.creditAmount = this.global.PersonData.guarantordata.creditAmount.toString().replace(/,/g, "");

      for (let i = 0; i < this.global.PersonData.applyProductList.data.length; i++) {
        if (this.global.PersonData.applyProductList.data[i].productPrice != "" && this.global.PersonData.applyProductList.data[i].productPrice != undefined && this.global.PersonData.applyProductList.data[i].productPrice != null) {
          this.global.PersonData.applyProductList.data[i].productPrice = this.global.PersonData.applyProductList.data[i].productPrice.toString().replace(",", "");
        }
      }

      // //psst added network test
      // if (this.global.flagNet == "success") {
      //   this.loading = this.loadingCtrl.create({
      //     content: "", spinner: 'circles',
      //     dismissOnPageChange: true
      //     //   duration: 3000
      //   });
      //   this.loading.present();
      this.dob = this.global.PersonData.applicantdata.dob.replace(/-/g, "");
      this.global.PersonData.applicantdata.dob = this.dob;
      this.dob = this.global.PersonData.guarantordata.dob.replace(/-/g, "");
      this.global.PersonData.guarantordata.dob = this.dob;

      //change font 

      if (this.changeFont == 'zg') {
        this.global.PersonData.applicantdata.member_name = this.changefont.ZgtoUni(this.global.PersonData.applicantdata.member_name);
        this.global.PersonData.applicantdata.nrc = this.changefont.ZgtoUni(this.global.PersonData.applicantdata.nrc);
        this.global.PersonData.applicantdata.current_address_street = this.changefont.ZgtoUni(this.global.PersonData.applicantdata.current_address_street);
        this.global.PersonData.applicantdata.company_name = this.changefont.ZgtoUni(this.global.PersonData.applicantdata.company_name);
        this.global.PersonData.applicantdata.department = this.changefont.ZgtoUni(this.global.PersonData.applicantdata.department);
        this.global.PersonData.applicantdata.current_position = this.changefont.ZgtoUni(this.global.PersonData.applicantdata.current_position);

        this.global.PersonData.guarantordata.member_name = this.changefont.ZgtoUni(this.global.PersonData.guarantordata.member_name);
        this.global.PersonData.guarantordata.nrc = this.changefont.ZgtoUni(this.global.PersonData.guarantordata.nrc);
        this.global.PersonData.guarantordata.current_address_street = this.changefont.ZgtoUni(this.global.PersonData.guarantordata.current_address_street);
        this.global.PersonData.guarantordata.company_name = this.changefont.ZgtoUni(this.global.PersonData.guarantordata.company_name);
        this.global.PersonData.guarantordata.department = this.changefont.ZgtoUni(this.global.PersonData.guarantordata.department);
        this.global.PersonData.guarantordata.current_position = this.changefont.ZgtoUni(this.global.PersonData.guarantordata.current_position);
      }

      console.log("INFO page from Persondata >>" + JSON.stringify(this.global.PersonData));
      this.http.post(this.global.ipAdressMFI + '/serviceMFI/applyLoan', this.global.PersonData).map(res => res.json()).subscribe(result => {

        if (result.msgCode == "0000") {
          this.storage.set('applicantsyskey',  result.memberSyskey);
          this.events.publish('applicantsyskey', result.memberSyskey);
          this.global.applicantsyskey = result.memberSyskey;

          for (var i = 0; i < this.global.PersonData.imagealldata.imageparsedata.length; i++) {
            console.log("image parse data" + JSON.stringify(this.global.PersonData.imagealldata.imageparsedata[i]));
            if (this.global.PersonData.imagealldata.imageparsedata[i].edit == "1") {
              this.uploadPhoto(this.global.PersonData.imagealldata.imageparsedata[i].imgname, this.global.PersonData.imagealldata.imageparsedata[i].profileImg, this.global.PersonData.imagealldata.imageparsedata[i].photoName, this.global.PersonData.imagealldata.imageparsedata[i].profile)
            }
          }
          this.global.PersonData = this.global.defaultData();
          this.global.frontnrcdata = this.getDefaultImgObj();
          this.global.backnrcdata = this.getDefaultImgObj();
          this.global.guafrontnrcdata = this.getDefaultImgObj();
          this.global.guabacknrcdata = this.getDefaultImgObj();
          this.global.householdfrontdata = this.getDefaultImgObj();
          this.global.householdbackdata = this.getDefaultImgObj();
          this.global.policecertificatedata = this.getDefaultImgObj();
          this.global.employmentletterdata = this.getDefaultImgObj();
          this.global.PersonData.imagealldata.imageparsedata = [];
          this.global.regtype = this.defaultregtype;
          this.global.outletSyskey = '';

          let confirm = this.alertCtrl.create({
            title: '',
            enableBackdropDismiss: false,
            message: "Submit Successfully.",
            buttons: [
              {
                text: 'OK',
                handler: () => {
                  console.log('OK clicked');
                  // this.navCtrl.setRoot(TabsPage, {});
                  // this.navCtrl.popToRoot();
                  this.navCtrl.setRoot(WalletPage, {});
                }
              }
            ]
          })
          confirm.present();
          //this.loading.dismiss();
        } else {
          let toast = this.toastCtrl.create({
            message: result.msgDesc,
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });
          toast.present(toast);
        }
        // this.loading.dismiss();
      }, error => {
        let toast = this.toastCtrl.create({
          message: this.util.getErrorMessage(error),
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });
        toast.present(toast);
        this.loading.dismiss();

      });
      // } else {
      //   let toast = this.toastCtrl.create({
      //     message: this.util.getErrorMessage("Check your internet connection!"),
      //     duration: 3000,
      //     position: 'bottom',
      //     //  showCloseButton: true,
      //     dismissOnPageChange: true,
      //     // closeButtonText: 'OK'
      //   });
      //   toast.present(toast);
      //   this.loading.dismiss();
      // }
    } else {
      let toast = this.toastCtrl.create({
        message: this.errormessagetext,
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
  }

  uploadPhoto(imgname, img, name, pro) {
    var url = this.global.ipAdressMFI + '/serviceMFI/mobileupload';
    this.profileImg = img;
    this.photoName = name;
    this.profile = pro;
    console.log("profile image :" + JSON.stringify(this.profileImg));
    console.log("Photo name :" + JSON.stringify(this.photoName));
    console.log("profile :" + JSON.stringify(this.profile));
    if (this.profileImg != undefined && this.profileImg != '' && this.profileImg != null) {

      let options: FileUploadOptions = {
        fileKey: 'file',
        fileName: this.photoName,
        chunkedMode: false,
      }

      const fileTransfer: FileTransferObject = this.transfer.create();
      console.log("Before upload" + url);
      fileTransfer.upload(this.profile, url, options).then((data) => {
        let abc = this.imglnk + this.photoName;
        // this.storage.set('localPhotoAndLink', abc);
        // this.events.publish('localPhotoAndLink', abc);
        this.closeProcessingLoading();
        let toast = this.toastCtrl.create({
          message: this.successMsg,
          duration: 3000,
          position: 'bottom',
          dismissOnPageChange: true,
        });

        toast.present(toast);

        //this.slimLoader.complete();

        console.log("uploadPhoto upload success" + JSON.stringify(data));
      },
        error => {
          this.closeProcessingLoading();

          let toast = this.toastCtrl.create({
            message: "Uploading photo is not successful.",
            duration: 3000,
            position: 'bottom',
            dismissOnPageChange: true,
          });

          toast.present(toast);
          console.log("uploadPhoto upload error" + JSON.stringify(error));
        }
      );
    } else {
      this.closeProcessingLoading();

      let toast = this.toastCtrl.create({
        message: this.successMsg,
        duration: 3000,
        position: 'bottom',
        dismissOnPageChange: true,
      });
      toast.present(toast);
    }
  }


  gobackmain() {
    if (this.global.startshop == 0) {
      this.navCtrl.setRoot(WalletPage,
        {
        });
    } else {
      this.navCtrl.setRoot(PaybillPage,
        {

        });
    }

  }

  ionViewDidEnter() {
    this.backButtonExit();
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      if (this.global.startshop == 0) {
        this.platform.exitApp();
      } else {
        this.navCtrl.setRoot(PaybillPage,
          {

          });
      }


    });
  }


  format(valString) {
    if (!valString) {
      return '';
    }
    let val = valString.toString();
    const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
    return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR)

  };

  unFormat(val) {
    if (!val) {
      return '';
    }
    val = val.replace(/^0+/, '').replace(/\D/g, '');
    if (this.GROUP_SEPARATOR === ',') {
      return val.replace(/,/g, '');
    } else {
      return val.replace(/\./g, '');
    }
  };

  touchRegion() {
    if (this.regionList == undefined || this.regionList == null || this.regionList.length <= 0) {
      this.getRegion();
    }
  }



}
