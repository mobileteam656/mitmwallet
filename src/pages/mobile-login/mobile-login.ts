import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, ModalController, LoadingController, ToastController, Events, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { SQLite } from '@ionic-native/sqlite';
import { Changefont } from '../changefont/changeFont';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { GlobalProvider } from '../../providers/global/global';
import { UtilProvider } from '../../providers/util/util';
import { TabsPage } from '../tabs/tabs';
import { Device } from '@ionic-native/device';
import { Platform } from 'ionic-angular/platform/platform';
import { AllserviceProvider } from '../../providers/allservice/allservice';
import { Network } from '@ionic-native/network';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { MobileLoginOtpPage } from '../mobile-login-otp/mobile-login-otp';
import { WalletPage } from '../wallet/wallet';

@Component({
  selector: 'page-mobile-login',
  templateUrl: 'mobile-login.html',
  providers: [Changefont]
})
export class MobileLoginPage {
  textEng: any = ['Mobile Login','User ID', 'Password', 'Enter User ID', 'Enter Password', 'Sign in', 'mBanking', 'Enter correct User ID', 'Access Code', 'PIN', 'Enter Access Code', 'Enter PIN'];
  textMyan: any = ['Mobile Login','အိုင်ဒီနံပါတ်', 'လျှို့ဝှက်နံပါတ်', 'အိုင်ဒီနံပါတ် ရိုက်ထည့်ပါ', "လျှို့ဝှက်နံပါတ် ရိုက်ထည့်ပါ", 'ဝင်မည်', 'mBanking', 'မှန်ကန်သော အိုင်ဒီနံပါတ် ရိုက်သွင်းပါ', 'Access Code', 'PIN', 'Enter Access Code', 'Enter PIN'];
  textData: string[] = [];
  keyboardfont: any;
  language: any;
  showFont: any;
  flag: string;
  signinData: any;

  ipaddress: string;
  ipaddressCMS: string;
  ipaddressWallet: string;
  public loading;
  uName: boolean = false;
  pWord: boolean = false;
  deviceID: string = '';
  location: string = '';
  loginmessage: string;
  result: any;
  loginStatus: any;
  userID: any;
  isenabled: boolean;
  normalizePhone: any;
 
  stateList: any = [];
  tempData: any;
  msgForUser: any;
  pdata:any={};
  uuid:string ="";
  loginData = {
    'userph': '',
    'userpsw': ''
  };
  /* end all user registration data acmy */

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public http: Http,
    public datePipe: DatePipe,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public sqlite: SQLite,
    public changefont: Changefont,
    public events: Events,
    public util: UtilProvider,
    public alertCtrl: AlertController,
    public global: GlobalProvider,
    private device: Device,
    public network: Network,
    public platform: Platform,
    private all: AllserviceProvider,
    private slimLoader: SlimLoadingBarService,
  ) {
    this.showFont = "uni";
    this.menuCtrl.swipeEnable(false);
    //this.ipaddress = this.global.ipaddressApp;
    this.storage.get('ipaddressApp').then((ip) => {
      if (ip !== undefined && ip !== "" && ip != null) {
        this.ipaddress = ip;
      } else {
        this.ipaddress = this.global.ipaddressApp;
      }
    }); 
    // this.events.subscribe('ipaddressApp', ip => {
    //   this.ipaddress = ip;
    // });
   
    
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });

    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });

    this.storage.get('font').then((font) => {
      this.keyboardfont = font;
    });

    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;
    });
	
	this.platform.registerBackButtonAction(() => {
      ////console.log("Active Page=" + this.navCtrl.getActive().name);
      let view = this.navCtrl.getActive().name;

      if (view == "SignUpPage") {
        this.navCtrl.pop();
      }
    });
  }

  changelanguage(lan) {
    this.language = lan;

    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }
    }
  }
  inputChange(a) {
    if (a === 1) {
      this.uName = true;
      this.pWord = false;
    } else if (a === 2) {
      this.uName = false;
      this.pWord = true;
    }
    this.loginmessage = "";
  }
  checkNetwork() {
    if (this.network.type == "none") {
      this.flag = 'none';
    } else {
      this.flag = 'success';
    }
  }
  onChange(i, a) {
    if (i == "09") {
      this.loginData.userph = "+959";
    }
    else if (i == "959") {
      this.loginData.userph = "+959";
    }
    if (a == 1) {
      this.uName = true;
      this.pWord = false;
    } else {
      this.uName = false;
      this.pWord = true;
    }
  }
  // goLogin() {
  //   this.http.get(this.ipaddress + "/service002/getLoginAuthentication").map(res => res.json()).subscribe(result => {
  //     this.isenabled = true;
  //     if (result.code == '0000') {
  //       this.loginStatus = result.status;
  //         this.goLoginUPO();
  //         // console.log("login with userId, password and otp");
  //       }
      
  //   },
  //     error => {
  //       this.all.showAlert('Warning!', this.all.getErrorMessage(error));
  //     })
  // }
  goLoginUPO() {//login with userid, password and otp
    let num = /^[0-9-\+]*$/;
    let flag = false;
    if (this.loginData.userph == '' || this.loginData.userph == null || this.loginData.userph == undefined) {
      this.isenabled = false;
      if (this.loginStatus == 5) {
        this.loginmessage = this.showFont[10];
      } else {
        this.loginmessage = this.showFont[2];
      }
    } else if (this.loginData.userpsw == '' || this.loginData.userpsw == null || this.loginData.userpsw == undefined) {
      if (this.loginStatus == 5) {
        this.loginmessage = this.showFont[11];
      } else {
        this.loginmessage = this.showFont[3];
      }
      this.isenabled = false;
    } else {
      if (num.test(this.loginData.userph)) {
        this.isenabled = false;
        this.normalizePhone = this.util.normalizePhone(this.loginData.userph);
        if (this.normalizePhone.flag == true) {
          flag = true;
          this.loginmessage = '';
          this.loginData.userph = this.normalizePhone.phone;
          this.userID = this.util.removePlus(this.loginData.userph);
        } else {
          flag = true;
          this.loginmessage = '';
          this.userID = this.loginData.userph
        }
      } else {
        this.isenabled = false;
        flag = true;
        this.loginmessage = '';
        this.userID = this.loginData.userph
      }
      if (flag) {
        this.isenabled = true;
        this.slimLoader.start(() => {
        });
        let param = {
          userID: this.userID,
          password: this.loginData.userpsw,
          deviceID: this.deviceID,
          location: this.location,
        };
        this.http.post(this.ipaddress + '/service002/getLoginOTP', param).map(res => res.json()).subscribe(data => {
          this.isenabled = false;
          let otpData = { userID: this.userID, password: this.loginData.userpsw, sessionID: data.sessionID, otpcode: data.otpCode, rKey: data.rKey, sKey: data.sKey };
          let loginData = data;
          if (data.code == "0000") {
            this.navCtrl.push(MobileLoginOtpPage, {
              data: otpData,
              userData: loginData
            })
            this.slimLoader.complete();
            //this.loading.dismiss();
          }
          else if (data.code == "0016") {
            this.logoutAlert(data.desc);
            this.slimLoader.complete();

          }
          else if (data.code == "0014") {
            let toast = this.toastCtrl.create({
              message: data.desc,
              duration: 3000,
              position: 'bottom',
              //  showCloseButton: true,
              dismissOnPageChange: true,
              // closeButtonText: 'OK'
            });
            toast.present(toast);
            this.slimLoader.complete();

          }
          else {
            this.all.showAlert('Warning!', data.desc);
            this.slimLoader.complete();
          }

        },
          error => {
            this.isenabled = false;
            this.all.showAlert('Warning!', this.all.getErrorMessage(error));
            this.slimLoader.complete();
          });
      } else {
        this.isenabled = false;
        this.all.showAlert('Warning!', "Check your internet connection!");
      }
      this.network.onDisconnect().subscribe(() => {
        this.isenabled = false;
        this.flag = 'none';
        this.all.showAlert('Warning!', "Check your internet connection!");
      }, error => console.error(error));
    }
  }
  logoutAlert(message) {
    let confirm = this.alertCtrl.create({
      title: 'Warning!',
      enableBackdropDismiss: false,
      message: message,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.events.publish('login_success', false);
            this.events.publish('lastTimeLoginSuccess', '');
            this.navCtrl.setRoot(WalletPage, {
            });
            this.navCtrl.popToRoot();
          }
        }
      ],
      cssClass: 'warningAlert',
    })
    confirm.present();
  }
  
  



 
  
}