import { Component, ViewChild } from '@angular/core';
import { Events, Content,PopoverController } from 'ionic-angular';
import { NavController, NavParams, Platform, LoadingController, ToastController, Navbar, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FunctProvider } from '../../providers/funct/funct';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { ReplyCommentPage } from '../reply-comment/reply-comment';
import { VideodetailPage } from '../videodetail/videodetail';
import { LikepersonPage } from '../likeperson/likeperson';
import { Changefont } from '../changefont/changeFont';
import { CreatePopoverPage } from '../create-popover/create-popover';
import { GlobalProvider } from '../../providers/global/global';
import { PostLikePersonPage } from '../post-like-person/post-like-person';
import { FbdetailPage } from '../fbdetail/fbdetail';
import { ViewPhotoMessagePage } from '../view-photo-message/view-photo-message';
import { PopoverCommentPage } from '../popover-comment/popover-comment';
import { Clipboard } from '@ionic-native/clipboard';

@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html',
  providers: [FunctProvider, CreatePopoverPage]
})
export class CommentPage {

  @ViewChild(Content) content: Content;
  @ViewChild(Navbar) navBar: Navbar;

  textMyan: any = ["မှတ်ချက်များ", "မှတ်ချက် မရှိပါ", "မှတ်ချက်ရေးရန်", "ကြိုက်တယ်", "မှတ်ချက်", "မျှဝေမည်", "အသေးစိတ်"];
  textEng: any = ["Replies", "No Replies", "Write a comment...", "Like", "Comment", "Share", "Details"];
  textdata: string[] = [];
  showFont: any;
  keyboardfont: any;
  language: any;

  ipaddress: string = '';

  photoLink: any;
  videoImgLink: any;
  imageLink: any;
  postTitle:any ='';

  passData: any;
  title: any = '';
  replyData: any = [];
  isLoading: any;
  time: any;
  comment: any = '';
  postcmt: any = { t3: '', t2: '' };
  popover: any;
  registerData: any = { t3: '' };
  loading: any;
  nores: any;
  profileImageLink: any;
  textData: any = [];
  like: any = true;
  unlike: any = false;
  likeCount: any = 0;
  replyCount: any = 3;
  detailData: any;
  exit: any = false;
  url: any;
  cmtphotoLink: any;
  textreply: any;
  personlike: any = [];
  img: any = '';
  status: any = false;
  imglink: any;

  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public alert: AlertController,
    public http: Http,
    public toastCtrl: ToastController,
    public platform: Platform,
    public funct: FunctProvider,
    public changefont: Changefont,
    public events: Events,
    public createPopover: CreatePopoverPage,
    public global: GlobalProvider,
    public popoverCtrl:PopoverController,
    private clipboard: Clipboard,
  ) {
    this.ipaddress = this.global.ipaddressCMS;
    this.showFont = "uni";

    this.photoLink = this.global.digitalMedia + "upload/image";
    this.imageLink = this.global.digitalMedia + "upload/smallImage/contentImage/";
    this.videoImgLink = this.global.digitalMedia +"upload/smallImage/videoImage/";

     this.profileImageLink = "";
    this.storage.get('profileImage').then((profileImage) => {
      if (profileImage != undefined && profileImage != null && profileImage != '') {
        this.profileImageLink = profileImage;
      } else {
        this.profileImageLink = this.global.digitalMediaProfile;
      }
    }); 

    this.detailData = this.navParams.get('data');
    if (this.detailData.t1 != undefined && this.detailData.t1 != null && this.detailData.t1 != '') {
      this.postTitle = this.detailData.t1;
    } else {
      this.postTitle = "";
    }
    console.log("detailData.uploadedPhoto == "+this.detailData.uploadedPhoto);
    this.title = this.navParams.get('title');
    this.passData = this.navParams.get("data");
    this.events.subscribe('changelanguage', lan => {
      this.changelanguage(lan);
    });
    this.storage.get('language').then((lan) => {
      this.changelanguage(lan);
    });
    this.storage.get('font').then((font) => {
      this.keyboardfont = font;

      if (font == "zg" && this.language != "eng") {
        this.textData[2] = this.changefont.UnitoZg(this.textMyan[2]);
      } else if (font == "uni" && this.language != "eng") {
        this.textData[2] = this.textMyan[2];
      }
    });
    this.events.subscribe('changeFont', font => {
      this.keyboardfont = font;

      if (font == "zg" && this.language != "eng") {
        this.textData[2] = this.changefont.UnitoZg(this.textMyan[2]);
      } else if (font == "uni" && this.language != "eng") {
        this.textData[2] = this.textMyan[2];
      }
    });
  }

  changelanguage(lan) {
    this.language = lan;

    if (lan == "eng") {
      for (let i = 0; i < this.textEng.length; i++) {
        this.textData[i] = this.textEng[i];
      }
    } else {
      for (let i = 0; i < this.textMyan.length; i++) {
        this.textData[i] = this.textMyan[i];
      }

      if (this.keyboardfont == 'zg') {
        this.textData[2] = this.changefont.UnitoZg(this.textMyan[2]);
      }
    }
  }

  ionViewDidEnter() {
    this.storage.get('appData').then((data) => {
      this.registerData = data;
      ////console.log("registerData = " + JSON.stringify(this.registerData));
      this.getCommentData();
    });

    this.backButtonExit();
  }

  ionViewDidLoad() {
    ////console.log('ionViewDidLoad CommentPage');
  }

  getCommentData() {     //tmn
    console.log("response data = " + JSON.stringify(this.passData.syskey +" And " +this.registerData.syskey));
    this.http.get(this.ipaddress + '/serviceQuestion/getCommentmobile?id=' + this.passData.syskey + '&userSK=' + this.registerData.syskey).map(res => res.json()).subscribe(result => {      
      this.isLoading = true;
      if (result.data.length > 0) {
        this.nores = 1;
        for (let i = 0; i < result.data.length; i++) {
          result.data[i].differenceTime = this.funct.getTimeDifference(result.data[i].modifiedDate, result.data[i].modifiedTime);
          result.data[i].modifiedDate = this.funct.getTransformDate(result.data[i].modifiedDate);          
          result.data[i].like = "Like";
          if (result.data[i].person.length > 0) {
            for (let k = 0; k < result.data[i].person.length; k++) {
              if (result.data[i].person[k].syskey == this.registerData.syskey) {
                result.data[i].like = "UnLike";
                break;
              }
            }
          }
          result.data[i].likeCount = result.data[i].person.length;
          result.data[i].replyCount = result.data[i].n3;
          if (result.data[i].n3 == 1) {
            result.data[i].textreply = "reply";
          }
          else if (result.data[i].n3 > 1) {
            result.data[i].textreply = "replies";
          }
        }
        this.replyData = result.data;
        this.content.scrollToBottom();
        console.log("replyData data = " + JSON.stringify(this.replyData));
      }
      else {
        this.nores = 0;
      }
	  if(this.title == 'comment'){//atn
      this.content.scrollToBottom();
    }
      this.isLoading = false;
    },
      error => {
		this.isLoading = false;
        this.getError(error);
      });
  }

  onFocus() {
    this.content.resize();
    this.content.scrollToBottom();
  }

  saveComment() {
    if (this.comment != '') {
      this.comment = this.comment.trim();

      if (this.keyboardfont == 'zg') {
        this.comment = this.changefont.ZgtoUni(this.comment);
      }

      this.status = true;
      let parameter = {
        t1: "answer",
        t2: this.comment,
        n1: this.passData.syskey,
        n5: this.registerData.syskey

      }
      //   this.postcmt = {t3: this.registerData.t3 , t2: this.comment};
      ////console.log("requst saveComment=" + JSON.stringify(parameter));
      //  this.replyData.push(this.postcmt);
      this.content.scrollToBottom();
      this.comment = '';
      this.http.post(this.ipaddress + '/serviceQuestion/saveAnswer', parameter).map(res => res.json()).subscribe(result => {
       // //console.log("return saveComment data = " + JSON.stringify(result));
        if (result.state && result.data.length > 0) {
          this.nores = 1;

          for (let i = 0; i < result.data.length; i++) {
            result.data[i].differenceTime = this.funct.getTimeDifference(result.data[i].modifiedDate, result.data[i].modifiedTime);

            result.data[i].modifiedDate = this.funct.getTransformDate(result.data[i].modifiedDate);

            result.data[i].like = "Like";
            if (result.data[i].person.length > 0) {
              for (let k = 0; k < result.data[i].person.length; k++) {
                if (result.data[i].person[k].syskey == this.registerData.syskey) {
                  result.data[i].like = "UnLike";
                  break;
                }
              }
            }

            result.data[i].likeCount = result.data[i].person.length;
            result.data[i].replyCount = result.data[i].n3;
            if (result.data[i].n3 == 1) {
              result.data[i].textreply = "reply";
            }
            else if (result.data[i].n3 > 1) {
              result.data[i].textreply = "replies";
            }
          }


          this.replyData = result.data;
          this.content.scrollToBottom();
          //this.comment = '';
          this.detailData.n3 = this.detailData.n3 + 1;
          //this.detailData.commentCount = this.funct.getChangeCount(this.detailData.n3);
          this.detailData.commentCount = this.detailData.n3;
        }
        else {
          if (this.replyData.length > 0)
            this.nores = 1;
          else
            this.nores = 0;
        }
        this.status = false;
        this.isLoading = false;
        this.content.resize();
        this.content.scrollToBottom();
      },
        error => {
          this.status = false;
          //this.comment = '';
          if (this.replyData.length > 0)
            this.nores = 1;
          else
            this.nores = 0;
          this.getError(error);
        });
    }
  }
  presentCommentPopover(ev,d) {
    let st;
      if(d.n5 != this.registerData.syskey){
            st = 0;
          }else{
            st = 1;
          }
      this.popover = this.popoverCtrl.create(PopoverCommentPage, {
      data:st
      });

      this.popover.present({
        ev: ev
      });

      let doDismiss = () => this.popover.dismiss();
      let unregBackButton = this.platform.registerBackButtonAction(doDismiss, 5);
      this.popover.onDidDismiss(unregBackButton);

      this.popover.onWillDismiss(data => {
        console.log("popover dismissed");
        console.log("Selected Item is " + data);
          if(data == "1"){
            let copytext = this.changefont.UnitoZg(d.t2);
          this.clipboard.copy(copytext);
        }
        else if(data == "2"){
          this.getDeleteMsg(d);
        }
      });
  }

  getError(error) {
    /* ionic App error
     ............001) url link worng, not found method (404)
     ........... 002) server not response (500)
     ............003) cross fillter not open (403)
     ............004) server stop (-1)
     ............005) app lost connection (0)
     */
    let code;
    if (error.status == 404) {
      code = '001';
    }
    else if (error.status == 500) {
      code = '002';
    }
    else if (error.status == 403) {
      code = '003';
    }
    else if (error.status == -1) {
      code = '004';
    }
    else if (error.status == 0) {
      code = '005';
    }
    else if (error.status == 502) {
      code = '006';
    }
    else {
      code = '000';
    }
    let msg = '';
    if (code == '005') {
      msg = "Please check internet connection!";
    }
    else {
      msg = "Can't connect right now. [" + code + "]";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 5000,
      position: 'bottom',
      //  showCloseButton: true,
      dismissOnPageChange: true,
      // closeButtonText: 'OK'
    });
    toast.present(toast);
    this.isLoading = false;
    // this.loading.dismiss();
    ////console.log("Oops!");
  }
  viewImage(i) {
    this.navCtrl.push(FbdetailPage, {
      detailData: i 
    });
  }
  singlePhoto(i) {
    console.log("viewImage == " + JSON.stringify(i));
    let localNavCtrl: boolean = false;
    if (localNavCtrl) {
      this.navCtrl.push(ViewPhotoMessagePage, {
        data: i,
        contentImg: "singlePhoto"
      });
    } else {
      this.navCtrl.push(ViewPhotoMessagePage, {
        data: i,
        contentImg: "singlePhoto"
      });
    }
  }

  changeLike(skey, index) {
    /*this.like = false;
    this.unlike = true;*/
    /*this.replyData[i].like = false;
    this.replyData[i].unlike = true;
    this.likeCount = this.likeCount + 1;*/
    this.replyData[index].likeCount = this.replyData[index].likeCount - 1;
    let parameter = {
      n1: this.registerData.syskey,
      n2: skey
    }
    console.log("Change Like"+ parameter)
    this.content.scrollToBottom();
    this.http.post(this.ipaddress + '/serviceQuestion/saveCommentLike', parameter).map(res => res.json()).subscribe(data => {
      if (data.state && data.data.length > 0) {
        for (let i = 0; i < data.data.length; i++) {
          if (data.data[i].person.length > 0) {
            for (let k = 0; k < data.data[i].person.length; k++) {
              if (data.data[i].person[k].syskey == this.registerData.syskey) {
                this.replyData[index].like = "UnLike";
                break;
              }
              else {
                this.replyData[index].like = "Like";
              }
            }

          }
          else {
            this.replyData[index].like = "Like";
          }

          this.replyData[index].likeCount = data.data[i].person.length;
          this.replyData[index].person = data.data[i].person;
        }
      }
    },
      error => {
        this.getError(error);
      });

  }  

  ClickReply(cmt) {
    this.navCtrl.push(ReplyCommentPage, {
      data : cmt,
      detaildata :this.detailData
    });
  }

  clickLike(data) {
    this.detailData.likeflag = true;
    if (!data.showLike) {
      this.detailData.showLike = true;
      this.detailData.n2 = this.detailData.n2 + 1;
      this.detailData.likeCount = this.detailData.n2;
      this.getLike(data);
    }
    else {
      this.detailData.showLike = false;
      this.detailData.n2 = this.detailData.n2 - 1;
      this.detailData.likeCount = this.detailData.n2;
      this.getUnlike(data);
    }
  }

  getLike(data) {
    let parameter = {
      key: data.syskey,
      userSK: this.registerData.usersyskey,
      type: data.t3
    }
    this.http.get(this.ipaddress + '/serviceArticle/clickLikeArticle?key=' + data.syskey + '&userSK=' + this.registerData.usersyskey + '&type=' + data.t3).map(res => res.json()).subscribe(data => {
      this.detailData.likeflag = false;
      if (data.state) {
        this.detailData.showLike = true;
      }
      else {
        this.detailData.showLike = false;
        this.detailData.n2 = this.detailData.n2 - 1;
      }
    }, error => {
      this.getError(error);
    });
  }

  getUnlike(data) {
    let parameter = {
      key: data.syskey,
      userSK: this.registerData.usersyskey,
      type: data.t3
    }
    this.http.get(this.ipaddress + '/serviceArticle/clickUnlikeArticle?key=' + data.syskey + '&userSK=' + this.registerData.usersyskey + '&type=' + data.t3).map(res => res.json()).subscribe(data => {
      this.detailData.likeflag = false;
      if (data.state) {
        this.detailData.showLike = false;
      }
      else {
        this.detailData.showLike = true;
        this.detailData.n2 = this.detailData.n2 + 1;
      }
    }, error => {
      this.getError(error);
    });
  }

  clickBookMark(data) {
    if (!data.showContent) {
      this.detailData.showContent = true;
      this.saveContent(data);
    }
    else {
      this.detailData.showContent = false;
      this.unsaveContent(data);
    }
  }

  saveContent(data) {
    let parameter = {
      t1: data.t3,
      t4: this.registerData.t3,
      n1: this.registerData.usersyskey,
      n2: data.syskey,
      n3: 1
    }  
    this.http.post(this.ipaddress + '/serviceContent/saveContent', parameter).map(res => res.json()).subscribe(data => {    
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  ionViewCanLeave() {
   // console.log("Now i am leaving="+ this.detailData.t2)
  }

  unsaveContent(data) {
    ////console.log("request unsaveContent = ", JSON.stringify(data));
    let parameter = {
      t1: data.t3,
      t4: this.registerData.t3,
      n1: this.registerData.usersyskey,
      n2: data.syskey,
      n3: 0
    }
    ////console.log("request unsaveContent parameter = ", JSON.stringify(parameter));
    this.http.post(this.ipaddress + '/serviceContent/unsaveContent', parameter).map(res => res.json()).subscribe(data => {
      ////console.log("response unsaveContent = ", JSON.stringify(data));

      //this.isLoading = false;
    }, error => {
      //console.log("signin error=" + error.status);
      this.getError(error);
    });
  }

  /*share(i) {
    let appStore = "";

    if (this.global.regionCode == '13000000') {
      //Yangon App (13000000)
      appStore = 'https://play.google.com/store/apps/details?id=dc.digitalyangon';
    }
    if (this.global.regionCode == '10000000') {
      //Mandalay App (10000000)
      appStore = 'https://play.google.com/store/apps/details?id=dc.digitalmandalay';
    }
    if (this.global.regionCode == '00000000') {
      //All App (00000000)
      appStore = 'https://play.google.com/store/apps/details?id=wallet.NSBmawalletdev';
    }

    ////console.log("this is share url == " + JSON.stringify(i));

    let sahareImg = this.photoLink + "/dc/dc_logo.png";
    if (i.uploadedPhoto.length > 0) {
      if (i.uploadedPhoto[0].t7 != '')
        sahareImg = i.uploadedPhoto[0].t7;
      else
        sahareImg = this.photoLink + "/dc/imgErr.png";
    }
    let title = this.changefont.UnitoZg(i.t1);
    const Branch = window['Branch'];
    //  this.url = "https://b2b101.app.link/R87NzKABHL";

    var propertiesObj = {
      canonicalIdentifier: 'content/123',
      canonicalUrl: 'https://example.com/content/123',
      title: title,
      contentDescription: '' + Date.now(),
      contentImageUrl: sahareImg,
      price: 12.12,
      currency: 'GBD',
      contentIndexingMode: 'private',
      contentMetadata: {
        custom: 'data',
        testing: i.syskey,
        this_is: true
      }
    }

    // create a branchUniversalObj variable to reference with other Branch methods
    var branchUniversalObj = null
    Branch.createBranchUniversalObject(propertiesObj).then(function (res) {
      branchUniversalObj = res
      //alert('Response1: ' + JSON.stringify(res))
      ////console.log('Response1: ' + JSON.stringify(res))
      // optional fields
      var analytics = {
        channel: 'facebook',
        feature: 'onboarding',
        campaign: 'content 123 launch',
        stage: 'new user',
        tags: ['one', 'two', 'three']
      }

      // optional fields
      var properties1 = {
        $desktop_url: appStore,
        $android_url: 'http://www.example.com/android',
        $ios_url: 'http://www.example.com/ios',
        $ipad_url: 'http://www.example.com/ipad',
        $deeplink_path: 'content/123',
        $match_duration: 2000,
        custom_string: i.syskey,
        custom_integer: Date.now(),
        custom_boolean: true
      }

      branchUniversalObj.generateShortUrl(analytics, properties1).then(function (res) {
        //alert('Response2: ' + JSON.stringify(res.url));
        ////console.log('Response2: ' + JSON.stringify(res.url));
        // optional fields
        var analytics = {
          channel: 'facebook',
          feature: 'onboarding',
          campaign: 'content 123 launch',
          stage: 'new user',
          tags: ['one', 'two', 'three']
        }

        // optional fields
        var properties = {
          $desktop_url: appStore,
          custom_string: i.syskey,
          custom_integer: Date.now(),
          custom_boolean: true
        }

        var message = 'Check out this link'

        // optional listeners (must be called before showShareSheet)
        branchUniversalObj.onShareSheetLaunched(function (res) {
          // android only
          ////console.log(res)
        })
        branchUniversalObj.onShareSheetDismissed(function (res) {
         // //console.log(res)
        })
        branchUniversalObj.onLinkShareResponse(function (res) {
          ////console.log(res)
        })
        branchUniversalObj.onChannelSelected(function (res) {
          // android only
          ////console.log(res)
        })

        // share sheet
        branchUniversalObj.showShareSheet(analytics, properties, message)
      }).catch(function (err) {
        //console.error('Error2: ' + JSON.stringify(err))
      })
    }).catch(function (err) {
      //console.error('Error1: ' + JSON.stringify(err))
    })
  }*/

  goDetail(url) {
    this.navCtrl.push(VideodetailPage, {
      url: url
    });
  }

  backButtonExit() {
    this.platform.registerBackButtonAction(() => {
      ////console.log("Active Page=" + this.navCtrl.getActive().name);
      this.navCtrl.pop();
    });
  }

  getDeleteMsg(d) {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: true
      //   duration: 3000
    });
    this.loading.present();
    ////console.log("request d.syskey =" + d.syskey);
    this.http.get(this.ipaddress + "/serviceQuestion/deleteComment?syskey=" + d.syskey).map(res => res.json()).subscribe(result => {
      ////console.log("response process msg =" + JSON.stringify(result));
      if (result.state) {
        this.getCommentData();
        this.detailData.n3 = this.detailData.n3 - 1;
        //this.detailData.commentCount = this.funct.getChangeCount(this.detailData.n3);
        this.detailData.commentCount = this.detailData.n3;
        this.loading.dismiss();
      }
      else {
        let toast = this.toastCtrl.create({
          message: 'Delete Failed! Please try again.',
          duration: 5000,
          position: 'bottom',
          //  showCloseButton: true,
          dismissOnPageChange: true,
          // closeButtonText: 'OK'
        });
        toast.present(toast);
        this.loading.dismiss();
      }
    }, error => {
      ////console.log("signin error=" + error.status);
      this.getError(error);
      this.loading.dismiss();
    });
  }

  presentPopover(ev) {
    this.createPopover.presentPopover(ev);
  }

  urlValidation(string) {
    var re = new RegExp("(?:(?:(?:ht|f)tp)s?://)?[\\w_-]+(?:\\.[\\w_-]+)+([\\w.,@?^=%&:/~+#-]*[\\w@?^=%&/~+#-])?");
    let res = re.test(string);
    return re.test(string);
  }

  getLikePerson(i){    
    this.navCtrl.push(PostLikePersonPage,{
      data:i.syskey
    })
  }
  likePerson(cmt) { 
    console.log("this.personlike == "+JSON.stringify(cmt));
    this.navCtrl.push(LikepersonPage, {//LikepersonPage
      data: cmt.syskey
    });     
  }
}
