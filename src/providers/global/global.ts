import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';

@Injectable()
export class GlobalProvider {
  ipaddressCMS: any;
  ipaddressWallet: any;
  ipaddressChat: any;
  ipaddressTicket: any;

  appIDChat: any;
  appName: any = '';
  domainChat: any;
  domainCMS: any;
  ipaddress: any;
  ipaddress3: string;
  ipAdressMFI:string;
  ipphoto:any;
  imglink: any;
  digitalMedia: any;
  digitalMediaProfile: any;
  region: string;
  regionCode: string;
  netWork: string = '';
  seen: boolean = true;
  unseen: boolean = false;
  amount: string;
  noticount=0;

  apptype: any = '';
  appCode: any;
  okDollarCode: string;
  waveMoneyCode: string;
  waveName1: string;
  okName2: string;
  nearbyIndex: any;
  DECIMAL_SEPARATOR = ".";
  GROUP_SEPARATOR = ",";
  public phone: any;
  public type: any = "android";
  public version: any = "1.0.12";
  ipaddressApp: string;
  outletSyskey: any;
  startshop: number = 0;
  PersonData = {
    'applicantdata': {
      'member_name': '', 'nrc': '', 'dob': '',
      'current_address_region': '', 'current_address_township': '',
      'current_address_street': '', 'current_address_houseno': '',
      'mobile': '', 'email': '', 'company_name': '', 'department': '',
      'current_position': '', 'years_of_service': '', 'monthly_basic_income': '',
      'company_ph_no': '', 'front_nrc_image_path': '', 'back_nrc_image_path': '',
      'syskey': '', 'creditForOhters': '0', 'creditAmount': ''
    },
    'guarantordata': {
      'member_name': '', 'nrc': '', 'dob': '',
      'current_address_region': '', 'current_address_township': '',
      'current_address_street': '', 'current_address_houseno': '',
      'mobile': '', 'email': '', 'company_name': '', 'department': '',
      'current_position': '', 'years_of_service': '', 'monthly_basic_income': '',
      'company_ph_no': '', 'creditForOhters': '0', 'creditAmount': '', 'syskey': '', "front_nrc_image_path": "", "back_nrc_image_path": ""
    },
    'loandata': {
      'householdRegistraionFileLocation': '', 'householdRegistrationImageFile': '', 'policeCertificateFIleLocation': '',
      'employmentLetterFileLocation': '', 'monthlyAmount': '', 'paymentTermSyskey': '', 'paymentTerm': '',
      'registrationType': '', 'totalAmount': '', 'promotionamount': '', 'loanSyskey': '', "loanid": '', 'loanAppliedDate': '',
      'status': ''
    },

    'applyProductList': { "data": [] },
    "shopDataList": {
      'shopdata': [{
        'outletName': '', 'outletCurrentAddressHomeNo': '', 'outletCurrentAddressStreet': '',
        'outletCurrentAddressRegion': '', 'outletCurrentAddressTownship': '',
        'organizationSyskey': '', 'outletSyskey': ''
      }]
    },
    'imagealldata': { 'imageparsedata': [] }

  };
  applicantsyskey: any;
  brand: any;
  productcount: number = 0;
  totalamount: number = 0;
  startloanstaus: number = 0;
  startschedulestatus: number = 0;
  regtype: any = '';
  comeHome = true;
  //for hire purchase
  frontnrcdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  backnrcdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  guafrontnrcdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  guabacknrcdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  householdfrontdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  householdbackdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  policecertificatedata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  employmentletterdata = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  // for staff loan
  appPhotoData = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  appNrcFrontData = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  appNrcBackData = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  deptChRecomData = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  familyRegFrontData = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  familyRegBackData = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  staffIdData = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  gua1NrcFrontData = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  gua1NrcBackData = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  gua2NrcFrontData = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  gua2NrcBackData = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  gua1SignImg = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };
  gua2SignImg = { 'imgname': '', 'profileImg': '', 'photoName': '', 'profile': '', "edit": "0" };

  constructor(public network: Network) {
    this.appName = "dc";
    this.appIDChat = "010";
    this.domainChat = "DC001";
    this.domainCMS = "001";
    //All
    this.region = "All";
    this.regionCode = "00000000";
    
    // this.ipaddressApp = "http://52.187.13.89:8080/AppService/module001";
    // this.ipaddress     = "http://52.187.13.89:8080/WalletService/module001";
    // this.ipaddressCMS  = "http://52.187.13.89:8080/WalletService/module001";
    // this.ipaddressWallet = "http://52.187.13.89:8080/WalletService/module001";
    // this.ipaddressTicket = "http://52.187.13.89:8080/WalletService/module001";                                                                                   
    // this.digitalMedia = "http://52.187.13.89:8080/DigitalMedia/";
    // this.digitalMediaProfile = "http://52.187.13.89:8080/DigitalMedia/upload/image/userProfile/";
    // this.ipaddress3 = "http://52.187.13.89:8080/chatting/api/v1/";
    // this.imglink = "http://52.187.13.89:8080/chatting";
    // // this.ipAdressMFI="http://122.248.120.16:8080/MFIWebService/module001"
    // this.ipAdressMFI="http://52.187.13.89:8080/WalletService/module001";
    // this.ipphoto = "http://52.187.13.89:8080/MFIMedia/";
    this.ipaddressApp = "http://52.187.13.89:8080/AppService/module001";
    this.ipaddress     = "http://192.168.206.195:8084/WalletService/module001";
    this.ipaddressCMS  = "http://192.168.206.195:8084/WalletService/module001";
    this.ipaddressWallet = "http://192.168.206.195:8084/WalletService/module001";
    this.ipaddressTicket = "http://192.168.206.195:8084/WalletService/module001";                                                                                   
    this.digitalMedia = "http://52.187.13.89:8080/DigitalMedia/";
    this.digitalMediaProfile = "http://52.187.13.89:8080/DigitalMedia/upload/image/userProfile/";
    this.ipaddress3 = "http://52.187.13.89:8080/chatting/api/v1/";
    this.imglink = "http://52.187.13.89:8080/chatting";
    // this.ipAdressMFI="http://122.248.120.16:8080/MFIWebService/module001"
    this.ipAdressMFI="http://192.168.206.195:8084/WalletService/module001";
    this.ipphoto = "http://52.187.13.89:8080/MFIMedia/";


    this.netWork = 'connected'; 
    //for MFI 
    //this.applicantsyskey = 0;
    this.waveMoneyCode = "000700";
    this.waveName1 = "MFI";
    this.okDollarCode = "000600";
    this.okName2 = "Operator B";
    this.brand = "0002"; // Yum Yum
    this.apptype = "001"; // zala   
    this.phone = "01507209";

    this.network.onDisconnect().subscribe(() => {
      this.netWork = 'disconnected';
      console.log('network was disconnected :-(');
    });

    this.network.onConnect().subscribe(() => {
      this.netWork = 'connected';
      console.log('network connected!');
    });

    //the following may be used for mobile banking
    this.appCode = "MA"; // OK
  
  
    this.waveMoneyCode = "000700";
    this.waveName1 = "dc";
    this.okName2 = "ok";
    this.okDollarCode = "000600";
    this.apptype = "001"; // zala   
    this.phone = "01507209";
  }

  format(valString) {
    if (!valString) {
      return '';
    }
    let val = valString.toString();
    const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
    return parts[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, this.GROUP_SEPARATOR)

  };

  unFormat(val) {
    if (!val) {
      return '';
    }
    val = val.replace(/^0+/, '').replace(/\D/g, '');
    if (this.GROUP_SEPARATOR === ',') {
      return val.replace(/,/g, '');
    } else {
      return val.replace(/\./g, '');
    }
  };
  defaultData() {
    return {
      'applicantdata': {
        'member_name': '', 'nrc': '', 'dob': '',
        'current_address_region': '', 'current_address_township': '',
        'current_address_street': '', 'current_address_houseno': '',
        'mobile': '', 'email': '', 'company_name': '', 'department': '',
        'current_position': '', 'years_of_service': '', 'monthly_basic_income': '',
        'company_ph_no': '', 'front_nrc_image_path': '', 'back_nrc_image_path': '',
        'syskey': '', 'creditForOhters': '0', 'creditAmount': ''
      },
      'guarantordata': {
        'member_name': '', 'nrc': '', 'dob': '',
        'current_address_region': '', 'current_address_township': '',
        'current_address_street': '', 'current_address_houseno': '',
        'mobile': '', 'email': '', 'company_name': '', 'department': '',
        'current_position': '', 'years_of_service': '', 'monthly_basic_income': '',
        'company_ph_no': '', 'creditForOhters': '0', 'creditAmount': '', 'syskey': '', "front_nrc_image_path": "", "back_nrc_image_path": ""
      },
      'loandata': {
        'householdRegistraionFileLocation': '', 'householdRegistrationImageFile': '', 'policeCertificateFIleLocation': '',
        'employmentLetterFileLocation': '', 'monthlyAmount': '', 'paymentTermSyskey': '', 'paymentTerm': '',
        'registrationType': '', 'totalAmount': '', 'promotionamount': '', 'loanSyskey': '', "loanid": '', 'loanAppliedDate': '',
        'status': ''
      },

      'applyProductList': { "data": [] },
      "shopDataList": {
        'shopdata': [{
          'outletName': '', 'outletCurrentAddressHomeNo': '', 'outletCurrentAddressStreet': '',
          'outletCurrentAddressRegion': '', 'outletCurrentAddressTownship': '',
          'organizationSyskey': '', 'outletSyskey': ''
        }]
      },
      'imagealldata': { 'imageparsedata': [] }
    }
  };
  ///for staff loan
  getDefaultObj() {
    return {
      "staffLoanData": {
        "loanAmount": '',
        "loanAmountInWords": "",
        "bankInterestRate": '',
        "serviceCharges": '',
        "departmentChiefRecommendation": "",
        "staffIDCardImagePath": "",
        "backFamilyRegImagePath": "",
        "frontFamilyRegImagePath": "",
        "loanTerm": "",
        "loanSyskey": 0,
        "loanType": "",
        "currentPosition": "",
        "department": "",
        "monthlyBasicIncome": "",
        "appliedDate": ""
      },
      "staffLoanApplicant": {
        "mobile": "",
        "staffID": "",
        "staffSyskey": 0,
        "photoPath": "",
        "department": "",
        "monthlyBasicIncome": '',
        "currentAddressRegion": "",
        "currentPosition": "",
        "currentAddressStreet": "",
        "currentAddressHouseNo": "",
        "currentAddressTownship": "",
        "backNRCImagePath": "",
        "frontNRCImagePath": "",
        "staffNrc": "",
        "staffName": ""
      },
      "staffLoanGuarantor1": {
        "mobile": "",
        "guarantorName": "",
        "guarantorNrc": "",
        "department": "",
        "currentAddressTownship": "",
        "currentAddressRegion": "",
        "currentPosition": "",
        "currentAddressStreet": "",
        "currentAddressHouseNo": "",
        "backNRCImagePath": "",
        "frontNRCImagePath": "",
        "guarantorSyskey": "",
        "guarantorPDFImagePath": "",
        "guarantorSignImagePath": ""
      },
      "staffLoanGuarantor2": {
        "mobile": "",
        "guarantorName": "",
        "guarantorNrc": "",
        "department": "",
        "currentAddressTownship": "",
        "currentAddressRegion": "",
        "currentPosition": "",
        "currentAddressStreet": "",
        "currentAddressHouseNo": "",
        "backNRCImagePath": "",
        "frontNRCImagePath": "",
        "guarantorSyskey": "",
        "guarantorPDFImagePath": "",
        "guarantorSignImagePath": ""
      },

      "staffAppliedLoanData": {
        "outletLoanTypeSyskey": 582059
      },
      'imagealldata': { 'imageparsedata': [] }
    };
  }
  loantype = {
    staffLoanType: "1",
    eduLoanType: "3"   
  
  }
  staffData = {
    "staffLoanData": {
      "loanAmount": '',
      "loanAmountInWords": "",
      "bankInterestRate": '',
      "serviceCharges": '',
      "departmentChiefRecommendation": "",
      "staffIDCardImagePath": "",
      "backFamilyRegImagePath": "",
      "frontFamilyRegImagePath": "",
      "loanTerm": "",
      "loanSyskey": 0,
      "loanType": "",
      "currentPosition": "",
      "department": "",
      "monthlyBasicIncome": "",
      "appliedDate": ""
    },
    "staffLoanApplicant": {
      "mobile": "",
      "staffID": "",
      "staffSyskey": 0,
      "photoPath": "",
      "department": "",
      "monthlyBasicIncome": '',
      "currentAddressRegion": "",
      "currentPosition": "",
      "currentAddressStreet": "",
      "currentAddressHouseNo": "",
      "currentAddressTownship": "",
      "backNRCImagePath": "",
      "frontNRCImagePath": "",
      "staffNrc": "",
      "staffName": ""
    },
    "staffLoanGuarantor1": {
      "mobile": "",
      "guarantorName": "",
      "guarantorNrc": "",
      "department": "",
      "currentAddressTownship": "",
      "currentAddressRegion": "",
      "currentPosition": "",
      "currentAddressStreet": "",
      "currentAddressHouseNo": "",
      "backNRCImagePath": "",
      "frontNRCImagePath": "",
      "guarantorSyskey": "",
      "guarantorPDFImagePath": "",
      "guarantorSignImagePath": ""
    },
    "staffLoanGuarantor2": {
      "mobile": "",
      "guarantorName": "",
      "guarantorNrc": "",
      "department": "",
      "currentAddressTownship": "",
      "currentAddressRegion": "",
      "currentPosition": "",
      "currentAddressStreet": "",
      "currentAddressHouseNo": "",
      "backNRCImagePath": "",
      "frontNRCImagePath": "",
      "guarantorSyskey": "",
      "guarantorPDFImagePath": "",
      "guarantorSignImagePath": ""
    },

    "staffAppliedLoanData": {
      "outletLoanTypeSyskey": 0
    },
    'imagealldata': { 'imageparsedata': [] }
  };



  historystaffData = {
    "staffLoanData": {
      "loanAmount": '',
      "loanAmountInWords": "",
      "bankInterestRate": '',
      "serviceCharges": '',
      "departmentChiefRecommendation": "",
      "staffIDCardImagePath": "",
      "backFamilyRegImagePath": "",
      "frontFamilyRegImagePath": "",
      "loanTerm": "",
      "loanSyskey": 0,
      "loanType": "",
      "currentPosition": "",
      "department": "",
      "monthlyBasicIncome": "",
      "appliedDate": ""

    },
    "staffLoanApplicant": {
      "mobile": "",
      "staffID": "",
      "staffSyskey": 0,
      "photoPath": "",
      "department": "",
      "monthlyBasicIncome": '',
      "currentAddressRegion": "",
      "currentPosition": "",
      "currentAddressStreet": "",
      "currentAddressHouseNo": "",
      "currentAddressTownship": "",
      "backNRCImagePath": "",
      "frontNRCImagePath": "",
      "staffNrc": "",
      "staffName": ""

    },
    "staffLoanGuarantor1": {
      "mobile": "",
      "guarantorName": "",
      "guarantorNrc": "",
      "department": "",
      "currentAddressTownship": "",
      "currentAddressRegion": "",
      "currentPosition": "",
      "currentAddressStreet": "",
      "currentAddressHouseNo": "",
      "backNRCImagePath": "",
      "frontNRCImagePath": "",
      "guarantorSyskey": "",
      "guarantorPDFImagePath": "",
      "guarantorSignImagePath": ""

    },
    "staffLoanGuarantor2": {
      "mobile": "",
      "guarantorName": "",
      "guarantorNrc": "",
      "department": "",
      "currentAddressTownship": "",
      "currentAddressRegion": "",
      "currentPosition": "",
      "currentAddressStreet": "",
      "currentAddressHouseNo": "",
      "backNRCImagePath": "",
      "frontNRCImagePath": "",
      "guarantorSyskey": "",
      "guarantorPDFImagePath": "",
      "guarantorSignImagePath": ""
    },

    "staffAppliedLoanData": {
      "outletLoanTypeSyskey": 0
    },
    'imagealldata': { 'imageparsedata': [] }
  };



}







