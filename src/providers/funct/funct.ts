import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';

@Injectable()
export class FunctProvider {

  public uuid: any;
  public latitude: any;
  public longitude: any;
  public ipaddress2: any;

  constructor(public datepipe: DatePipe) {
    this.ipaddress2="http://122.248.120.16:8080/DigitalMedia/upload/image/userProfile/";
  }

  setUuid(uuid) {
    this.uuid = uuid;
  }

  getUuid() {
    return Promise.resolve(this.uuid);
  }

  setLocation(latitude, longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  getTransformDate(date) {
    let tranDate;
    let getdate;
    let month;
    let year;
    let day;

    year = date.slice(0, 4);
    month = date.slice(4, 6);
    day = date.slice(6, 8);

    switch (month) {
      case '01':
        month = 'Jan'
        break;
      case '02':
        month = 'Feb'
        break;
      case '03':
        month = 'Mar'
        break;
      case '04':
        month = 'Apr'
        break;
      case '05':
        month = 'May'
        break;
      case '06':
        month = 'Jun'
        break;
      case '07':
        month = 'Jul'
        break;
      case '08':
        month = 'Aug'
        break;
      case '09':
        month = 'Sep'
        break;
      case '10':
        month = 'Oct'
        break;
      case '11':
        month = 'Nov'
        break;
      case '12':
        month = 'Dec'
        break;
      default:
    }
    tranDate = month + " " + day + ", " + year;
    return tranDate;
  }

  getChangeCount(count) {
    let myanCount = count.toString();
    let strlength = myanCount.length;
    let temp;

    for (let i = 0; i < strlength; i++) {
      if (myanCount.indexOf("1") > -1) {
        temp = myanCount.replace("1", "၁");
        myanCount = temp;
      }
      if (myanCount.indexOf("2") > -1) {
        temp = myanCount.replace("2", "၂");
        myanCount = temp;
      }
      if (myanCount.indexOf("3") > -1) {
        temp = myanCount.replace("3", "၃");
        myanCount = temp;
      }
      if (myanCount.indexOf("4") > -1) {
        temp = myanCount.replace("4", "၄");
        myanCount = temp;
      }
      if (myanCount.indexOf("5") > -1) {
        temp = myanCount.replace("5", "၅");
        myanCount = temp;
      }
      if (myanCount.indexOf("6") > -1) {
        temp = myanCount.replace("6", "၆");
        myanCount = temp;
      }
      if (myanCount.indexOf("7") > -1) {
        temp = myanCount.replace("7", "၇");
        myanCount = temp;
      }
      if (myanCount.indexOf("8") > -1) {
        temp = myanCount.replace("8", "၈");
        myanCount = temp;
      }
      if (myanCount.indexOf("9") > -1) {
        temp = myanCount.replace("9", "၉");
        myanCount = temp;
      }
      if (myanCount.indexOf("0") > -1) {
        temp = myanCount.replace("0", "၀");
        myanCount = temp;
      }
    }
    //console.log("count == " + myanCount);
    return myanCount;
  }

  getTimeDifference(date, time) {
    let year = date.slice(0, 4);
    let month = date.slice(4, 6);
    let day = date.slice(6, 8);
    let serverdate = month + '/' + day + '/' + year;
    //system date  
    let today = new Date();
    let thour = today.getHours();
    let tmin = today.getMinutes();
    let temp = this.datepipe.transform(today, 'MM/dd/yyyy');
    let aa = new Date(temp);
    let bb = new Date(serverdate);
    let timeDiff = Math.abs(aa.getTime() - bb.getTime());
    let diffDays = Math.floor(timeDiff / (1000 * 3600 * 24));
    let dtime;
    let parm;
    let arr, arr1;
    if (diffDays > 0) {
      if (diffDays == 7) {
        dtime = Math.floor(diffDays / 7);
        parm = "W";
      }
      else if (diffDays > 7) {
        dtime = '';
        parm = "W";
      }
      else {
        dtime = diffDays;
        parm = 'd';
      }
    } else {
      let full;
      arr = time.replace(':', ' ');
      arr1 = arr.split(' ');
      if (arr1[2] == 'pm' || arr1[2] == 'PM') {
        if (parseInt(arr1[0]) == 12) {
          full = parseInt(arr1[0]);
        }
        else {
          full = parseInt(arr1[0]) + 12;
        }
      } else {
        full = parseInt(arr1[0]);
      }
      let tal = (full * 3600) + (parseInt(arr1[1]) * 60);
      let ee = (thour * 3600) + (tmin * 60);
      dtime = Math.floor((ee - tal) / 60);
      if (dtime == 0 || dtime < 0) {
        dtime = '';
        parm = 'Just now';
      } else if (dtime >= 60) {
        dtime = Math.floor(dtime / 60);
        parm = 'h';
      } else {
        parm = 'm';
      }
    }
    let answer = dtime + ' ' + parm;
    return answer;
  }

}
